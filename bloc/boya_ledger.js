var db = require('./mysql');
// const jenga = require("../equity_jenga/b2b");
const B2B_WORKPAY = "MPESA B2B - WORKPAY";
const B2C_MPESA = "MPESA B2C & B2B";
const B2B_MPESA = "MPESA B2B - WORKPAY";
const JENGA = "JENGA ACCOUNT";
const AFRICASTALKING = "AFRICAS-TALKING";
const IPAY = "IPAY";

function updateLedgerBalance(group,receiver,amount,balance,ref) {
    const account = getAccount(group,receiver);
    db.insertRecord("boya","boya_ledger",
        {account:account,amount:amount,ref:ref,balance:balance},(resp,err)=>{
            if(err){
                console.error(err);
            }
        });
}
function updateLedgerAccount(group,receiver,amount,ref) {
    const account = getAccount(group,receiver);
    getBalance(account,(bal)=>{
        const newbal = parseFloat(bal) + parseFloat(-amount);
        db.insertRecord("boya","boya_ledger",
            {account:account,amount:amount,ref:ref,balance:newbal},(resp,err)=>{
                if(err){
                    console.error(err);
                }
            });
    });
}

function getBalance(account,callback) {
    db.runQuery(`select balance from ${process.env.DATABASE}.boya_ledger where account = '${account}' order by last_updated desc limit 1`,
        (row)=>{return row.balance},(results,err)=>{
            if(!err && results.length>0){
                callback(results[0]);
            }else {
                console.error(err);
                callback(0);
            }
        });
}
function getB2B_WORKPAYBalance(callback) {
    db.runQuery(`select balance from ${process.env.DATABASE}.boya_ledger where account = '${B2B_WORKPAY}' order by last_updated desc limit 1`,
        (row)=>{return row.balance},(results,err)=>{
            if(!err && results.length>0){
                callback(results[0]);
            }else {
                console.error(err);
                callback(0);
            }
        });
}
function getB2C_Balance(callback) {
    db.runQuery(`select balance from ${process.env.DATABASE}.boya_ledger where account = '${B2C_MPESA}' order by last_updated desc limit 1`,
        (row)=>{return row.balance},(results,err)=>{
            if(!err && results.length>0){
                callback(results[0]);
            }else {
                console.error(err);
                callback(0);
            }
        });
}
function getAirtimeBalance(callback) {
    db.runQuery(`select balance from ${process.env.DATABASE}.boya_ledger where account = '${AFRICASTALKING}' order by last_updated desc limit 1`,
        (row)=>{return row.balance},(results,err)=>{
            if(!err && results.length>0){
                callback(results[0]);
            }else {
                console.error(err);
                callback(0);
            }
        });
}
function getJengaBalance(callback) {
    // jenga.boyaBalance((accountBal)=>{
    //     let balance = accountBal["balances"][0].amount;
    //     callback(balance);
    // });
}
function getAccount(group,receiver) {
    var account = group.toString().toUpperCase();
    switch (group) {
        case "tills":
            account = B2B_WORKPAY;
            break;
        case "mpesa":
            account = B2C_MPESA;
            break;
        case "airtime":
            account = AFRICASTALKING;
            break;
        case "rent":
            account = JENGA;
            break;
        case "bills":
            if(receiver==="kplc_prepaid" ||
                receiver==="kplc_postpaid" ||
                receiver==="GoTV" ||
                receiver==="DSTV"){
                account = IPAY;
            }else{
                account = B2B_WORKPAY;
            }
            break;
    }
    return account;
}
module.exports = {
    updateAccount: (group,receiver,amount,ref)=>updateLedgerAccount(group,receiver,amount,ref),
    updateAccountBalance: (group,receiver,amount,balance,ref)=>updateLedgerBalance(group,receiver,amount,balance,ref),
    getB2B_WORKPAYBalance,
    getB2C_Balance,
    getAirtimeBalance,
    getJengaBalance
};
