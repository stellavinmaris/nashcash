import mysql = require("../mysql");

const dateformat = require("dateformat");
const moment = require("moment");
import {Transaction} from "./Transaction";
import {
    MERCHANT_TRANSACTION_TYPE,
    SCHEDULED_PAYMENT_FREQUENCY,
    SETTLEMENT_SCHEDULE,
    StatusType
} from "../utils/StatusType";
import {Groups} from "../participants/Groups";
import {Card} from "../vgs/Card";
import {LocalDate} from "js-joda";

export enum AccountTypes {
    MPESA_B2C,
    MPESA_B2B,
    AIRTIME,
    iPAY,
    JENGA
}

export class DataStore {

    getTransactionByRef(ref: String,): Promise<{ status: number, response: Object }> {
        return new Promise<{ status: number, response: Object }>((resolve, reject) => {
            mysql.runQuery(`select * from ${process.env.DATABASE}.transactions where ref = '${ref}'`,
                (row) => {
                    const transaction = {
                        id: row.id,
                        uid: row.customer_id,
                        customer_id: row.customer_id,
                        provider1_resp: row.provider1_resp,
                        provider1_resp2: row.provider1_resp2,
                        provider2_resp: row.provider2_resp,
                        ref: ref,
                        chargeId: row.chargeId,
                        timestamp: dateformat(row.timestamp, "yyyy-mm-dd HH:MM"),
                        txRef: ref,
                        source: row.source,
                        accno: row.accno,
                        group: row.group,
                        ConversationID: row.ConversationID,
                        amount: row.amount,
                        phone: row.phone,
                        receiver: row.receiver,
                        email: row.email,
                        description: row.description,
                        fee: row.fees,
                        status: row.status,
                        merchant_id: row.merchant_id,
                        payment_type: row.payment_type,
                    };
                    try {
                        transaction.provider1_resp = JSON.parse(row.provider1_resp);
                        transaction["card_response"] = transaction.provider1_resp;
                    } catch (e) {

                    }
                    try {
                        transaction.provider2_resp = JSON.parse(row.provider2_resp);
                    } catch (e) {

                    }
                    return transaction;
                }, (result, err) => {
                    if (!err) {
                        console.log("no of records matching", result.length);
                        console.log("records matching", result.length > 0 ? result[0].provider1_resp2 : {});
                        resolve({status: StatusType.SUCCESS, response: result.length > 0 ? result[0] : {}});
                    } else {
                        resolve({status: StatusType.ERROR, response: {errorMessage: err.toString()}})
                    }
                })
        });
    }

    getTransactionReceiptByRef(ref: String):Promise<{}>{
        return new Promise<{}>(resolve => {
            mysql.runQuery(`select * from ${process.env.DATABASE}.transactions where ref = '${ref}'`,
                (row) => {
                    const transaction = {
                        receipt_link: row.receipt_link,
                    };
                    return transaction;
                }, (result, err) => {
                    resolve(result[0]);
                })
        })
    }

    getTransactionByRefForRefund(ref):Promise<{}>{
        return new Promise<{}>(resolve => {
            mysql.runQuery(`select * from ${process.env.DATABASE}.transactions where ref = '${ref}'`,
                (row) => {
                    return this.formCharge(row)
                }, (result, err) => {
                        resolve(result[0]);

                })
        })
    }
    getTransactionFee(group: string, amount): Promise<number> {
        return new Promise<number>((resolve, reject) => {
            const sql = `select amount from ${process.env.DATABASE}.transaction_fees where channel = '${group}' and
            ${amount} >= lower_lim  and ${amount} <= upper_lim`
            console.log(sql)
            mysql.runQuery(sql, (row) => {
                return row.amount;
            }, (rows, err) => {
                let fee = 0.0;
                if (!err && rows.length > 0) {
                    fee = rows[0];
                }
                resolve(fee);
            })
        })


    }
    getAllTransactionFees(): Promise<Array<object>> {
        return new Promise<Array<object>>((resolve, reject) => {
            const sql = `select * from ${process.env.DATABASE}.transaction_fees`
            console.log(sql)
            mysql.runQuery(sql, (row) => {
                return {
                    fees:row.amount,
                    upper_lim:row.upper_lim,
                    lower_lim:row.lower_lim,
                    group:row.channel,
                };
            }, (rows, err) => {
                resolve(rows);
            })
        })


    }

    getTransactionBizname(tillno: String): Promise<string> {
        let name = `${tillno}`;
        return new Promise<string>((resolve, reject) => {
            mysql.runQuery(`select * from ${process.env.DATABASE}.receivers_index where receiver = '${tillno}'`, (row) => {
                return row.name;
            }, (rows, err) => {
                if (!err && rows.length > 0) {
                    name = rows[0];
                }
                resolve(name);
            })
        })


    }

    getTransactionCustomerName(customer_id: String): Promise<string> {
        let name = "";
        return new Promise<string>((resolve) => {
            mysql.runQuery(`select * from ${process.env.DATABASE}.customers where uid = '${customer_id}'`, (row) => {
                return row.name;
            }, (rows, err) => {
                if (!err && rows.length > 0) {
                    name = rows[0];
                }
                resolve(name);
            })
        })


    }

    //creates a new transaction record in the DB
    createTransaction(transaction: Transaction): Promise<boolean> {
        return new Promise<boolean>((resolve, reject) => {
            const newTransaction = {
                amount: transaction.amount,
                fees: transaction.fee,
                status: 1,
                phone: transaction.phone,
                provider1: transaction.provider1,
                receiver: transaction.receiver,
                group: transaction.group,
                description: transaction.description,
                customer_id: transaction.customer_id,
                payment_type: transaction.payment_type,
                ref: transaction.txRef,
                txRef: transaction.txRef,
                currency: transaction.currency,
                accno: transaction.accno,
                initial: transaction.initial,
                promocode: transaction.promocode,
                promo_amount: transaction.promo_amount,
                source: transaction.source
            };
            if(transaction.is_business){
                newTransaction["is_business"] = 1;
                newTransaction["business_id"] = transaction.business_id;
            }
            if (transaction.accno !== undefined)
                newTransaction.accno = transaction.accno;
            if (transaction.lat !== undefined) {
                newTransaction["lat"] = transaction.lat;
            }
            if (transaction.long !== undefined) {
                newTransaction["long"] = transaction.long;
            }
            if (transaction.ip !== undefined) {
                newTransaction["ip"] = transaction.ip;
            }
            if (transaction.merchant_id !== undefined) {
                newTransaction["merchant_id"] = transaction.merchant_id;
            }
            if (transaction.deviceInfo !== undefined) {
                try {
                    newTransaction["device_info"] = JSON.stringify(transaction.deviceInfo);
                } catch (e) {

                }
            }
            if (transaction.bank !== undefined)
                newTransaction['bank'] = JSON.stringify(transaction.bank);
            newTransaction["tdump"] = JSON.stringify(transaction);
            mysql.insertRecord(process.env.DATABASE, "transactions", newTransaction, (resp, err) => {
                resolve(err != null)
            })
        });
    }

    private getCustomerAccount() {

    }
    getTransactionsByCustomer(customer_id:string){
        return new Promise<Array<object>>((resolve, reject) => {
            const sql = `select * from ${process.env.DATABASE}.transactions where customer_id = '${customer_id}' and status = 0 and initial = 0 and timestamp > '2020-11-01' order by timestamp desc`
            console.log(sql)
            mysql.runQuery(sql, (row) => {
                return {
                    id: row.id,
                    uid: row.customer_id,
                    customer_id: row.customer_id,
                    ref: row.txRef,
                    provider_ref: row.provider_ref,
                    timestamp: dateformat(row.timestamp, "yyyy-mm-dd HH:MM"),
                    txRef: row.txRef,
                    source: row.source,
                    accno: row.accno,
                    group: row.group,
                    amount: row.amount,
                    phone: row.phone,
                    receiver: row.receiver,
                    email: row.email,
                    description: row.description,
                    fee: row.fees,
                };
            }, (rows, err) => {
                resolve(rows);
            })
        })
    }

    updateTransaction(ref: String, transaction: {}): Promise<boolean> {
        return new Promise<boolean>((resolve, reject) => {
            const fields = [];
            const values = [];
            for (const key in transaction) {
                fields.push(`${key}`);
                if (transaction[`${key}`] instanceof Object)
                    values.push(JSON.stringify(transaction[`${key}`]));
                else values.push(transaction[`${key}`]);
            }
            fields.push("ref");
            values.push(ref);
            mysql.updateRecord(process.env.DATABASE, "transactions", values, fields, (resp, err) => {
                if (!err) resolve(true);
                else {
                    console.error(err);
                    reject(false);
                }
            });
        });
    }

    /**
     * Update a transaction using the vendor's request Id i.2 M-pesa reference for M-pesa payments
     * @param ref -request or response id
     * @param transaction
     */
    updateTransactionWithProviderRef(ref: String, transaction: {}): Promise<boolean> {
        return new Promise<boolean>((resolve, reject) => {
            const fields = [];
            const values = [];
            for (const key in transaction) {
                fields.push(`${key}`);
                if (transaction[`${key}`] instanceof Object)
                    values.push(JSON.stringify(transaction[`${key}`]));
                else values.push(transaction[`${key}`]);
            }
            fields.push("ref");
            values.push(ref);
            mysql.updateRecord(process.env.DATABASE, "transactions", values, fields, (resp, err) => {
                if (!err) resolve(true);
                else {
                    console.error(err);
                    reject(false);
                }
            });
        });
    }

    updateTransactionWithid(id: number, transaction: {}): Promise<boolean> {
        return new Promise<boolean>((resolve, reject) => {
            const fields = [];
            const values = [];
            for (const key in transaction) {
                fields.push(`${key}`);
                if (transaction[`${key}`] instanceof Object)
                    values.push(JSON.stringify(transaction[`${key}`]));
                else values.push(transaction[`${key}`]);
            }
            fields.push("id");
            values.push(id);
            mysql.updateRecord(process.env.DATABASE, "transactions", values, fields, (resp, err) => {
                if (!err) resolve(true);
                else {
                    console.error(err);
                    reject(false);
                }
            });
        });
    }
    zucchiniTill(receiver): Promise<string>{
        return new Promise<string>((resolve, reject) => {
            mysql.runQuery(`select * from ${process.env.DATABASE}.zucchini_tills where tillno = '${receiver}'`,
                (row: {}) => {
                return {name: row["name"],phone: row["phone_no"]};
            }, (result, err) => {
                if (!err && result.length > 0) {
                    resolve(result[0]);
                } else {
                    reject(null);
                }
            })
        });
    }
    getMerchantConfig(ref: String): Promise<string> {
        return new Promise<string>((resolve, reject) => {
            mysql.runQuery(`select * from ${process.env.DATABASE}.merchants where ref = '${ref}'`, (row: {}) => {
                return {id: row["id"]};
            }, (result, err) => {
                if (!err && result.length > 0) {
                    resolve(result[0]);
                } else {
                    reject(null);
                }
            })
        });
    }

    getMerchantByShortcode(tillno: String): Promise<Object> {
        return new Promise<Object>((resolve, reject) => {
            mysql.runQuery(`select * from ${process.env.DATABASE}.metchant_tills_view where shortcode = '${tillno}'`, (row: {}) => {
                return {
                    id: row["merchant_id"],
                    email: row["email"],
                    phone: row["phone"],
                    shortcode: row["shortcode"],
                    name: row["name"]

                };
            }, (result, err) => {
                if (!err && result.length > 0) {
                    resolve(result[0]);
                } else {
                    resolve(0);
                }
            })
        });
    }

    getMerchantById(mid: String): Promise<Object> {
        return new Promise<Object>((resolve, reject) => {
            mysql.runQuery(`select * from ${process.env.DATABASE}.merchant_staff where id = '${mid}'`, (row: {}) => {
                return {
                    id: row["id"],
                    email: row["email"],
                    phone: row["phone"],
                    name: row["name"],
                    business_name: row["business_name"]
                };
            }, (result, err) => {
                if (!err && result.length > 0) {
                    resolve(result[0]);
                } else {
                    resolve({"phone": "0718555832"});
                }
            })
        });
    }
    getMerchantBusinessById(mid: String): Promise<Object> {
        return new Promise<Object>((resolve, reject) => {
            mysql.runQuery(`select * from ${process.env.DATABASE}.merchants where id = '${mid}'`, (row: {}) => {
                return {
                    id: row["id"],
                    email: row["email"],
                    phone: row["phone"],
                    name: row["name"],
                    business_name: row["business_name"]
                };
            }, (result, err) => {
                if (!err && result.length > 0) {
                    resolve(result[0]);
                } else {
                    resolve({"phone": "0718555832"});
                }
            })
        });
    }

    getMerchantPrimaryAcc(merchant_id): Promise<{ accno, bank_id, acc_name, bankCode }> {
        return new Promise<{ accno, bank_id, acc_name, bankCode }>(resolve => {
            mysql.runQuery(`select * from ${process.env.DATABASE}.merchant_banks_view 
        where merchant_id = '${merchant_id}' order by isprimary desc limit 1`,
                (row) => {
                    return {
                        accno: row.bank_acc,
                        bank_name: row.bank_name,
                        bank_id: row.bank_id,
                        acc_name: row.acc_name,
                        bankCode: row.bankCode,
                    };
                }, (result, err) => {
                    if (!err) {
                        if (result.length > 0) {
                            return resolve(result[0]);
                        }

                    }
                    resolve({accno: undefined, acc_name: undefined, bank_id: undefined, bankCode: undefined});

                });
        })
    }
    getReceiverName(receiver: String): Promise<string> {
        return new Promise<string>((resolve, reject) => {
            mysql.runQuery(`select * from ${process.env.DATABASE}.receivers_index where receiver = '${receiver}'`, (row: {}) => {
                return {
                    name: row["name"]
                };
            }, (result, err) => {
                if (!err && result.length > 0) {
                    resolve(result[0].name);
                } else {
                    resolve(undefined);
                }
            })
        });
    }
    createSettlement(merchant_id, settlementType, amount: number, ref, accno, fees: number, bank_id, provider_response): Promise<boolean> {

        return new Promise<boolean>(resolve => {
            this.debitSettlemtInMerchantAccount(merchant_id, ref, (amount + fees)).then((result) => {
                mysql.insertRecord(process.env.DATABASE, "merchant_settlements", {
                    merchant_id: merchant_id,
                    txRef: `${accno}-${ref}`,
                    amount: amount,
                    bank_id: bank_id,
                    provider_response: provider_response,
                    fees: fees,
                    status: 2,
                    settlement_type: settlementType
                }, (result, err) => {
                    resolve(!err);

                });
            })

        })
    }

    getMerchantBalance(merchant_id): Promise<number> {
        return new Promise<number>(resolve => {
            let balance = 0;
            mysql.runQuery(`select balance from ${process.env.DATABASE}.merchant_ledger 
        where merchant_id = '${merchant_id}' order by timestamp desc limit 1`,
                (row) => {
                    return row.balance;
                }, (result, err) => {
                    if (!err) {
                        if (result.length > 0) {
                            balance = result[0]; //update account balance
                        }

                    }
                    resolve(balance);

                });
        });
    }

    getMerchantSettlementFee(merchant_id): Promise<number> {
        return new Promise<number>(resolve => {
            let rate = 0.01;
            mysql.runQuery(`select settlement_fee from ${process.env.DATABASE}.merchants 
        where id = '${merchant_id}' order by timestamp desc limit 1`,
                (row) => {
                    return row.settlement_fee;
                }, (result, err) => {
                    if (!err) {
                        if (result.length > 0) {
                            rate = result[0];
                        }

                    }
                    resolve(rate);

                });
        });
    }

    async creditMerchantAccount(merchant_id, txRef, amount, callback) {
        //check last balance
        let balance = await this.getMerchantBalance(merchant_id);
        balance += amount;
        mysql.insertRecord(`${process.env.DATABASE}`, "merchant_ledger", {
            merchant_id: merchant_id,
            txRef: txRef,
            amount: amount,
            balance: balance
        }, (result, err) => {
            if (!err) {
                callback(StatusType.SUCCESS);
            } else {
                callback(StatusType.ERROR, err);
            }
        });

    }

    async merchantHasBankAccount(merchant_id):Promise<boolean>{
        return new Promise<boolean>(resolve => {
            mysql.runQuery(`select * from boya.merchant_banks 
        where merchant_id = '${merchant_id}'`,
                (row) => {
                    return row.id;
                }, (result, err) => {
                        resolve(result.length > 0);
                });
        });
    }

    async transactionErrorDescription(code, group): Promise<string> {
        return new Promise<string>(resolve => {
            mysql.runQuery(`select * from ${process.env.DATABASE}.error_codes where code = '${code}' and channel = '${group}'`,
                (row) => {
                    return row.description
                }, (result, err) => {
                    resolve(!err && result.length > 0 ? result[0] : "Transaction failed");
                })
        });
    }
    async creditMerchantCustomerLoyalty(transaction: Transaction): Promise<boolean> {
        const pointsEarned = await this.getPointPerSpend(transaction.merchant_id, transaction.amount)
        return new Promise<boolean>(resolve => {
            mysql.insertRecord(`${process.env.DATABASE}`, "merchant_customer_loyalty", {
                merchant_id: transaction.merchant_id,
                ref: transaction.ref,
                customer_id: transaction.customer_id,
                spend: transaction.amount,
                points: pointsEarned,
            }, (result, err) => {
                if (!err) {
                    resolve(true);
                } else {
                    resolve(false);
                }
            });
        })


    }

    async debitSettlemtInMerchantAccount(merchant_id, txRef, amount): Promise<boolean> {
        //check last balance
        let balance = await this.getMerchantBalance(merchant_id);
        return new Promise(resolve => {
            balance -= amount;
            mysql.insertRecord(`${process.env.DATABASE}`, "merchant_ledger", {
                merchant_id: merchant_id,
                txRef: txRef,
                amount: amount,
                balance: balance,
                type: MERCHANT_TRANSACTION_TYPE.SETTLEMENT
            }, (result, err) => {
                resolve(!err);
            });
        });

    }

    async getPointPerSpend(mid, amount): Promise<number> {
        return new Promise<number>(resolve => {
            mysql.runQuery(`select * from ${process.env.DATABASE}.merchant_loyalty where merchant_id = '${mid}' order by timestamp desc limit 1`,
                (row) => {
                    return {
                        status: row.status,
                        amount_per_spend: row.amount_per_spend,
                        points_per_spend: row.points_per_spend
                    }
                }, (result, err) => {
                    let points = 0;
                    if (result.length > 0) {
                        const loyalty = (result[0]);
                        points = (amount / parseInt(`${loyalty.amount_per_spend}`)) * parseInt(`${loyalty.points_per_spend}`)
                    }
                    resolve(points);

                });
        })
    }

    async debitMerchantAccountRefund(merchant_id, txRef, reason, amount, callback) {
        //check last balance
        let balance = await this.getMerchantBalance(merchant_id);
        balance -= amount;
        mysql.insertRecord(process.env.DATABASE, "merchant_ledger", {
            merchant_id: merchant_id,
            txRef: txRef,
            amount: amount,
            balance: balance,
            narrative: reason,
            type: MERCHANT_TRANSACTION_TYPE.CUSTOMER_REFUND
        }, (result, err) => {
            if (!err) {
                callback(StatusType.SUCCESS);
            } else {
                callback(StatusType.ERROR, err);
            }
        });

    }

    updateCustomerProfile(customer, uid): Promise<boolean> {

        return new Promise<boolean>(resolve => {
            const fields = [];
            const values = [];
            for (const key in customer) {
                fields.push(`${key}`);
                if (customer[`${key}`] instanceof Object)
                    values.push(JSON.stringify(customer[`${key}`]));
                else values.push(customer[`${key}`]);
            }
            fields.push("uid");
            values.push(uid);
            mysql.updateRecord(process.env.DATABASE, "customers", values, fields, (resp, err) => {
                if (!err) resolve(!err);
                else {
                    console.error(err);
                    resolve(false);
                }
            });
        });
    }

    createKycReviewRequest(customer_id, customerPhoto): Promise<boolean> {
        return new Promise<boolean>( resolve => {
            this.hasPendingKycReview(customer_id).then((hasPendingRequest)=>{
                if (hasPendingRequest) {
                    mysql.updateRecord(process.env.DATABASE, "pending_kyc_verifications",
                        [customerPhoto.national_id_photo?customerPhoto.national_id_photo:"", customerPhoto.national_id?customerPhoto.national_id:"", customerPhoto.photo?customerPhoto.photo:"", customer_id],
                        ["national_id_photo", 'national_id', 'selfie_photo', 'customer_id'],
                        (resp, err) => {
                            resolve(!err);
                        }
                    )
                } else {
                    mysql.insertRecord(process.env.DATABASE, "pending_kyc_verifications", {
                        customer_id: customer_id,
                        selfie_photo: customerPhoto.photo?customerPhoto.photo:"",
                        national_id: customerPhoto.national_id?customerPhoto.national_id:"",
                        national_id_photo: customerPhoto.national_id_photo?customerPhoto.national_id_photo:""
                    }, (result, err) => {
                        resolve(!err);
                    });
                }
            });

        });
    }

    hasPendingKycReview(customer_id): Promise<boolean> {
        return new Promise<boolean>(resolve => {
            mysql.runQuery(`select * from ${process.env.DATABASE}.pending_kyc_verifications where customer_id = '${customer_id}' and status = 1`,
                (row) => {
                    return row.id
                }, (result, err) => {
                    resolve(!err && result.length > 0);
                });
        });
    }

    customerKycReviewResult(customer_id): Promise<object> {
        return new Promise<object>(resolve => {
            mysql.runQuery(`select * from ${process.env.DATABASE}.pending_kyc_verifications where customer_id = ${customer_id} order by timestamp desc limit 1`,
                (row) => {
                    return {status: row.status, review_comment: row.review_comment}
                }, (result, err) => {
                    resolve(result[0]);
                });
        });
    }

    updateCustomerLegalData(uid, customerData): Promise<boolean> {
        return new Promise<boolean>(resolve => {
            mysql.updateRecord(process.env.DATABASE, "customers",
                [JSON.stringify(customerData), uid],
                ["legal_data", 'uid'],
                (resp, err) => {
                    resolve(!err);
                }
            )
        });
    }

    updateCustomerImageVerification(uid, customerData): Promise<boolean> {
        return new Promise<boolean>(resolve => {
            mysql.updateRecord(process.env.DATABASE, "customers",
                [JSON.stringify(customerData), uid],
                ["image_verification_data", "uid"],
                (resp, err) => {
                    resolve(!err);
                }
            )
        });
    }

    createCustomer(mcustomer): Promise<boolean> {
        const customer = {
            name: mcustomer.name,
            phone: mcustomer.phone,
            email: mcustomer.email,
            uid: mcustomer.uid,
            sharecode: mcustomer.sharecode,
            invitecode: mcustomer.invitecode,
            stripe_cust_id: mcustomer.stripe_cust_id
        };
        if (mcustomer.national_id) {
            customer["national_id"] = mcustomer.national_id;
        }
        return new Promise<boolean>(resolve => {
            mysql.runQuery(`select * from ${process.env.DATABASE}.customers where phone = '${customer.phone}' `,
                (row) => {
                    return row.id
                }, (result, err) => {
                    if (result.length > 0) {
                        //already exists
                        resolve(false);
                    } else {
                        mysql.insertRecord(`${process.env.DATABASE}`, "customers",
                            customer, (result, err) => {
                                if (!err) {
                                    console.log('customer created');
                                    return resolve(true)
                                }
                                return resolve(false)
                            });
                    }
                });
        })

    }

    createWallet(mcustomer): Promise<boolean> {
        const wallet = {
            customer_id: mcustomer.customer_id,
            account_number: mcustomer.account_number,
            debit: mcustomer.debit,
            credit: mcustomer.credit,
            amount: mcustomer.amount,
            fees: mcustomer.fees
        };
        return new Promise<boolean>(resolve => {
            mysql.runQuery(`select * from ${process.env.DATABASE}.wallet where account_number = '${wallet.account_number}' `,
                (row) => {
                    return row.id
                }, (result, err) => {
                    if (result.length > 0) {
                        //already exists
                        resolve(false);
                    } else {
                        mysql.insertRecord(`${process.env.DATABASE}`, "wallet",
                            wallet, (result, err) => {
                                if (!err) {
                                    console.log('wallet created');
                                    return resolve(true)
                                }
                                return resolve(false)
                            });
                    }
                });
        })

    }


    creditWallet(credit,debit, total, customer_id): Promise<StatusType>{
        return new Promise<StatusType>(resolve => {
            mysql.updateRecordMultipleWhere(process.env.DATABASE, "wallet",
                [credit,debit,total,customer_id],
                ["credit","debit","amount","customer_id"], 1, (result, err) => {
                    if (!err) {
                        resolve(StatusType.SUCCESS);
                    } else {
                        resolve(StatusType.ERROR);
                    }
                });
        })
    }


    debitWallet(credit,debit, total, customer_id): Promise<StatusType>{
        return new Promise<StatusType>(resolve => {
            mysql.updateRecordMultipleWhere(process.env.DATABASE, "wallet",
                [credit,debit,total,customer_id],
                ["credit","debit","amount","customer_id"], 1, (result, err) => {
                    if (!err) {
                        resolve(StatusType.SUCCESS);
                    } else {
                        resolve(StatusType.ERROR);
                    }
                });
        })
    }

    getWalletByID(customer_id): Promise<{ debit: number, credit: number, amount: number, fees: number}> {
        return new Promise<{ debit: number, credit: number, amount: number, fees: number }>(resolve => {
            mysql.runQuery(`select debit,credit,amount from
             ${process.env.DATABASE}.wallet where status = 1 and customer_id = '${customer_id}' order by timestamp desc limit 1`,
                (row) => {
                    return {
                        debit: row.debit,
                        credit: row.credit,
                        amount: row.amount,
                        fees: row.fee,
                    }
                }, (result, err) => {
                    if (!err && result.length > 0) {
                        resolve(result[0])
                    } else {
                        resolve(err);
                    }
                });
        })

    }

    logRefund(transaction,reason,callback){
        mysql.insertRecord(process.env.DATABASE, "refunds", {
            customer_id: transaction.customer_id,
            ref: transaction.ref,
            amount: transaction.amount+transaction.fee,
            status: 0,
            channel:transaction.group,
            reason:JSON.stringify(reason)
        }, (result, err) => {
            if (!err) {
                callback(StatusType.SUCCESS);
            } else {
                callback(StatusType.ERROR, err);
            }
        });
    }

    refundCustomer(customer_id, txRef, amount, callback) {
        //check last balance
        mysql.runQuery(`select sum(amount) as balance from ${process.env.DATABASE}.customer_account 
        where customer_id = '${customer_id}' and status=1`,
            (row) => {
                return row.balance;
            }, (result, err) => {
                if (!err) {
                    let balance = amount;
                    if (result.length > 0) {
                        balance += result[0]; //update account balance
                    }
                    mysql.insertRecord(process.env.DATABASE, "customer_account", {
                        customer_id: customer_id,
                        transaction_ref: txRef,
                        amount: amount,
                        running_bal: balance,
                        status: 1,
                        credit_reason: 2
                    }, (result, err) => {
                        if (!err) {
                            callback(StatusType.SUCCESS);
                        } else {
                            callback(StatusType.ERROR, err);
                        }
                    });
                } else {
                    //failed to get last balance.
                    //cancel transaction
                    console.log('no access to last balance');
                    callback(StatusType.ERROR, err);

                }

            })

    }

    getCustomerInviteCode(customer_id): Promise<{ invitecode: string, avalaible: boolean }> {
        return new Promise<{ invitecode: string, avalaible: boolean }>(resolve => {
            mysql.runQuery(`select invitecode from ${process.env.DATABASE}.customers where uid = '${customer_id}'`,
                (row) => {
                    return row.invitecode
                }, (results, err) => {
                    const res = {invitecode: '', avalaible: results.length > 0};
                    if (results.length > 0) {
                        res.invitecode = results[0];
                    }
                    resolve(res);
                })
        });
    }

    usedInviteCode(inviteCode, customer_id): Promise<boolean> {
        return new Promise<boolean>(resolve => {
            mysql.runQuery(`select * from ${process.env.DATABASE}.customer_account
            where customer_id = '${customer_id}' and referral_code = '${inviteCode}'`,
                (row) => {
                    return row.id
                }, (results, err) => {
                    resolve(!err && results.length > 0);
                })
        });
    }


    sharedMax(inviteCode, customer_id): Promise<boolean> {
        return new Promise<boolean>(resolve => {
            mysql.runQuery(`select count(referral_code) from ${process.env.DATABASE}.customer_account
            where customer_id = '${customer_id}' and referral_code = '${inviteCode}' and credit_reason = 1`,
                (row) => {
                    return row.id
                }, (results, err) => {
                    resolve(!err && results.length >= 10);
                })
        });
    }

    partnerPromoExhausted(max, inviteCode): Promise<boolean> {
        return new Promise<boolean>(resolve => {
            mysql.runQuery(`select count(referral_code) from ${process.env.DATABASE}.customer_account
            where referral_code = '${inviteCode}' and credit_reason = 1`,
                (row) => {
                    return row.id
                }, (results, err) => {
                    resolve(!err && results.length >= max);
                })
        });
    }

    getCustomerShareCode(customer_id): Promise<{ sharecode: string, avalaible: boolean }> {
        return new Promise<{ sharecode: string, avalaible: boolean }>(resolve => {
            mysql.runQuery(`select sharecode from ${process.env.DATABASE}.customers where uid = '${customer_id}'`,
                (row) => {
                    return row.sharecode
                }, (results, err) => {
                    const res = {sharecode: '', avalaible: results.length > 0};
                    if (results.length > 0) {
                        res.sharecode = results[0];
                    }
                    resolve(res);
                })
        });
    }

    getPartnerByInviteCode(code): Promise<{ partner: string, amount: number, max: number,spend:number }> {
        return new Promise<{ amount: number, partner: string, max: number, spend:number }>(resolve => {
            mysql.runQuery(`select * from ${process.env.DATABASE}.partner_referral_codes where code = '${code}' and status = 1`,
                (row) => {
                    return {partner: row.partner, amount: row.amount, max: row.max,spend:row.spend}
                }, (results, err) => {
                    if (results.length > 0) {
                        return resolve(results[0]);
                    }
                    resolve({amount: 0, partner: null, max: 0,spend:0});
                })
        });
    }

    getCustomerByInviteCode(invitecode): Promise<{ customer_id: string, phone: string }> {
        return new Promise<{ customer_id: string, phone: string }>(resolve => {
            mysql.runQuery(`select * from ${process.env.DATABASE}.customers where sharecode = '${invitecode}'`,
                (row) => {
                    return {customer_id: row.uid, phone: row.phone}
                }, (results, err) => {
                    if (results.length > 0) {
                        return resolve(results[0]);
                    }
                    resolve({customer_id: null, phone: null});
                })
        });
    }

    getReferralCompensationAmount(): Promise<number> {
        return new Promise<number>(resolve => {
            mysql.runQuery(`select amount from ${process.env.DATABASE}.referral_rates where type = 'invite'`,
                (row) => {
                    return row.amount
                }, (results, err) => {
                    if (results.length > 0) {
                        return resolve(results[0]);
                    }
                    resolve(0);
                })
        });
    }

    creditCustomerForReferral(customer_id, txRef, referral_code, amount): Promise<StatusType> {
        //check last balance
        return new Promise<StatusType>(resolve => {
            mysql.runQuery(`select sum(amount) as balance from ${process.env.DATABASE}.customer_account 
        where customer_id = '${customer_id}' and status=1`,
                (row) => {
                    return row.balance;
                }, (result, err) => {
                    if (!err) {
                        let balance = amount;
                        if (result.length > 0) {
                            balance += result[0]; //update account balance
                        }
                        mysql.insertRecord(process.env.DATABASE, "customer_account", {
                            customer_id: customer_id,
                            transaction_ref: txRef,
                            amount: amount,
                            referral_code: referral_code,
                            running_bal: balance,
                            status: 1,
                            credit_reason: 1
                        }, (result, err) => {
                            if (!err) {
                                resolve(StatusType.SUCCESS);
                            } else {
                                resolve(StatusType.ERROR);
                            }
                        });
                    } else {
                        //failed to get last balance.
                        //cancel transaction
                        console.log('no access to last balance');
                        resolve(StatusType.ERROR);

                    }

                })
        });


    }
    formCharge(row){
        const charge= {
            uid: row.customer_id,
            timestamp: dateformat(row.timestamp, "yyyy-mm-dd HH:MM"),
            card_response: JSON.parse(row.provider1_resp),
            chargeId: row.chargeId,
            ref: row.ref,
            sourceId: row.source,
            provider1: row.provider1,
            provider_ref: row.provider_ref,
            amount: row.amount,
            fees: row.fees>0?row.fees:0,
            stripeAmount: row.amount
        };
        if(JSON.parse(row.provider1_resp).data) {
            const data = JSON.parse(row.provider1_resp).data;
            charge["isOnhold"] = data["status"] == "pending-capture"
            charge["chargeId"] = data.id
            charge["provider_ref"] = data["flwRef"];
        }
        return charge;
    }
    getSourceCard(source):Promise<object>{
        return new Promise<object>(resolve => {
            mysql.runQuery(`select * from ${process.env.DATABASE}.transactions
             where source = '${source}' and provider1_resp != '' and chargeId !='' limit 1`,
                (row) => {

                    let card =  JSON.parse(row.provider1_resp).source;
                    if(card==undefined){
                    console.log('source has no card',source);
                        return
                    }
                    if(card!=undefined && card.card){
                        card = card.card;
                    }
                    if(card.exp_year.toString().length>2)
                        card.exp_year = card.exp_year.toString().substring(2)
                    if(card.exp_month.toString().length == 1)
                        card.exp_month = `0${card.exp_month}`
                    return card;
                }, (result, err) => {
                    if (!err) {
                        resolve(result.length>0?result[0]:undefined)
                    } else {
                        resolve(undefined);
                    }
                });
        });
    }
    async getRelatedCard(customer_id, source):Promise<object> {
        const card = await this.getSourceCard(source);
        let tokencard = undefined;
        if(card!=undefined)
             tokencard = await this.getCustomerCardUsingLast4(
                customer_id, card["last4"], card["exp_year"]);
            return new Promise<object>(resolve => {
                    resolve(tokencard);
            })



    }
    async shouldUpdateCvC(customer_id, source):Promise<boolean> {
        const vgscard = await this.getCustomerCardAbs(customer_id,source);
        const card = await this.getSourceCard(source);
        let shouldUpdate = true;
        if(card!=undefined)
            shouldUpdate = !(await this.cardHasCvv(
                customer_id, card["last4"], card["exp_year"]));
        else if(vgscard!=undefined)
            shouldUpdate = !(await this.cardHasCvv(
                customer_id, vgscard["last4"], vgscard["exp_year"]));
            return new Promise<boolean>(resolve => {
                    resolve(shouldUpdate);
            })



    }

    getPendingVerificationList(): Promise<Array<{}>> {
        return new Promise<Array<{}>>(resolve => {
            mysql.runQuery(`select * from ${process.env.DATABASE}.transactions
             where status = 0 and initial = 1 and description = 'verify card' and provider1_resp !='' and timestamp > '2020-07-01' order by timestamp desc`,
                (row) => {
                    return this.formCharge(row);
                }, (result, err) => {
                    if (!err) {
                        resolve(result)
                    } else {
                        resolve([]);
                    }
                });
        })
    }
    getCardsPendingVerification(): Promise<Array<{}>> {
                    return new Promise<Array<Card>>(resolve => {
                        mysql.runQuery(`select * from ${process.env.DATABASE}.customer_cards where verified = 0 and timestamp > '2020-10-01'`, (row) => {
                            const card= {
                                id: row.id,
                                number: row.number,
                                cvc: row.cvc,
                                exp_year: row.exp_year,
                                exp_month: row.exp_month,
                                brand: row.brand,
                                last4: row.last4,
                                bin: row.bin,
                                expiry: `${row.exp_month}/${row.exp_year}`,
                                verified: false,
                                is_default: row.is_default == 1,
                                can_preauth: row.can_preauth == 1,
                            }
                            return card;
                        }, (results, err) => {
                            resolve(results)
                        })
                    });
    }

    getVerificationsFailedTransaction(source): Promise<Array<{}>> {
        return new Promise<Array<{}>>(resolve => {
            mysql.runQuery(`select id,provider1_resp,source,provider1,status,amount as amount,phone,customer_id from ${process.env.DATABASE}.transactions where timestamp > 2020-10-01 and source = '${source}' order by timestamp asc`,
                (row) => {
                    return {
                        customer_id: row.customer_id,
                        id: row.id,
                        phone: row.phone,
                        name: row.name,
                        source: row.source,
                        status: row.status,
                        provider1: row.provider1,
                        provider1_resp: JSON.parse(row.provider1_resp),
                        amount: row.amount
                    }
                }, (result, err) => {
                    if (!err) {
                        resolve(result)
                    } else {
                        resolve([]);
                    }
                });
        })
    }

    getBankBin(bin): Promise<object> {
        return new Promise<object>(resolve => {
            mysql.runQuery(`select * from ${process.env.DATABASE}.bank_bins
            where bin = '${bin}'`, (row) => {
                return {
                    bank: row.bank_name
                }
            }, (results, err) => {
                if(results.length > 0){
                    resolve(results[0])
                }else{
                  resolve(null)
                }
            })
        });
    }


    getChargeableList(): Promise<Array<{}>> {
        return new Promise<Array<{}>>(resolve => {
            mysql.runQuery(`select sum(amount) as amount,phone,currency,source,customer_id,stripe_cust_id from ${process.env.DATABASE}.pending_charges where status = 1 group by customer_id`,
                (row) => {
                    return {
                        customer_id: row.customer_id,
                        phone: row.phone,
                        stripe_cust_id: row.stripe_cust_id,
                        source: row.source,
                        currency: row.currency,
                        amount: row.amount
                    }
                }, (result, err) => {
                    if (!err) {
                        resolve(result)
                    } else {
                        resolve([]);
                    }
                });
        })
    }

    getChargeList(customer_id): Promise<Array<{}>> {
        return new Promise<Array<{}>>(resolve => {
            mysql.runQuery(`select amount, description, transaction_ref from ${process.env.DATABASE}.pending_charges where status = 1 and customer_id = '${customer_id}'`,
                (row) => {
                    return {
                        description: row.description,
                        transaction_ref: row.transaction_ref,
                        source: row.source,
                        amount: row.amount
                    }
                }, (result, err) => {
                    if (!err) {
                        resolve(result)
                    } else {
                        resolve([]);
                    }
                });
        })
    }

    getCustomerBalance(customer_id): Promise<number> {
        return new Promise<number>(resolve => {
            mysql.runQuery(`select sum(amount) as balance from ${process.env.DATABASE}.customer_account 
        where customer_id = '${customer_id}' and status=1`,
                (row) => {
                    return row.balance;
                }, (result, err) => {
                    if (!err) {
                        let balance = 0.0;
                        if (result.length > 0) {
                            balance = result[0]; //update account balance
                        }
                        resolve(balance);
                    } else {
                        console.log('no access to last balance');
                        resolve(0);
                    }
                })
        });

    }
    customerCreditLim(customer_id):Promise<number>{
        return new Promise<number>(resolve => {
            mysql.runQuery(`select credit_lim as credit_lim from ${process.env.DATABASE}.customers 
        where uid = '${customer_id}' and vip = 1`,
                (row) => {
                    return row.credit_lim;
                }, (result, err) => {
                    if (!err) {
                        let balance = 0.0;
                        if (result.length > 0) {
                            balance = result[0]; //update account balance
                        }
                        resolve(balance);
                    } else {
                        console.log('no access to credit_lim');
                        resolve(0);
                    }
                })
        });
    }
    customerLoan(customer_id):Promise<number>{
        return new Promise<number>(resolve => {
            mysql.runQuery(`select sum(amount) as balance from ${process.env.DATABASE}.boya_lending 
        where customer_id = '${customer_id}' and status=1`,
                (row) => {
                    return row.balance;
                }, (result, err) => {
                    if (!err) {
                        let balance = 0.0;
                        if (result.length > 0) {
                            balance = result[0]; //update account balance
                        }
                        resolve(balance);
                    } else {
                        console.log('no access to last balance');
                        resolve(0);
                    }
                })
        });
    }
    addCustomerLoan(transaction:Transaction):Promise<boolean>{
        return new Promise<boolean>(resolve => {
                mysql.insertRecord(process.env.DATABASE, "boya_lending", {
                    customer_id:transaction["customer_id"],
                    amount:-(transaction.amount+transaction.fee-transaction.promo_amount),
                    running_bal:transaction.loanBal-(transaction.amount+transaction.fee-transaction.promo_amount),
                    transaction_ref:transaction["ref"],
                    card:JSON.stringify(transaction["card"]),
                    email:transaction["email"],
                    description:transaction["description"]
                }, (result, err) => {
                    if (!err) {
                        resolve(true);
                    } else {
                        resolve(false);
                    }
                });
        })
    }
     cancelTransactionLoan(transaction):Promise<boolean>{
        return new Promise<boolean>(resolve => {
            mysql.updateRecordMultipleWhere(process.env.DATABASE, "boya_lending",
                [0, transaction.ref,transaction.customer_id],
                ["status", 'transaction_ref','customer_id'],2,
                (resp, err) => {
                    resolve(!err);
                }
            )
        });
    }
    updateBoyaLoanRepayment(transaction,loanBal:number):Promise<boolean>{
        return new Promise<boolean>(resolve => {
            mysql.insertRecord(process.env.DATABASE, "boya_lending", {
                customer_id:transaction["customer_id"],
                amount:transaction.amount,
                running_bal:loanBal+transaction.amount,
                transaction_ref:transaction["ref"],
                card:JSON.stringify(transaction["card"]),
                email:transaction["email"],
                type:"REPAYMENT",
                status:0,
                description:transaction["description"]
            }, (result, err) => {
                if (!err) {
                    return this.cancelTransactionLoan(transaction);
                } else {
                    resolve(false);
                }
            });
        })
    }
    getPendingLoans():Promise<Array<object>>{
        const customer_ids = [];
        const results = [];
        return new Promise<Array<object>>(resolve => {
            mysql.runQuery(`select customer_id,abs(amount) as amount,running_bal,transaction_ref,card,email,description,created_on from ${process.env.DATABASE}.boya_lending 
        where status = 1 and type = 'LOAN'`,
                (row) => {

                    const debt = {
                        "ref":row.transaction_ref,
                        amount:row.amount,
                        email:row.email,
                        customer_id:row.customer_id,
                        card:JSON.parse(row.card),
                        description:row.description,
                        created_on:dateformat(row.created_on,"yyyy-mm-dd HH:MM")};
                    if(!customer_ids.includes(row.customer_id)){
                        customer_ids.push(row.customer_id)
                        results.push(debt)
                    }

                return debt
                }, (result, err) => {
                    if (!err) {
                        resolve(results);
                    } else {
                        resolve([]);
                    }
                })
        })
    }

    getRecurringPayment(ref):Promise<number>{
        return new Promise<number>(resolve => {
            mysql.runQuery(`select * from ${process.env.DATABASE}.scheduled_payments 
        where ref = '${ref}'`,
                (row) => {
                    return {"frequency":row.frequency};
                }, (result, err) => {
                    if (!err) {
                        let frequency = 0;
                        if (result.length > 0) {
                            frequency = result[0].frequency; //update account balance
                        }
                        resolve(frequency);
                    } else {
                        resolve(0);
                    }
                })
        });
    }
    getRecurringPaymentsForFrequency(frequency:SCHEDULED_PAYMENT_FREQUENCY):Promise<Array<{}>>{
        return new Promise<Array<{}>>(resolve => {
            mysql.runQuery(`select * from ${process.env.DATABASE}.scheduled_payments 
             where status = 1 and frequency = ${frequency}`,
                (row) => {
                    return row;
                }, (result, err) => {
                    if (!err) {
                        resolve(result)
                    } else {
                        resolve([]);
                    }
                });
        })
    }

    stopRecurringPayment(ref):Promise<StatusType>{
        return new Promise<StatusType>(resolve => {
            mysql.updateRecord(process.env.DATABASE, "scheduled_payments",
                [0, ref],
                ["frequency","ref"],  (result, err) => {
                    if (!err) {
                        resolve(StatusType.SUCCESS);
                    } else {
                        resolve(StatusType.ERROR);
                    }
                });
        });
    }
    addRecurringPayment(frequency,transaction): Promise<StatusType> {
        return new Promise<StatusType>(resolve => {
            mysql.insertRecord(process.env.DATABASE, "scheduled_payments", {
                customer_id:transaction["customer_id"],
                amount:transaction["amount"],
                ref:transaction["ref"],
                source:transaction["source"],
                receiver:transaction["receiver"],
                phone:transaction["phone"],
                email:transaction["email"],
                merchant_id:transaction["merchant_id"],
                accno:transaction["accno"],
                group:transaction["group"],
                description:transaction["description"],
                frequency:frequency,
                last_payment_date:transaction["timestamp"],
            }, (result, err) => {
                if (!err) {
                    resolve(StatusType.SUCCESS);
                } else {
                    // console.log(err);
                    console.log('new schedule error',err.code);
                    if(err.code == 'ER_DUP_ENTRY'){
                        transaction["last_payment_date"]= transaction.timestamp;
                        this.updateRecurringPayment(transaction,transaction.customer_id).then((update)=>{
                            resolve(update);
                        });
                    }else{
                        resolve(StatusType.ERROR);
                    }

                }
            });
        });
    }

    updateRecurringPayment(payment, customer_id): Promise<StatusType>{
        return new Promise<StatusType>(resolve => {
            mysql.updateRecordMultipleWhere(process.env.DATABASE, "scheduled_payments",
                [payment.count,payment.last_payment_date,payment.ref,customer_id],
                ["count","last_payment_date","ref","customer_id"], 2, (result, err) => {
                    if (!err) {
                        resolve(StatusType.SUCCESS);
                    } else {
                        resolve(StatusType.ERROR);
                    }
                });
        })
    }


    deleteRecurringPayment(ref,customer_id): Promise<StatusType> {
        return new Promise<StatusType>(resolve => {
            mysql.deleteRecord(`delete from ${process.env.DATABASE}.scheduled_payments 
        where ref = ?`,ref, (err) => {
                    if (!err) {
                        resolve(StatusType.SUCCESS);
                    } else {
                        resolve(StatusType.ERROR);
                    }
                })
        });

    }

    updateRecurringBillNotif(payment): Promise<StatusType>{
        return new Promise<StatusType>(resolve => {
            mysql.updateRecordMultipleWhere(process.env.DATABASE, "bill_notifications",
                [payment.repeat_after,payment.biller,payment.customer_id],
                ["repeat_after","biller","customer_id"], 2, (result, err) => {
                    if (!err) {
                        resolve(StatusType.SUCCESS);
                    } else {
                        resolve(StatusType.ERROR);
                    }
                });
        })
    }

    addCustomerCard(card: Card, customer_id): Promise<StatusType> {
        const newCard = Object.assign(card, {
            customer_id: customer_id
        })
        if (card["last4"]) {
            newCard["last4"] = card["last4"];
        }
        if (card["brand"]) {
            newCard["brand"] = card["brand"];
        }
        return new Promise<StatusType>(resolve => {
            mysql.insertRecord(process.env.DATABASE, "customer_cards", newCard, (result, err) => {
                if (!err) {
                    resolve(StatusType.SUCCESS);
                } else {
                    resolve(StatusType.ERROR);
                }
            });
        });
    }
    updateCardCvvToken(card, customer_id): Promise<StatusType>{
        return new Promise<StatusType>(resolve => {
            mysql.updateRecordMultipleWhere(process.env.DATABASE, "customer_cards",
                [card.cvv, card.last4,card.exp_year, customer_id],
                ["cvc", "last4", "exp_year","customer_id"], 3, (result, err) => {
                    if (!err) {
                        resolve(StatusType.SUCCESS);
                    } else {
                        resolve(StatusType.ERROR);
                    }
                });
        })
    }

    updateCardToken(token, card,customer_id): Promise<StatusType>{
        return new Promise<StatusType>(resolve => {
            mysql.updateRecordMultipleWhere(process.env.DATABASE, "customer_cards",
                [token, card.number,customer_id],
                ["flutterwave_token","number","customer_id"], 2, (result, err) => {
                    if (!err) {
                        resolve(StatusType.SUCCESS);
                    } else {
                        resolve(StatusType.ERROR);
                    }
                });
        })
    }

    updateEquityCardToken(token, card,customer_id): Promise<StatusType>{
        return new Promise<StatusType>(resolve => {
            mysql.updateRecordMultipleWhere(process.env.DATABASE, "customer_cards",
                [token, card.number,customer_id],
                ["equity_token","number","customer_id"], 2, (result, err) => {
                    if (!err) {
                        resolve(StatusType.SUCCESS);
                    } else {
                        resolve(StatusType.ERROR);
                    }
                });
        })
    }

    verifyCustomerCard(cardno, customer_id): Promise<StatusType> {
        return new Promise<StatusType>(resolve => {
            mysql.updateRecordMultipleWhere(process.env.DATABASE, "customer_cards",
                [1, cardno, customer_id],
                ["verified", "number", "customer_id"], 2, (result, err) => {
                    if (!err) {
                        resolve(StatusType.SUCCESS);
                    } else {
                        resolve(StatusType.ERROR);
                    }
                });
        });
    }
    verifyCard(cardno): Promise<StatusType> {
        return new Promise<StatusType>(resolve => {
            mysql.updateRecord(process.env.DATABASE, "customer_cards",
                [1, cardno],
                ["verified", "number"],(result, err) => {
                    if (!err) {
                        resolve(StatusType.SUCCESS);
                    } else {
                        resolve(StatusType.ERROR);
                    }
                });
        });
    }

    setCustomerCardDefault(card: Card, customer_id): Promise<StatusType> {
        return new Promise<StatusType>( resolve => {
            mysql.updateRecord(process.env.DATABASE, "customer_cards",
                [0, customer_id],
                ["is_default", "uid"], (result, err) => {
                    if (!err) {
                        mysql.updateRecordMultipleWhere(process.env.DATABASE, "customer_cards",
                            [1, card.number, customer_id],
                            ["is_default", "number", "uid"], 2, (result, err) => {
                                if (!err) {
                                    resolve(StatusType.SUCCESS);
                                } else {
                                    resolve(StatusType.ERROR);
                                }
                            });
                    } else {
                        resolve(StatusType.ERROR);
                    }
                });


        });
    }

    updateCustomerCardBrand(brand: string, card: Card, customer_id): Promise<StatusType> {
        return new Promise<StatusType>(resolve => {
            mysql.updateRecordMultipleWhere(process.env.DATABASE, "customer_cards",
                [brand, card.number, customer_id],
                ["brand", "number", "uid"], 2, (result, err) => {
                    if (!err) {
                        resolve(StatusType.SUCCESS);
                    } else {
                        resolve(StatusType.ERROR);
                    }
                });
        });
    }

    updateTransactionReceiptLink(receipt_link: string, ref, customer_id): Promise<StatusType> {
        return new Promise<StatusType>(resolve => {
            mysql.updateRecordMultipleWhere(process.env.DATABASE, "transactions",
                [receipt_link, ref, customer_id],
                ["receipt_link", "ref", "customer_id"], 2, (result, err) => {
                    if (!err) {
                        resolve(StatusType.SUCCESS);
                    } else {
                        resolve(StatusType.ERROR);
                    }
                });
        });
    }
    getAllCards(): Promise<Array<Card>> {
        return new Promise<Array<Card>>(resolve => {
            mysql.runQuery(`select * from ${process.env.DATABASE}.customer_cards order by verified desc`, (row) => {
                const card= {
                    id: row.id,
                    number: row.number,
                    cvc: row.cvc,
                    exp_year: row.exp_year,
                    exp_month: row.exp_month,
                    brand: row.brand,
                    last4: row.last4,
                    bin: row.bin,
                    expiry: `${row.exp_month}/${row.exp_year}`,
                    verified: row.verified == 1,
                    is_default: row.is_default == 1,
                    can_preauth: row.can_preauth == 1,
                    flutterwave_token: row.flutterwave_token,
                    equity_token: row.equity_token,
                }
                return card;
            }, (results, err) => {
                    resolve(results)



            })
        });
    }
    getAllVerfiedCardNos(): Promise<Array<string>> {
        return new Promise<Array<string>>(resolve => {
            mysql.runQuery(`select distinct number from ${process.env.DATABASE}.customer_cards where verified = 1 order by verified desc`,
                (row) => {
                return row.number;
            }, (results, err) => {
                resolve(results)



            })
        });
    }
    deleteCard(cardid): Promise<StatusType> {
        return new Promise<StatusType>(resolve => {
            mysql.deleteRecord(`delete from ${process.env.DATABASE}.customer_cards 
        where id = ?`,cardid,(err) => {
                    if (!err) {
                        resolve(StatusType.SUCCESS);
                    } else {
                        resolve(StatusType.ERROR);
                    }
                })
        });

    }
    getCustomerCard(customer_id, cardno): Promise<object> {
        return new Promise<object>(resolve => {
            mysql.runQuery(`select * from ${process.env.DATABASE}.customer_cards
             where customer_id = '${customer_id}' and number = '${cardno}' and cvc != '' order by verified desc`, (row) => {
                const card= {
                    number: row.number,
                    cvc: row.cvc,
                    exp_year: row.exp_year,
                    exp_month: row.exp_month,
                    brand: row.brand,
                    last4: row.last4,
                    employee_id: row.employee_id,
                    bin: row.bin,
                    expiry: `${row.exp_month}/${row.exp_year}`,
                    verified: row.verified == 1,
                    is_default: row.is_default == 1,
                    can_preauth: row.can_preauth == 1,
                    flutterwave_token: row.flutterwave_token,
                    equity_token: row.equity_token,
                }
                console.log("employee_id null",card.employee_id==null);
                return card;
            }, (results, err) => {
                if(results.length > 0){
                    resolve(results[0])
                }else{
                    this.getRelatedCard(customer_id,cardno).then((card)=>{
                        resolve(card);
                    })
                }



            })
        });
    }
    getCustomerCardAbs(customer_id, cardno): Promise<object> {
        return new Promise<object>(resolve => {
            mysql.runQuery(`select * from ${process.env.DATABASE}.customer_cards
             where customer_id = '${customer_id}' and number = '${cardno}' order by verified desc`, (row) => {
                const card= {
                    number: row.number,
                    cvc: row.cvc,
                    exp_year: row.exp_year,
                    exp_month: row.exp_month,
                    brand: row.brand,
                    last4: row.last4,
                    bin: row.bin,
                    employee_id: row.employee_id,
                    expiry: `${row.exp_month}/${row.exp_year}`,
                    verified: row.verified == 1,
                    is_default: row.is_default == 1,
                    can_preauth: row.can_preauth == 1,
                    flutterwave_token: row.flutterwave_token,
                    equity_token: row.equity_token,
                }
                return card;
            }, (results, err) => {
                if(results.length > 0){
                    resolve(results[0])
                }else{
                    this.getRelatedCard(customer_id,cardno).then((card)=>{
                        resolve(card);
                    })
                }



            })
        });
    }
    getCustomerCardUsingLast4(customer_id, last4,expiry_year): Promise<object> {
        console.log('looking for related token with last4',last4)
        return new Promise<object>(resolve => {
            mysql.runQuery(`select * from ${process.env.DATABASE}.customer_cards
             where customer_id = '${customer_id}' and exp_year = '${expiry_year}'
                and last4 = '${last4}' and cvc != ''
               order by verified desc`, (row) => {
                const card= {
                    number: row.number,
                    cvc: row.cvc,
                    exp_year: row.exp_year,
                    exp_month: row.exp_month,
                    brand: row.brand,
                    last4: row.last4,
                    bin: row.bin,
                    employee_id: row.employee_id,
                    expiry: `${row.exp_month}/${row.exp_year}`,
                    verified: row.verified == 1,
                    is_default: row.is_default == 1,
                    can_preauth: row.can_preauth == 1,
                    flutterwave_token: row.flutterwave_token,
                    equity_token: row.equity_token,
                }
                if(card.cvc.toString().length>2)
                    return card;
                return {"verified":false}
            }, (results, err) => {
                resolve(results.length > 0 ? results[0] : null);
            })
        });
    }
    corporateDbName(employee_id):Promise<string>{
        return new Promise<string>(resolve => {
            mysql.runQuery(`select database_name as name from ${process.env.DATABASE}.corporate_employee where employee_id = '${employee_id}'`,
                (row) => {
                    return row.name;
                }, (results, err) => {
                    if(results.length>0)
                        return resolve(results[0])
                    resolve("")


                })
        })
    }
    corporateDbNameByBiz(businessID):Promise<string>{
        return new Promise<string>(resolve => {
            mysql.runQuery(`select database_name as name from ${process.env.DATABASE}.corporate_employee where business_id = '${businessID}' limit 1`,
                (row) => {
                    return row.name;
                }, (results, err) => {
                    if(results.length>0)
                        return resolve(results[0])
                    resolve("")


                })
        })
    }
    async linkVirtualCard(card,provider): Promise<boolean> {
        const dbName = await this.corporateDbName(card.employee_id)
        return new Promise<boolean>(resolve => {
            mysql.insertRecord(dbName, "cards", {
                "vcn":card.id,
                "employee_id": card.employee_id,
                "name": card["cardHolder"],
                "balance": card["amount"],
                "brand": card["brand"],
                "expiry": card["expiration"],
                "last4": card["last4"],
                "provider": provider,
                "currency": card["currency"]?card["currency"]:"USD"
            }, (result, err) => {
                console.log('has error',err)
                resolve(!err);
            });
        })
    }
    businessID(employee_id):Promise<string>{
        return new Promise<string>(resolve => {
            mysql.runQuery(`select business_id as id from ${process.env.DATABASE}.corporate_employee where employee_id = '${employee_id}'`,
                (row) => {
                    return row.id;
                }, (results, err) => {
                    if(results.length>0)
                        return resolve(results[0])
                    resolve(null)


                })
        })
    }
    virtualCardActive(dbName,employee_id,vcn):Promise<boolean>{
        return new Promise<boolean>(resolve => {
            mysql.runQuery(`select retired,stopped from ${dbName}.cards where employee_id = '${employee_id}' and vcn = '${vcn}'`,
                (row) => {
                    return {"active": row.retired==0 && row.stopped==0};
                }, (results, err) => {
                    if(results.length>0)
                        return resolve(results[0].active)
                    console.log('card is not found');
                    resolve(false)


                })
        })
    }
    async updateExpenseCategory(employee_id,categoryId,groupId,transactionRef):Promise<boolean> {
        const database = await this.corporateDbName(employee_id);
        return new Promise<boolean>(resolve => {
            mysql.updateRecord(database, "expenses",
                [categoryId,groupId,transactionRef],
                ["category_id","group_id", "transaction_ref"], (result, err) => {
                    if (!err) {
                        resolve(true);
                    } else {
                        resolve(false);
                    }
                });
        })
    }
    async updateExpenseReciptUrl(employee_id,receiptUrl,transactionRef,notes=""):Promise<boolean> {
        const database = await this.corporateDbName(employee_id);
        return new Promise<boolean>(resolve => {
            mysql.updateRecord(database, "expenses",
                [receiptUrl,notes,transactionRef],
                ["physical_receipt", "notes","transaction_ref"], (result, err) => {
                    if (!err) {
                        resolve(true);
                    } else {
                        resolve(false);
                    }
                });
        })
    }
    async getCorporateGroups(employee_id): Promise<Object> {
        const database = await this.corporateDbName(employee_id);
        console.log("db name",database);
        const groups = [];
        const indexer = {};
        return new Promise<Object>(resolve => {
            const sql = `SELECT * from ${database}.groups where status='1' order by name asc`
            mysql.runQuery(sql,
                (row) => {
                    const gg = {
                        id: row.id,
                        name: row.name,
                        icon: row.icon,
                        edit: false,
                        description: row.description,
                        categories: []
                    }
                    groups.push(gg);
                    indexer[gg.name] = groups.length-1;
                    return gg;
                }, (result, err) => {
                    const sql = `SELECT c.*,(select name from ${database}.groups where id = c.groupid) as groupname from ${database}.categories c where status='1'`
                    mysql.runQuery(sql,
                        (row) => {
                            const cc = {
                                id: row.id,
                                name: row.name,
                                icon: row.icon,
                                edit: false,
                                description: row.description
                            }
                            if(indexer[row.groupname]!=undefined)
                            groups[indexer[row.groupname]]["categories"].push(cc);
                            return cc;
                        }, (result, err) => {
                            if (result.length > 0) {
                                resolve({status: StatusType.SUCCESS, data: groups ? groups : {}});
                            } else {
                                resolve({status: StatusType.ERROR, data: {errorMessage: err.toString()}})
                            }
                        });
                });
        })

    }
    async getCorporateFwCards(businessId): Promise<Array<string>> {
        const database = await this.corporateDbNameByBiz(businessId);
        console.log("db name",database);
        const groups = [];
        const indexer = {};
        return new Promise<Array<string>>(resolve => {
            const sql = `SELECT * from ${database}.cards where provider = 'FLUTTERWAVE' order by name asc`
            mysql.runQuery(sql,
                (row) => {
                    const gg = {
                        vcn: row.vcn,
                        employee_id:row.employee_id
                    }
                    return gg;
                }, (result, err) => {
                    if (result.length > 0) {
                        resolve(result);
                    } else {
                        resolve([])
                    }
                });
        })

    }
    async getActiveCorps(): Promise<Array<object>> {

        return new Promise<Array<object>>(resolve => {
            const sql = `SELECT * from ${process.env.DATABASE}.corporate_accounts order by timestamp desc`
            mysql.runQuery(sql,
                (row) => {
                    const gg = {
                        business_id: row.business_id,
                        name: row.name,
                        address: row.address,
                        currency: row.currency,
                        created_at: dateformat(row.timestamp, "yyyy-mm-dd HH:MM"),
                    }
                    return gg;
                }, (result, err) => {
                    if (result.length > 0) {
                        resolve(result);
                    } else {
                        resolve([])
                    }
                });
        })

    }
    async getCorporateExpenses(businessId): Promise<Array<object>> {
        const database = await this.corporateDbNameByBiz(businessId);
        console.log("db name",database);
        const groups = [];
        const indexer = {};
        return new Promise<Array<object>>(resolve => {
            const sql = `SELECT * from ${database}.expense_view order by created_at desc`
            mysql.runQuery(sql,
                (row) => {
                    const gg = {
                        expense_id: row.expense_id,
                        objectID: row.expense_id,
                        business_id: businessId,
                        category_id: row.category_id,
                        group_id: row.group_id,
                        description: row.title,
                        employee_id: row.employee_id,
                        notes: row.notes,
                        person: row.person,
                        team: row.team,
                        team_code: row.team_code,
                        amount: row.amount,
                        category: row.category,
                        group: row.group,
                        physical_receipt: row.physical_receipt,
                        boya_receipt: row.boya_receipt,
                        fees: row.fees,
                        status: row.status,
                        transaction_ref: row.transaction_ref,
                        created_at: dateformat(row.created_at, "yyyy-mm-dd HH:MM"),
                        updated_at: dateformat(row.updated_at, "yyyy-mm-dd HH:MM"),
                        timestamp: new Date(row.created_at).getTime()
                    }
                    return gg;
                }, (result, err) => {
                    if (result.length > 0) {
                        resolve(result);
                    } else {
                        resolve([])
                    }
                });
        })

    }
    employeeSpendWeekly(dbname,employee_id,onWeek):Promise<number>{
        return new Promise<number>(resolve => {
            mysql.runQuery(`select sum(amount) as spent from ${dbname}.expense_view where employee_id = '${employee_id}' and week(created_at) = ${onWeek}`,
                (row) => {
                    return row.spent;
                }, (results, err) => {
                    if(results.length>0)
                        return resolve(results[0])
                    resolve(0)


                })
        })
    }
    employeeSpendMonthly(dbname,employee_id,onMonth):Promise<number>{
        return new Promise<number>(resolve => {
            mysql.runQuery(`select sum(amount) as spent from ${dbname}.expense_view where employee_id = '${employee_id}' and month(created_at) = ${onMonth}`,
                (row) => {
                    return row.spent;
                }, (results, err) => {
                    if(results.length>0)
                        return resolve(results[0])
                    resolve(0)


                })
        })
    }
    employeeSpendPerioFrom(dbname,employee_id,fromMonth:string,toMonth:string):Promise<number>{
        return new Promise<number>(resolve => {
            mysql.runQuery(`select sum(amount) as spent from ${dbname}.expense_view where employee_id = '${employee_id}' and created_at >= '${fromMonth}' and created_at <= '${toMonth}'`,
                (row) => {
                    return row.spent;
                }, (results, err) => {
                    if(results.length>0)
                        return resolve(results[0])
                    resolve(0)


                })
        })
    }
     getQuarterStartEndDates():{start:string, end:string}{

        const localDate = LocalDate.now();
        console.log(localDate);
        // Get the month of the current date
        const i = localDate.monthValue();
        let start = localDate.withDayOfMonth(1);
        let end = localDate.plusMonths(1).withDayOfMonth(1)
        if(i<4){
            start = localDate.withMonth(1).withDayOfMonth(1);
            end = localDate.withMonth(4).withDayOfMonth(1);
        }else if(i<7){
            start = localDate.withMonth(4).withDayOfMonth(1);
            end = localDate.withMonth(7).withDayOfMonth(1);
        }else if(i<10){
            start = localDate.withMonth(7).withDayOfMonth(1);
            end = localDate.withMonth(10).withDayOfMonth(1);
        }else{
            start = localDate.withMonth(10).withDayOfMonth(1);
            end = localDate.withMonth(12).plusMonths(1).withDayOfMonth(1);
        }

         return {start:start.toString(),end:end.toString()}
    }

    getemployeeAllocation(dbname):Promise<any>{
        return new Promise<any>(resolve => {
            mysql.runQuery(`select employee_id, limit_per_purchase, max_spend_per_period,periodic_limit from ${dbname}.employee order by max_spend_per_period ASC`,
                (row) => {
                    return {
                        limit_per_purchase:row.limit_per_purchase,
                        max_spend_per_period:row.max_spend_per_period,
                        periodic_limit:row.periodic_limit,
                        employee_id:row.employee_id,
                        spent:0
                    };
                }, (results, err) => {

                    if(!err &&  results.length>0){
                        const alloc = results;

                        // get wallet allocation
                        this.getWalletTotal(dbname).then((result)=>{
                            console.log('alloc-----0------',result.credit, result.debit)
                            let balance
                           let newbalance=0

                            if(result.credit == null || result.credit == ''){
                                balance=0
                            }else {
                                balance =(result.credit - result.debit)
                            }

                            let i;
                            newbalance=balance
                            for (i = 0; i < alloc.length; i++) {
                                const employeeObj = alloc[i]
                                console.log('alloc-----0------',newbalance )

                                if(newbalance >= employeeObj.max_spend_per_period){
                                    newbalance = newbalance - employeeObj.max_spend_per_period
                                    this.newemployeeAllocation(employeeObj.employee_id, employeeObj.max_spend_per_period,dbname).then((result)=>{
                                        resolve(result);
                                    })

                                }else{
                                    this.newemployeeAllocationStatus(employeeObj.employee_id,dbname).then((result)=>{
                                        resolve(result);
                                    })

                                }

                            }

                        })



                    }else{
                        resolve(false)
                    }
                })
        })


    }

    getWalletTotal(dbName):Promise<any>{
        return new Promise<any>(resolve => {
            mysql.runQuery(`select SUM(debit) as totalbebit, SUM(credit) as totalcredit from ${dbName}.wallet_ledger`,
                (row) => {
                    return {
                        credit: row.totalcredit,
                        debit: row.totalbebit
                    };
                }, (results, err) => {
                    resolve(results.length>0?results[0]:[])
                })
        })

    }

    employeeAllocation(dbname,employee_id):Promise<{
        limit_per_purchase:number,
        max_spend_per_period:number,
        periodic_limit:number,
        spent:number
    }>{
        return new Promise<{
            limit_per_purchase:number,
            max_spend_per_period:number,
            periodic_limit:number,
            spent:number
        }>(resolve => {
            mysql.runQuery(`select limit_per_purchase, max_spend_per_period,periodic_limit,status from ${dbname}.employee where employee_id = '${employee_id}'`,
                (row) => {
                    return {
                        limit_per_purchase:row.limit_per_purchase,
                        max_spend_per_period:row.max_spend_per_period,
                        periodic_limit:row.periodic_limit,
                        spent:0,
                        status:row.status
                    };
                }, (results, err) => {
                    if(!err &&  results.length>0){
                        let alloc = results[0];
                        const qtr = this.getQuarterStartEndDates()
                        if(alloc.status!=1){
                            alloc = {
                                limit_per_purchase:0,
                                max_spend_per_period:0,
                                periodic_limit:0,
                                spent:0,
                            }
                        }else
                        switch (alloc.periodic_limit) {
                            case 0:
                                //weekly alloc
                                this.employeeSpendWeekly(dbname,employee_id,LocalDate.now().isoWeekOfWeekyear()).then((result)=>{
                                    alloc.spent = result!=null? result:0
                                    resolve(alloc);
                                })
                                break;
                            case 1:
                                //monthly alloc
                                this.employeeSpendMonthly(dbname,employee_id,LocalDate.now().monthValue()).then((result)=>{
                                    alloc.spent = result!=null? result:0
                                    resolve(alloc);
                                })
                                break;
                            case 2:
                                //quarterly alloc
                                // const month = LocalDate.now().;
                                this.employeeSpendPerioFrom(dbname,employee_id,qtr.start,qtr.end).then((result)=>{
                                    alloc.spent = result!=null? result:0
                                    resolve(alloc);
                                })
                                break;
                            default:
                                //quarterly alloc
                                // const month = LocalDate.now().;
                                this.employeeSpendPerioFrom(dbname,employee_id,LocalDate.now().withMonth(1).toString(),
                                    LocalDate.now().toString()).then((result)=>{
                                    alloc.spent = result!=null? result:0
                                    resolve(alloc);
                                })
                                break;
                        }

                    }else{
                        resolve({
                            limit_per_purchase:0,
                            max_spend_per_period:0,
                            periodic_limit:0,
                            spent:0
                        })
                    }
                })
        })
    }
    employeeBalance(employee_id):Promise<number>{
        return new Promise<number>(resolve => {
            this.corporateDbName(employee_id).then((dbName)=>{
                this.employeeAllocation(dbName,employee_id).then((result)=>{
                    if(result.max_spend_per_period!=0){
                        resolve(result.max_spend_per_period-result.spent)
                    }else{
                        resolve(0);
                    }
                })
            })
        })
    }
    employeeLimits(employee_id):Promise<object>{
        return new Promise<object>(resolve => {
            this.corporateDbName(employee_id).then((dbName)=>{
                this.employeeAllocation(dbName,employee_id).then((result)=>{
                    resolve(result);
                })
            })
        })
    }
    getcorporateDbsName():Promise<string>{
        return new Promise<string>(resolve => {
            mysql.runQuery(`select DISTINCT database_name as name from ${process.env.DATABASE}.corporate_employee`,
                (row) => {
                    return row.name;
                }, (results, err) => {
                    if(results.length>0)
                        return resolve(results)
                    resolve("")

                })
        })
    }
    getCustomerIdByEmpId(employee_id):Promise<object>{
        return new Promise<object>(resolve => {
            mysql.runQuery(`select * from ${process.env.DATABASE}.corporate_employee where employee_id = '${employee_id}'`,
                (row) => {
                    return {
                        customer_id:row.customer_id,
                        phone:row.phone_number,
                        email:row.organization_email,
                    };
                }, (results, err) => {
                    if(results.length>0)
                        return resolve(results[0])
                    resolve({})

                })
        })
    }

    updateemployeeLimits():Promise<Boolean>{
        return new Promise<Boolean>(resolve => {
            this.getcorporateDbsName().then((dbNames)=>{
                // loop through db names
                let i;
                for (i = 0; i < dbNames.length; i++) {
                    const dbName = dbNames[i]
                    this.getemployeeAllocation(dbName).then((result)=>{
                        resolve(result);
                    })
                }

            })
        })
    }

    blockedCategories(employee_id,cardno):Promise<object>{
        return new Promise<object>(resolve => {
            this.corporateDbName(employee_id).then((dbName)=>{
                mysql.runQuery(`select blocked_categories from ${dbName}.cards where vcn = '${cardno}'`,
                    (row) => {
                        return {
                            blocked_categories:row.blocked_categories,
                        };
                    }, (results, err) => {
                        resolve(results.length>0?results[0]:[])
                    })
            })
        })
    }
    employeePurchaseWithinLimit(purchaseAmount:number,employee_id):Promise<boolean>{
        return new Promise<boolean>(resolve => {
            if(employee_id==null)
                return false;
            this.corporateDbName(employee_id).then((dbName)=>{
                this.employeeAllocation(dbName,employee_id).then((result)=>{
                    if(result.max_spend_per_period!=0){
                        const balance = (result.max_spend_per_period-result.spent);
                            if(result.limit_per_purchase==0)
                                return resolve(purchaseAmount<balance)
                            resolve(purchaseAmount<balance && purchaseAmount < result.limit_per_purchase)
                    }else{
                        resolve(false);
                    }
                })
            })
        })
    }
    employeeCardActive(employee_id,cvn):Promise<boolean>{
        return new Promise<boolean>(resolve => {
            if(employee_id==null)
                return false;
            this.corporateDbName(employee_id).then((dbName)=>{
                this.virtualCardActive(dbName,employee_id,cvn).then((result)=>{
                    resolve(result);
                })
            })
        })
    }
    randomRef() {
        const anysize = 10;//the size of string
        const charset = "abcdefghijklmnopqrstuvwxyz".toUpperCase(); //from where to create
        let result = "";
        for (let i = 0; i < anysize; i++)
            result += charset[Math.floor(Math.random() * charset.length)];
        console.log(result);
        return result;
    }
    async debitCorporateWalletLedger(employee_id,amount,transaction):Promise<StatusType>{
        const business_db_name = await this.corporateDbName(employee_id);
        return new Promise<StatusType>(resolve => {
            mysql.insertRecord(business_db_name, "expenses", {
                "expense_id":this.randomRef(),
                "employee_id":employee_id,
                "transaction_ref":transaction["txRef"],
                "title":transaction["description"],
                "group_id":transaction["groupid"]?transaction["groupid"]:'1',
                "category_id":transaction["categoryid"]?transaction["categoryid"]:'11',
                "status":1,
            }, (result, err) => {
                mysql.insertRecord(business_db_name, "wallet_ledger", {
                    "debit":amount,
                    "credit":0,
                    "ref":transaction["txRef"],
                }, (result, err) => {
                    if(!err){
                        resolve(StatusType.SUCCESS)
                    }else{
                        resolve(StatusType.ERROR)
                    }
                });
            });
        })



    }
    cardHasCvv(customer_id,last4,expiry_year):Promise<boolean>{
        return new Promise<boolean>(resolve => {
            mysql.runQuery(`select * from ${process.env.DATABASE}.customer_cards
             where customer_id = '${customer_id}' and exp_year = '${expiry_year}'
                and last4 = '${last4}' and cvc != ''
               order by verified desc`, (row) => {
                const card= {
                    number: row.number,
                    exp_year: row.exp_year,
                    exp_month: row.exp_month,
                    employee_id: row.employee_id,
                    brand: row.brand,
                    last4: row.last4,
                    expiry: `${row.exp_month}/${row.exp_year}`,
                    verified: row.verified == 1,
                }
                return card
            }, (results, err) => {
                resolve(results.length > 0);
            })
        });
    }
    cardExistsUnderDifffCustomer(customer_id, cardno): Promise<boolean> {
        return new Promise<boolean>(resolve => {
            mysql.runQuery(`select * from ${process.env.DATABASE}.customer_cards
             where customer_id != '${customer_id}' and number = '${cardno}'`, (row) => {
                return {
                    number: row.number,
                    cvc: row.cvc,
                    exp_year: row.exp_year,
                    exp_month: row.exp_month,
                    brand: row.brand,
                    last4: row.last4,
                    expiry: `${row.exp_month}/${row.exp_year}`,
                    verified: row.verified == 1,
                    is_default: row.is_default == 1,
                }
            }, (results, err) => {
                resolve(results.length > 0);
            })
        });
    }

    getCustomerCards(customer_id): Promise<Array<object>> {
        return new Promise<Array<object>>(resolve => {
            mysql.runQuery(`select * from ${process.env.DATABASE}.customer_cards
             where customer_id = '${customer_id}'`, (row) => {
                return {
                    number: row.number,
                    cvc: row.cvc,
                    exp_year: row.exp_year,
                    exp_month: row.exp_month,
                    brand: row.brand,
                    bin: row.bin,
                    verified: row.verified == 1,
                    is_default: row.is_default == 1,
                }
            }, (results, err) => {
                resolve(results);
            })
        });
    }

    fetchUserPhoto(userId): Promise<object> {
        return new Promise<object>(resolve => {
            mysql.runQuery(`select * from ${process.env.DATABASE}.customers where uid = '${userId}'`,(row)=>{
                return {photo:row.photo,
                    name:row.name,
                    national_id:row.national_id,
                    uid:userId,
                    national_id_photo:row.national_id_photo,
                    legal_data:row.legal_data}
            },(results,err)=>{
                let photo = undefined;
                if (!err && results.length > 0) {
                    photo = results[0];
                    try{
                        photo.legal_data = JSON.parse(photo.legal_data);
                    }catch (e) {
                        console.log(e)
                    }

                }
                resolve(photo);
            })
        });
    }

    saveSmileVericationStatus(customerId, jobDetails): Promise<boolean> {
        return new Promise<boolean>(resolve => {
            mysql.updateRecord(process.env.DATABASE, "customers",
                [JSON.stringify({smile: jobDetails}), customerId],
                ["verification_json", "uid"], (result, err) => {
                    if (!err) {
                        resolve(true);
                    } else {
                        resolve(false);
                    }
                });
        });
    }

    isBlockedCustomerId(customer_id): Promise<boolean> {
        return new Promise<boolean>(resolve => {
            mysql.runQuery(`select * from ${process.env.DATABASE}.blocklist 
        where customer_id = '${customer_id}'`,
                (row) => {
                    return row.id;
                }, (result, err) => {
                    resolve(!err && result.length > 0);
                })
        });

    }
    customerMaxLimit(customer_id,group): Promise<number> {
        return new Promise<number>(resolve => {
            mysql.runQuery(`select * from ${process.env.DATABASE}.transaction_limits 
        where customer_id = '${customer_id}' and channel = '${group}' and status = 1`,
                (row) => {
                    return row.maximum;
                }, (result, err) => {
                    resolve((!err && result.length > 0)? result[0]:0);
                })
        });

    }

    hasClearanceKyc(customer_id): Promise<boolean> {
        return new Promise<boolean>(resolve => {
            mysql.runQuery(`select * from ${process.env.DATABASE}.customers 
        where uid = '${customer_id}'`,
                (row) => {
                    let confidence = 0;
                    let selfie_data = row.image_verification_data;
                    try {
                        selfie_data = JSON.parse(selfie_data);
                        confidence = selfie_data["ConfidenceValue"];
                    } catch (e) {

                    }
                    return {"confidence": confidence};
                }, (result, err) => {
                    resolve(!err && result.length > 0 && result[0].confidence > 80);
                });
        });

    }

    isOldEnough(customer_id): Promise<boolean> {
        return new Promise<boolean>(resolve => {
            mysql.runQuery(`select * from ${process.env.DATABASE}.customers 
        where uid = '${customer_id}' and timestamp < '2020-05-01'`,
                (row) => {
                    return row.id;
                }, (result, err) => {
                    resolve(!err && result.length > 0);
                });
        });

    }

    isBlockedPhone(phone): Promise<boolean> {
        return new Promise<boolean>(resolve => {
            mysql.runQuery(`select * from ${process.env.DATABASE}.blocklist 
        where phone like '%${phone}'`,
                (row) => {
                    return row.id;
                }, (result, err) => {
                    resolve(!err && result.length > 0);
                })
        });

    }

    isBlockedCard(sourceId): Promise<boolean> {
        return new Promise<boolean>(resolve => {
            mysql.runQuery(`select * from ${process.env.DATABASE}.blocklist 
        where sourceId = '${sourceId}'`,
                (row) => {
                    return row.id;
                }, (result, err) => {
                    resolve(!err && result.length > 0);
                })
        });

    }

    isBlockedIP(ip): Promise<boolean> {
        return new Promise<boolean>(resolve => {
            mysql.runQuery(`select * from ${process.env.DATABASE}.blocklist 
        where ip = '${ip}'`,
                (row) => {
                    return row.id;
                }, (result, err) => {
                    resolve(!err && result.length > 0);
                })
        });

    }
    // isAboveLimit(customer_id): Promise<boolean> {
    //     return new Promise<boolean>(resolve => {
    //         mysql.runQuery(`select * from ${process.env.DATABASE}.blocklist
    //     where ip = '${ip}'`,
    //             (row) => {
    //                 return row.id;
    //             }, (result, err) => {
    //                 resolve(!err && result.length > 0);
    //             })
    //     });
    //
    // }

    isBlockedAccno(accno): Promise<boolean> {
        return new Promise<boolean>(resolve => {
            mysql.runQuery(`select * from ${process.env.DATABASE}.blocklist
        where accno = '${accno}'`,
                (row) => {
                    return row.id;
                }, (result, err) => {
                    resolve(!err && result.length > 0);
                })
        });

    }

    isBlockedTill(till): Promise<boolean> {
        return new Promise<boolean>(resolve => {
            mysql.runQuery(`select * from ${process.env.DATABASE}.blocklist
        where till = '${till}'`,
                (row) => {
                    return row.id;
                }, (result, err) => {
                    resolve(!err && result.length > 0);
                })
        });

    }

    getCustomerFreeMoneyBalance(customer_id): Promise<number> {
        return new Promise<number>(resolve => {
            mysql.runQuery(`select sum(amount) as amount,phone,currency,source,customer_id,stripe_cust_id from
             ${process.env.DATABASE}.pending_charges where status = 1 and customer_id = '${customer_id}' group by customer_id`,
                (row) => {
                    return {
                        customer_id: row.customer_id,
                        phone: row.phone,
                        stripe_cust_id: row.stripe_cust_id,
                        source: row.source,
                        currency: row.currency,
                        amount: row.amount
                    }
                }, (result, err) => {
                    if (!err && result.length > 0) {
                        resolve(result[0].amount)
                    } else {
                        resolve(0.0);
                    }
                });
        })

    }

    async getCustomerShareCounts(customer_id): Promise<number> {
        const res = await this.getCustomerShareCode(customer_id);
        return new Promise<number>(resolve => {
            mysql.runQuery(`select count(uid) as balance from ${process.env.DATABASE}.customers
        where invitecode = '${res.sharecode}'`,
                (row) => {
                    return row.balance;
                }, (result, err) => {
                    if (!err) {
                        let balance = 0.0;
                        if (result.length > 0) {
                            balance = result[0]; //update account balance
                        }
                        resolve(balance);
                    } else {
                        console.log('no access to last balance');
                        resolve(0);
                    }
                })
        });

    }

    async getCustomerPhone(customer_id): Promise<string> {
        return new Promise<string>(resolve => {
            mysql.runQuery(`select phone from ${process.env.DATABASE}.customers
        where uid = '${customer_id}'`,
                (row) => {
                    return row.phone;
                }, (result, err) => {
                    if (!err) {
                        let phone = "";
                        if (result.length > 0) {
                            phone = result[0]; //update account balance
                        }
                        resolve(phone);
                    } else {
                        console.log('no phone');
                        resolve("0718555832");
                    }
                })
        });

    }

    async getCustomerEmail(customer_id): Promise<string> {
        console.log("looking for email", customer_id);
        return new Promise<string>(resolve => {
            mysql.runQuery(`select email from ${process.env.DATABASE}.customers
        where uid = '${customer_id}'`,
                (row) => {
                    return row.email;
                }, (result, err) => {
                    if (!err) {
                        let phone = "";
                        if (result.length > 0) {
                            phone = result[0]; //update account balance
                        }
                        resolve(phone);
                    } else {
                        console.log('no phone');
                        resolve("robert@boya.co");
                    }
                })
        });

    }

    getCustomerStatement(customer_id): Promise<{}> {
        return new Promise<{}>(resolve => {
            mysql.runQuery(`select id,group,description,amount,timestamp,receiver,fees,mode,status,refund_reason, accno, ref,bank from ${process.env.DATABASE}.transactions
        where customer_id = '${customer_id}' and status=1`,
                (row) => {
                    return {
                        id: row.id,
                        group: row.group,
                        amount: row.amount,
                        fees: row.fees,
                        description: row.description,
                        status: row.status,
                        mode: row.mode,
                        accno: row.accno,
                        receiver: row.receiver,
                        timestamp: dateformat(row.timestamp, 'yyyy-mm-dd HH:MM'),
                        ref: row.ref,
                    };
                }, (result, err) => {
                    if (!err) {
                        resolve(result);
                    } else {
                        resolve({});
                    }
                })
        });

    }


    getCustomerWalletStatement(customer_id): Promise<{}> {
        return new Promise<{}>(resolve => {
            mysql.runQuery(`select * from ${process.env.DATABASE}.transactions
        where customer_id = '${customer_id}' and status=1 and transaction_type =2 or transaction_type=3`,
                (row) => {
                    return {
                        id: row.id,
                        group: row.group,
                        amount: row.amount,
                        wallet_charge: row.wallet_charge,
                        fees: row.fees,
                        description: row.description,
                        status: row.status,
                        mode: row.mode,
                        accno: row.accno,
                        transaction_type: row.transaction_type,
                        receiver: row.receiver,
                        timestamp: dateformat(row.timestamp, 'yyyy-mm-dd HH:MM'),
                        ref: row.ref,
                    };
                }, (result, err) => {
                    if (!err) {
                        resolve(result);
                    } else {
                        resolve({});
                    }
                })
        });

    }

    getPaybills(): Promise<[]> {
        return new Promise<[]>(resolve => {
            mysql.runQuery(`select * from ${process.env.DATABASE}.mpesa_paybills where status=1`,
                (row) => {
                    return {
                        id: row.id,
                        paybill: row.paybill,
                        name: row.name
                    };
                }, (result, err) => {
                    if (!err) {
                        resolve(result);
                    } else {
                        resolve([]);
                    }
                })
        });

    }

    flagAsFraud(uid): Promise<boolean> {
        return new Promise<boolean>(resolve => {
            mysql.updateRecord(process.env.DATABASE, "customers",
                [1, uid],
                ["fraudulent", "uid"], (resp, err) => {
                    resolve(!err);
                })
        });
    }

    newemployeeAllocation(employee_id, allocation, db): Promise<boolean> {
        const format1 = "YYYY-MM-DD HH:mm:ss"
        const date = moment().format(format1);
        return new Promise<boolean>(resolve => {
            mysql.updateRecord(db, "employee",
                [allocation,date,1, employee_id],
                ["max_spend_per_period","updated_at", "status","employee_id"], (resp, err) => {
                    if(!err){
                        resolve(resp);
                    }else{
                        resolve(err);
                    }

                })
        });
    }

    newemployeeAllocationStatus(employee_id, db): Promise<boolean> {

        return new Promise<boolean>(resolve => {
            mysql.updateRecord(db, "employee",
                [4, employee_id],
                ["status", "employee_id"], (resp, err) => {
                    if(!err){
                        resolve(resp);
                    }else{
                        resolve(err);
                    }

                })
        });
    }

    blockUser(transaction: Transaction, reason): Promise<StatusType> {
        return new Promise<StatusType>(resolve => {
            const bdata = {
                customer_id: transaction.customer_id,
                phone: transaction.phone,
                email: transaction.email,
                ref: transaction.ref,
                sourceId: transaction.source,
                ip: transaction.ip,
                amount: transaction.amount,
                reason: reason,
            }
            if (transaction.deviceInfo) {
                try {
                    bdata["device_info"] = JSON.stringify(transaction.deviceInfo);
                } catch (e) {

                }
            }
            if (transaction.group == Groups.tills) {
                bdata["till"] = transaction.receiver;
            }
            if (transaction.group == Groups.bills) {
                bdata["accno"] = transaction.accno;
            }
            if (transaction.ip) {
                bdata["ip"] = transaction.ip;
            }
            mysql.insertRecord(process.env.DATABASE, "blocklist", bdata, (result, err) => {
                this.flagAsFraud(transaction.customer_id).then((added) => {

                })
                if (!err) {
                    resolve(StatusType.SUCCESS);
                } else {
                    resolve(StatusType.ERROR);
                }
            });
        });
    }

    resetPin(nonce, encipheredotp, customer_id): Promise<StatusType> {
        return new Promise<StatusType>(resolve => {
            mysql.insertRecord(process.env.DATABASE, "pin_resets", {
                uid: customer_id,
                otp: encipheredotp,
                nonce: nonce,
            }, (result, err) => {
                if (!err) {
                    resolve(StatusType.SUCCESS);
                } else {
                    resolve(StatusType.ERROR);
                }
            });
        });
    }

    verifyPin(nonce, code): Promise<boolean> {
        return new Promise<boolean>(resolve => {
            mysql.runQuery(`select * from ${process.env.DATABASE}.pin_resets where nonce = '${nonce}' and otp = '${code}' and status = 1`, (row) => {
                return row.counted;
            }, (results, err) => {
                const available = results.length > 0;
                if (available) {
                    mysql.updateRecord(`${process.env.DATABASE}`, "pin_resets", [0, nonce], ["status", "nonce"], (result, err) => {
                    });
                }
                resolve(available);
            })
        });
    }

    verifyMidPin(mid, code): Promise<boolean> {
        return new Promise<boolean>(resolve => {
            mysql.runQuery(`select * from ${process.env.DATABASE}.pin_resets where uid = '${mid}' and otp = '${code}' and status = 1`, (row) => {
                return row.counted;
            }, (results, err) => {
                const available = results.length > 0;
                if (available) {
                    mysql.updateRecord(`${process.env.DATABASE}`, "pin_resets", [0, mid], ["status", "uid"], (result, err) => {
                    });
                }
                resolve(available);
            })
        });
    }

    updatePendingCharges(transaction_ref, chargeId, charge_result): Promise<boolean> {
        return new Promise<boolean>(resolve => {
            mysql.updateRecord(`${process.env.DATABASE}`, "pending_charges",
                [chargeId, JSON.stringify(charge_result), 0, transaction_ref],
                ["chargeId", "charge_result", "status", "transaction_ref"], (resp, err) => {
                    resolve(!err);
                })
        });
    }

    addPendingCharge(transaction: Transaction): Promise<StatusType> {
        //check last balance
        return new Promise<StatusType>(resolve => {
            mysql.insertRecord(`${process.env.DATABASE}`, "pending_charges", {
                customer_id: transaction.customer_id,
                stripe_cust_id: transaction.stripe_cust_id,
                currency: transaction.currency,
                transaction_ref: transaction.txRef,
                amount: transaction.amount,
                status: 1,
                description: transaction.description,
                phone: transaction.phone,
                group: transaction.group,
                source: transaction.source
            }, (result, err) => {
                if (!err) {
                    resolve(StatusType.SUCCESS);
                } else {
                    console.log(err);
                    resolve(StatusType.ERROR);
                }
            });
        })


    }

    debitCustomerPromoUse(customer_id, txRef, promocode, amount): Promise<StatusType> {
        //check last balance
        return new Promise<StatusType>(resolve => {
            mysql.runQuery(`select sum(amount) as balance from ${process.env.DATABASE}.customer_account
        where customer_id = '${customer_id}' and status=1`,
                (row) => {
                    return row.balance;
                }, (result, err) => {
                    if (!err) {
                        let balance = -amount;
                        if (result.length > 0) {
                            balance += result[0]; //update account balance
                        }
                        mysql.insertRecord(process.env.DATABASE, "customer_account", {
                            customer_id: customer_id,
                            transaction_ref: txRef,
                            amount: -amount,
                            promo_code_used: promocode,
                            running_bal: balance,
                            status: 1,
                            credit_reason: 3
                        }, (result, err) => {
                            if (!err) {
                                resolve(StatusType.SUCCESS);
                            } else {
                                console.log(err);
                                resolve(StatusType.ERROR);
                            }
                        });
                    } else {
                        //failed to get last balance.
                        //cancel transaction
                        console.log('no access to last balance');
                        resolve(StatusType.ERROR);

                    }

                })
        })


    }

    hasSpentOver(customer_id, amount): Promise<boolean> {
        return new Promise<boolean>(resolve => {
            //check if its an initial transaction
            mysql.runQuery(`select sum(amount) as spent from ${process.env.DATABASE}.transactions where customer_id = '${customer_id}' and initial != 1 and status = 0 and provider1_resp!=''`, (row) => {
                return row.spent;
            }, (results, err) => {
                resolve(results[0] >= amount);
            })
        });

    }
    hasSpentOverInPeriod(customer_id,date,amount): Promise<boolean> {
        return new Promise<boolean>(resolve => {
            //check if its an initial transaction
            mysql.runQuery(`select sum(amount) as spent from ${process.env.DATABASE}.transactions where customer_id = '${customer_id}' and timestamp > '${date}' and initial != 1 and status = 0 and provider1_resp!=''`, (row) => {
                return row.spent;
            }, (results, err) => {
                resolve(results[0] >= amount);
            })
        });

    }

    isClearedVIP(customer_id): Promise<boolean> {
        return new Promise<boolean>(resolve => {
            //check if its an initial transaction
            mysql.runQuery(`select * from ${process.env.DATABASE}.customers where uid = '${customer_id}' and vip = 1`, (row) => {
                return row.email;
            }, (results, err) => {
                resolve(results.length > 0);
            })
        });

    }

    recentSignup(customer_id): Promise<boolean> {
        return new Promise<boolean>(resolve => {
            //check if its an initial transaction
            const today = dateformat(new Date(), "yyyy-mm-dd");
            mysql.runQuery(`select timestamp as signedUp from ${process.env.DATABASE}.customers where uid = '${customer_id}'  and timestamp > '${today}'`, (row) => {
                return row.signedUp;
            }, (results, err) => {
                resolve(results.length > 0);
            })
        });

    }

    hasToomanyAttempts(customer_id): Promise<boolean> {
        return new Promise<boolean>(resolve => {
            //check if its an initial transaction
            mysql.runQuery(`select count(id) as attempts from ${process.env.DATABASE}.transactions where customer_id = '${customer_id}' and initial = 1`, (row) => {
                return row.attempts;
            }, (results, err) => {
                let counted = 0;
                try {
                    counted = results.length > 0 ? results[0] : 0
                } catch (e) {

                }
                resolve(counted > 4);
            })
        });

    }

    updateCustomer(updatenames, updatevalues, callback) {
        mysql.updateRecord(`${process.env.DATABASE}`, "customers", updatevalues, updatenames, callback);

    }

    getLastBillPaid(biller, customer_id): Promise<object> {
        const sql = `select * from ${process.env.DATABASE}.transactions where status = 0 and customer_id = '${customer_id}'
         and initial = 0 and (receiver like '${biller}' or description like '${biller}') order by timestamp desc limit 1`;
        return new Promise<object>(resolve => {
            mysql.runQuery(sql, (row) => {
                return {
                    paid_on: dateformat(row.timestamp, "yyyy-mm-dd"),
                    amount: row.amount,
                    accno: row.accno,
                };
            }, (results, err) => {
                resolve(results.length > 0 ? results[0] : {paid_on: dateformat(new Date(), "yyyy-mm-dd")});
            })
        });
    }

    getBillerClients(biller): Promise<Array<string>> {
        const sql = `select * from ${process.env.DATABASE}.transactions where status = 0
         and initial = 0 and (receiver like '${biller}' or description like '${biller}') group by customer_id`;
        return new Promise<Array<string>>(resolve => {
            mysql.runQuery(sql, (row) => {
                return (row.customer_id);
            }, (results, err) => {
                resolve(results);
            })
        });
    }

    lastBillReminder(customer_id, biller): Promise<{lastSeen:string,repeat_after:number}> {
        const sql = `select * from ${process.env.DATABASE}.bill_notifications where customer_id  = '${customer_id}'
         and biller like '${biller}' order by sent_on desc limit 1`;
        return new Promise<{lastSeen:string,repeat_after:number}>(resolve => {
            mysql.runQuery(sql, (row) => {
                return {lastSeen:dateformat(row.sent_on, "yyyy-mm-dd"),repeat_after:row.repeat_after};
            }, (results, err) => {
                resolve(results.length > 0 ? results[0] : {lastSeen:"2020-01-01",repeat_after:0});
            })
        });
    }

    recordNotification(customer_id, biller, amount): Promise<boolean> {
        return new Promise<boolean>(resolve => {
            mysql.insertRecord(process.env.DATABASE, "bill_notifications", {
                customer_id: customer_id,
                amount: amount,
                biller: biller
            }, (result, err) => {
                console.log(err);
                resolve(!err);
            });
        });
    }
    getMerchantsWithFunds():Promise<Array<object>>{
        return new Promise<Array<object>>(
            resolve => {
                const sql = `select * from
                (select m.*,
                (select balance from ${process.env.DATABASE}.merchant_ledger
                 where merchant_id=m.id order by timestamp desc limit 1) as balance
                  from ${process.env.DATABASE}.merchants m) as n
                  where n.balance > 0 `
                mysql.runQuery(sql, (row) => {
                    return {
                        merchant_id: row.id,
                        balance: row.balance,
                        phone: row.phone,
                        business_name: row.business_name,
                        settlement_type: row.settlement_type,
                        floor_limit: row.floor_limit,
                        email: row.email
                    };
                }, (results, err) => {
                    resolve(results);
                })
            }

        )
    }
    getScheduledSettledMerchants(schedule:SETTLEMENT_SCHEDULE){
        return new Promise<Array<object>>(
            resolve => {
                const sql = `select * from
                (select m.*,
                (select balance from ${process.env.DATABASE}.merchant_ledger
                 where merchant_id=m.id order by timestamp desc limit 1) as balance
                  from ${process.env.DATABASE}.merchants m) as n
                  where n.balance > 0 and n.settlement_type = ${schedule}`
                mysql.runQuery(sql, (row) => {
                    return {
                        merchant_id: row.id,
                        balance: row.balance,
                        phone: row.phone,
                        business_name: row.business_name,
                        settlement_type: row.settlement_type,
                        floor_limit: row.floor_limit,
                        email: row.email
                    };
                }, (results, err) => {
                    resolve(results);
                })
            }

        )
    }
    getAllTransactionsSince(fromdate): Promise<object> {
        const sql = `select * from ${process.env.DATABASE}.transactions where status = 0
         and initial = 0 and timestamp > '${fromdate}' order by timestamp desc`;
        return new Promise<object>(resolve => {
            mysql.runQuery(sql, (row) => {
                return {
                    provider1_resp: JSON.parse(row.provider1_resp),
                    provider2_resp: JSON.parse(row.provider2_resp),
                    id: row.id,
                    customer_id: row.customer_id,
                    ref: row.ref,
                    status: row.status,
                    group: row.group
                };
            }, (results, err) => {
                resolve(results);
            })
        });

    }

    /**
     * Get a transaction using a vendor's reference
     * @param ref
     */
    getTransactionByProviderRef(ref): Promise<object>{
        const sql = `select t.*, (select name from ${process.env.DATABASE}.customers where uid = t.customer_id) as name from ${process.env.DATABASE}.transactions t where provider_ref = '${ref}' order by timestamp desc`;
        return new Promise<object>(resolve => {
            mysql.runQuery(sql, (row) => {
                return {
                    id: row.id,
                    customer_id: row.customer_id,
                    receiver: row.receiver,
                    name: row.name,
                    phone: row.phone,
                    chargeId: row.chargeId,
                    provider1: row.provider1,
                    amount: row.amount,
                    ref: row.ref,
                    status: row.status,
                    timestamp: dateformat(row.timestamp,"mmm dd HH:MM"),
                    fees: row.fees,
                };
            }, (results, err) => {
                resolve(results.length>0?results[0]:undefined);
            })
        });
    }
    getAllTransactionsFrom(fromdate): Promise<Array<object>> {
        const sql = `select t.*,(select r.name from ${process.env.DATABASE}.receivers_index r where trim(r.receiver) = trim(t.receiver)) as receivername
        from ${process.env.DATABASE}.transactions t where
           timestamp > '${fromdate}'  order by timestamp desc`;
        return new Promise<Array<object>>(resolve => {
            mysql.runQuery(sql, (row) => {
                const t= {
                    id: row.id,
                    status: row.status,
                    customer_id: row.customer_id,
                    ref: row.ref,
                    txRef: row.txRef,
                    group: row.group,
                    reference: row.ref,
                    receiver: row.receiver,
                    receivername: row.receivername,
                    description: row.description,
                    amount: row.amount,
                    fees: row.fees,
                    chargeId: row.chargeId,
                    accno: row.accno,
                    timestamp: dateformat(row.timestamp,"yyyy-mm-dd HH:MM:ss"),
                    dateString: dateformat(row.timestamp,"yyyy-mm-dd HH:MM:ss"),
                    payment_type: row.payment_type,
                    phone: row.phone,
                    email: row.email,
                    initial: row.initial,
                    source: row.source,
                    promo_amount: row.promo_amount,
                    provider_ref: row.provider_ref!=null?row.provider_ref:row.ref,
                };
                let provider1_resp = row.provider1_resp;
                try{
                    provider1_resp = JSON.parse(provider1_resp);
                    t["payment_method_details"] = provider1_resp["payment_method_details"];
                }catch (e) {

                }
                let provider2_resp = row.provider2_resp;
                try{
                    provider2_resp = JSON.parse(provider2_resp);
                    if(provider2_resp["receiver_data"]){
                        t["receiver_data"] = provider2_resp["receiver_data"];
                        if(t.group == Groups.bills ||
                            t.group == Groups.mpesa ||
                            (t.group == Groups.tills && t.receivername == null || t.receivername == 'null' || t.receivername==undefined))
                                t["receivername"] = provider2_resp["receiver_data"].name;
                    }
                    if((t.receivername == undefined || t.receivername == null) && provider2_resp.ResultParameters){
                        try{
                            const params = provider2_resp.ResultParameters.ResultParameter;
                            params.forEach((param)=>{
                                if(param["Key"] == "ReceiverPartyPublicName"){
                                    t.receivername = param["Value"];
                                    throw Error("found name");
                                }
                            })
                        }catch (e) {

                        }
                    }
                    if(provider2_resp["details"]){
                        t["details"] = provider2_resp["details"];
                        t["token"] = provider2_resp["details"].token;
                    }

                }catch (e) {

                }
                const name = t.receivername;
                try{
                    if(name!=null && name !=undefined && name.includes("-") && t.group == 'mpesa')
                        t.receivername  = (name.split("-")[1]).trim();
                }catch (e) {

                }

                return t;
            }, (results, err) => {
                resolve(results);
            })
        });

    }

    /**
     * Fetch a transaction using the Boya's given reference
     * @param ref
     */
    getTransactionsOfRef(ref): Promise<Array<object>> {
        const sql = `select t.*,(select r.name from ${process.env.DATABASE}.receivers_index r where trim(r.receiver) = trim(t.receiver)) as receivername
        from ${process.env.DATABASE}.transactions t where
           ref = '${ref}'  order by timestamp desc`;
        return new Promise<Array<object>>(resolve => {
            mysql.runQuery(sql, (row) => {
                const t= {
                    id: row.id,
                    status: row.status,
                    customer_id: row.customer_id,
                    ref: row.ref,
                    txRef: row.txRef,
                    group: row.group,
                    reference: row.ref,
                    receiver: row.receiver,
                    receivername: row.receivername,
                    description: row.description,
                    amount: row.amount,
                    fees: row.fees,
                    chargeId: row.chargeId,
                    accno: row.accno,
                    timestamp: dateformat(row.timestamp,"yyyy-mm-dd HH:MM:ss"),
                    dateString: dateformat(row.timestamp,"yyyy-mm-dd HH:MM:ss"),
                    payment_type: row.payment_type,
                    phone: row.phone,
                    email: row.email,
                    initial: row.initial,
                    source: row.source,
                    promo_amount: row.promo_amount,
                    provider_ref: row.provider_ref!=null?row.provider_ref:row.ref,
                };
                let provider1_resp = row.provider1_resp;
                try{
                    provider1_resp = JSON.parse(provider1_resp);
                    t["payment_method_details"] = provider1_resp["payment_method_details"];
                }catch (e) {

                }
                let provider2_resp = row.provider2_resp;
                try{
                    provider2_resp = JSON.parse(provider2_resp);
                    if(provider2_resp["receiver_data"]){
                        t["receiver_data"] = provider2_resp["receiver_data"];
                        if(t.group == Groups.bills ||
                            t.group == Groups.mpesa ||
                            (t.group == Groups.tills && t.receivername == null || t.receivername == 'null' || t.receivername==undefined))
                            t["receivername"] = provider2_resp["receiver_data"].name;
                    }
                    if((t.receivername == undefined || t.receivername == null) && provider2_resp.ResultParameters){
                        try{
                            const params = provider2_resp.ResultParameters.ResultParameter;
                            params.forEach((param)=>{
                                if(param["Key"] == "ReceiverPartyPublicName"){
                                    t.receivername = param["Value"];
                                    throw Error("found name");
                                }
                            })
                        }catch (e) {

                        }
                    }
                    if(provider2_resp["details"]){
                        t["details"] = provider2_resp["details"];
                        t["token"] = provider2_resp["details"].token;
                    }

                }catch (e) {

                }
                const name = t.receivername;
                try{
                    if(name!=null && name !=undefined && name.includes("-") && t.group == 'mpesa')
                        t.receivername  = (name.split("-")[1]).trim();
                }catch (e) {

                }

                return t;
            }, (results, err) => {
                resolve(results);
            })
        });

    }
}
