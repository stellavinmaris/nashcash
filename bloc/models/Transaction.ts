import {Card} from "../vgs/Card";

export class Transaction {
    amount:number;
    stripeAmount:number;
    wallet_charge: number;
    jamboAmount:number;
    fee:number;
    status:number;
    tries:number;
    phone:string;
    receiver:string;
    receivername:string;
    receiver_data:string;
    group:string;
    description:string;
    timestamp:string;
    refund_reason:string;
    customer_id:string;//uid
    stripe_cust_id:string;//stripe customer id
    chargeId:string;//stripe chargeId
    isPreauth:boolean;//stripe chargeId
    avoidPreauth:boolean;//stripe chargeId
    card_response:{};//stripe chargeId
    provider1:string;//stripe chargeId
    business_id:string;//stripe chargeId
    provider_response:{};//stripe chargeId
    merchant_id:string;
    merchant_obj:object;
    payment_type:string;
    ref:string;
    reference:string;
    txRef:string;
    currency:"KES";
    email:string;
    customer_name:string;
    loanBal:number;
    credit_lim:number;
    bank:{BANKNAME:String,BANKCODE:String};
    accno:string;
    token:string;
    deviceInfo:string;
    ticket:string;
    lat:number;
    long:number;
    ip:string;
    CommandID:string;
    ConversationID:string;
    checkoutId:string;
    initial:boolean;
    is_business:boolean;
    promocode:String;
    promo_amount:number;
    pending_charge:number;
    source:String;
    card:object;
    receipt_email:"info@boya.co";
    unit: String;
    quantity: number;
    validity: String;
    cardNumber: Number;
    expYear: String;
    expMonth: String;
    itemName: String;
    timeStamp: String;
    card_token: String;
    cvv: Number;
    payeeEmail: String;
    accountNumber: String;
    billerCode: String;
    wallet_accno: String;
    transaction_type: Number;
    hasWallet: Boolean;
    walletAmount: number;
    equityAmount: number;
    preauthMc:boolean;//Equity preauth
    groupid;
    categoryid;




}
