import {BoyaIssuerCodesMessage, IssuerCodes, StatusType} from "../utils/StatusType";

export interface Result {
    status:StatusType;
    response:object
}

export class TransactionResult implements Result{
    constructor(status:number,provider_response:Object) {
        this.status= status;
        this.provider_response= provider_response;
    }
    status:StatusType;
    response:object;
    provider_response:object

    static resolveIssuerResponse(respCode,data):TransactionResult{
        switch (respCode){
            case IssuerCodes.BANK_DECLINE:
                return new TransactionResult(StatusType.ERROR,{
                    errorMessage: BoyaIssuerCodesMessage.BANK_DECLINE,
                    error: BoyaIssuerCodesMessage.BANK_DECLINE,
                    data:JSON.stringify(data)});
            case IssuerCodes.INSUFFICIENT_FUNDS:
                return new TransactionResult(StatusType.ERROR,{
                    errorMessage: BoyaIssuerCodesMessage.INSUFFICIENT_FUNDS,
                    error: BoyaIssuerCodesMessage.INSUFFICIENT_FUNDS,
                    "data":JSON.stringify(data)});
            case IssuerCodes.FRAUD_DETECTED:
                return new TransactionResult(StatusType.ERROR,{
                    errorMessage: BoyaIssuerCodesMessage.FRAUD_DETECTED,
                    error: BoyaIssuerCodesMessage.FRAUD_DETECTED,
                    "data":JSON.stringify(data)});
            case IssuerCodes.AMOUNT_ERROR:
                return new TransactionResult(StatusType.ERROR,{
                    errorMessage: BoyaIssuerCodesMessage.AMOUNT_ERROR,
                    error: BoyaIssuerCodesMessage.AMOUNT_ERROR,
                    "data":JSON.stringify(data)});
            case IssuerCodes.APPROVAL:
                return new TransactionResult(StatusType.SUCCESS,{
                    errorMessage: BoyaIssuerCodesMessage.APPROVAL,
                    error: BoyaIssuerCodesMessage.APPROVAL,
                    "data":JSON.stringify(data)});
            case IssuerCodes.CARDNO_ERROR:
                return new TransactionResult(StatusType.ERROR,{
                    errorMessage: BoyaIssuerCodesMessage.CARDNO_ERROR,
                    error: BoyaIssuerCodesMessage.CARDNO_ERROR,
                    "data":JSON.stringify(data)});
            case IssuerCodes.SYSTEM_ERROR:
                return new TransactionResult(StatusType.ERROR,{
                    errorMessage: BoyaIssuerCodesMessage.SYSTEM_ERROR,
                    error: BoyaIssuerCodesMessage.SYSTEM_ERROR,
                    "data":JSON.stringify(data)});
            case IssuerCodes.STOLEN_CARD:
                return new TransactionResult(StatusType.ERROR,{
                    errorMessage: BoyaIssuerCodesMessage.STOLEN_CARD,
                    error: BoyaIssuerCodesMessage.STOLEN_CARD,
                    "data":JSON.stringify(data)});
            case IssuerCodes.EXPIRED_CARD:
                return new TransactionResult(StatusType.ERROR,{
                    errorMessage: BoyaIssuerCodesMessage.EXPIRED_CARD,
                    error: BoyaIssuerCodesMessage.EXPIRED_CARD,
                    "data":JSON.stringify(data)});
            case IssuerCodes.INCORRECT_CVV2:
                return new TransactionResult(StatusType.ERROR,{
                    errorMessage: BoyaIssuerCodesMessage.INCORRECT_CVV2,
                    error: BoyaIssuerCodesMessage.INCORRECT_CVV2,
                    "data":JSON.stringify(data)});
            case IssuerCodes.WITHDRAWAL_LIMIT:
                return new TransactionResult(StatusType.ERROR,{
                    errorMessage: BoyaIssuerCodesMessage.WITHDRAWAL_LIMIT,
                    error: BoyaIssuerCodesMessage.WITHDRAWAL_LIMIT,
                    "data":JSON.stringify(data)});
            case IssuerCodes.MERCHANT_NOT_FOUND:
                return new TransactionResult(StatusType.ERROR,{
                    errorMessage: BoyaIssuerCodesMessage.MERCHANT_NOT_FOUND,
                    error: BoyaIssuerCodesMessage.MERCHANT_NOT_FOUND,
                    "data":JSON.stringify(data)});
            case IssuerCodes.CALL_BANK:
                return new TransactionResult(StatusType.ERROR,{
                    errorMessage: BoyaIssuerCodesMessage.CALL_BANK,
                    error: BoyaIssuerCodesMessage.CALL_BANK,
                    "data":JSON.stringify(data)});
            case IssuerCodes.NO_SUCH_ISSUER:
                return new TransactionResult(StatusType.ERROR,{
                    errorMessage: BoyaIssuerCodesMessage.NO_SUCH_ISSUER,
                    error: BoyaIssuerCodesMessage.NO_SUCH_ISSUER,
                    "data":JSON.stringify(data)});
            case IssuerCodes.INVALID_CARD:
                return new TransactionResult(StatusType.ERROR,{
                    errorMessage: BoyaIssuerCodesMessage.INVALID_CARD,
                    error: BoyaIssuerCodesMessage.INVALID_CARD,
                    "data":JSON.stringify(data)});
            case IssuerCodes.TERMINAL_ID_ERROR:
                return new TransactionResult(StatusType.ERROR,{
                    errorMessage: BoyaIssuerCodesMessage.TERMINAL_ID_ERROR,
                    error: BoyaIssuerCodesMessage.TERMINAL_ID_ERROR,
                    "data":JSON.stringify(data)});
            case IssuerCodes.NO_ORIGINAL:
                return new TransactionResult(StatusType.ERROR,{
                    errorMessage: BoyaIssuerCodesMessage.NO_ORIGINAL,
                    error: BoyaIssuerCodesMessage.NO_ORIGINAL,
                    "data":JSON.stringify(data)});
        }
    }
}
export class PrepResult implements Result{
    constructor(status:number,resp:Object) {
        this.status= status;
        this.response= resp;
    }
    response: object;
    status: StatusType;
    message:string

}
