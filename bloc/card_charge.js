const Firestore = require('@google-cloud/firestore');
const stripe = require("stripe")(process.env.STRIPE_PK);
// const stripe = require("stripe")(process.env.STRIPE_TEST_PK);

var dateformat = require('dateformat');
const settings = {
    timestampsInSnapshots: true
};
const firestore = require('../firebase');
// async function chargeListner() {
//     const collection = firestore.collection('cardcharges').where("status", "==", 1);
//     var unsub = collection.onSnapshot(docsnap => {
//         console.log('card-charge next tick');
//         docsnap.docs.forEach(async (doc) => {
//             var charge = doc.data();
//             console.log('charge found',charge.txRef?charge.txRef:charge.ref);
//             await firestore.collection('cardcharges').doc(`${charge.txRef}`).update({"status":2});
//             if(charge.source === undefined){
//                 console.log('no source specified');
//                 const srcsnap = await firestore.collection('users')
//                     .doc(charge.uid).collection('sources').get();
//                 if(srcsnap.docs.length>0){
//                         var data = srcsnap.docs.pop().data();
//                         var mm = {source: data.id,source_status:data.status}
//                         charge = Object.assign(mm,charge);
//                 }
//
//             }
//             // try {
//             //     if(charge.group!==undefined && (charge.group === "mpesa" || charge.group === "rent")){
//             //         charge.fee = 45;
//             //     }else {
//             //         charge.fee = 0.0;
//             //     }
//             // }catch (e) {
//             //     charge.fee = 0.0;
//             // }
//
//             console.log(`source `,charge.source);
//             var bank = {};
//             var accno = "none";
//             try{
//                 bank = charge.bank;
//                 accno =  charge.accno;
//             }catch(e){
//                 console.error(e);
//             }
//             var receiver = "none";
//             var receiver_type = "none";
//             var receiver_data = "none";
//             var group = "other";
//             try{
//                 if(charge.receiver!==undefined)
//                 receiver = charge.receiver;
//                 if(charge.receiver_type!==undefined)
//                 receiver_type = charge.receiver_type;
//                 if(charge.receiver_data!==undefined)
//                 receiver_data = charge.receiver_data;
//                 group = charge.group;
//
//             }catch(e){
//
//             }
//             if(receiver==undefined){
//                 receiver = "none";
//                 receiver_type = "none";
//                 receiver_data = "none";
//                 group = "other";
//             }
//             var initial = true;
//             try{
//                 initial = charge.initial;
//             }catch(e){
//
//             }
//             if(initial){
//                 charge.receipt_email = "robert@boya.co";
//             }
//             // if(charge.group === "mpesa"){
//             //     var resp = {status:0,error:'Unable to complete M-PESA at the moment'};
//             //     await firestore.collection('cardcharges').doc(`${charge.txRef}`).update(resp);
//             //     await firestore.collection('transactions').doc(charge.txRef).create({errorMessage:'Sorry,We are unable to complete M-pesa transactions at the moment',status:0});
//             //     return;
//             // }
//
//             chargeCard(charge,async (err,response)=>{
//                 if(err){
//                     console.log(err);
//                     await firestore.collection('users').doc(`${charge.uid}`).collection(`charges`)
//                         .doc(`${charge.chargeId}`).update({error:err.message!==undefined?err.message:err});
//                     var resp = {status:0,error:err.message};
//                     await firestore.collection('cardcharges').doc(`${charge.txRef}`).update(resp);
//                     await firestore.collection('transactions').doc(charge.txRef).create({errorMessage:err.message,status:0});
//                 }else{
//                     console.log('card charged');
//                     var resp = {status:2,errorcapture:err};
//                     resp = Object.assign(resp,response);
//                     await firestore.collection('cardcharges').doc(`${charge.txRef}`).update(resp);
//                     await firestore.collection('users').doc(`${charge.uid}`).collection(`charges`)
//                         .doc(`${charge.chargeId}`).update(resp);
//                     var c2bOb = {
//                         uid: charge.uid,
//                         amount: charge.amount / 100,
//                         currency: charge.currency,
//                         dateString: charge.dateString,
//                         description: charge.description,
//                         receiver: charge.receiver,
//                         receiver_type: receiver_type,
//                         receiver_data: receiver_data,
//                         bank: bank,
//                         accno: accno,
//                         payment_type: "CARD",
//                         status: 1,
//                         ref: charge.txRef,
//                         phone: charge.phone,
//                         // fee: charge.fee,
//                         group: charge.group,
//                         card_response: response != undefined ? response : "none"
//                     };
//
//                     if (response.id !== undefined) {
//                         //a successful charge
//                         //create a c2b request
//                         c2bOb.status = 1;
//                         if (initial == true){
//                             c2bOb.status = 4;//refunding
//                             try{
//                                 await firestore.collection('users').doc(`${charge.uid}`).collection(`sources`)
//                                     .doc(`${resp.source.card.fingerprint}`).create(resp.source);
//                             }catch (e) {
//                                 console.log(e)
//                             }
//                             //try to attach source
//
//                         }
//                     } else {
//                         c2bOb.status = 0;//ignore
//                         c2bOb.errorMessage = response.error ? response.error : "error charging";//ignore
//                         //set a failed state
//                     }
//                     await firestore.collection('transactions').doc(charge.txRef).create(c2bOb);
//                     if (c2bOb.status == 1) {
//                         console.log('creating c2b doc')
//                         await firestore.collection('c2b').doc(charge.txRef).create(c2bOb);
//                     }
//                 }
//
//             });
//         });
//     });
//     setTimeout(()=>{
//         unsub();
//         console.log("remove tick stripe");
//         setTimeout(()=>{
//             chargeListner();
//         },1000)
//     },60000*30);
//
// }
async function createSource(uid,card_charge_resp) {
    try {
        await firestore.collection('users').doc(`${uid}`).collection(`sources`)
            .doc(`${card_charge_resp.card.fingerprint}`).create(card_charge_resp);
    } catch (e) {
        console.log('error adding source',e.toString())
    }
}

function charge(paymentObj, callback) {
    var amount = paymentObj.stripeAmount;
    stripe.charges.create({
        amount: amount,
        currency: paymentObj.currency,
        source: paymentObj.source, // obtained with Stripe.js
        customer: paymentObj.stripe_cust_id, // obtained with Stripe.js
        receipt_email: paymentObj.receipt_email,
        description: paymentObj.description
    }, {idempotency_key: paymentObj.txRef}, (err, charge) => {
        try {
            console.log(paymentObj.receipt_email)
        } catch (e) {

        }
        callback(err, charge);

    });
}

function attachSource(customer_id, source_id, callback) {
    stripe.customers.createSource(
        customer_id,
        {
            source: source_id,
        },
        function (err, source) {
            // asynchronously called
            callback(err, source);
        }
    );
}async function createStripeCustomer(email,callback) {
    stripe.customers.create(
        {
            email: email,
        },
        function (err, customer) {
            // asynchronously called
            callback(customer.id);
        }
    );

}

function isReusableSource(source_id, callback) {
    stripe.sources.retrieve(
        source_id,
        function (err, source) {
            // asynchronously called
            if (err) {
                console.log(err.message);
                callback(false, err)

            } else {
                callback(source.status === "chargeable", source)
            }
        }
    );
}

function refundCharge(chargeId, callback) {
    stripe.refunds.create({
        charge: chargeId
    }, function (err, refund) {
        // asynchronously called
        callback(err, refund);
    });
}

module.exports = {
    charge,
    verifyToken:isReusableSource,
    attachSource,
    createSource,
    createStripeCustomer,
    refundCharge
};
