import {Transaction} from "./models/Transaction";
import {TransactionManager} from "./interfaces/TransactionManager";
import {Result, TransactionResult} from "./models/TransactionResult";
import {ParticipantAdapter, Particpant} from "./participants/ParticipantAdapter";
import {TransactionParticipant} from "./interfaces/TransactionParticipant";
import {Groups, PSPs} from "./participants/Groups";
import {StatusType} from "./utils/StatusType";
// import {getCurrentTime, sendSlackAlert, sendSlackAlertMsg} from "./utils/commonfuncs";

const {DataStoreFactory} = require("./interfaces/TransactionParticipant");
const db = DataStoreFactory.getDatabase();
const common = require('./utils/common');

export class TransactionContext implements TransactionManager {
    transaction: Transaction;
    // types of wallet status
    // state_1 = no funds
    // state_2 = less funds
    //  state_3 = more funds
    wallet_status: String;

    constructor(transaction) {
        this.transaction = transaction;
    }

    private getChargeParticipant(): TransactionParticipant {
        if (this.transaction.payment_type == "MPESA")
        return ParticipantAdapter.getParticipant(Particpant.MPESAC2B, this.transaction);
    //TODO get the right the participant using transaction.provider1
    else return ParticipantAdapter.getParticipant(Particpant.FLUTTERWAVE, this.transaction);

    }



    randomRef() {
        const anysize = 10;//the size of string
        const charset = "abcdefghijklmnopqrstuvwxyz".toUpperCase(); //from where to create
        let result = "";
        for (let i = 0; i < anysize; i++)
            result += charset[Math.floor(Math.random() * charset.length)];
        console.log(result);
        return result;
    }


    private getNotificationParticipant(): TransactionParticipant {
        return ParticipantAdapter.getParticipant(Particpant.NOTIF, this.transaction);
    }

    private getRefundParticipant(): TransactionParticipant {
        return ParticipantAdapter.getParticipant(Particpant.REFUND, this.transaction);
    }

    private resolveBillingParticipant(): TransactionParticipant {
        
        return ParticipantAdapter.getParticipant(Particpant.MPESAB2B, this.transaction);

        // if (this.transaction.merchant_id !== undefined && this.transaction.merchant_obj["shortcode"] === this.transaction.receiver) {
        //     console.log('its a signed up merchant');
        //     return ParticipantAdapter.getParticipant(Particpant.MERCHANT_PAY, this.transaction);
        // }
        // if (
        //     this.transaction.receiver === "kplc_prepaid" ||
        //     this.transaction.receiver === "kplc_postpaid") {
        //     return ParticipantAdapter.getParticipant(Particpant.PDSL, this.transaction);
        // }
        // if (this.transaction.receiver === "kplc_prepaid" ||
        //     this.transaction.receiver === "kplc_postpaid" ||
        //     this.transaction.receiver === "GoTV" ||
        //     this.transaction.receiver === "GoTv" ||
        //     this.transaction.receiver === "DSTV") {
        //     return ParticipantAdapter.getParticipant(Particpant.IPAY, this.transaction);

        // } else return ParticipantAdapter.getParticipant(Particpant.MPESAB2B, this.transaction);
    }

    private resolveTillsParticipant(): TransactionParticipant {
        if (this.transaction.merchant_id !== undefined && this.transaction.merchant_obj["shortcode"] === this.transaction.receiver) {
            console.log('its a signed up merchant');
            return ParticipantAdapter.getParticipant(Particpant.MERCHANT_PAY, this.transaction);
        } else {
            return ParticipantAdapter.getParticipant(Particpant.MPESAB2B, this.transaction);
        }
    }

    private getBusinessParticipant(): Promise<TransactionParticipant> {
        return new Promise<TransactionParticipant>(resolve => {
            console.log('obj------', this.transaction.group)
            switch (this.transaction.group) {
                case Groups.mpesa:
                    resolve(ParticipantAdapter.getParticipant(Particpant.MPESAB2C, this.transaction));
                    break;
                case Groups.tills:
                    resolve(this.resolveTillsParticipant());
                    break;
                case Groups.bills:
                    resolve(this.resolveBillingParticipant());
                    break;
               
            }
        })


    }

    setGroup() {
        switch (this.transaction.txRef.substring(0, 1)) {
            case "M":
                this.transaction.group = Groups.mpesa;
                break;
            case "T":
                this.transaction.group = Groups.tills;
                break;
            case "B":
                this.transaction.group = Groups.bills;
                break;
        }
    }

    private setBillers() {
    
    }

    init() {
        this.setBillers();
        // this.transaction.timestamp = getCurrentTime();
    }

    refund(): Promise<Result> {
        const refundParticipant = this.getRefundParticipant();
        refundParticipant.process();
        return new Promise<Result>(resolve => {
            return resolve(new TransactionResult(StatusType.SUCCESS, {"errorMessage": "refunded"}));
        })
    }

    private async completeTransaction(business: TransactionParticipant) {
       
        //TODO send alerts
        const alerts = this.getNotificationParticipant();
        await alerts.process();
        // sendSlackAlert(this.transaction);
        console.log("alerts processed");

    }

    private initialOtpTransaction(): Promise<Result> {
        console.log('initial transaction');
        return new Promise<Result>(async (resolve) => {
            const customer = this.getChargeParticipant();
            const prep_result = await customer.prepare();
            if (prep_result.status === StatusType.ERROR) {
                //failed validation
                console.log(`[${customer.TAG}]>> prep failed`);
                this.transaction.status = StatusType.ERROR;
                return resolve(prep_result);
            }
            const transaction_result = await customer.process();
            // if(transaction_result.status == StatusType.SUCCESS)
            //     transaction_result = await customer.complete();
            resolve(transaction_result);
        });
    }

    transact(): Promise<Result> {
        if (this.transaction.initial) {
            return this.initialOtpTransaction();
        }
        return new Promise<Result>(async (resolve) => {
            this.init();
            if (this.transaction.receiver === "JTL") {
                this.transaction.receiver = "776611";
            }
            const obj = await db.getWalletByID(this.transaction.customer_id)

            if (obj) {
                this.transaction.hasWallet = true;
                this.transaction.walletAmount = obj.amount;
            }
            //use one card participant to attempt transaction in another PSPs if one PSP fails.
          const customer = await this.getChargeParticipant();
            // initialize wallet participant here
            const business = await this.getBusinessParticipant();
            console.log('call prepare')
            //call prep in parallel
            const prep_result = await Promise.all([customer.prepare(), business.prepare()]);
            console.log('prep-----000---', prep_result)

            if (prep_result[0].status === StatusType.ERROR) {
                //failed validation
                console.log(`[${customer.TAG}]>> prep failed`);
                this.transaction.status = StatusType.ERROR;
                return resolve(prep_result[0]);
            }
            if (prep_result[1].status === StatusType.ERROR) {
                //failed validation
                try {
                    console.log(`[${business.TAG}]>> prep failed`);
                    // sendSlackAlertMsg(`${this.transaction.customer_name} 's Transaction start failed ${business.TAG} , ${this.transaction.ref}, ${prep_result[1].response["errorMessage"]}`)
                } catch (e) {

                }

                this.transaction.status = StatusType.ERROR;
                return resolve(prep_result[1]);
            }
            console.log(`[${customer.TAG}]>> prep passed`);
            console.log(`[${business.TAG}]>> prep passed`);
            //call process in sequence. charge card/line then process transaction
            const transaction_stage1_result = await customer.process();

            if (transaction_stage1_result.status === StatusType.ERROR || transaction_stage1_result.status === StatusType.REFUND) {
                //failed to charge consumer
                console.log(`[${customer.TAG}]>> transaction failed`);
                try {
                    // sendSlackAlertMsg(`${this.transaction.customer_name} 's Card Charge failed, ${this.transaction.ref} , ${(transaction_stage1_result as TransactionResult).provider_response["errorMessage"]}`)
                } catch (e) {
                    console.error(e);
                }
                this.transaction.status = StatusType.ERROR;
                return resolve(transaction_stage1_result);
            }
            //charge successful, do business

            // let transaction_stage2_result =  await business.process() as (TransactionResult);
            const ref = this.transaction.ref;
            let transaction_stage2_result = undefined;
            // if(this.transaction.provider1 == PSPs.BOYA){
            //     //TODO remove this once live cards are issued or authorization logic is in place for boya cards
            //     transaction_stage2_result =
            //         new TransactionResult(StatusType.SUCCESS,{"status":"success"})
            // }else{
            transaction_stage2_result = await Promise.race([
                business.process(),
                new Promise(function (resolve) {
                    setTimeout(function () {
                        if (transaction_stage2_result == undefined) {
                            try {
                                // sendSlackAlertMsg(`${this.transaction.customer_name} 's ${business.TAG} Transaction  timed out after 1 min 30 sec, ref: ${ref}`)
                            } catch (e) {

                            }

                            resolve(new TransactionResult(StatusType.REFUND, {errorMessage: "Timeout getting response"}));
                        } else {
                            console.log('did not timeout');
                        }
                    }, 180000);
                }),
            ]) as (TransactionResult);
            this.transaction.provider_response = transaction_stage2_result.provider_response;
            if (transaction_stage2_result.status !== StatusType.SUCCESS) {
                //failed to transaction with third party
                //refund

                if (transaction_stage2_result.provider_response["errorMessage"] !== undefined) {
                    this.transaction.refund_reason = transaction_stage2_result.provider_response["errorMessage"]
                } else {
                    transaction_stage2_result.provider_response["errorMessage"] = "transaction failed.";
                    this.transaction.refund_reason = "transaction failed."
                }
                try {
                    // sendSlackAlertMsg(`${this.transaction.customer_name} 's ${business.TAG} Transaction  failed, ${this.transaction.ref} reason: ${transaction_stage2_result.provider_response["errorMessage"]}`)
                } catch (e) {

                }
                transaction_stage2_result.status = StatusType.REFUND;
                if("Timeout getting response" != this.transaction.refund_reason){

                    await customer.abort();
                }else{
                    // sendSlackAlertMsg(`Perform Manual refund if necessary ${this.transaction.ref}, on ${this.transaction.provider1}`)
                }
                console.log(`[${business.TAG}]>> transaction failed`);
                this.transaction.status = StatusType.REFUND;
                return resolve(transaction_stage2_result);
            }
            //a successful transactions is done.
            //call complete if settlement needed
            this.transaction.status = StatusType.SUCCESS;
            //here if the settlement fails, it will be queued for action later. no need to check result.
            if (!this.transaction.initial)
                await customer.complete();//capture transaction
            await this.completeTransaction(business);
            // debit the wallet
            // fetch current wallet and get credit and debit then update
            const walletObj = await db.getWalletByID(this.transaction.customer_id);


            // if(this.transaction.receiver == "wallet"){
            //     const walletObj = await db.getWalletByID(this.transaction.customer_id);
            //     let credit = walletObj.credit + this.transaction.amount
            //     let debit = walletObj.debit;
            //     let amount = credit - debit;
            //     await db.creditWallet(credit, debit, amount, this.transaction.customer_id)
            // }

            if (this.wallet_status == "state_2") {
                const TransactionAmount = this.transaction.amount + this.transaction.fee
                const credit = 0
                const debit = 0
                const amount = 0
                const customer_id = this.transaction.customer_id

                await db.debitWallet(credit, debit, amount, customer_id)
                await db.updateTransaction(this.transaction.ref, {
                    transaction_type: 2,
                    provider2: "WALLET",
                    status: 0,
                    wallet_charge: walletObj.credit
                }).then((saved) => {
                }).catch((failed) => {
                });
            }


            resolve(transaction_stage2_result);
            // }

        });
    }

    //will load a merchant onto the current transaction
    configureMerchant(merchant) {
        if (merchant) {
            this.transaction.merchant_id = merchant["id"];
            this.transaction.merchant_obj = merchant;
        }
    }
}
