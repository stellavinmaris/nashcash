import {TransactionResult} from "../models/TransactionResult";
import {StatusType} from "./StatusType";
import { sha256 } from 'js-sha256';

import smileIdentityCore = require("smile-identity-core");
import {Card} from "../vgs/Card";
import {Transaction} from "../models/Transaction";
// import {getCardToken} from "../vgs/stripe";
const ID_TYPES = ["NATIONAL_ID","PASSPORT","ALIEN_CARD"];
export enum DOC_TYPE {
    NATIONAL_ID,
    PASSPORT,
    ALIEN_CARD
}
const AfricasTalking = require('africastalking')({
    "apiKey": process.env.AFRICAS_API_KEY,
    "username": process.env.AFRICAS_USERNAME,
    "sender_id": "Boya"
});
const mpesab2c =  require(`../../mpesa/b2b`);
const firestore = require("../../firebase");
const dateformat = require("dateformat");
// Initialize a service e.g. SMS
const sms = AfricasTalking.SMS;
const airtime = AfricasTalking.AIRTIME;


export function validatePhone(inputPhone) {
    const phoneRe = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;
    const digits = inputPhone.replace(/\D/g, "");
    return phoneRe.test(digits);
}
export function formatAirtimePhone(phone) {
    const countrycode = "+254";
    let phone2 = phone.trim().replace(/\(/,'').replace(/\)/,'').split(' ').join('').match(/\d+/)[0];
    if (phone.toString().substring(0, 1) === "+") {
        phone2 = phone;
    }else if(phone.toString().substring(0,2) == "00"){
        phone2 = countrycode+""+phone.toString().substring(2)
    }else if(phone.toString().substring(0,1) == "0"){
        phone2 = countrycode+""+phone.toString().substring(1)
    }else if(phone.length==12){
        phone2 = "+"+phone;
    }
    return phone2;
}

export function parseReceiver(tillno) {
    const phone2 = tillno.trim().replace(/\(/,'').replace(/\)/,'').split(' ').join('').match(/\d+/)[0];
    return phone2;
}

export function formatMpesaPhone(inphone) {
    const phone = inphone.trim().replace(/\(/,'').replace(/\)/,'');
    const countrycode = "254";
    let phone2 = phone.match(/\d+/)[0];
    if (phone.toString().trim().substring(0, 1) === "+") {
        phone2 = phone.toString().substring(1);
    } else if (phone.toString().substring(0, 2) == "00") {
        phone2 = countrycode + "" + phone.toString().substring(2)
    } else if (phone.toString().substring(0, 1) == "0") {
        phone2 = countrycode + "" + phone.toString().substring(1)
    } else if (phone.length == 12) {
    }
    return phone2.trim();
}

export function generateRandomPin(){
    const gen =  new Date().getSeconds();
    const gen2 =  `${Date.now()}`.substring(8,11);
    return (gen+gen2).substring(0,4);

}

export function generateRandomPin6(){
    const gen =  new Date().getSeconds();
    const gen2 =  `${Date.now()}`.substring(8,12);
    let pin = (gen+""+gen2).substring(0,6);
    if(pin.length<6){
        pin = `${pin}0`
    }
    return pin;

}
export function generateRandomPin8(){
    const gen =  new Date().getSeconds();
    const gen2 =  `${Date.now()}`.substring(5,12);
    let pin = (gen+""+gen2).substring(0,8);
    if(pin.length<6){
        pin = `${pin}0`
    }
    return pin;

}
export function getCurrentTime(){
    return dateformat(new Date(),'yyyy-mm-dd HH:MM:ss');
}

export async function dumpTransactionResultToFirestore(customer_id,ref,transactionresult){
    console.log('dumping...');
    const keys = Object.keys(transactionresult);
    const result = {};
    keys.forEach((key)=>{
        if(transactionresult[key] != undefined && transactionresult[key] !=null){
            result[key] = transactionresult[key];
        }
    })
    await firestore.collection('users').doc(customer_id)
        .collection("transactions").doc(ref).set(result)
}
export async function updateTransactionResultToFirestore(customer_id,ref,transactionresult){
    console.log('dumping...');
    const keys = Object.keys(transactionresult);
    const result = {};
    keys.forEach((key)=>{
        if(transactionresult[key] != undefined && transactionresult[key] !=null){
            result[key] = transactionresult[key];
        }
    })

     firestore.collection('users').doc(customer_id)
        .collection("transactions").doc(ref).update(result).then((resp)=>{}).catch((e)=>{
            console.log(e);
         firestore.collection('users').doc(customer_id)
             .collection("transactions").doc(ref).set(result);
        });
}
export async function removePendingPayment(amount,accno,uid){
    try{
        let unsub;
        const collection =  firestore.collection("users").doc(uid)
            .collection("pending_payments")
            // .where("amount", "==", amount)
            .where("accno", "==", accno)

        unsub = collection.onSnapshot(docsnap => {
            docsnap.docs.forEach(async (doc) => {
                await firestore.collection("users").doc(uid)
                    .collection("pending_payments").doc(doc.id).delete();
                console.log("removed pending_payment");
            })
        })
        setTimeout(()=>{
            unsub();
        },1500)
    }catch (e) {
       console.log(e);
    }
}
export async function updateTransactionStatus(uid, ref, status) {
    await firestore.collection('users').doc(uid)
        .collection("transactions").doc(ref).update({"status":parseInt(status)})
    console.log('updated status',ref)
}
export async function dumpUserToFirestore(user){
    await firestore.collection('users').doc(user.uid)
        .set(user)
}
export async function updateUserLegalInfo(uid,legalData){
    await firestore.collection('users').doc(uid).collection("legal").doc(uid).set(legalData);
}
export async function forceUserKyc(uid){
    // await firestore.collection('users').doc(uid).update({"forceKyc":true,"confidenceValue":2});
}
export async function flagAsFraud(uid){
    await firestore.collection('users').doc(uid).update({"fraudulent":true});
}
export async function updateUserSelfieInfo(uid,legalData){
    await firestore.collection('users').doc(uid).collection("selfie").doc(uid).set(legalData);
}
export async function updateUserInfo(uid,legalData){
    await firestore.collection('users').doc(uid).update(legalData);
}
export async function removePendingAmountState(uid,sourceId){
    await firestore.collection('users').doc(uid).collection("cardotp").doc(sourceId).delete();
}
export async function createPendingAmountState(transaction:Transaction,chargeId){

    if(transaction.card!=null){


        try{
            await firestore.collection('users').doc(transaction.customer_id).collection(`vgscards`)
                .doc(transaction.source).create({id:transaction.source});
        }catch (e) {
            console.error(e)
        }
        try{
            await firestore.collection('users').doc(transaction.customer_id).collection(`vgscards`)
                .doc(transaction.source).update(transaction.card);
        }catch (e) {
            console.error(e)
        }try{
            await firestore.collection('users').doc(transaction.customer_id).collection("cardotp").doc(transaction.source).set({
                amount:transaction.amount,
                chargeId:chargeId,
                source:transaction.source,
                ref:transaction.ref,
            });
        }catch (e) {
            console.error(e)
        }

    }else{
        await firestore.collection('users').doc(transaction.customer_id)
            .collection("cardotp").doc(transaction.source).set({
            amount:transaction.amount,
            chargeId:chargeId,
            source:transaction.card_response["source"],
            ref:transaction.ref,
        });
        await firestore.collection('users').doc(transaction.customer_id)
            .collection("sources").doc(transaction.card_response["source"]["card"]["fingerprint"]).set(transaction.card_response["source"]);
    }

}
export async function createVerifiedCard(uid,card){
    try{
        card.verified = true;
        card.id = card.number;
        await firestore.collection('users').doc(uid).collection(`vgscards`)
            .doc(card.number).set(card);
    }catch (e) {
        console.error(e)
    }
}
export async function createPendingCardVerification(uid,card:Card){
    await firestore.collection('users').doc(uid).collection("vgscard").doc("unverified").set(card);
}
// export async function getCardName(card:Card):Promise<object>{
//     const token = await getCardToken(card);
//    return new Promise(( resolve => {
//        resolve(token);
//    }))

// }
export async function sendFundConsumerRequest(amount,phone,ref,count=1):Promise<TransactionResult>{
    return new Promise<TransactionResult>(resolve => {
        mpesab2c.sendMoneyToPhone(formatMpesaPhone(phone), amount, ref,
            `${process.env.HOST}/b2cCallback`,`${process.env.HOST}/b2cTimeout`, (error,body)=> {

                if(!error && body.error===undefined && body.errorMessage===undefined){
                    const resp = body;
                    if (parseInt(resp.ResponseCode) != 0) {
                        console.log(JSON.stringify(resp));
                        resolve(new TransactionResult(StatusType.ERROR,{errorMessage:resp.ResponseDescription}));
                    } else {
                        console.log('mpesa send money request accepted',resp);
                        resolve(new TransactionResult(StatusType.SUCCESS,resp))

                    }
                } else {
                    console.error('b2c mpesa error ',error);
                    if(body.errorMessage == "ETIMEDOUT"){
                        if(count<4)
                        return sendFundConsumerRequest(amount,phone,ref,count+1)
                        else return resolve(new TransactionResult(StatusType.ERROR,{"errorMessage":"Sorry, we could not complete this payment to Mpesa due to service disconnect. Please try again after some time"}))
                    }
                    if(body.errorMessage.includes('Invalid PartyB')){
                        resolve(new TransactionResult(StatusType.ERROR,{errorMessage:'Sorry, we could not complete this payment. Please check the phone number'}));
                    }
                    if(error){
                        resolve(new TransactionResult(StatusType.ERROR,{"errorMessage":error}))
                    } else{
                        resolve(new TransactionResult(StatusType.ERROR,{errorMessage:"We are sorry, we could not complete this payment.",error:body}));
                    }

                }
            });
    })

}export async function sendFundstoBusiness(paymentObj):Promise<TransactionResult>{
    return new Promise<TransactionResult>(resolve => {
        mpesab2c.sendMoneyToTill(paymentObj, (error, body)=> {

                if(!error && body.error===undefined && body.errorMessage===undefined){
                    const resp = body;
                    if (parseInt(resp.ResponseCode) != 0) {
                        console.log(JSON.stringify(resp));
                        resolve(new TransactionResult(StatusType.ERROR,{errorMessage:resp.ResponseDescription}));
                    } else {
                        console.log('mpesa b2b request accepted',resp);
                        resolve(new TransactionResult(StatusType.SUCCESS,resp))

                    }
                } else {
                    console.error('b2b mpesa error ',error);
                    if(body.errorMessage)
                        resolve(new TransactionResult(StatusType.ERROR,body));
                    else resolve(new TransactionResult(StatusType.ERROR,{"errorMessage":error}));
                }
            });
    })

}
// firestore.collection("users").where("email", "==", "snderitu53@gmail.com").onSnapshot(docsnap => {
//     docsnap.docs.forEach(async (doc) => {
//         let c2bObj = doc.data();
//         c2bObj.status = 1;
//         if(c2bObj.uid !==undefined){
//             console.log(` uid ${c2bObj.uid} and name ${c2bObj.name}`);
//             firestore.collection("users").doc(c2bObj.uid).collection("sources").delete();
//         }
//     })
// });
export function sendAirtime(amount,phone,callback){
        const mobileno = formatAirtimePhone(phone);
    const request = require('request');
    const rcvs = [{"phoneNumber":mobileno,"amount": `KES ${amount}`}];
    const options = {
        'method': 'POST',
        'url': 'https://api.africastalking.com/version1/airtime/send',
        'headers': {
            'Content-Type': 'application/x-www-form-urlencoded',
            'apiKey': process.env.AFRICAS_API_KEY,
            'Idempotency-Key': `req-${generateRandomPin()}-${getCurrentTime()}`,
            'Accept': 'application/json'
        },
        form: {
            'username': process.env.AFRICAS_USERNAME,
            'senderId': process.env.AFRICAS_SENDER_ID,
            'sender_id': process.env.AFRICAS_SENDER_ID,
            'recipients': JSON.stringify(rcvs)
        }
    };
    request(options, function (error, response) {
        if (error) {
            console.error('error purchasing airtime',error);
            callback(new TransactionResult(StatusType.ERROR, {errorMessage:"error purchasing airtime",error:error.toString()}));
        }
        let body = response.body;
        try {
            body = JSON.parse(body);
        }catch (e) {

        }
        if (body.numSent > 0) {
            console.log(body.numSent);
            callback(new TransactionResult(StatusType.SUCCESS, body));
        } else {
            console.log(JSON.stringify(body));
            callback(new TransactionResult(StatusType.ERROR, {"errorMessage":"error sending airtime"}))
        }
    });

}
export function checkMpesaTransactionStatus(){
 mpesab2c.checkStatus((error,resp)=>{
     console.log(error);
     console.log(resp);
 })
}


export function processCard(Amount,CardNumber,ExpYear,ExpMonth,Cvv,PayeeEmail,TimeStamp,callback){
    const request = require('request');

    const options = {
        'method': 'POST',
        'url': 'https://www.jambopay.com/eCheckout/api/Checkout/CreateCheckout',
        'headers': {
            'Content-Type': 'application/json',
            'AppKey': process.env.JAMBOPAY_CARD_API_KEY,
            'Checksum': sha256(process.env.JAMBOPAY_CARD_API_KEY + Amount.toString() + PayeeEmail + TimeStamp),
            'Accept': 'application/json'
        },
        json: {
                "Currency": "KES",
                "BusinessEmail": "demo@webtribe.co.ke",
                "PayeeEmail": PayeeEmail,
                "Amount": Amount,
                "OrderNumber": "723",
                "CardNumber": CardNumber,
                "ExpYear": ExpYear,
                "ExpMonth": ExpMonth,
                "ItemName": "Test",
                "TimeStamp": TimeStamp,
                "AmountUSD":0,
                "Cvv": Cvv
            }
    };
    request(options, function (error, response) {
        if (error) {
            console.error('error processing card ',error);
            callback(new TransactionResult(StatusType.ERROR, {errorMessage:"error processing card",error:error.toString()}));
        }
        let body = response.body;
        try {
            body = JSON.parse(body);
        }catch (e) {

        }
        if (body.Message == "Transaction completed successfully") {
            console.log(body.Message);
            callback(new TransactionResult(StatusType.SUCCESS, body));
        } else {
            console.log(JSON.stringify(body));
            callback(new TransactionResult(StatusType.ERROR, {"errorMessage":"error processing card"}))
        }
    });

}

// processCard(100,"4242424242424242","2023",'09','123','john@gmail.com',"1594115002",(response)=>{console.log(response)});


export function sendDataBundles(units,validity,quantity,phone,callback){
    const mobileno = formatAirtimePhone(phone);
    const request = require('request');
    const rcvs = [{
                    phoneNumber:mobileno,
                    quantity:quantity,
                    unit:units,
                    validity: validity,
                    metadata:{}
                }];

    const options = {
        'method': 'POST',
        'url': 'https://payments.africastalking.com/mobile/data/request',
        'headers': {
            'Content-Type': 'application/json',
            'apiKey': process.env.AFRICAS_API_KEY,
            'Idempotency-Key': `req-${generateRandomPin()}-${getCurrentTime()}`,
            'Accept': 'application/json'
        },
        json: {
            'username': process.env.AFRICAS_USERNAME,
            'apiKey': process.env.AFRICAS_API_KEY,
            'productName': process.env.AFRICAS_SENDER_ID,
            'recipients': rcvs
        }
    };
    request(options, function (error, response) {
        if (error) {
            console.error('error purchasing data bundles',error);
            callback(new TransactionResult(StatusType.ERROR, {errorMessage:"error purchasing bundles",error:error.toString()}));
        }
        let body = response.body;
        try {
            body = JSON.parse(body);
        }catch (e) {

        }
        if (body.numSent > 0) {
            console.log(body.numSent);
            callback(new TransactionResult(StatusType.SUCCESS, body));
        } else {
            console.log(JSON.stringify(body));
            callback(new TransactionResult(StatusType.ERROR, {"errorMessage":"error sending bundles"}))
        }
    });

}

// sendDataBundles('MB','Daily',50,'254718555832',(response)=>{console.log(response)});

export function sendSms(message,phone,callback) {
    const recv = formatAirtimePhone(phone);
    console.log('sms receiver ',recv);
    const request = require('request');
    const options = {
        'method': 'POST',
        'url': 'https://api.africastalking.com/version1/messaging',
        'headers': {
            'Content-Type': 'application/x-www-form-urlencoded',
            'apiKey': process.env.AFRICAS_API_KEY,
            'Accept': 'application/json'
        },
        form: {
            'username': 'boya',
            'to': recv,
            'from': 'BOYA',
            'message': message
        }
    };
    request(options, function (error, response) {
        if(error){
            callback({status:0,response:'invalid content',message:error},true)
        }else{
            let body = response.body;
            try {
                body= JSON.parse(body);
            }catch (e) {

            }
            try{
                if(body.SMSMessageData.Recipients.length>0){
                    const statusCode = body.SMSMessageData.Recipients[0].statusCode;
                    callback(body,statusCode>102)
                }else{
                    callback(body,true)
                }
            }catch (e){
                console.error(e)
                callback(body,true)
            }

        }
    });
}
export async function updateFAuthUser(admin, uid,update){
    admin.auth().updateUser(uid, update)
        .then(function(userRecord) {
            // See the UserRecord reference doc for the contents of userRecord.
            console.log('Successfully updated user', userRecord.toJSON());
        })
        .catch(function(error) {
            console.log('Error updating user:', error);
        });
}
export async function getUserToken(uid):Promise<string> {
    const doc = await firestore.collection("users").doc(uid).get()
    return new Promise<string>(resolve => {
        const user = doc.data();
        if(user.token){
            return resolve(user.token)
        }
        return resolve(null);
    });

}
export async function getUserName(uid):Promise<string> {
    const doc = await firestore.collection("users").doc(uid).get()
    return new Promise<string>(resolve => {
        const user = doc.data();
        return resolve(user.name);
    });

}
export function elecPostPaidMsg(customerName,billAmount,accno):{body,title,accno,biller,amount,ref}{
    return  {"body":`${customerName}, your KPLC bill is Ksh. ${billAmount}. Click to pay`,
        "biller":"kplc_postpaid",
        "accno":accno,
        "ref": `B-${generateRandomPin8()}`,
        "amount":billAmount,
        "title":"Your KPLC bill is ready."}
}
export function prePaidMsg(customerName,accno):{body,title,accno,biller,amount,ref}{
    return  {"body":`${customerName}, you can buy KPLC tokens today. Click to buy`,
        "biller":"kplc_prepaid",
        "accno":accno,
        "ref": `B-${accno}${generateRandomPin6()}`,
        "amount":200,
        "title":"Buy KPLC tokens easily with Boya"}
}
export function zukuBillMsg(customerName,accno,billAmount):{body,title,accno,biller,amount,ref}{
    return  {"body":`${customerName}, your ZUKU bill is Ksh ${billAmount}. Click to pay`,
        "biller":"ZUKU",
        "accno":accno,
        "amount":billAmount,
        "ref": `B-${billAmount}${generateRandomPin6()}`,
        "title":"Your ZUKU bill is ready."}
}
export function goTvBillMsg(customerName,accno,billAmount):{body,title,accno,biller,amount,ref}{
    return  {"body":`${customerName}, your GoTv bill is Ksh ${billAmount}. Click to pay`,
        "biller":"GoTv",
        "accno":accno,
        "ref": `B-${billAmount}${generateRandomPin6()}`,
        "amount":billAmount,
        "title":"Your GoTv bill is ready."}
}
export function goDstvBillMsg(customerName,accno,billAmount):{body,title,accno,biller,amount,ref}{
    return  {"body":`${customerName}, your DSTV bill is Ksh ${billAmount}. Click to pay`,
        "biller":"DSTV",
        "accno":accno,
        "ref": `B-${billAmount}${generateRandomPin6()}`,
        "amount":billAmount,
        "title":"Your DSTV bill is ready."}
}
export function goHomeFibreBillMsg(customerName,accno,billAmount):{body,title,accno,biller,amount,ref}{
    return  {"body":`${customerName}, your Safaricom Home Fibre bill is Ksh ${billAmount}. Click to pay`,
        "biller":"Safaricom Home Fibre",
        "accno":accno,
        "ref": `B-${billAmount}${generateRandomPin6()}`,
        "amount":billAmount,
        "title":"Your Home Fibre bill is ready."}
}
function hashValues(jsonOb){
    const crypto = require('crypto')
    let stringbuffer = "";
    const keys = [];
    for (const key in jsonOb){
        keys.push(key);
    }
    keys.sort();
    keys.forEach((key)=>{
        stringbuffer += `&${key}=${jsonOb[key]}`
    });
    stringbuffer = stringbuffer.substring(1);
    return crypto.createHmac('sha256', process.env.IPAY_KEYHASH).update(stringbuffer).digest('hex')
}
export function getKplcBill(biller_name,accountno):Promise<number> {
    const request = require('request');
    return new Promise<number>(resolve => {
        const data = {
            account:accountno,
            account_type:biller_name,
            vid:process.env.IPAY_VENDORID,

        };
        const hash = hashValues(data);
        data["hash"] = hash;
        console.log(data);
        request({
                method: 'POST',
                url: `https://apis.ipayafrica.com/ipay-billing/billing/validate/account`,
                headers: {
                    Accept: '*/*',
                    'Content-Type': 'application/json'
                },
                body: data,
                json:true
            },
            function (err, httpResponse, resp) {
                if (err) {
                    console.log(err);
                    return resolve(0)
                }
                let bill = resp;
                try{
                    bill = JSON.parse(resp);
                }catch (e) {

                }
                console.log(bill)
                if(200 === parseInt(bill.header_status) && 1 === parseInt(bill.status)){
                    // {
                    //     "account": "44XXXX718-01",
                    //     "name": "JOHN DOE 44XXXX718-01",
                    //     "amountdue": "237.62"
                    // }
                    return resolve(bill.account_data.amountdue);
                }else{
                    return resolve(0);
                }


            });
    });


}
export function goSafPostPayBillMsg(customerName,accno,billAmount):{body,title,accno,biller,amount,ref}{
    return  {"body":`${customerName}, your Safaricom Postpay bill is Ksh ${billAmount}. Click to pay`,
        "biller":"Safaricom Postpay ",
        "accno":accno,
        "amount":billAmount,
        "ref": `B-${billAmount}${generateRandomPin6()}`,
        "title":"Your Safaricom Postpay bill is ready."}
}
export async function sendBillPushNotification(uid:string, token:string, msg: { body:string, title:string, accno:string, biller:string, amount, ref:string,organization }) {
    await sendPushNotification(uid,token,msg)
    await firestore.collection('users').doc(uid)
        .collection("pending_payments").doc(`${msg.ref}`).set(msg)
}
export async function sendPushNotification(uid:string, token:string, msg: { body:string, title:string, accno:string, biller:string, amount, ref:string,organization:string }) {
    const request = require('request');
    msg["click_action"] = "FLUTTER_NOTIFICATION_CLICK";
    msg["id"] = "1";
    msg["message"] = msg.body;
    msg["status"] = "done";
    const options = {
        'method': 'POST',
        'url': 'https://fcm.googleapis.com/fcm/send',
        'headers': {
            'Authorization': process.env["FIRESTORE_SERVER_TOKEN"],
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(
            {
                "notification": {"body": msg.body, "title": msg.title},
                "data": msg,
                "priority": "high", "to": token
            })

    };
    request(options, function (error, response) {
        if (error) throw new Error(error);
        console.log('sent push notification?');
        console.log('sent push notification?', response.body);
    });
}
function decodeStatus(status) {
    switch (status) {
        case StatusType.SUCCESS:
            return "Successful";
        case StatusType.REFUND:
            return "Reversed";
        default:
            return "Cancelled";
    }
}
export async function verifyNationalId(jobId:string,uid:string,nationalID: string,type:DOC_TYPE):Promise<object> {
    return new Promise( resolve => {
        const IDApi = smileIdentityCore.IDApi;
        const connection = new IDApi(process.env.SMILE_PARTER_ID, process.env.SMILE_API_KEY, 1);
        const partner_params = {
            job_id: `${jobId}`,
            "user_id":`${uid}`,
            "job_type":5
        };
        const id_info = {
            first_name: "",
            last_name: "",
            country: "KE",
            id_type: ID_TYPES[type],
            id_number: nationalID,
            phone_number: "",

        };
        connection.submit_job(partner_params, id_info).then((result)=>resolve(result)).catch((err)=>{
            console.log('catch verify id error',err.message);
            resolve({error:err.message});
        });

    });


}
export async function verifyNationalIdPhotoAndSelfie(jobId:string,
                                                     uid:string,
                                                     nationalIDBase64: string,
                                                     selfieBase64:string, national_id:string,type:DOC_TYPE):Promise<object> {
    return new Promise( resolve => {
        const smileIdentityCore = require("smile-identity-core");
        const WebApi = smileIdentityCore.WebApi;
        const connection = new WebApi(process.env.SMILE_PARTER_ID,`${process.env.HOST}/smileBack`, process.env.SMILE_API_KEY, 1);
        const partner_params = {
            job_id: `${jobId}`,
            user_id:`${uid}`,
            job_type:1
        };
        const id_info = {
            first_name: '',
            last_name: '',
            country: "KE",
            id_type: type,
            id_number: national_id,
            entered: "true"
        };
        const image_details =  [
            {
                image_type_id: 2,
                image: selfieBase64
            },
            { // Not required if suppling ID number info or job type 4 - registration only
                image_type_id: 3,
                image: nationalIDBase64
            }
        ];
        connection.submit_job(partner_params, image_details,id_info,{
            return_job_status: true,
            return_history: false,
            return_image_links: false
        }).then((result)=>resolve(result["result"])).catch((err)=>{
            // console.log(err);
            console.log('catch verify id error',err.message);
            resolve({error:err.message});
        });

    });


}
export async function sendAjuaInfo(tillno,score,description,phone){
    // const JoinCode = 'BOYA1';

    // const url = `https://api.msurvey.co.ke/v1/forms/questions?surveyJoincode=${JoinCode}`;
    // const url2 = "https://api.ajua.com/v1/forms/response";
    const url2 = "https://api.msurvey.co.ke/v1/forms/response";
    const request = require('request');
    const options = {
        'method': 'POST',
        'url': url2,
        'headers': {
            'Content-Type': ['application/json', 'application/json'],
            'Authorization': 'Basic ' + new Buffer(  'boya:wovne3-bofmic-Bynzam').toString('base64')
        },
        body: JSON.stringify({"commId":phone,
            "metadata":{"Till":tillno,"Location":tillno},
            "responseList":[{"questionId":85065,"responseText":score},
                {"questionId":85064,"responseText":description}],
            "surveyJoincode":"BOYA1"})

    };
    request(options, function (error, response) {
        if (error) throw new Error(error);
        console.log(response.body);
    });
}
export async function getBase64image(url):Promise<string>{
    return new Promise<string>( resolve => {
        const request = require('request').defaults({ encoding: null });
        request.get(url, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                const data = new Buffer(body).toString('base64');
                // console.log("base64",data);
                resolve(data);
            }
        });
    });

}
export async function compareSelfie(uid,jobId,idNo: string, base64Photo: string,type:DOC_TYPE):Promise<object> {
    return new Promise( resolve => {
        const smileIdentityCore = require("smile-identity-core");
        const WebApi = smileIdentityCore.WebApi;
        const partner_params = {
            job_id: jobId,
            "user_id":uid,
            "job_type":1
        };
        const connection = new WebApi(process.env.SMILE_PARTER_ID,`${process.env.HOST}/smileBack`, process.env.SMILE_API_KEY, 1);
        const image_details =  [
            {
                image_type_id: 2,
                image: base64Photo
            },
            // { // Not required if suppling ID number info or job type 4 - registration only
            //     image_type_id: <1 | 3>,
            //     mage: '<path to ID card image or base64 image string>'
            // }
        ];
        // Create ID number info
        const id_info = {
            first_name: '',
            last_name: '',
            country: "KE",
            id_type: type,
            id_number: idNo,
            entered: "true"
        };
        // Create options
        const options = {
            return_job_status: false,
            return_history: true,
            return_image_links: false
        };
        connection.submit_job(partner_params, image_details, id_info, options);
        setTimeout(async ()=>{
            const resp = await checkSelfieJobStatus(jobId,uid)
            resolve(resp["result"]);
        },16000)

        //     .then((response)=>{
        //         console.log("smile response",response);
        //         resolve(response);
        //     }).catch((err)=>{
        //     console.log('selfie match error ',err.message);
        //     resolve({error:err.message});
        // });
    });


}
export async function getMerchantReviews(till): Promise<object>{
    return new Promise(resolve => {
        const unsub = firestore.collection("feedback").doc(till).collection("good").onSnapshot(docsnap => {
            const snaps = [];
            docsnap.docs.forEach(async (doc) => {
                const feedback = doc.data();
                snaps.push({
                    "date":dateformat(feedback.date,"yyyy-mm-dd HH:MM"),
                    "reasons":feedback.reasons,
                    "uid":feedback.uid,
                    "till":till,
                    "score":10,
                })
            })
            unsub();
            const unsub2 = firestore.collection("feedback").doc(till).collection("bad").onSnapshot(docsnap => {
                docsnap.docs.forEach(async (doc) => {
                    const feedback = doc.data();
                    snaps.push({
                        "date":dateformat(feedback.date,"yyyy-mm-dd HH:MM"),
                        "reasons":feedback.reasons,
                        "till":till,
                        "uid":feedback.uid,
                        "score":2,
                    })
                })
                unsub2();
                resolve(snaps);
            });
        });
    });
}
export async function addToAlgolia(transaction) {
    const algoliasearch = require('algoliasearch');
    const client = algoliasearch('2NBHF1WPXL', 'aa8a8b09559fab2814cd12a92e723332');
    const index = client.initIndex('transactions');
    index.saveObject(transaction, {
        // All the following parameters are optional
        autoGenerateObjectIDIfNotExist: true,
        // any other requestOptions
    })
    updateTransactionResultToFirestore(transaction["customer_id"],transaction["ref"],transaction)
}
export async function storeExpenseInAlgolia(expenseList){
    // For the default version
    const algoliasearch = require('algoliasearch');

    const client = algoliasearch('TFNEMJ0UVA', 'b9ed7310ae927d4424c4e3a4286329a0');
    const index = client.initIndex('prod_expenses');
    index.replaceAllObjects(expenseList, {
        autoGenerateObjectIDIfNotExist: false
    }).then(({ objectIDs }) => {
        console.log(objectIDs);
    });
}
export async function storeTransactionInAlgolia(transactionList:Array<{}>){
    // For the default version
    // const algoliasearch = require('algoliasearch');
    //
    // const client = algoliasearch('2NBHF1WPXL', 'aa8a8b09559fab2814cd12a92e723332');
    // const index = client.initIndex('transactions');
    // index.replaceAllObjects(transactionList, {
    //     autoGenerateObjectIDIfNotExist: true
    // }).then(({ objectIDs }) => {
    //     console.log(objectIDs);
    // });
    console.log("start ",transactionList.length)
    let count = 1;
    transactionList.forEach((transaction)=>{
        // delete transaction["group"];
        if(transaction["customer_id"] == 'r6BXRcEndkRBc5HL4KEcMqDB4UD2'){
            updateTransactionResultToFirestore(transaction["customer_id"],transaction["ref"],transaction)
        }
        console.log("at ",count);
        count++;
    });

}
export async function checkSelfieJobStatus(jobId,uid):Promise<object> {
    return new Promise( resolve => {
        const smileIdentityCore = require("smile-identity-core");
        const WebApi = smileIdentityCore.WebApi;
        const partner_params = {
            job_id: jobId,
            user_id: uid,
            job_type: 1,
        };
        const connection = new WebApi(process.env.SMILE_PARTER_ID,`${process.env.HOST}/smileBackStatus`, process.env.SMILE_API_KEY, 1);
        connection.get_job_status(partner_params, {
            return_job_status: false,
            return_history: false,
            return_image_links: false
        }).then((response)=>{
            console.log('status response',response);
            resolve(response);
        });

    });

}
export function sendSlackAlert(transactionObj) {
    newSlackMsg('https://hooks.slack.com/services/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',
        `\t\t\tTransaction ${decodeStatus(transactionObj.status)}
-----------------------------------------------
Ref: ${transactionObj.ref}
Amount: Kes ${transactionObj.amount}
Description: ${transactionObj.description}
Payment Type: ${transactionObj.payment_type}
Status: ${transactionObj.status}
Name: ${transactionObj.customer_name}
-----------------------------------------------`);
}
export function sendSlackAlertMsg(msg) {
    newSlackMsg("https://hooks.slack.com/services/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",msg);
}
export function sendKycAlert(customerObj) {
    try{
        let msg = `\t\t\tKYC Review Request\n`;
        msg  += "-----------------------------------------------\n"
        msg  += `Name: ${customerObj.name}\n`
        msg  += `UID: ${customerObj.uid}\n`
        msg  += `ID NO/Passport: ${customerObj.national_id}\n`
        msg  += `ID Card Photo: ${customerObj.national_id_photo}\n`
        msg  += `Selfie: ${customerObj.photo}\n`
        msg  += "-----------------------------------------------\n"
        newSlackMsg("https://hooks.slack.com/services/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",msg);
    }catch (e) {
        console.log(e);
    }

}
export function depositAlert(transactionObj){
    const url = "https://hooks.slack.com/services/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";
    const msg = `\t\t\tTransaction ${decodeStatus(transactionObj.status)}
-----------------------------------------------
Ref: ${transactionObj.ref}
Amount: Kes ${transactionObj.amount}
Description: ${transactionObj.description}
Payment Type: ${transactionObj.payment_type}
Status: ${transactionObj.status}
Name: ${transactionObj.customer_name}
-----------------------------------------------`
    newSlackMsg(url,msg);
}
 function newSlackMsg(url,msg) {
    const request = require('request');
    const options = {
        'method': 'POST',
        'url': url,
        'headers': {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        },
        json: {
            text: msg
        }
    };
    request(options, function (error, response) {

    });
}
export function getReferralDynamicLinkUrl(referralCode:string):Promise<string>{
    return new Promise<string>(resolve => {
        const url = `https://firebasedynamiclinks.googleapis.com/v1/shortLinks?key=AIzaSyDp57j52WB7AFHevKyIVZgv0dhKomR81JE`
        const payload =  {
            "suffix": {
                "option": "UNGUESSABLE"
            },
            "longDynamicLink": `https://payments.boya.co/?link=https://payments.boya.co?invitecode=${referralCode}&apn=boya.com.boyaapp&amv=2100&ibi=boya.co.boya&isi=1459785086&ofl=https://boya.co`
        }
        const request = require('request');
        const options = {
            'method': 'POST',
            'url': url,
            'headers': {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
            json: payload
        };
        request(options, function (error, response) {
            console.log("link response",response.body);
            resolve(response.body.shortLink);
        });
    })


}
// Process KPLC PostPaid
// generateToken(100,'34002576','254718555832',(response)=>{console.log(response)})

export function generateToken(amount,account,phone,callback) {
    const request = require('request');
    console.log('im here 1')

    const options = { method: 'POST',
        url: 'http://services.jambopay.co.ke/jambopayservices/token',
        headers:
            { 'cache-control': 'no-cache',
                'Cache-Control': 'no-cache',
                Accept: '*/*',
                'Content-Type':'application/x-www-form-urlencoded'},
        form: {
            "grant_type":process.env.GRANT_TYPE,
            "username":process.env.JAMBOPAY_USERNAME,
            "password":process.env.JAMBOPAY_PASSWORD
            }
         };
         request(options, function (error, response, body) {
            if (error){ callback(body,error); throw new Error(error)}

            let resp = body;
            try {
                resp = JSON.parse(resp);
            }catch (e) {

            }
            const token = resp.access_token;

            validateAccountPLCPostPaid(amount,account,phone,token,callback)
            // return resp.access_token;

        });

}

export function validateAccountPLCPostPaid(amount,account,phone,token,callback){

    const mobileno = formatAirtimePhone(phone);
    const request = require('request');

    const options = {
        'method': 'POST',
        'url': 'http://services.jambopay.co.ke/jambopayservices/api/KenyaPower/ValidateAccount?accountNumber='+account,
        'headers': {
            'Authorization':"Bearer" + token,
            'api_key': process.env.JAMBOPAY_APIKEY,
        }
    };

    request(options, function (error, response) {

        if (error) {
            console.error('error procesing postpaid',error);
            callback(new TransactionResult(StatusType.ERROR, {errorMessage:"error procesing postpaid",error:error.toString()}));
        }
        let body = response.body;
        console.log('data----', body)
        console.log('error----', error)
        try {
            body = JSON.parse(body);
        }catch (e) {

        }
        if(body.status ==0){
            processKPLCPostPaid(amount,account,phone,token,callback);
        }else{

        }

    });

}


export function processKPLCPostPaid(amount,account,phone,token,callback){

    const mobileno = formatAirtimePhone(phone);
    const request = require('request');
    console.log('im here')

    const options = {
        'method': 'POST',
        'url': 'http://services.jambopay.co.ke/jambopayservices/api/KenyaPower/',
        'headers': {
            'Authorization':"Bearer" + token,
            'api_key': process.env.JAMBOPAY_APIKEY,
        },
        form: {
            "Stream":'kplc',
            "Amount":amount,
            "MeterNumber":account,
            "MerchantID":'KPLC',
            "PaymentTypeID":'1',
            "phoneNumber":phone,
            "PaidBy":'stella S'
        }
    };

    request(options, function (error, response) {

        if (error) {
            console.error('error procesing postpaid',error);
            callback(new TransactionResult(StatusType.ERROR, {errorMessage:"error procesing postpaid",error:error.toString()}));
        }
        let body = response.body;
        console.log('data----', body)
        console.log('error----', error)
        try {
            body = JSON.parse(body);
        }catch (e) {

        }
        if(body.TransactionID != undefined){
            chargeKPLCPostPaid(body.TransactionID,body.PaidBy,body.PhoneNumber,body.Pin,token,callback)
        }else{

        }

    });

}



export function chargeKPLCPostPaid(TransactionID,PaidBy,phone,pin,token,callback){

    const mobileno = formatAirtimePhone(phone);
    const request = require('request');

    const options = {
        'method': 'PUT',
        'url': 'http://services.jambopay.co.ke/jambopayservices/api/KenyaPower/',
        'headers': {
            Accept: '*/*',
            Authorization:"Bearer" + token ,
            api_key: process.env.JAMBOPAY_APIKEY,
        },
        form: {
            Stream:'KPLC',
            TransactionID:TransactionID,
            PhoneNumber:mobileno,
            PaidBy:PaidBy,
            PaymentTypeID:"1",
            MerchantID:'KPLC',
            Tendered:"2",
            PIN:pin

        }

    };
    request(options, function (error, response) {
        if (error) {
            console.error('error charging postpaid',error);
            callback(new TransactionResult(StatusType.ERROR, {errorMessage:"error charging postpaid",error:error.toString()}));
        }
        let body = response.body;
        console.log('im here 4')

        console.log(body)
        try {
            body = JSON.parse(body);
        }catch (e) {

        }

    });

}
// Process KPLC Prepaid

export function processKPLCPrePaid(amount,account,phone,token,callback){

    const mobileno = formatAirtimePhone(phone);
    const request = require('request');

    const options = {
        'method': 'POST',
        'url': 'http://services.jambopay.co.ke/jambopayservices/api/payments/',
        'headers': {
            Accept: '*/*',
            Authorization:"Bearer" + token,
            api_key: process.env.JAMBOPAY_APIKEY,
        },
        form: {
            "Stream":'kplc',
            "Amount":amount,
            "MeterNumber":account,
            "MerchantID":'KPLC',
            "PaymentTypeID":'1',
            "phoneNumber":phone,
            "PaidBy":'stella S'
        }
    };
    request(options, function (error, response) {
        if (error) {
            console.error('error procesing postpaid',error);
            callback(new TransactionResult(StatusType.ERROR, {errorMessage:"error procesing postpaid",error:error.toString()}));
        }
        let body = response.body;
        try {
            body = JSON.parse(body);
        }catch (e) {

        }

        if(body.TransactionID != undefined){
            chargeKPLCPrePaid(body.TransactionID,body.PaidBy,body.PhoneNumber,body.Pin,token,callback)
        }else{

        }

    });

}

export function chargeKPLCPrePaid(TransactionID,PaidBy,phone,pin,token,callback){

    const mobileno = formatAirtimePhone(phone);
    const request = require('request');

    const options = {
        'method': 'PUT',
        'url': 'http://services.jambopay.co.ke/jambopayservices/api/payments/',
        'headers': {
            Accept: '*/*',
            Authorization:"Bearer" + token,
            api_key: process.env.JAMBOPAY_APIKEY,
        },
        form: {
            Stream:'KPLC',
            TransactionID:TransactionID,
            PhoneNumber:mobileno,
            PaidBy:PaidBy,
            PaymentTypeID:"1",
            MerchantID:'KPLC',
            Tendered:"2",
            PIN:pin

        }

    };
    request(options, function (error, response) {
        if (error) {
            console.error('error charging postpaid',error);
            callback(new TransactionResult(StatusType.ERROR, {errorMessage:"error charging postpaid",error:error.toString()}));
        }
        let body = response.body;
        try {
            body = JSON.parse(body);
        }catch (e) {

        }

    });

}
export async function getTransactionTag(customer_id:string,ref:string):Promise<string>{
    const rec = await firestore.collection('users').doc(customer_id)
        .collection("transactions").doc(ref).get();
    return new Promise<string>(resolve => {
        const transaction = rec.data();
        if(transaction!=undefined)
            resolve(transaction["tag"])
        else resolve(undefined)
    })


}

export async function sendExpense(transaction,employee_id){
    const request = require('request');
    const options = {
        'method': 'PUT',
        'url': 'https://testbiz.boya.co/expenses/filter',
        'headers': {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(
            {"title":transaction["description"],
                "transaction_ref":transaction["txRef"],
                "employee_id":employee_id,
                "category_id":"11",
                "status":1})

    };
    request(options, function (error, response) {
        if (error) throw new Error(error);
        console.log('>>>>expense reporting response',response.body);
    });

}



