const {updateFAuthUser} = require("./commonfuncs");


const {chargeFW, tokenCharge,issueVirtualCard,refundChargeFW,STATUS_200,
    terminateVirtualCard,getVirtualCardTransactions,
    blockVirtualCard,unBlockVirtualCard,fetchVirtualCardDetails} = require("../vgs/flutterwave");

const {PSPs,Groups} = require("../participants/Groups");

const {captureFw} = require("../vgs/flutterwave");
const {DOC_TYPE} =  require("./commonfuncs");
const dateFormat = require('dateformat');
// const {
//     initiateRefundForVerifiedCharge,refundConsumer} = require("./pendingRefunds");
// const {runPendingCharges} = require("./pending_charges");
const {generateRandomPin,compareSelfie,generateRandomPin6,sendSlackAlertMsg,
    elecPostPaidMsg,
    storeTransactionInAlgolia,depositAlert,
    checkMpesaTransactionStatus,
    getUserToken, goDstvBillMsg, goHomeFibreBillMsg,
    goSafPostPayBillMsg,
    goTvBillMsg,getKplcBill,
    sendBillPushNotification,storeExpenseInAlgolia,
    zukuBillMsg,
    sendSlackAlert,createVerifiedCard,removePendingPayment,
    // updateTransactionStatus,
    sendFundstoBusiness,getTransactionTag,sendPushNotification,
    verifyNationalId,updateUserLegalInfo,updateUserSelfieInfo,updateUserInfo,updateTransactionResultToFirestore,
    getBase64image,sendAjuaInfo,createPendingCardVerification,forceUserKyc,getCardName,verifyNationalIdPhotoAndSelfie,sendKycAlert,sendSms,getMerchantReviews} = require("./commonfuncs");

const {dumpUserToFirestore} = require("./commonfuncs");
const LocalDateTime =  require('js-joda').LocalDateTime
const LocalDate =  require('js-joda').LocalDate
const {DataStoreFactory} = require("../interfaces/TransactionParticipant");
// const mailer = require('./../sendmail');
const {PrepResult} = require("../models/TransactionResult");
const {StatusType,SETTLEMENT_SCHEDULE,SCHEDULED_PAYMENT_FREQUENCY} = require("./StatusType");
const {App} = require("../../bloc/App");
// const jenga = require('./../../equity_jenga/b2b');
const express = require('express');
const crypto = require('crypto');
const request = require('request');
const mysql = require(`../mysql`);
const mpesa_sec = require(`../../mpesa/security`);
const mpesa = require(`../../mpesa/env`);
const transUtil = require('../../encrypt');
const {createStripeCustomer} = require("../card_charge");
const db = DataStoreFactory.getDatabase();
const KENYA_NATIONAL_ID_REGEX = /^[0-9]{1,9}$/
const KENYA_PASSPORT_REGEX = /^[A-Z0-9]{7,9}$/
const KENYA_ALIEN_CARD_REGEX = /^[0-9]{6,9}$/
 const SETTLEMT_INTERNAL = "INTERNAL"
 const SETTLEMT_PESALINK = "PESALINK"

/**
 *
 * @param {*} phone
 */
function formatJengaPhone(phone) {
    const countrycode = "+254";
    let phone2 = phone.match(/\d+/)[0];
    if (phone2.toString().substring(0, 1) === "2") {
        phone2 = "0" + phone2.substring(3);
    } else if (phone2.toString().substring(0, 2) == "00") {
        phone2 = countrycode + "" + phone.toString().substring(2)
    } else if (phone2.toString().substring(0, 1) == "0") {
        // phone2 = countrycode + "" + phone.toString().substring(1)
    }
    console.log(`formatted ${phone2}`);
    return phone2;
}

/**
 *
 * @param {*} customer_id
 * @param {*} callback
 */

async function customerBalance(customer_id, callback) {
    const balance = await db.getCustomerBalance(customer_id);
    callback({
        status: StatusType.SUCCESS,
        message: transUtil.encrypt(JSON.stringify({balance: balance ? balance : 0.0, customer_id: customer_id}))
    });
}
/**
 *
 * @param {*} customer_id
 * @param {*} callback
 */
async function walletBalance(customer_id, callback) {
    const balance = await db.getWalletByID(customer_id);
    callback({
        status: StatusType.SUCCESS,
        message: transUtil.encrypt(JSON.stringify({balance: balance ? balance : {"credit": 0, "debit": 0, "amount": 0}, customer_id: customer_id}))
    });
}

/**
 *
 * @param {*} wallet_charge
 * @param {*} customer_id
 * @param {*} callback
 */
async function debitWallet(wallet_charge,customer_id,callback){
    const walletObj = await db.getWalletByID(customer_id);
    const credit = walletObj.credit - wallet_charge;
    const debit = walletObj.debit;
    const amount = credit + debit;
    await db.debitWallet(credit, debit, amount, customer_id)

}
/**
 *
 * @param {*} req
 * @param {*} res
 */

async function creditWallet(req,res){

    const data = await transUtil.decrypt(req.body.payload);
    const transObj = JSON.parse(data)
    const walletObj = await db.getWalletByID(transObj.customer_id);

    console.log(walletObj)
    const credit = walletObj.credit + transObj['amount'] + transObj['fee']  ;
    const debit = walletObj.debit;
    const amount = credit - debit;

    await db.creditWallet(credit, debit, amount, transObj.customer_id)

    // update transaction
    const response = await updateDbTransaction(transObj);
  return res.end(JSON.stringify({status: StatusType.SUCCESS, message: transUtil.encrypt(JSON.stringify(response))}));


}
/**
 *
 * @param {*} transObj
 */
async function updateDbTransaction(transObj){
    const timestamp = dateFormat(new Date(), "yyyymmddHHMMss");

    await db.createTransaction(
        {
            ref:transObj.ref,
            cust_id:transObj.stripe_cust_id,
            initial:transObj.initial?1:0,
            customer_id:transObj.customer_id,
            phone:transObj.phone,
            amount:transObj.amount,
            dateString:timestamp,
            timestamp:timestamp,
            // provider_response:response,
            description:transObj.description,
            group:transObj.group,
            receiver:transObj.receiver,
            accno:transObj.accno,
            payment_type:"WALLET",
            provider1:"Wallet",
            txRef:transObj.ref,
            fee: transObj.fee

        }).then((saved)=>{
        console.log("[creditWallet]: updated DB transaction");
             db.updateTransaction(transObj.ref,{
                mode: "Wallet",
                transaction_type: 3,
                status:0,
            }).then((saved)=>{
            }).catch((failed)=>{
            });

    }).catch((failed)=>{
        console.error("[creditWallet]:failed to update transaction");
    });
}
/**
 *
 * @param {*} customer_id
 * @param {*} callback
 */

// async function updateTransactionsWithStatus(){
//     const alltransactions = await db.getAllTransactionsSince("2020-06-23")
//     alltransactions.forEach((tr)=>{
//         updateTransactionStatus(tr.customer_id,tr.ref,tr.status)
//     })
// }
async function resetPin(customer_id, callback) {
    const pin = generateRandomPin();
    const nonce = crypto.randomBytes(32).toString('hex');
    const phone = await db.getCustomerPhone(customer_id);
    const balance = await db.resetPin(nonce, transUtil.encrypt(`${pin}`), customer_id);
    if (balance === StatusType.SUCCESS) {
        sendSms(`Use ${pin} as your One-Time-Password to reset your account`, phone, (resp) => {
            console.log(resp);
        });
    }
    callback({status: balance, message: transUtil.encrypt(JSON.stringify({nonce: nonce, customer_id: customer_id}))});
}

/**
 *
 * @param {*} mid
 * @param {*} callback
 */
async function resetMerchantPin(mid, callback) {
    const pin = generateRandomPin6();
    const nonce = crypto.randomBytes(32).toString('hex');
    const phone = (await db.getMerchantById(mid)).phone;
    const state = await db.resetPin(nonce, transUtil.encrypt(`${pin}`), mid);
    if (state === StatusType.SUCCESS) {
        sendSms(`${pin} is your One-Time-Password`, phone, (rest) => {
            console.log(rest);
        });
    }
    callback({status: state});
}
/**
 *
 * @param {*} code
 * @param {*} mid
 * @param {*} callback
 */

async function verifyMerchantCode(code, mid, callback) {
    const exists = await db.verifyMidPin(mid, transUtil.encrypt(`${code}`));
    console.log('exists', exists);
    callback(exists);
}
async function verifyPin(code, nonce, callback) {
    const exists = await db.verifyPin(nonce, transUtil.encrypt(`${code}`));
    console.log('exists', exists);
    callback(exists);
}
/**
 *
 * @param {*} customer_id
 * @param {*} callback
 */

async function shareCounts(customer_id, callback) {
    const balance = await db.getCustomerShareCounts(customer_id);
    callback({
        status: StatusType.SUCCESS,
        message: transUtil.encrypt(JSON.stringify({counted: balance, customer_id: customer_id}))
    });
}

/**
 *
 * @param {*} customer_id
 * @param {*} callback
 */

async function customerStatement(customer_id, callback) {
    const statement = await db.getCustomerStatement(customer_id);
    callback({status: StatusType.SUCCESS, message: transUtil.encrypt(JSON.stringify(statement))});
}
/**
 *
 * @param {*} customer_id
 * @param {*} callback
 */
async function customerWalletStatement(customer_id, callback) {
    const statement = await db.getCustomerWalletStatement(customer_id);
    callback({status: StatusType.SUCCESS, message: transUtil.encrypt(JSON.stringify(statement))});
}
/**
 *
 * @param {*} group
 * @param {*} amount
 * @param {*} callback
 */

async function getTransactionFee(group,amount, callback) {
    const fee = await db.getTransactionFee(group,amount)
    callback({status: StatusType.SUCCESS, fee: fee});
}
/**
 *
 * @param {*} callback
 */
async function getTransactionFees(callback) {
    const fees = await db.getAllTransactionFees()
    callback({status: StatusType.SUCCESS, fees: fees});
}

/**
 *
 * @param {*} req
 * @param {*} res
 */
async function refundMerchantsCustomer(req,res){
    if(req.body.mid && req.body.paymentRef && req.body.key == "A_7d34c531e60c2037b739b63b695652c8"){
        const mid = req.body.mid;
        const reason = req.body.reason;
        const paymentRef =  req.body.paymentRef;
        const tCheck = await db.getTransactionByRef(paymentRef);
        if(tCheck.status == StatusType.SUCCESS &&  tCheck.response.merchant_id!=undefined
            && mid == tCheck.response.merchant_id && tCheck.response.status == StatusType.SUCCESS){
            const transaction =  tCheck.response;
            if(transaction.provider1_resp){
                transaction.card_response = transaction.provider1_resp;
                transaction.stripeAmount = transaction.amount;
            }
            const charge = await db.getTransactionByRefForRefund(paymentRef)
            // refundConsumer(charge,(refunded)=>{
            //     if(refunded){
            //         db.debitMerchantAccountRefund(mid,transaction.ref,reason,transaction.amount,(state)=>{
            //             if(state == StatusType.SUCCESS){
            //                 res.json({"status":"success"});
            //             }else{
            //                 sendSlackAlertMsg("Failed to debit merchant account after customer refund",paymentRef)
            //                 res.json({"status":"success"});
            //             }
            //         })
            //     }else{
            //         sendSlackAlertMsg("Refund failed. Failed to refund merchant's customer",paymentRef)
            //         res.status(500).json({"status":"failed"});
            //     }
            // })
        }else{
            res.status(500).json({"status":"failed"});
        }
    }else{
        return res.status(500).send(JSON.stringify({error:"missing ref/mid"}));
    }


}

/**
 *
 * @param {*} callback
 */

async function paybills(callback) {
    const paybills = await db.getPaybills();
    callback({status: StatusType.SUCCESS, message: transUtil.encrypt(JSON.stringify(paybills))});
}
/**
 *
 * @param {*} req
 * @param {*} res
 */
async function processTransaction(req, res) {
    var start = new Date().getTime();

    //TODO process transaction
    if (req.body.payload !== undefined && req.body.uid !== undefined) {
        console.log(req.body.payload)

        let transObj = '{}'
        try{
        // transObj = transUtil.decrypt(req.body.payload);
        transObj = req.body.payload;
       }catch(e){
        console.log(e);
        try{
            console.log('could not decrypt payload ',req.body.payload);
           transObj = transUtil.decrypt(JSON.stringify(req.body.payload));
       }catch(e){
        console.log(e);
       }
       }

         // transObj = transObj.replace(/\u0019/g, '')
        try {
            transObj = JSON.stringify(transObj).replace(/\\u0019/g, '')
            transObj = JSON.parse(transObj);
            try{
                transObj = JSON.parse(transObj);
            }catch (e) {
                console.log('first catch falls')
            }
            console.log('>>>>transObj is ',transObj);
            console.log('>>>>amount is ',transObj.amount);
        } catch (e) {
            console.log('error parsing to json',e)
            console.log(transObj);
            try {
                transObj = JSON.stringify(transObj).replace(/\\u0019/g, '');
            } catch (e) {
                console.log('error stringify to json',e)
                console.log(transObj);
            }
            try {
                transObj = JSON.parse(transObj);
            } catch (e) {
                console.log('error parsing to json',e)
                console.log(transObj);
            }
        }

        if (transObj.uid  !== req.body.uid) {
            console.log("uid is ",req.body.uid);
            console.log('invalid payload. body uid!= decrypted uid',transObj);

        }


        if (transObj["amount"] == undefined) {
            console.log('invalid payload. no amount',transObj["amount"]);
            try{
                // sendSms(transObj,"0718555832",(resp)=>console.log(resp));
                sendSms(transObj,"0718555832",(resp)=>console.log(resp));
            }catch (e) {
                console.error(e);
            }
            sendSms(`failing transaction. missing amount, ${transObj["phone"]}`,"0718555832",(resp)=>console.log(resp));
            return res.status(500).json(new PrepResult(StatusType.ERROR, {
                "errorMessage": "Failed, Please check missing amount",
                "error": "Failed, Please check missing amount",
                "errorCode": 300015
            }));
        }

        if (parseFloat(`${transObj["amount"]}`) > 320000.0) {
            console.log('invalid amount',transObj);
            sendSlackAlertMsg(`float limit block,${transObj["amount"]}`)
            return res.status(500).json(new PrepResult(StatusType.ERROR, {
                "errorMessage": "Sorry, the maximum you can transact is 320K",
                "error": "Sorry, the maximum you can transact is 320K",
                "errorCode": 300015
            }));
        }
        // TODO UNDO
        const ip ="ip"
        // const ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
        transObj["ip"] = ip;
        try {
            if (transObj["receiver_name"] !== undefined && transObj["receiver_name"] != null) {
                transObj["receivername"] = transObj["receiver_name"] !== "" ? transObj["receiver_name"] : transObj["receiver"];
            }
        } catch (e) {
            console.log('error ',e);
            console.error(e)
        }

        console.log('call app-----')

        const app = new App(transObj);

        const response = await app.processTransaction();

        var end = new Date().getTime();
        var time = end - start;
        console.log('time taken------', time)
// console.log('transaction status',response);

        // return res.end(JSON.stringify({status: response.status, message: transUtil.encrypt(JSON.stringify(response))}));
        return res.end(JSON.stringify({status: response.status, message: response}));

    }
    return res.status(500).end();
}

function purchaseAirtime(){
    let payload ={
         "amount": 100, 
         "fee": 0, 
         "phone": "+254718555832", 
         "receiver": "123456", 
         "receivername": "+254718555832", 
         "receiver_data": null, 
         "group": "bills", 
         "description": "Pay to 123456", 
         "customer_id": "HyTJgV9kjRcfyJ0tGGK7IpS4tm63", 
         "uid": "HyTJgV9kjRcfyJ0tGGK7IpS4tm63", 
         "stripe_cust_id": "cus_FBGYjoRz4LmGhl", 
         "chargeId": null, 
         "payment_type": "CARD", 
         "payment_method_details": null, 
         "ref": "Test-9", 
         "reference": "Test-9", 
         "txRef": "Test-9", 
         "currency": null, 
         "email": "stella@gmail.com", 
         "bank": null, 
         "accno": '123456', 
         "token": null, 
         "deviceInfo": null, 
         "ticket": null, 
         "status": null,
         "provider1": "FLUTTERWAVE",
         "lat": null, 
         "long": null, 
         "ip": null, 
         "initial": false, 
         "source": "5377283645077450"
     }
    //  let cipher = 'c67c0a48dd68c55f65841e7c7b374d52fafd9defb80772edac9e0c919b4639f4b04963515211f46369323f37e6183ab015786504e6f16037c9f0d10adbdc35035ee10e777f9a3af2add60897044f2f5630f72e84bebc5268606fc398127119fa50dd77babb737de93939f5741b09e1c2cd4741fa2f0caed36c82ac018e343d2850f3d39c8d05ec6a79c0bb80a3709dde4a40afc5a357cd4d4bf998b5a00da27a6ee0ca7a763dae1803aec95313f4fd6976da7b388aaccb918e96b039653c8753617d0fe6a50e54d06cba1b78776fca3402dbd3ca68aeb86d0243293df9dda1d05444ba20cada8525e8ff9aeee7b546865544d63b68b0896fb8b1454b2d4943482d3282774ca587de7858e2417f1d82bca5fc10341aa40accc9db8551d2e64c8837129c4d268affc98fe4434a37f4ebf2ba9b2e5fc398984f31eff7c7b470c6e9b11852f81dc842f1453f07379f2a88ba194fe68201097a9f4f95d1cb4fb065ab94e8aaea62453e85b81116e10bde1a8f8cbe845906d7f2dd96d8dc27294031cb37c81ba6c4f491a490413311fe7967dbc484a9e4f5eca105335f2bcda0a0b5c206cc5ad2d6f5e88bacac44b98e2e2b45c3983a0b1f6fb45c58048859ae072012ac15b469f56310c579ee8e3761e9a03ecc31a63d6207117e95b393021ac290276be72fd0b71addb90644ee50b62fab37f752c6f0236953eceb72ef45f0f14722b3f0a37f0d89f75a346c05f4c79d8ef12f66b2f783a9fff09a5d3d98a40a98aeaa0d10322808388896e45e39048a36f019fef5b74d0b032e5ef39a1900d60476358290954e4618b757d9caa324c3d4a18738e14ba33e905b51a3a84e43317e716ce5d0d0edbcae9db5ea532b310461f3145ec6c26a13b0c947d5f575c62f50ec08e4d36ffc6d3bbafd137f3adca3d5e7a74b86e6250eefab'
    // let cipher = transUtil.encrypt(payload) 

    var transit = {body:{"payload": payload, "uid": payload["uid"], "amount": payload["amount"], "customer_id": payload["customer_id"]}}
     processTransaction(transit)
     // let m = transUtil.decrypt("89c411a1164a034d3bec04dab0352a3b5a6949eb689fd2914f8de7c3516549a557939f6911d822954a59bf35236376c649c20bbe7f8610ce1c60e3c62d9f70991a0f05f71bd799283b03b6226e354cae34e8c2d7f579e7765f2087fc9702a43a")
     // console.log(m)
  }
  //purchaseAirtime();

/**
 *
 * @param {*} req
 * @param {*} res
 */

async function creditMyWallet(req, res) {
       //TODO process transaction

       if (req.body.payload !== undefined && req.body.uid !== undefined) {
           console.log(req.body.payload)

           let transObj = '{}'
           try{
              transObj = transUtil.decrypt(req.body.payload);
        //    transObj = req.body.payload;
          }catch(e){
           console.log(e);
           try{
               console.log('could not decrypt payload ',req.body.payload);
              transObj = transUtil.decrypt(JSON.stringify(req.body.payload));
          }catch(e){
           console.log(e);
          }
          }

            // transObj = transObj.replace(/\u0019/g, '')
           try {
               transObj = JSON.stringify(transObj).replace(/\\u0019/g, '')
               transObj = JSON.parse(transObj);
               try{
                   transObj = JSON.parse(transObj);
               }catch (e) {
                   console.log('first catch falls')
               }
               console.log('>>>>transObj is ',transObj);
               console.log('>>>>amount is ',transObj.amount);
           } catch (e) {
               console.log('error parsing to json',e)
               console.log(transObj);
               try {
                   transObj = JSON.stringify(transObj).replace(/\\u0019/g, '');
               } catch (e) {
                   console.log('error stringify to json',e)
                   console.log(transObj);
               }
               try {
                   transObj = JSON.parse(transObj);
               } catch (e) {
                   console.log('error parsing to json',e)
                   console.log(transObj);
               }
           }

           if (transObj.uid  !== req.body.uid) {
               console.log("uid is ",req.body.uid);
               console.log('invalid payload. body uid!= decrypted uid',transObj);
           }


           if (transObj["amount"] == undefined) {
               console.log('invalid payload. no amount',transObj["amount"]);
               try{
                   // sendSms(transObj,"0718555832",(resp)=>console.log(resp));
                   sendSms(transObj,"0718555832",(resp)=>console.log(resp));
               }catch (e) {
                   console.error(e);
               }
               sendSms(`failing transaction. missing amount, ${transObj["phone"]}`,"0718555832",(resp)=>console.log(resp));
               return res.status(500).json(new PrepResult(StatusType.ERROR, {
                   "errorMessage": "Failed, Please check missing amount",
                   "error": "Failed, Please check missing amount",
                   "errorCode": 300015
               }));
           }

           if (parseFloat(`${transObj["amount"]}`) > 500000.0) {
               console.log('invalid amount',transObj);
               return res.status(500).json(new PrepResult(StatusType.ERROR, {
                   "errorMessage": "Invalid transaction amount. Maximum is 500K",
                   "error": "Invalid transaction amount. Maximum is 500K",
                   "errorCode": 300015
               }));
           }
           // TODO UNDO
           const ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
           transObj["ip"] = ip;
           try {
               if (transObj["receiver_name"] !== undefined && transObj["receiver_name"] != null) {
                   transObj["receivername"] = transObj["receiver_name"] !== "" ? transObj["receiver_name"] : transObj["receiver"];
               }
           } catch (e) {
               console.error(e)
           }

           //check if user has wallet

           const obj = await db.getWalletByID(transObj['customer_id'])
           if(obj.debit == undefined){
            //   user has no wallet
            const randomAlphaNumeric = this.randomRef()
            const account_number = `W-${randomAlphaNumeric.toUpperCase()}`

                const walletObj = {
                    customer_id: transObj['customer_id'],
                    account_number: account_number,
                    debit: 0,
                    credit: 0,
                    amount: 0,
                    fees: 0
                }

            await db.createWallet(walletObj);
            }


           const app = new App(transObj);
           const response = await app.processTransaction();
           return res.end(JSON.stringify({status: response.status, message: transUtil.encrypt(JSON.stringify(response))}));
       }
       return res.status(500).end();
   }

/**
 *
 * @param {*} amount
 * @param {*} phone
 * @param {*} ref
 * @param {*} callback
 * @param {*} mpesaCallBackURL
 */
function initiateMPESAPush(amount, phone, ref, callback, mpesaCallBackURL) {
    mpesa_sec.getToken(false, (authToken) => {
        const timestamp = dateFormat(new Date(), "yyyymmddHHMMss");
        request(
            {
                method: 'POST',
                url: mpesa.PUSH_URL,
                headers: {
                    "Authorization": authToken
                },
                json: {
                    "BusinessShortCode": process.env.MPESA_C2B_SHORTCODE,
                    "Password": mpesa_sec.getPassword(timestamp),
                    "Timestamp": timestamp,
                    "TransactionType": "CustomerPayBillOnline",
                    "Amount": amount,
                    "PartyA": phone,
                    "PartyB": process.env.MPESA_C2B_SHORTCODE,
                    "PhoneNumber": phone,
                    "CallBackURL": mpesaCallBackURL,
                    "AccountReference": ref,
                    "TransactionDesc": "payment"
                }
            },
            function (error, response, body) {

                if (error) {
                    console.log('error procesing push request');
                    console.log(error);
                    callback({status: 0, error: error, message: 'error procesing push request'})
                    return;
                }
                console.log('push response √√');

                var results = body;
                try {
                    results = JSON.parse(results)
                    // eslint-disable-next-line no-empty
                } catch (e) {
                    console.error(e)
                }
                console.log(`response status ${response.status}`);
                console.log(results);
                if (mpesaCallBackURL !== undefined) {
                    results.CallBackURL = mpesaCallBackURL
                }
                if (results.ResponseCode !== undefined && parseInt(results.ResponseCode) === 0) {
                    results.status = 1;
                    callback(results);
                } else {
                    const resp = {status: 0, errorMessage: results.errorMessage};
                    callback(resp);
                }


            }
        );
    });

}
/**
 *
 * @param {*} callback
 */
function mpesaTimeOutRoute(callback) {
    const router = express.Router();
    router.route('/')
        .all((req, res, next) => {
            res.setHeader("Access-Control-Allow-Origin", "*");
            // res.writeHead(200, {'Content-Type': 'application/json'});
            next();
        }).post(callback);
    return router;

}
/**
 *
 * @param {*} status
 * @param {*} resp
 * @param {*} ConversationID
 */

function updateB2CWithCID(status, resp, ConversationID) {
       function update(status, resp, ConversationID){
           mysql.updateRecord("boya", "transactions",
               [status, JSON.stringify(resp), ConversationID], ["status", "provider2_resp", "ConversationID"], (result, err) => {
                   if (err) {
                       console.log("error saving B2C MPESA response");
                   } else {
                       console.log("[update] B2C MPESA response", resp);
                   }});
       }
       function updateWhereRef(status, resp, ConversationID,ref){
           mysql.updateRecord("boya", "transactions",
               [status, JSON.stringify(resp), ConversationID,ref],
               ["status", "provider2_resp", "ConversationID","txRef"],(result, err) => {
                   if (err) {
                       console.log("error saving B2C MPESA response");
                   } else {
                       console.log("[updateWith Ref] B2C MPESA response", resp);
                   }});
       }
    update(status, resp, ConversationID);

        try {
            mysql.runQuery(`select * from ${process.env.DATABASE}.transactions
         where ConversationID = '${ConversationID}'`,(row)=>{return {id:row.id}},(results)=>{
                if(results.length>0){
                    update(status, resp, ConversationID);
                }else{
                    console.log(`no record matching ConversationID`,ConversationID)
                    sendSms(`no record matching ConversationID ${ConversationID}`,
                        "0718555832",(resp)=>{})
                    try{
                    resp = JSON.parse(resp)
                    }catch (e){
                        console.error(e)
                    }
                    try{
                        let paidmount = 0;
                        let paidAccno;
                        let paidReceiver = "";
                        if(resp["ResultParameters"] && resp["ResultParameters"]["ResultParameter"]){
                            const params = resp["ResultParameters"]["ResultParameter"];

                            params.forEach((record)=>{
                                if(record.Key == "Amount"){
                                    paidmount = record["Value"]
                                }
                                if(record.Key == "TransactionAmount"){
                                    paidmount = record["Value"]
                                }
                                if(record.Key == "ReceiverPartyPublicName"){
                                    paidReceiver = record["Value"].toString().split("-")[0].trim()
                                }
                            })
                        }
                        try{
                            if(resp["ReferenceData"] && resp["ReferenceData"]["ReferenceItem"]){
                                const params = resp["ReferenceData"]["ReferenceItem"]
                                if(Array.isArray(params))
                                params.forEach((record)=>{
                                    if(record.Key == "BillReferenceNumber"){
                                        paidAccno = record["Value"]
                                    }
                                })
                            }
                        }catch (e){
                            console.error(e)
                        }

                        const transactionScope = dateFormat(LocalDateTime.now().minusSeconds(180).toString(),"yyyy-mm-dd HH:MM")
                        const transactionNow = dateFormat(new Date(),"yyyy-mm-dd HH:MM")
                        let sql = "";
                        if(paidAccno)
                         sql =`select * from ${process.env.DATABASE}.transactions where amount = ${paidmount}
                         and accno = '${paidAccno}' and status != 0 and timestamp >= '${transactionScope}' and timestamp <= '${transactionNow}' and ConversationID is NULL  order by timestamp desc limit 1`
                        else sql =`select * from ${process.env.DATABASE}.transactions where amount = ${paidmount}
                         and status != 0 and timestamp >= '${transactionScope}' and timestamp <= '${transactionNow}' and ConversationID is NULL  order by timestamp desc limit 1`
                        console.log('sql',sql)
                        if(paidReceiver!='')
                         sql = `select * from ${process.env.DATABASE}.transactions where amount = ${paidmount}
                         and (accno = '${paidAccno}' or receiver like '%${paidReceiver.substring(3)}') and status != 0 and timestamp >= '${transactionScope}' and timestamp <= '${transactionNow}' and ConversationID is NULL  order by timestamp desc limit 1`
                        mysql.runQuery(sql,
                            (row)=>{
                            return {ref:row.txRef}
                            },(results,err)=>{
                                    if(results.length){
                                        updateWhereRef(0,resp,ConversationID,results[0].ref)
                                    }
                            })

                    } catch (e){
                        console.error(e)
                        sendSms(`error matching ConversationID ${ConversationID}`,
                            "0718555832",(resp)=>{})
                    }
                    setTimeout((status,resp,ConversationID)=>{
                        update(status, resp, ConversationID);
                    },4000)
                }
            })

        }catch (e){
            console.error("error saving transaction ref",e);
        }


}
/**
 *
 * @param {*} status
 * @param {*} resp
 * @param {*} checkoutId
 */

function updateC2BWithCheckoutID(status, resp, checkoutId) {
    mysql.updateRecord("boya", "transactions",
        [resp, checkoutId], ["provider1_resp2", "checkoutId"], (result, err) => {
            if (err) {
                console.log("error saving B2C MPESA response");
            } else {
                console.log("[updateC2BWithCheckoutID] B2C MPESA response", resp);
            }
        });
}
function createTransactionLog(status, resp) {
    const transaction = {status:status,payload:JSON.stringify(resp),payment_type:1};
    if(resp.Body && resp.Body.stkCallback){
        resp.Body.stkCallback.CallbackMetadata.Item.forEarch((item)=>{
            switch (item["Name"]){
                case "Amount":
                    transaction.amount = item["Value"]
                    break;
                case "MpesaReceiptNumber":
                    transaction.reference = item["Value"]
                    break;
                case "BillRefNumber":
                    transaction.account = item["Value"]
                    break;
            }
        })
    }else{
        if(resp.TransAmount)
        transaction.amount = resp.TransAmount
        if(resp.BillRefNumber)
        transaction.account = resp.BillRefNumber
        if(resp.TransID)
        transaction.reference = resp.TransID
    }

    mysql.insertRecord("boya", "transaction_alerts",
        transaction, (result, err) => {
            if (err) {
                console.log("error saving callback MPESA alert");
            } else {
                console.log("[createTransactionLog] B2C MPESA response", resp);
            }
        });
}
/**
 *
 * @param {*} req
 * @param {*} res
 */
function b2cCallbackResults(req, res) {
    try {
        const resp = req.body.Result;
        const ConversationID = resp.ConversationID;
        console.log('b2callback', ConversationID);
        if (resp.ResultCode === 0 || resp.ResultCode === "0") {
            console.log(JSON.stringify(resp));
            updateB2CWithCID(StatusType.SUCCESS, JSON.stringify(resp), ConversationID);
        } else {
            console.log(JSON.stringify(resp));
            updateB2CWithCID(StatusType.ERROR, JSON.stringify(resp), ConversationID);
        }
        res.status(200).end();
    } catch (e) {
        console.error('error b2c callback', e);
    }
}
/**
 *
 * @param {*} req
 * @param {*} res
 */

function b2bCallbackResults(req, res) {
    try {
        const resp = req.body.Result;
        const ConversationID = resp.ConversationID;
        console.log('b2bcallback', ConversationID);
        if (resp.ResultCode === 0 || resp.ResultCode === "0") {
            console.log(JSON.stringify(resp));
            updateB2CWithCID(StatusType.SUCCESS, JSON.stringify(resp), ConversationID);
        } else {
            console.log(JSON.stringify(resp));
            updateB2CWithCID(StatusType.ERROR, JSON.stringify(resp), ConversationID);
        }
        res.status(200).end();
    } catch (e) {
        console.error('error b2c callback', e);
    }
}
/**
 *
 * @param {*} req
 * @param {*} res
 */
function reversalCallbackResults(req, res) {
    try {
        console.log('reversalcallback',req.body);
        const resp = req.body.Result;
        const ConversationID = resp.ConversationID;
        console.log('reversalcallback',ConversationID);
        if (resp.ResultCode === 0 || resp.ResultCode === "0") {
            console.log(JSON.stringify(resp));
            updateB2CWithCID(StatusType.SUCCESS, JSON.stringify(resp), ConversationID);
        } else {
            console.log(JSON.stringify(resp));
            updateB2CWithCID(StatusType.ERROR, JSON.stringify(resp), ConversationID);
        }
        res.status(200).end();
    } catch (e) {
        console.error('error reversal callback',e);
    }
}
/**
 *
 * @param {*} req
 * @param {*} res
 */
function reversalCall(req, res) {
    var b2cRev= require('../../mpesa/b2b');
    try {
        var body  = req.body;
        b2cRev.reverseTransaction(body.ref,body.code,body.amount,(err,resp)=>{
            res.status(200).end(JSON.stringify(resp));
        })
    } catch (e) {
        console.error('error reversal callback',e);
    }
}
/**
 *
 * @param {*} req
 * @param {*} res
 */
function c2bCallbackResults(req, res) {
    var results = req.body;
    try {
        results = JSON.parse(results)
        // eslint-disable-next-line no-empty
    } catch (e) {

    }
    console.log('c2b results', results);
    const resp = results.Body.stkCallback;
    if (parseInt(resp.ResultCode) === 1032) {
        updateC2BWithCheckoutID(StatusType.ERROR, JSON.stringify(results), resp.CheckoutRequestID);
        createTransactionLog(StatusType.ERROR, results);
    }
    if (parseInt(resp.ResultCode) === 0) {
        updateC2BWithCheckoutID(StatusType.SUCCESS, JSON.stringify(results), resp.CheckoutRequestID);
        createTransactionLog(StatusType.SUCCESS, results);
    } else {
        updateC2BWithCheckoutID(StatusType.ERROR, JSON.stringify(results), resp.CheckoutRequestID);
        createTransactionLog(StatusType.ERROR, results);
    }

}
function jengaCallbackResults(req, res) {
    var results = req.body;
    try {
        results = JSON.parse(results)
        // eslint-disable-next-line no-empty
    } catch (e) {

    }
    if(results.transaction==undefined)
        return res.status(500).end()
    if(results.transaction.amount==undefined)
       return  res.status(500).end()
    if(results.bank==undefined)
      return  res.status(500).end()
    mysql.insertRecord("boya", "transaction_alerts",
        {
            status:0,
            payment_type:2,
            amount:results.transaction.amount,
            reference:results.bank.reference,
            account:results.transaction.reference,
            payload:JSON.stringify(results),
        }, (result, err) => {
            if (err) {
                console.log("error saving callback jengaCallbackResults alert");
                res.status(200).end()
            } else {
                try {
                    depositAlert({
                        ref:results.bank.reference,
                        amount:results.transaction.amount,
                        status:0,
                        payment_type:results.bank.transactionType=="C"? "CREDIT":"DEBIT",
                        customer_name:"N/A",
                        description:results.transaction.additionalInfo,
                    })
                }catch (e) {
                    console.log('slack deposit alert error>>',e)
                }
                console.log("[jengaCallbackResults] B2C MPESA response", results);
                res.status(200).end()
            }
        });


}
function confirmTopup(req, res) {
    var results = req.body;
    try {
        results = JSON.parse(results)
        // eslint-disable-next-line no-empty
    } catch (e) {

    }
    if(results.reference==undefined || results.reference=='' || results.reference.trim().length<5)
        return res.status(500).end(JSON.stringify({"error":"Missing reference/Invalid Reference"}))
    mysql.runQuery(`select * from boya.transaction_alerts where status = 0`,
        (row)=>{
            return {payload:JSON.parse(row.payload),payment_type:row.payment_type,
                amount:row.amount,reference:row.reference}
        },(results,err)=>{
            if(results.length){
                //loop through available records to check for reference
                let payment = undefined
                const ref = req.body.reference;
                results.forEach((record)=>{
                    if(record.payload.transaction && record.payload.transaction.additionalInfo){
                        if(record.payload.transaction.additionalInfo.toString().toLowerCase().includes(ref.toString().toLowerCase())){
                            payment = {amount:record.amount,reference:ref,tid:record.reference};
                        }
                    }
                    if(record.reference == ref){
                        payment = {amount:record.amount,reference:ref,tid:record.reference};
                    }
                })
                if(payment==undefined)
                    return res.status(500).end("none")
                return res.status(200).json(payment)
            }else{
                return res.status(500).end("none")
            }
        })


}
function updateTopup(req, res) {
    const tId = req.body.tid
    if(tId==undefined)
        return res.status(500).end(JSON.stringify({"error":"Missing tid"}))
    mysql.updateRecord("boya", "transaction_alerts",
        [1, tId], ["status", "reference"], (result, err) => {
            if (err) {
                console.log("error updating transaction_alerts");
                return res.status(500).json({status:1,"error":err})
            } else {
                console.log("[updateTopup] Okay");
                return res.status(200).json({status:0})
            }
        });
}
async function linkVirtualCardInfo(res,cardObj){
    const required = ["employee_id","exp_year","exp_month","last4","number","customer_id","cardHolder","brand","organization","cvc"]
    let error = null
    required.forEach((field)=>{
        if(cardObj[field]==undefined)
            error = `Missing field ${field}`
    })
    if(error!=null)
        return res.status(501).json({status:1,message:error,data:[]})
    console.log('got card',cardObj);
    await createVerifiedCard(cardObj["customer_id"],cardObj)
    const token = await getUserToken(cardObj["customer_id"]);
    if(token!=null && token !==undefined){
        // const name = await db.getTransactionCustomerName(cardObj["customer_id"])
        await sendPushNotification(cardObj["customer_id"],token,
            {title:`You have a new ${cardObj["brand"]} card`,
                organization:cardObj["organization"],
                body:`${cardObj["organization"]} has issued you a new card for business expenses.`})
        //TODO send email to alert user
    }
    return res.status(200).json({status:0,message:"card linked",data:[]})
}
async function issueFwCard(res,cardObj){
    const required = ["employee_id","cardHolder","organization","amount","customer_id"]
    let error = null
    required.forEach((field)=>{
        if(cardObj[field]==undefined)
            error = `Missing field ${field}`
    })
    if(error!=null)
        return res.status(400).json({status:1,message:error,data:[]})
    console.log('got card',cardObj);
    const data = await issueVirtualCard(cardObj["cardHolder"],cardObj["amount"])
    if(data.status==STATUS_200){
        const card = data.response.data;
        const mask = card.masked_pan.toString();
        card.last4 = mask.substring(mask.length-4)
        card.bin = mask.substring(0,6)
        card.customer_id = cardObj["customer_id"]
        card.expiry = card["expiration"]
        card.cvc = card["cvv"]
        card.brand = card["card_type"]
        card.employee_id = cardObj["employee_id"]
        card.cardHolder = cardObj["cardHolder"]
        card.name = cardObj["cardHolder"]
        const created = await db.linkVirtualCard(card,"FLUTTERWAVE")
        if(created){
            card.number = card["id"]
            card.exp_month = card["expiration"].toString().split("-")[1]
            card.exp_year = card["expiration"].toString().split("-")[0]
            card.organization = cardObj["organization"]
            await linkVirtualCardInfo(res,card)
        }else{
            console.error("Unable to create card on db",card);
            sendSlackAlertMsg(`Unable save virtual card for ${cardObj["organization"]}`)
            return res.status(200).json({status:1,message:'failed to save card on business',data:[]})
        }
    }else{
        //failed to issue card
        return res.status(data.status).json({status:1,message:data.response.message,data:[]})
    }
}
async function blockFWCard(res,cardObj){
    if(cardObj.vcn == undefined){
        return res.status(400).json({status:1,message:'missing id',data:[]})
    }
    const data = await blockVirtualCard(cardObj["vcn"])
    if(data.status!=STATUS_200)
        return res.status(data.status).json({status:1,message:data.response.message,data:[]})
    res.json({"status":0,"message":"Done"})
}
async function UnBlockFWCard(res,cardObj){
    if(cardObj.vcn == undefined){
        return res.status(400).json({status:1,message:'missing id',data:[]})
    }
    const data = await unBlockVirtualCard(cardObj["vcn"])
    if(data.status!=STATUS_200)
        return res.status(data.status).json({status:1,message:data.response.message,data:[]})
    res.json({"status":0,"message":"Done"})
}
async function cardOnFWCard(res,cardObj){
    if(cardObj.vcn == undefined){
        return res.status(400).json({status:1,message:'missing id',data:[]})
    }
    const data = await fetchVirtualCardDetails(cardObj["vcn"])

    if(data.status!=STATUS_200)
        return res.status(data.status).json({status:1,message:data.response.message,data:[]})
    res.json({"status":0,"message":"Card Details","data":data.response.data})
}
async function deleteVirtualCard(res,cardObj){
    if(cardObj.vcn == undefined){
        return res.status(400).json({status:1,message:'missing id',data:[]})
    }
    const data = await terminateVirtualCard(cardObj["vcn"])
    if(data.status!=STATUS_200)
        return res.status(data.status).json({status:1,message:data.response.message,data:[]})
    res.json({"status":0,"message":"Card Deleted","data":data.response.data})
}
async function syncCardTransactions(){
    const accounts = await db.getActiveCorps();
    async function runBiZCardTransactions(account,cards){
        for (const card of cards) {
            const customer = await db.getCustomerIdByEmpId(card["employee_id"])
            getVirtualCardTransactions(card.vcn,
                LocalDate.now().minusDays(3).toString(),
                LocalDate.now().plusDays(1).toString(),0)
                .then((resp)=>{
                    if(resp.status==200){
                        const transactions = resp.response.data;
                        console.log("card online transactions",transactions.length)
                        for (const transaction of transactions) {
                            if(transaction["indicator"] == "D" && transaction["status"] == "Successful"){
                                const t = {
                                    amount:transaction["amount"] * process.env.USD_KES_EXCHANGE_RATE,
                                    fees:transaction["fee"]* process.env.USD_KES_EXCHANGE_RATE,
                                    description:transaction["gateway_reference_details"],
                                    timestamp:transaction["created_at"],
                                    status:0,
                                    source:card.vcn,
                                    ref:transaction.id,
                                    provider1:"FLUTTERWAVE",
                                    provider_ref:transaction["reference"],
                                    chargeId:transaction["reference"],
                                    txRef:transaction["id"],
                                    provider_response:JSON.stringify(transaction),
                                    group: Groups.online,
                                    initial: 0,
                                    receiver: transaction["gateway_reference_details"],
                                    customer_id: customer.customer_id,
                                    phone: customer.phone,
                                    currency: transaction["currency"],
                                    is_business: 1,
                                    business_id: account.business_id,
                                }
                                db.getTransactionByRef(transaction.id).then(result=>{
                                    if(result.status == StatusType.SUCCESS && result.response.length>0){
                                        console.log("online transaction already logged,will update");
                                        db.updateTransaction(t.ref,t)
                                    }else if(result.status == StatusType.SUCCESS && result.response.length==0){
                                        console.log("online transaction not logged,creating one");
                                        db.createTransaction(t).then(result2=>{
                                            if(result2){
                                                db.debitCorporateWalletLedger(card["employee_id"],
                                                    (transaction["amount"] * process.env.USD_KES_EXCHANGE_RATE),t
                                                    )
                                                updateTransactionResultToFirestore(t["customer_id"],t["ref"],t)
                                            }
                                        })
                                    }
                                })
                            }

                        }
                    }
            })
        }
    }
    for (const account of accounts) {
        const cards = await db.getCorporateFwCards(account["business_id"])
        runBiZCardTransactions(account,cards);
    }
}
async function getCorporateGroups(res,payload){
    if(payload.eid == undefined){
        return res.status(400).json({status:1,message:'missing id',data:[]})
    }
    const data = await db.getCorporateGroups(payload.eid)
    if(data.status!=StatusType.SUCCESS)
        return res.status(400).json({status:1,message:data.data.errorMessage,data:[]})
    res.json(data)
}
async function tagCorporateExpense(res,payload){
    if(payload.eid == undefined){
        return res.status(400).json({status:1,message:'missing id',data:[]})
    }
    if(payload.category == undefined){
        return res.status(400).json({status:1,message:'missing category',data:[]})
    }
    if(payload.group == undefined){
        return res.status(400).json({status:1,message:'missing group',data:[]})
    }
    const data = await db.updateExpenseCategory(payload.eid,payload.category,payload.group,payload.ref)
    if(!data)
        return res.status(400).json({status:1,message:data.data.errorMessage,data:[]})
    res.json({"status":0,"message":"success"})
}
async function addCorporateExpenseReceipt(res,payload){
    if(payload.eid == undefined){
        return res.status(400).json({status:1,message:'missing id',data:[]})
    }
    if(payload.receiptUrl == undefined){
        return res.status(400).json({status:1,message:'missing receiptUrl',data:[]})
    }
    if(payload.ref == undefined){
        return res.status(400).json({status:1,message:'missing group',data:[]})
    }
    const data = await db.updateExpenseReciptUrl(payload.eid,payload.receiptUrl,payload.ref,payload.notes?payload.notes:"")
    if(!data)
        return res.status(400).json({status:1,message:data.data.errorMessage,data:[]})
    res.json({"status":0,"message":"success"})
}
/**
 *
 * @param {*} req
 * @param {*} res
 */
function mpesaC2BResultCallbackRoute(req, res) {
    return c2bCallbackResults(req, res)

}
/**
 *
 * @param {*} chargeId
 * @param {*} callback
 */

// async function initiateRefund(chargeId, callback) {

//     await initiateRefundForVerifiedCharge(chargeId, callback);
// }
/**
 *
 * @param {*} req
 * @param {*} res
 */
async function createCustomerAccount(req, res) {
    let customerObj = transUtil.decrypt(req.body.payload);
    // let customerObj = (req);
    try {
        customerObj = JSON.parse(`${customerObj}`);
    } catch (e) {
        console.log(customerObj);
    }
    if (customerObj.email === undefined) {
        return res.status(500)
            .end(JSON.stringify({'error': 'missing email'}));
    }
    if (customerObj.uid === undefined) {
        return res.status(500)
            .end(JSON.stringify({'error': 'missing uid'}));
    }
    if (customerObj.phone === undefined) {
        return res.status(500)
            .end(JSON.stringify({'error': 'missing phone'}));
    }
    if (customerObj.name === undefined) {
        return res.status(500)
            .end(JSON.stringify({'error': 'missing name'}));
    }
    if (customerObj.sharecode === undefined) {
        return res.status(500)
            .end(JSON.stringify({'error': 'missing sharecode'}));
    }
    //create stripe account
    createStripeCustomer(customerObj.email, async (id) => {
        console.log('got stripe id', id);
        customerObj.stripe_cust_id = id;
        if (customerObj.stripe_cust_id === undefined && customerObj.stripe_cust_id !== '') {
            return res.status(500)
                .end(JSON.stringify({'error': 'missing cust_id'}));
        }
        const added = await db.createCustomer(customerObj);
        console.log(added)
        if (added) {
            dumpUserToFirestore(customerObj);
            const randomAlphaNumeric = await randomRef()
            const account_number = `W-${randomAlphaNumeric.toUpperCase()}`

            const walletObj = {
                customer_id: customerObj.uid,
                account_number: account_number,
                debit: 0,
                credit: 0,
                amount: 0,
                fees: 0
            }
        await db.createWallet(walletObj);
            return res.status(200).end();
        } else {
            return res.status(500)
                .end(JSON.stringify({'error': 'already exists'}));
        }
    });


}
/**
 *
 * @param {*} req
 * @param {*} res
 */
async function submitManualKycRquest(req, res) {
    let customerObj = transUtil.decrypt(req.body.payload);
    customerObj = JSON.parse(customerObj);
    try {
        customerObj = JSON.parse(`${customerObj}`);
    } catch (e) {
        console.log(customerObj);
    }
    if (customerObj.uid === undefined) {
        return res.status(500)
            .end(JSON.stringify({'error': 'missing id'}));
    }
    const customerPhoto = await db.fetchUserPhoto(customerObj.uid)
    try {
        const updated = await db.createKycReviewRequest(customerObj.uid,customerPhoto);
        if (updated) {
            sendKycAlert(customerPhoto)
            return res.status(200).end();
        } else {
            return res.status(500)
                .end(JSON.stringify({'error': 'update failed'}));
        }
    }catch (e) {
        sendKycAlert(customerPhoto)
        return res.status(200).end();
    }

}
/**
 *
 * @param {*} req
 * @param {*} res
 */
async function hasPendingKycReview(req,res){
    let customerObj = transUtil.decrypt(req.body.payload);
    customerObj = JSON.parse(customerObj);
    try {
        customerObj = JSON.parse(`${customerObj}`);
    } catch (e) {
        console.log(customerObj);
    }
    if (customerObj.uid === undefined) {
        return res.status(500)
            .end(JSON.stringify({'error': 'missing id'}));
    }
    const hasPendingReview = await db.hasPendingKycReview(customerObj.uid);
    if (hasPendingReview) {
        return res.status(200).end();
    } else {
        return res.status(500)
            .end(JSON.stringify({'error': 'no pending kyc'}));
    }
}
/**
 *
 * @param {*} req
 * @param {*} res
 */
async function updateProfile(req,res){
    let customerObj = transUtil.decrypt(req.body.payload);
    customerObj = JSON.parse(customerObj);
    // let customerObj = (req.body.payload);
    try {
        customerObj = JSON.parse(`${customerObj}`);
    } catch (e) {
        console.log(customerObj);
    }
    if (customerObj.phone === undefined) {
        return res.status(500)
            .end(JSON.stringify({'error': 'missing phone'}));
    }
    if (customerObj.uid === undefined) {
        return res.status(500)
            .end(JSON.stringify({'error': 'missing id'}));
    }
    if (customerObj.email === undefined) {
        return res.status(500)
            .end(JSON.stringify({'error': 'missing email'}));
    }
    if (customerObj.name === undefined) {
        return res.status(500)
            .end(JSON.stringify({'error': 'missing name'}));
    }

    const customer = {
        phone:customerObj.phone,
        email:customerObj.email,
        name:customerObj.name,
    };
    if (customerObj.national_id != undefined && customerObj.national_id!="") {
        customer.national_id = customerObj.national_id;
    }
    if (customerObj.national_id_photo != undefined && customerObj.national_id_photo!="") {
        customer.national_id_photo = customerObj.national_id_photo;
    }
    if (customerObj.photo != undefined && customerObj.photo!="") {
        customer.photo = customerObj.photo;
    }
    const updated = await db.updateCustomerProfile(customer,customerObj.uid);
    if (updated) {
        return res.status(200).end();
    } else {
        return res.status(500)
            .end(JSON.stringify({'error': 'update failed'}));
    }
}
/**
 *
 * @param {*} req
 * @param {*} res
 */
async function schedulePayment(req, res) {
    let body = req.body;
    try{
        body = JSON.parse(body);
    }catch (e) {

    }
    if (body.frequency && body.ref && body.customer_id) {
        const transaction = (await db.getTransactionByRef(body.ref)).response;
        if(transaction.customer_id.trim() == body.customer_id.trim()){
            let periodic = 1;
            switch(body.frequency){
                case "weekly":
                    periodic = 2;
                    break;
                case "daily":
                    periodic = 3;
                    break;
            }
            const saved = await db.addRecurringPayment(periodic,transaction)
            if(saved == StatusType.SUCCESS){
                return res.status(200).end();
            }else if(saved == StatusType.ERROR){
                return res.status(500).end();
            }
        }else{
            console.log('transaction ref',transaction.ref);
            console.log('transaction customer_id',transaction.customer_id);
            console.error("scheduling client is not authorised. missing customer")
            return res.status(500)
                .end(JSON.stringify({'error': 'unverified account'}));
        }

    } else {
        return res.status(500)
            .end(JSON.stringify({'error': 'missing params'}));
    }
}
/**
 *
 * @param {*} req
 * @param {*} res
 */
async function isSchedulePayment(req, res) {
    const body = req.query;
    if (body.ref) {
        const frequency = await db.getRecurringPayment(body.ref);
        let periodic = "none";
        switch(frequency){
            case 2:
                periodic = "weekly";
                break;
            case 3:
                periodic = "daily";
                break;
        }
            return res.status(200).end(periodic);
    } else {
        return res.status(500)
            .end(JSON.stringify({'error': 'missing params'}));
    }
}
/**
 *
 * @param {*} req
 * @param {*} res
 */
async function unSchedulePayment(req, res) {
    const body = req.query;
    if (body.ref) {
        const status = await db.stopRecurringPayment(body.ref);
        return res.status(status==StatusType.SUCCESS?200:500).end();
    } else {
        return res.status(500)
            .end(JSON.stringify({'error': 'missing params'}));
    }
}
/**
 *
 * @param {*} customer_id
 */
 function verifySelfie(customer_id){
    return new Promise(async resolve => {
        const userPhoto = await db.fetchUserPhoto(customer_id);
        const jobId = `${generateRandomPin()}${Date.now().toString().substr(8)}`;
        const base64Img = await getBase64image(userPhoto.photo)
            const response = await compareSelfie(`${jobId}-${customer_id}`,
                jobId,userPhoto.national_id,base64Img,userPhoto.legal_data.IDType);
            if(response.error!=undefined){
                return resolve(response);
            }
            // if(!response.success){
            //     return res.status(500).json(response);
            // }
            return resolve(response);
    });

}
/**
 *
 * @param {*} customer_id
 */
function matchIdWithSelfie(customer_id){
    return new Promise(async resolve => {
        const userPhoto = await db.fetchUserPhoto(customer_id);
        const jobId = `${generateRandomPin()}${Date.now().toString().substr(8)}`;
        const base64SelfiePhoto = await getBase64image(userPhoto.photo)
        const base64IDCardPhoto = await getBase64image(userPhoto.national_id_photo)
            const response = await verifyNationalIdPhotoAndSelfie(`${jobId}-${customer_id}`,
                customer_id,base64IDCardPhoto,base64SelfiePhoto,userPhoto.national_id,userPhoto.legal_data.IDType);
            if(response.error!=undefined){
                return resolve(response);
            }
            // if(!response.success){
            //     return res.status(500).json(response);
            // }
            return resolve(response);
    });

}
/**
 *
 * @param {*} res
 * @param {*} req
 */
async function comparePhotos(res,req){
    let userObj = transUtil.decrypt(req.body.payload);
    userObj = JSON.parse(userObj);
    if (userObj.customer_id === undefined) {
        console.log("missing customer_id");
        return res.status(500)
            .end(JSON.stringify({'error': 'missing customer_id'}));
    }
        const response = await matchIdWithSelfie(userObj.customer_id);
        if(response.error!=undefined){
            return res.status(500).json(response);
        }
        await db.saveSmileVericationStatus(userObj.customer_id,response);
    const confidenceValue = response["ConfidenceValue"];
    const SmileJobID = response["SmileJobID"];
    await db.updateCustomerImageVerification(userObj.customer_id, response);
    await updateUserInfo(userObj.customer_id,{confidenceValue:confidenceValue})
    await updateUserSelfieInfo(userObj.customer_id, {
        confidenceValue:confidenceValue,
        SmileJobID:SmileJobID
    });
    return res.json(response);
}
/**
 *
 * @param {*} uid
 */
async function clearConfidence(uid){
    await forceUserKyc(uid);
}
/**
 *
 * @param {*} res
 * @param {*} req
 */

async function matchPhoto(res,req){
    let userObj = transUtil.decrypt(req.body.payload);
    userObj = JSON.parse(userObj);
    if (userObj.customer_id === undefined) {
        console.log("missing customer_id");
        return res.status(500)
            .end(JSON.stringify({'error': 'missing customer_id'}));
    }
    const userPhoto = await db.fetchUserPhoto(userObj.customer_id);
    // console.log('selfie Obj',userPhoto);
    if(userPhoto.national_id == undefined){
        console.log("missing national_id");
        return res.status(500)
            .end(JSON.stringify({'error': 'missing national id/passport no'}));
    }
    if(userPhoto.photo == undefined || userPhoto.photo=="" || userPhoto.photo == null){
        console.log("missing selfie");
        return res.status(500)
            .end(JSON.stringify({'error': 'missing selfie'}));
    }
    if(userPhoto.legal_data == undefined){
        console.log("missing legal_data");
        return res.status(500)
            .end(JSON.stringify({'error': 'Id not verified yet'}));
    }
    if(userPhoto!=undefined){
        const response = await verifySelfie(userObj.customer_id);
        if(response.error!=undefined){
            return res.status(500).json(response);
        }
        await db.saveSmileVericationStatus(userObj.customer_id,response);
        return res.json(response);
    }
    return res.status(500).json({errorMessage:"Invalid photo"});
}

/**
 *
 * @param {*} res
 * @param {*} userObjCipher
 */
async function verifyID(res,userObjCipher){
    let userObj = transUtil.decrypt(userObjCipher);
    userObj = JSON.parse(userObj);
    if(userObj.national_id==undefined){
        console.log('no national id');
        return res.status(500).send(JSON.stringify({error:"missing national_id"}));
    }
    if(userObj.uid==undefined){
        console.log('no uid');
        return res.status(500).send(JSON.stringify({error:"missing userid"}));
    }
    const national_id = userObj.national_id;
    const uid = userObj.uid;
    const jobId = `${generateRandomPin()}${Date.now().toString().substr(8)}`;
    let response = {error:'unknown cardno'};
    if(national_id.toString().match(KENYA_NATIONAL_ID_REGEX) && national_id.toString().match(/^\d/)){
        console.log("is kenyan national id");
        response = await verifyNationalId(jobId,uid,userObj.national_id,DOC_TYPE.NATIONAL_ID);
    }
   else
    if(national_id.toString().match(KENYA_PASSPORT_REGEX)!=null && national_id.toString().match(KENYA_PASSPORT_REGEX).length){
        console.log("is kenyan passport");
        response = await verifyNationalId(jobId,uid,userObj.national_id,DOC_TYPE.PASSPORT);
    }
    else
    if(national_id.toString().match(KENYA_ALIEN_CARD_REGEX)){
        console.log("is kenyan alien Card ID");
        response = await verifyNationalId(jobId,uid,userObj.national_id,DOC_TYPE.ALIEN_CARD);
    }
    if(response.error!=undefined || response.Actions.Verify_ID_Number == 'Not Verified'){
        return res.status(500).json(response);
    }
    const legalData = { FullName:response.FullName,
        Country:response.Country,
        ExpirationDate:response.ExpirationDate,
        Photo:response.Photo,
        Surname:response.FullData.Surname,
        First_Name:response.FullData.First_Name,
        Gender:response.FullData.Gender,
        Date_of_Birth:response.FullData.Date_of_Birth,
        Address:response.Address,
        IDType:response.IDType,
        national_id:national_id
    };
    await db.updateCustomerLegalData(uid,legalData);
    await updateUserLegalInfo(uid,legalData);
    return res.json(legalData);
}
/**
 *
 * @param {*} res
 * @param {*} cardObj
 */
async function shouldUpdateCvv(res,cardObj){
    console.log('source check ',cardObj.source);
    const needsUpdate = await db.shouldUpdateCvC(cardObj.customer_id,cardObj.source);
    if(needsUpdate){
        console.log('should update cvv');
        const card = await db.getSourceCard(cardObj.source)
        const vgscard = await db.getCustomerCardAbs(cardObj.customer_id,cardObj.source)

        res.status(200).end(JSON.stringify((card!=undefined?card:vgscard)))
    }else{
        res.status(400).end()
    }
}
/**
 *
 * @param {*} res
 * @param {*} cardObj
 */
async function updateCardCvv(res,cardObj){
    let mcard = cardObj;
    let customer_id =''
    console.log('got cvv token for card',cardObj);
    try{
        mcard = JSON.parse(cardObj);
    }catch (e) {

    }
    customer_id = mcard.uid;
    if(mcard.card_data)
        mcard.cvc = mcard.card_data.cardCvc;
    if(mcard.cardCvc){
        mcard.cvc = mcard.cardCvc;
    }
    const card = {
        cvv:mcard.cvc,
        last4:mcard.last4,
        exp_month:mcard.exp_month,
        exp_year:mcard.exp_year,
    };
   const result = await db.updateCardCvvToken(card,customer_id)
    if(result == StatusType.SUCCESS){
        return res.json({"status":"success"});
    }else{
        res.status(500).end({"status":"Internal Error, try again later",error:"Internal Error, try again later"});
    }

}
/**
 *
 * @param {*} res
 * @param {*} cardObj
 */
async function saveCardInfo(res,cardObj){
    try{
        let mcard = cardObj;
        let customer_id =''
        let bin =''
        let brand =''
        console.log('got a new card',cardObj);
        try{
            mcard = JSON.parse(cardObj);

        }catch (e) {

        }
        customer_id = mcard.uid;
        if(mcard.bin){
            bin = mcard.bin;
        }
        if(mcard.brand){
            brand = mcard.brand;
        }else{
            card.brand = 'Card'
        }
        if(mcard.card_data)
            mcard = mcard.card_data;
        const card = {
            cvc:mcard.cardCvc,
            number:mcard.cardNumber,
            cardHolder:mcard.personal_data?mcard.personal_data.cardHolder:mcard.cardHolder,
            bin:bin,
            brand:brand
        };
        if(mcard.personal_data !=undefined && mcard.personal_data.secret!==undefined){

            mcard.expiration = mcard.personal_data.secret.expDate
        }
        if(mcard.expiration){
            card.exp_month = mcard.expiration.toString().split("/")[0];
            card.exp_year = mcard.expiration.toString().split("/")[1];
        }

        if(mcard.last4){
            card.last4 = mcard.last4;
        }else{
            card.last4 = card.number.toString().substring(card.number.length-4)
        }
        const isLinkedToDiffUser = await db.cardExistsUnderDifffCustomer(customer_id,card.number)
        if(isLinkedToDiffUser){
            console.log("Invalid Card. This card already linked to another user")
            try{
                sendSms(`Card already linked to user ${customer_id} cardno ${card.number}`,
                    "0718555832",(resp)=>{})
            }catch (e) {

            }
            res.status(500).end({"status":"card already linked to another user",error:"card already linked to another user"});
        }
        // let token = await getCardName(card);
        const card_added = await db.addCustomerCard(card,customer_id)
        if(card_added == StatusType.SUCCESS){
            await createPendingCardVerification(customer_id,card)
            return res.json({"status":"success"});
        }
        // if(token.id){
        //     card["brand"] = token["card"]["brand"]
        //     card["last4"] = token["card"]["last4"]
        //     let card_added = await db.addCustomerCard(card,customer_id)
        //     if(card_added == StatusType.SUCCESS){
        //         await createPendingCardVerification(customer_id,card)
        //         return res.json({"status":"success"});
        //     }
        //     res.status(500).end({"status":"failed to add card","message":"failed to add card"});
        // }else if(token.error){
        //         res.status(500).end(token.error.message);
        // }
        // else{
        //     res.status(500).end({"status":"failed to process card"});
        // }

    }catch (e) {
        console.log(e)
        res.status(500).json({"status":"success"});
    }


}
/**
 *
 * @param {*} res
 * @param {*} npsObj
 */
async function postNps(res,npsObj) {
    console.log("received npsObj",npsObj)
    if(npsObj.phone==undefined){
        console.log('no phone ');
        return res.status(500).send(JSON.stringify({error:"missing phone"}));
    }
    if(npsObj.tillno==undefined){
        console.log('no tillno');
        return res.status(500).send(JSON.stringify({error:"missing tillno"}));
    }
    if(npsObj.score==undefined){
        console.log('no score');
        return res.status(500).send(JSON.stringify({error:"missing score"}));
    }
    if(npsObj.description==undefined){
        console.log('no description');
        return res.status(500).send(JSON.stringify({error:"missing description"}));
    }
    await sendAjuaInfo(npsObj.tillno,npsObj.score,npsObj.description,npsObj.phone);
    res.json({});
}
/**
 *
 */
async function updateTransactionRefs(){
    const transactions = await db.getAllTransactionsSince("2020-06-01");
    console.log('got transactions',transactions.length);

    transactions.forEach((t)=>{
        if(t.provider1_resp !=null && t.provider1_resp["id"]){
            console.log("charge id ",t.provider1_resp["id"]);
            db.updateTransactionWithid(t.id,{
                "chargeId":t.provider1_resp["id"]
            })
        }
        if(t.group === 'bills' || t.group === 'mpesa' || t.group === 'tills'){
            const ref = extractMpesaRef(t.provider2_resp);
            if(ref!=undefined){
                console.log('updating transaction ref',ref);
                db.updateTransactionWithid(t.id,{
                    "provider_ref":ref
                })
            }

        }else if(t.group === 'airtime'){
            const ref = extractAirtimeRef(t.provider2_resp);
            if(ref!=undefined){
                console.log('updating transaction ref',ref);
                db.updateTransactionWithid(t.id,{
                    "provider_ref":ref
                })
            }
        }

    });

}
/**
 *
 * @param {*} mpesaResponseObj
 */

function extractMpesaRef(mpesaResponseObj){
    if(mpesaResponseObj==null){
        return  undefined;
    }
    if(mpesaResponseObj.TransactionID){
        return mpesaResponseObj.TransactionID;
    }
    if(mpesaResponseObj.ipay_reference){
        return mpesaResponseObj.ipay_reference;
    }
    if(mpesaResponseObj.Result && mpesaResponseObj.Result.TransactionID){
        // console.log("found a workpay response",mpesaResponseObj.Result.TransactionID);
        return mpesaResponseObj.Result.TransactionID;
    }
    return undefined;
}
/**
 *
 * @param {*} resp
 */
function extractAirtimeRef(resp){
    return resp.responses[0].requestId
}
/**
 *
 * @param {*} transaction
 */
async function processRefund(transaction){
    let resp = false;
    switch (transaction["provider1"]) {
        case PSPs.FLUTTERWAVE:
            resp = (await refundChargeFW(transaction.chargeId)).status == "success";
            console.log("sent fw refund");
            break;
        default:
            //do nothing
            break;

    }
    if(resp){
        sendSms(`${transaction["name"].toString().split(" ")[0]}, we have refunded you Kes${transaction.amount+transaction.fees}. The transaction you did at ${transaction.timestamp} was not successful. Sorry for the inconvenience.`)
    }else{
        sendSlackAlertMsg(`Failed to refund transaction ${transaction.ref} for ${transaction.name}. Please refund manually`);
    }
}
/**
 * @param body {requestId:<vendorReference>,status:<"Failed"/"Success">}
 * */
async function processAfAirtimeCallback(body) {
        console.log("?>>>>>>>>>>.af airtime callback",body);
        if(body.status == "Failed"){
            await db.updateTransactionWithProviderRef(body.requestId,
                {status:2,refund_reason:"Failed to send airtime"});
            console.log("updated airtime transaction as failed");
            const transaction = await db.getTransactionByProviderRef(body.requestId);
            if(transaction != undefined){
                await processRefund(transaction);
            }
        }
}
/**
 *
 * @param {*} smileResp
 */
async function processSmileCallBack(smileResp) {
        console.log("?>>>>>>>>>>.smile says match accepted",smileResp);
        const result = smileResp;
        const uid = result["PartnerParams"].user_id.toString().split("-")[1];
        const confidenceValue = result["ConfidenceValue"];
        const SmileJobID = result["SmileJobID"];
        await db.updateCustomerImageVerification(uid, result);
        await updateUserInfo(uid,{confidenceValue:confidenceValue})
        await updateUserSelfieInfo(uid, {
            confidenceValue:confidenceValue,
            SmileJobID:SmileJobID
        });



}
/**
 *
 * @param {*} smileResp
 */
 async function processSmileCallBackStatus(smileResp) {
    console.log("?>>>>>>>>>>.smile status says match accepted",smileResp);
    const result = smileResp;
    const uid = result["PartnerParams"].user_id.toString().split("-")[1];
    const confidenceValue = result["ConfidenceValue"];
    const SmileJobID = result["SmileJobID"];
    await db.updateCustomerImageVerification(uid, result);
    await updateUserInfo(uid,{confidenceValue:confidenceValue})
    await updateUserSelfieInfo(uid, {
        confidenceValue:confidenceValue,
        SmileJobID:SmileJobID
    });



}
/**
 *
 * @param {*} req
 * @param {*} res
 */
async function getReviews(req,res){
    if(req.query.till){
        const scores= await getMerchantReviews(req.query.till);
        console.log('got tills',scores.length);
        if(scores.length>0){
            const reviews = [];
            for (const score of scores) {
                const name = await db.getTransactionCustomerName(score.uid)
                const review = Object.assign(score,{customer:name.split(" ")[0]});
                reviews.push(review)
                if(reviews.length>=scores.length)
                    res.json(reviews);
            }
        }else{
            res.json([]);
        }


    }else{
        return res.status(500).send(JSON.stringify({error:"missing till"}));
    }
}
/**
 *
 * @param {*} req
 * @param {*} res
 */
async function sendOutboundSms(req,res){
    if(req.body.phone && req.body.msg && req.body.key == "A_7d34c531e60c2037b739b63b695652c8"){
          sendSms(req.body.msg,req.body.phone,(resp)=>{
            res.json(resp);
        });
    }else{
        return res.status(500).send(JSON.stringify({error:"missing phone/message"}));
    }
}
/**
 *
 * @param {*} req
 * @param {*} res
 */
async function doMerchantOtp(req,res){
    if(req.body.mid && req.body.key == "A_7d34c531e60c2037b739b63b695652c8"){
        resetMerchantPin(req.body.mid,(resp)=>{
            res.json(resp);
        })
    }else{
        return res.status(500).send(JSON.stringify({error:"missing phone/message"}));
    }
}
/**
 *
 * @param {*} merchant_id
 * @param {*} callback
 */
// async function sendFundsToMerchant(merchant_id,callback) {
//     const merchant = await db.getMerchantBusinessById(merchant_id);
//     const reference = `${new Date().getMilliseconds()}${Date.now()}`.substring(0, 12);
//     const bankInfo = await db.getMerchantPrimaryAcc(merchant_id);
//     if (bankInfo.bankCode != undefined) {
//         const balance = await db.getMerchantBalance(merchant_id)
//         const transferFee = (await db.getMerchantSettlementFee(merchant_id)) * balance;
//         const settlementAmount = balance - transferFee;
//         if (settlementAmount < 10) {
//             return callback(500,JSON.stringify({error: "balance below transfer limit"}));
//         }
//         jenga.boyaBalance((accountBal) => {
//             if (accountBal && accountBal["balances"][0].amount > settlementAmount) {
//                 const bankcode = bankInfo.bankCode;
//                 if (bankcode == "68") {
//                     //do internal transfer;
//                     console.log('doing internal transfer');
//                     jenga.sendMoneyToEquityAccount(bankInfo.accno, "68", `${settlementAmount}`, reference, (resp) => {
//                         let result = resp;
//                         try {
//                             result = JSON.parse(result)
//                         } catch (e) {

//                         }
//                         const status = result.status !== undefined ? result.status : result.response_status !== undefined ? result.response_status : result.status;
//                         console.log(result);
//                         if (status === 'SUCCESS') {
//                             db.createSettlement(merchant_id, SETTLEMT_INTERNAL, parseInt(settlementAmount), reference,
//                                 bankInfo.accno, transferFee, bankInfo.bank_id, JSON.stringify(result))
//                                 .then((result) => {
//                                     if (result) {
//                                         callback(200,JSON.stringify({status: "success"}));
//                                         sendSms(`Transaction completed. We have settled Kes ${settlementAmount} to your primary account - ${bankInfo.acc_name}. Ref ${reference}`,
//                                             merchant.phone, (result) => {
//                                             });
//                                         sendSlackAlert({
//                                             ref:reference,
//                                             amount:parseInt(settlementAmount),
//                                             status:0,
//                                             payment_type:"SETTLEMENT",
//                                             customer_name:merchant.business_name,
//                                             description:"Merchant Settlement - INTERNAL TRANSFER",
//                                         })
//                                     } else {
//                                         sendSlackAlertMsg(`Failed to debit merchant account ${merchant_id}, debit ${balance}`)
//                                     }
//                                 });
//                         } else {
//                             return callback(200,JSON.stringify({error: "transfer failed. Please try again later"}));
//                         }
//                     });
//                 } else {
//                     //receivername, accno, bankCode, amount, ref, mobileNumber
//                     jenga.sendMoneyToBankAccWithPesalink(bankInfo.acc_name, bankInfo.accno, bankcode, `${parseInt(settlementAmount)}`, reference, formatJengaPhone(merchant.phone),
//                         (resp) => {
//                             let result = resp;
//                             try {
//                                 result = JSON.parse(result)
//                             } catch (e) {

//                             }
//                             console.log('pesalink response',result);
//                             const status = result.status !== undefined ? result.status : result.response_status !== undefined ? result.response_status : result.status;
//                             if (status === 'SUCCESS') {
//                                 db.createSettlement(merchant_id, SETTLEMT_PESALINK, parseInt(settlementAmount), reference,
//                                     bankInfo.accno, transferFee, bankInfo.bank_id, JSON.stringify(result))
//                                     .then((result) => {
//                                         if (result) {
//                                             callback(200,JSON.stringify({status: "success"}));
//                                             sendSms(`Transaction completed. We have settled Kes ${settlementAmount} to your primary account - ${bankInfo.acc_name}. Ref ${reference}`,
//                                                 merchant.phone, (result) => {
//                                                 });
//                                             sendSlackAlert({
//                                                 ref:reference,
//                                                 amount:parseInt(settlementAmount),
//                                                 status:0,
//                                                 payment_type:"SETTLEMENT",
//                                                 customer_name:merchant.business_name,
//                                                 description:"Merchant Settlement - PESALINK",
//                                             })
//                                         } else {
//                                             sendSlackAlertMsg(`Failed to debit merchant account ${merchant_id}, debit ${balance}`)
//                                         }
//                                     });
//                             } else {
//                                 return callback(500,JSON.stringify({error: "transfer failed. Please try again later"}));
//                             }
//                         });
//                 }
//             } else {
//                 callback(500,JSON.stringify({status: "failed. Unable to complete transfers at the moment"}));
//             }
//         });

//     } else {
//         //missing bank details
//         callback(500,JSON.stringify({status: "failed. No bank details specified"}));
//     }
// }

/**
 *
 * @param {*} req
 * @param {*} res
 */
// async function settleMerchant(req,res){
//     if(req.body.merchant_id!==undefined){
//         const merchant_id = req.body.merchant_id;
//         sendFundsToMerchant(merchant_id,(status,resp)=>{
//             res.status(status).send(resp);
//         })

//     }else{
//         return res.status(500).send(JSON.stringify({error:"missing mid"}));
//     }
// }
/**
 *
 * @param {*} req
 * @param {*} res
 */
async function sendReceiptMail(req,res){
    if(req.query.ref){
        const transaction = (await db.getTransactionByRef(req.query.ref)).response
        transaction.email = await db.getCustomerEmail(transaction.customer_id)
        transaction.email = {"email":transaction.email}
        console.log("to",transaction.email);
        // await mailer.serveReceiptMail(transaction);
        res.json({"status":"success"});
    }else{
        return res.status(500).send(JSON.stringify({error:"missing ref"}));
    }

}
/**
 *
 * @param {*} req
 * @param {*} res
 */

async function getReceiptByRef(req,res){
    if(req.query.ref){
        const transaction = (await db.getTransactionReceiptByRef(req.query.ref)).response
        if(transaction != undefined && transaction != null){
            res.json({
                "status":StatusType.SUCCESS,
                "message":transaction.receipt_link
            });
        }else {
            res.json({"status": StatusType.ERROR});
        }

    }else{
        return res.status(500).send(JSON.stringify({error:"missing ref"}));
    }

}

/**
 *
 * @param {*} req
 * @param {*} res
 */
async function getTillBusiness(req,res){
    if(req.query.tillno){
        const biz = await db.getTransactionBizname(req.query.tillno);
        res.json({name:biz});
    }else{
        return res.status(500).send(JSON.stringify({error:"missing tillno"}));
    }
}
/**
 *
 * @param {*} req
 * @param {*} res
 */
async function doMerchantOtpVerification(req,res){
    if(req.body.mid && req.body.code && req.body.key == "A_7d34c531e60c2037b739b63b695652c8"){
        verifyMerchantCode(req.body.code,req.body.mid,(valid)=>{
            if(valid){
              return  res.json({status:"verified"});
            }
            return  res.status(500).json({status:"invalid OTP"});
        })
    }else{
        return res.status(500).send(JSON.stringify({error:"missing phone/message"}));
    }
}
/**
 *
 * @param {*} req
 * @param {*} res
 */
async function sendPaybillOtp(req,res){

    const callbackUrl = req.body.callbackUrl;
    const isTill = req.body.isTill;
    const mid = req.body.mid;
    const amount = req.body.amount;
    const tillno = req.body.tillno;
    const key = req.body.key;
    if(amount && tillno && key == "A_7d34c531e60c2037b739b63b695652c8"){
        const payload = {
            "Initiator": process.env.MPESA_B2C_USERNAME,
            "SecurityCredential": "",
            "CommandID": isTill?  "BusinessBuyGoods":"BusinessPayBill",
            "SenderIdentifierType": "4",
            "RecieverIdentifierType": isTill?  "2":"4",
            "Amount": amount,
            "PartyA": process.env.MPESA_B2C_SHORTCODE,
            "PartyB": tillno,
            "AccountReference": 'none',
            "Remarks": `OTP-${mid}`,
            "QueueTimeOutURL": `${process.env.HOST}/b2bTimeout`,
            "ResultURL": callbackUrl,
        };
         const resp =  await sendFundstoBusiness(payload);
         if(resp.status == StatusType.SUCCESS){
             return  res.json({status:"initiated","ConversationID":resp.provider_response["ConversationID"]});
         }
    }else{
        return res.status(500).send(JSON.stringify({error:"missing isTill/amount/tillno/mid"}));
    }
}
/**
 *
 * @param {*} date
 * @param {*} daysToAdd
 */
function isRepeatToday(date,daysToAdd){
    return LocalDate.parse(dateFormat(date,"yyyy-mm-dd")).plus(daysToAdd).equals(LocalDate.now());
}
/**
 *
 */
async function alertZukuClients() {
    const biller_index = "%zuku%";
    const biller = "ZUKU";
    const zuku = await db.getBillerClients(biller_index);
    for (const customer_id of zuku) {
        const transaction = await db.getLastBillPaid(biller_index, customer_id);
        const date = new Date(transaction.paid_on);
        const today = new Date()
        const daysApart =  (today-date)/86400000;
        if(daysApart > 28){
            const sent= await db.lastBillReminder(customer_id,biller);
            const last_sent= sent.lastSeen;
            const date = new Date(last_sent);
            const today = new Date()
            const daysApart =  (today-date)/86400000;
            if(daysApart>28 || (sent.repeat_after>0 && isRepeatToday(date,sent.repeat_after))){
                console.log('found a zuku candidate ',customer_id);
                const token = await getUserToken(customer_id);
                if(token!=null && token !==undefined){
                    const name = await db.getTransactionCustomerName(customer_id)
                    sendBillPushNotification(customer_id,token,zukuBillMsg(name,transaction.accno,transaction.amount))
                    await db.recordNotification(customer_id,biller,transaction.amount)
                }
            }
        }

    }

}
/**
 *
 */
async function alertKplcPostPaidClients() {
    const biller_index = "%kplc_postpaid%";
    const biller = "kplc_postpaid";
    const elecPostPaid = await db.getBillerClients(biller_index);
    const postpaidPaybill = await db.getBillerClients("%888888%");
    postpaidPaybill.forEach((customer_id)=>{
        if(!elecPostPaid.includes(customer_id)){
            elecPostPaid.push(customer_id);
        }
    })
    for (const customer_id of elecPostPaid) {
        const transaction = await db.getLastBillPaid(biller_index, customer_id);
        const currentBill = await getKplcBill(biller, transaction.accno);
        const date = new Date(transaction.paid_on);
        const today = new Date()
        const daysApart =  (today-date)/86400000;
        if(daysApart > 28){
            const sent= await db.lastBillReminder(customer_id,biller);
            const last_sent= sent.lastSeen;
            const date = new Date(last_sent);
            const today = new Date()
            const daysApart =  (today-date)/86400000;
            if(daysApart>28 || (sent.repeat_after>0 && isRepeatToday(date,sent.repeat_after))){
                console.log('found a kplc_postpaid candidate ',customer_id);
                const token = await getUserToken(customer_id);
                if(token!=null && token !==undefined && currentBill > 200){
                    const name = await db.getTransactionCustomerName(customer_id)
                    sendBillPushNotification(customer_id,token,elecPostPaidMsg(name,currentBill,transaction.accno))
                    await db.recordNotification(customer_id,biller,currentBill)
                }
            }
        }

    }

}

/**
 *
 */
async function alertSafaricomPostPayClients() {
    const biller_index = "%Safaricom Postpay%";
    const biller = "Safaricom Postpay";
    const safPostPay = await db.getBillerClients(biller_index);
    for (const customer_id of safPostPay) {
        const transaction = await db.getLastBillPaid(biller_index, customer_id);
        const date = new Date(transaction.paid_on);
        const today = new Date()
        const daysApart =  (today-date)/86400000;
        if(daysApart > 28){
            const sent= await db.lastBillReminder(customer_id,biller);
            const last_sent= sent.lastSeen;
            const date = new Date(last_sent);
            const today = new Date()
            const daysApart =  (today-date)/86400000;
            if(daysApart>28 || (sent.repeat_after>0 && isRepeatToday(date,sent.repeat_after))){
                console.log(`found a ${biller} candidate `,customer_id);
                const token = await getUserToken(customer_id);
                if(token!=null && token !==undefined){
                    const name = await db.getTransactionCustomerName(customer_id)
                    sendBillPushNotification(customer_id,token,goSafPostPayBillMsg(name,transaction.accno,transaction.amount))
                    await db.recordNotification(customer_id,biller,transaction.amount)
                }
            }
        }

    }
}

/**
 *
 */
async function alertGoTvClients() {
    const biller_index = "%GoTv%";
    const biller = "GoTv";
    const gotvs = await db.getBillerClients(biller_index);
    for (const customer_id of gotvs) {
        const transaction = await db.getLastBillPaid(biller_index, customer_id);
        const date = new Date(transaction.paid_on);
        const today = new Date()
        const daysApart =  (today-date)/86400000;
        if(daysApart > 28){
            const sent= await db.lastBillReminder(customer_id,biller);
            const last_sent= sent.lastSeen;
            const date = new Date(last_sent);
            const today = new Date()
            const daysApart =  (today-date)/86400000;
            if(daysApart>28 || (sent.repeat_after>0 && isRepeatToday(date,sent.repeat_after))){
                console.log(`found a ${biller} candidate `,customer_id);
                const token = await getUserToken(customer_id);
                if(token!=null && token !==undefined){
                    const name = await db.getTransactionCustomerName(customer_id)
                    sendBillPushNotification(customer_id,token,goTvBillMsg(name,transaction.accno,transaction.amount))
                    await db.recordNotification(customer_id,biller,transaction.amount)
                }
            }
        }

    }
}

/**
 *
 */
async function alertDstvClients() {
    const biller_index = "%DSTV%";
    const biller = "DSTV";
    const dstvs = await db.getBillerClients("%DSTV%");
    for (const customer_id of dstvs) {
        const transaction = await db.getLastBillPaid(biller_index, customer_id);
        const date = new Date(transaction.paid_on);
        const today = new Date()
        const daysApart =  (today-date)/86400000;
        if(daysApart > 28){
            const sent= await db.lastBillReminder(customer_id,biller);
            const last_sent= sent.lastSeen;
            const date = new Date(last_sent);
            const today = new Date()
            const daysApart =  (today-date)/86400000;
            if(daysApart>28 || (sent.repeat_after>0 && isRepeatToday(date,sent.repeat_after))){
                console.log(`found a ${biller} candidate `,customer_id);
                const token = await getUserToken(customer_id);
                if(token!=null && token !==undefined){
                    const name = await db.getTransactionCustomerName(customer_id)
                    sendBillPushNotification(customer_id,token,goDstvBillMsg(name,transaction.accno,transaction.amount))
                    await db.recordNotification(customer_id,biller,transaction.amount)
                }
            }
        }

    }
}
/**
 *
 */
async function alertHomeFibreClients() {
    const biller_index = "%Safaricom Home Fibre%";
    const biller = "Safaricom Home Fibre";
    const elecPostPaid = await db.getBillerClients(biller_index);
    const postpaidPaybill = await db.getBillerClients("%150501%");
    postpaidPaybill.forEach((customer_id)=>{
        if(!elecPostPaid.includes(customer_id)){
            elecPostPaid.push(customer_id);
        }
    })
    for (const customer_id of elecPostPaid) {
        const transaction = await db.getLastBillPaid(biller_index, customer_id);
        const date = new Date(transaction.paid_on);
        const today = new Date()
        const daysApart =  (today-date)/86400000;
        if(daysApart > 28){
            const sent= await db.lastBillReminder(customer_id,biller);
            const last_sent= sent.lastSeen;
            const date = new Date(last_sent);
            const today = new Date()
            const daysApart =  (today-date)/86400000;
            if(daysApart>28 || (sent.repeat_after>0 && isRepeatToday(date,sent.repeat_after))){
                console.log('found a home fibre candidate ',customer_id);
                const token = await getUserToken(customer_id);
                if(token!=null && token !==undefined){

                    const name = await db.getTransactionCustomerName(customer_id)
                    sendBillPushNotification(customer_id,token,goHomeFibreBillMsg(name,transaction.accno,transaction.amount))
                    await db.recordNotification(customer_id,biller,transaction.amount)
                }
            }
        }

    }
}

/**
 *
 */

async function settleDailyMerchants(){
    const merchants = await db.getScheduledSettledMerchants(SETTLEMENT_SCHEDULE.DAILY)
    settleMultipleMerchants(merchants);
}

/**
 *
 */
async function settleInstantMerchants() {
    const merchants = await db.getScheduledSettledMerchants(SETTLEMENT_SCHEDULE.INSTANT_PAYOUT)
    settleMultipleMerchants(merchants);
    updateAlgoliaExpense();
    syncCardTransactions();
}
async function updateAlgoliaExpense(){
    const accounts = await db.getActiveCorps();
    for (const account of accounts) {
        const expenses = await db.getCorporateExpenses(account["business_id"])
        await storeExpenseInAlgolia(expenses);
    }

}
/**
 *
 * @param {*} merchants
 */
function settleMultipleMerchants(merchants){
    merchants.forEach((merchant)=>{
        if(parseInt(`${merchant.balance}`)>0){
            sendFundsToMerchant(merchant.merchant_id,(status,resp)=>{
                console.log(`merchant settlement returned status ${status} resp`,resp);
            })
        }else{
            // console.log("nothing to settle for merchant >> ",merchant.business_name)
        }

    })
}

/**
 *
 */
async function settleWeeklyMerchants(){
    const merchants = await db.getScheduledSettledMerchants(SETTLEMENT_SCHEDULE.WEEKLY)
    settleMultipleMerchants(merchants);
}
/**
 *
 */

async function getMonthlyRecurringPayment(){
    const payments = await db.getRecurringPaymentsForFrequency(SCHEDULED_PAYMENT_FREQUENCY.MONTHLY)
        for (const payment of payments) {
            const dateStr = dateFormat(payment.last_payment_date,"yyyy-mm-dd HH:MM")
            const expDate = LocalDateTime.parse(dateStr.split(" " ).join("T")).plusMonths(1)
            const now = LocalDateTime.now()
            const isDue = now.isAfter(expDate);
            if(isDue){
                console.log("got a monthly recurring payment")
                const randomAlphaNumeric = await randomRef()
                const ref = `${payment.group.toString().substring(0, 1).toUpperCase()}-${randomAlphaNumeric.toUpperCase()}`
                const transObj= {
                    customer_id:payment.customer_id,
                    ref:ref,
                    txRef:ref,
                    group:payment.group,
                    phone:payment.phone,
                    amount:payment.amount,
                    source:payment.source,
                    email:payment.email,
                    receiver:payment.receiver,
                    accno:payment.accno,
                    description:payment.description,
                    payment_type:"CARD",
                    initial:0
                }
                // run process transaction
                processNewScheuledPayment(payments.count,payments.last_payment_date,payment.ref,transObj);
            }
        }
}
/**
 *
 */

async function getWeeklyRecurringPayment(){
    const payments = await db.getRecurringPaymentsForFrequency(SCHEDULED_PAYMENT_FREQUENCY.WEEKLY)

        //create transaction object
        for (const payment of payments) {
            const dateandtime = new Date(payment.last_payment_date).getTime()
            const nowdate = new Date().getTime();
            const isDue = (nowdate-dateandtime)>(86400000*7)
            if(isDue){
                console.log("got a weekly recurring payment")
                const randomAlphaNumeric = await randomRef()
                const ref = `${payment.group.toString().substring(0, 1).toUpperCase()}-${randomAlphaNumeric.toUpperCase()}`

                const transObj= {
                    customer_id:payment.customer_id,
                    ref:ref,
                    txRef:ref,
                    group:payment.group,
                    phone:payment.phone,
                    amount:payment.amount,
                    source:payment.source,
                    email:payment.email,
                    receiver:payment.receiver,
                    accno:payment.accno,
                    description:payment.description,
                    payment_type:"CARD",
                    initial:0
                }
                // run process transaction
                processNewScheuledPayment(payments.count,payments.last_payment_date,payment.ref,transObj);
            }
        }


}

/**
 *
 */

async function randomRef(){
    var anysize = 10;//the size of string
    var charset = "abcdefghijklmnopqrstuvwxyz".toUpperCase(); //from where to create
    let result="";
    for( var i=0; i < anysize; i++ )
            result += charset[Math.floor(Math.random() * charset.length)];
    console.log(result);
    return result;
}


/**
 *
 * @param {*} count
 * @param {*} last_payment_date
 * @param {*} oldRef
 * @param {*} transaction
 */
async function processNewScheuledPayment(count,last_payment_date,oldRef,transaction){
    const app = new App(transaction);
    const response=await app.processTransaction();
    const nowdate = dateFormat(new Date(),"yyyy-mm-dd HH:MM:ss");
    if(response.status == StatusType.SUCCESS){
        const paymentData ={
            ref:oldRef,
            last_payment_date: nowdate,
        }
        await db.updateRecurringPayment(paymentData,transaction["customer_id"]);
    }else{
        const name = await db.getTransactionCustomerName(transaction.customer_id)
        // update cron job time to run after 5 hrs
        var moment = require('moment');

        const date = moment(last_payment_date);
        const x = date.add(5, 'h');
        const finalDate = moment(x).formaeat('YYYY-MM-DD HH:mm:ss')
        const noOfCount = count + 1

        if(noOfCount < 3){
            const paymentData ={
                ref:oldRef,
                last_payment_date: finalDate,
                count: noOfCount
            }
            await db.updateRecurringPayment(paymentData,transaction["customer_id"]);

            sendSms(`${name.toString().split(" ")[0]}, your scheduled payment of KES ${transaction.amount} to - ${transaction.receiver} has failed due to this reason: ${response["errorMessage"]}. We will try again after 5 hours.`,
            transaction.phone, (result) => {
            });
            sendSlackAlertMsg(`Failed to run recurring payment Ref: ${oldRef}`);

        }else {
            // delete cron
            const res = await db.deleteRecurringPayment(transaction.ref,transaction.customer_id);
            if(res  == StatusType.SUCCESS){
                sendSms(`${name.toString().split(" ")[0]}, your scheduled payment of KES ${transaction.amount} to - ${transaction.receiver} has failed due to this reason: ${response["errorMessage"]}.The recurring payment has now been removed from Boya.`,
                transaction.phone, (result) => {
                });
                sendSlackAlertMsg(`Failed to run recurring payment Ref- ${oldRef} and it was deleted from database`)
            }else{
                sendSlackAlertMsg(`Failed to remove recurring payment Ref: ${oldRef}`);

            }

        }


    }
}
/**
 *
 * @param {*} req
 * @param {*} res
 */
async function rescheduleBillNotif(req,res){
    const paymentObj = req.body;
    if(paymentObj.customer_id && paymentObj.biller && paymentObj.repeat_after){
        const status = await db.updateRecurringBillNotif(paymentObj);
        if(status == StatusType.SUCCESS){
            res.end(JSON.stringify({"status":"success"}));
        }else{
            res.status(500).end();
        }
    }else{
        res.status(500).end(JSON.stringify({"error":"missing params customer_id/biller/repeat_after"}));
    }

}
/**
 *
 */
async function getDailyRecurringPayment(){
    const payments = await db.getRecurringPaymentsForFrequency(SCHEDULED_PAYMENT_FREQUENCY.DAILY)
        //create transaction object
        for (const payment of payments) {
            const dateandtime = new Date(payment.last_payment_date).getTime()
            const nowdate = new Date().getTime();
            const isDue = (nowdate-dateandtime)>86400000
            if(isDue){
                console.log("got a daily recurring payment")
                const randomAlphaNumeric = await randomRef()
                const ref = `${payment.group.toString().substring(0, 1).toUpperCase()}-${randomAlphaNumeric.toUpperCase()}`

                const transObj= {
                    customer_id:payment.customer_id,
                    ref:ref,
                    txRef:ref,
                    group:payment.group,
                    phone:payment.phone,
                    amount:payment.amount,
                    source:payment.source,
                    email:payment.email,
                    receiver:payment.receiver,
                    accno:payment.accno,
                    description:payment.description,
                    payment_type:"CARD",
                    initial:0
                }
                processNewScheuledPayment(payments.count,payments.last_payment_date,payment.ref,transObj);
            }

        }



}
/**
 * get unrefunded verified cards
 * calls @function refundConsumer pass transaction object
 * @param transaction pass transaction object
 */

async function autoRefundAmount(){
    const transactions = await db.getPendingVerificationList()
    //loop through the transaction object
        for (const transaction of transactions) {
            const dateandtime = new Date(transaction.timestamp).getTime()
            const nowdate = new Date().getTime();
            const isDue = (nowdate-dateandtime)>(86400000*1)

            if(isDue){
                console.log('refund is due',transaction.ref)
                // await refundConsumer(transaction,(refunded)=>{
                //     if(!refunded){
                //         sendSlackAlertMsg(`Refund failed. Failed to refund verification amount to customer ${transaction["ref"]}`)
                //     }
                // })
            }

        }
}

/**
 *
 */

async function settleMonthlyMerchants(){
    const merchants = await db.getScheduledSettledMerchants(SETTLEMENT_SCHEDULE.MONTHLY)
    settleMultipleMerchants(merchants);
}
/**
 *
 */
async function settleFloorLimitMerchants(){
    //merchant who need balance on hold to reach a set amount before settling
    const merchants = await db.getScheduledSettledMerchants(SETTLEMENT_SCHEDULE.ABOVE_LIMIT)
    const aboveLimit = [];
    merchants.forEach(merchant=>{
        if(parseFloat(`${merchant.balance}`) >= parseFloat(`${merchant.floor_limit}`)){
            aboveLimit.push(merchant)
        }
    })
    settleMultipleMerchants(aboveLimit);
}
/**
 *
 */
function updateTransactionsIndex(){
    const transactions = db.getAllTransactionsFrom("2020-01-01");
}

/**
 *
 */
async function collectLoans() {
    const debts = await db.getPendingLoans();
    console.log('debts',debts)
    async function proceed(debt, resp) {
        if (resp["status"] != undefined && resp["status"] == "success" &&
            resp["data"].chargeResponseCode == "00") {
            console.log("later charge success")
            const loanBal = await db.customerLoan(debt.customer_id)
            await db.updateTransaction(debt.ref, {provider1_resp: resp, chargeId: resp["data"]["flwRef"]});
            await db.updateBoyaLoanRepayment(debt,loanBal);
            const transx = (await db.getTransactionsOfRef(debt.ref))[0];
            await updateTransactionResultToFirestore(transx["customer_id"],transx["ref"],transx)
        } else {
            sendSlackAlertMsg(`Failed to charge card for transaction ref: ${debt.ref}, amount ${debt.amount}. Reason: ${resp["data"]["message"]}.`)
        }
    }
    for (const debt of debts) {
        const transx = await db.getTransactionByRef(debt.ref);
        if(transx.response.status!=0){
            await db.cancelTransactionLoan(transx);
        }else{
            if(transx.chargeId ==null || transx.chargeId == undefined || transx.chargeId == ''){
                if(debt.card.flutterwave_token){
                    console.log("token charge");
                    const chargeResp = await tokenCharge(debt.email,debt.ref,debt.amount,debt.description,debt.card)
                    chargeResp["payment_method_details"] = {
                        card:debt.card
                    }
                    chargeResp["source"] =  debt.card
                    if (chargeResp["status"] != undefined &&
                        chargeResp["status"] == "success" &&
                        chargeResp["data"].chargeResponseCode == "00")
                        chargeResp["id"] =  chargeResp["data"]["id"]
                    proceed(debt,chargeResp);
                }else{
                    console.log("card charge");
                    const chargeResp = await chargeFW(debt.email,debt.email,debt.ref,debt.amount,debt.description,debt.card)
                    chargeResp["payment_method_details"] = {
                        card:debt.card
                    }
                    chargeResp["source"] =  debt.card
                    if (chargeResp["status"] != undefined
                        && chargeResp["status"] == "success"
                        && chargeResp["data"].chargeResponseCode == "00"
                    )
                        chargeResp["id"] =  chargeResp["data"]["id"]
                    proceed(debt,chargeResp);
                }
            }else{
                sendSms(`Potential double charge `,debt.ref,(resp)=>{console.log(resp)});
            }
        }


    }
    // TODO for each transaction
    //TODO check if token is present, charge with token, else do pre-auth without token
    //TODO once charged, save chargeId to db including provider1_resp
}

/**
 *
 */
function runLoanRepayments(){
    collectLoans();
}
module.exports = {
    createCustomerAccount,
    mpesaTimeOutRoute,
    processTransaction,
    formatJengaPhone,
    initiateMPESAPush,
    debitWallet,
    mpesaC2BResultCallbackRoute,
    customerStatement,
    customerBalance,
    walletBalance,
    shareCounts,
    resetPin,
    verifyPin,
    b2cCallbackResults,
    b2bCallbackResults,
    reversalCallbackResults,
    reversalCall,
    paybills,
    updateProfile,
    matchPhoto,
    verifyID,
    comparePhotos,
    processSmileCallBack,
    processSmileCallBackStatus,
    postNps,
    clearConfidence,
    submitManualKycRquest,
    hasPendingKycReview,
    getReviews,
    sendOutboundSms,
    doMerchantOtpVerification,
    doMerchantOtp,
    refundMerchantsCustomer,
    getTillBusiness,
    creditWallet,
    creditMyWallet,
    sendReceiptMail,
    getTransactionFee,
    getTransactionFees,
    saveCardInfo,
    updateCardCvv,
    sendPaybillOtp,
    shouldUpdateCvv,
    schedulePayment,
    customerWalletStatement,
    isSchedulePayment,
    unSchedulePayment,
    rescheduleBillNotif,
    getReceiptByRef,
    processAfAirtimeCallback,
    runLoanRepayments,
    alertHomeFibreClients,
    alertDstvClients,
    alertGoTvClients,
    alertSafaricomPostPayClients,
    alertKplcPostPaidClients,
    alertZukuClients,
    settleInstantMerchants,
    settleWeeklyMerchants,
    settleDailyMerchants,
    settleFloorLimitMerchants,
    autoRefundAmount,
    settleMonthlyMerchants,
    getMonthlyRecurringPayment,
    getWeeklyRecurringPayment,
    getDailyRecurringPayment,
    jengaCallbackResults,
    confirmTopup,
    updateTopup,
    linkVirtualCardInfo,
    getEmployeeLimits,
    updateEmployeeLimits,
    blockedCategories,
    issueFwCard,
    UnBlockFWCard,
    deleteVirtualCard,
    blockFWCard,
    getCorporateGroups,
    tagCorporateExpense,
    addCorporateExpenseReceipt,
    cardOnFWCard
    // updateAuthUser
};




//shareEmailReceipt({},(response)=>(console.log(response)));
async function removeDuplicateCards(){
    const carnos = await db.getAllVerfiedCardNos()
    const ledger = [];
    const counter = {};
    const toRemove = [];
    //update all verified entries to be verified
    // let i = 0;
    // console.log(`updating ${carnos.length}`)
    // for (const cardno of carnos) {
    //     i++;
    //     await db.verifyCard(cardno);
    //     console.log('updated card no',i)
    // }
    // await setTimeout(()=>{console.log("30 sec gone")},30000)
    const cards = await db.getAllCards()

    cards.forEach((card)=>{
        if(counter[card.number]!=undefined){
            toRemove.push(card["id"])
        }else{
            counter[card.number] = card;
        }
    })
    console.log(`${toRemove.length} dups to remove`)

    toRemove.sort().forEach((card)=>{
        console.log(`${card} will be removed`)
        db.deleteCard(card);
    })


}

async  function runCardErrors(){
    const records = await db.getCardsPendingVerification()
    const cards_verifications = [];
    for (const record of records) {
        const trecords = await db.getVerificationsFailedTransaction(record["number"]);
        const cards_link_attempts = {number:record["number"],"provider":'',"status":'',attempts:0};
        trecords.forEach(rec=>{
            console.log("provider is ",rec["provider1"])
            cards_link_attempts.attempts += 1;
            cards_link_attempts.provider = PSPs.FLUTTERWAVE;
            if(rec["provider1"] == PSPs.FLUTTERWAVE && rec.provider1_resp.data!=undefined){
                try {
                    if(JSON.parse(rec.provider1_resp.data).data.tx){
                        const code = JSON.parse(rec.provider1_resp.data).data["tx"]["chargeResponseCode"].split("-")[1]
                        console.log("error code=>",code)
                        cards_link_attempts.status = code;
                    }else{
                        cards_link_attempts.status = StatusType.VALIDATION_ERROR;
                    }
                }catch (e){
                    cards_link_attempts.status = rec.provider1_resp.data.chargeResponseCode;
                    console.log(rec.provider1_resp);
                }




            }
            if(rec["provider1"] == PSPs.EQUITY && rec.provider1_resp["authorizationResponse"]!=undefined){
                // console.log(rec["authorizationResponse"])
                const code = parseInt(rec.provider1_resp["authorizationResponse"]["responseCode"])
                console.log("error code=>",code)
                cards_link_attempts.status = code;
                cards_link_attempts.provider = PSPs.EQUITY;
            }
        })
        cards_verifications.push(cards_link_attempts)
        console.log(cards_link_attempts);
    }
}
async function runTransactionTags(customer_id){
    const transactions = await db.getTransactionsByCustomer(customer_id);
    console.log("found ",transactions.length);
    console.log("transactions");
    for (const record of transactions) {
        const tag = await getTransactionTag(customer_id,record["ref"]);
        if(tag!=undefined){
            db.updateTransaction(record["ref"],{"tag":tag});
            console.log('updated tag',tag)
        }
    }
    console.log('finished tagging')

}
async function getEmployeeLimits(employee_id,callback){
    const limits = await db.employeeLimits(employee_id);
    callback(limits);
}


async function updateEmployeeLimits(req,res){

    const limits = await db.updateemployeeLimits();
    return limits
}
async function blockedCategories(employee_id,vcn,callback){
    const limits = await db.blockedCategories(employee_id,vcn);
    callback(limits);
}
// runTransactionTags("8Usqxp7aoxWEhggYfAvH3fZhcvW2");
// runTransactionTags("dtu3vrgrpIRqUrOfkCEBhdQAkFJ2");
// updateTransactionRefs();
// async function runIndex() {
//     const transactions = await db.getAllTransactionsFrom("2020-10-01 12:05");
//     console.log('found no of transactions. ',transactions.receivername);
//     await storeTransactionInAlgolia(transactions);
// }
// runIndex();
// sendBillPushNotification("rP2AEL4yjgMywR6IEv1RqfA64Ue2",
//     "eyLri_0h7U96ra7OeUC5RB:APA91bHuBylabvhK0l5CCS-fGsKcAmiANURJtPysYzqkp7URI4xxR2ahLNi4ao-c0ARtANcLlOEwwULYTj8NsP3aFonxu_VLd8HpxbPpiUPONJd2WHIZ_7Q665p4EwF1B-seW8pAohUV",
//     {amount:"1000",biller:"ZUKU",title:"Your ZUKU bill is ready","accno":"123456",
//         body:`Robert, your Buy Goods bill is Ksh 1,000. Click to pay`,ref:"T-3433NJJ53"})
// async function runCard(){
//     const uid = "rP2AEL4yjgMywR6IEv1RqfA64Ue2";
//     const card = await db.getCustomerCardAbs(uid,"tok_live_4c7BhviMWJ7ewc8ozvXxAW_0034");
//     console.log('got card',card);
//     await createVerifiedCard(uid,card)
// }
// runCard();
// async function  updateAuthUser(admin){
//     await updateFAuthUser(admin,"cxdgrAByIIaQrm0NbFujIa1KDMl1",{
//         email: 'daikisato129@gmail.com',
//         displayName: 'Daiki Sato',
//         phoneNumber: '+254728385726',
//         emailVerified: true
//     })
//
// }
// updateAuthUser();
// initiateRefundForVerifiedCharge(
//     "verify-160197682317098",(done)=>{
//         console.log('processed refund',done);
//     });
// removeDuplicateCards();
// runCardErrors();
// checkMpesaTransactionStatus();

// sendSms(`Transaction confirmed OKQ6W28T50. Your payment amount Kes 54,000 has been made to till 5254669 on ${dateFormat(new Date("2020-11-26 15:29:34"),
//     "dd/mm/yy HH:MM")}, Mpesa Ref: OKQ6W28T50`,"+254721271746",(resp,err)=>{
//     console.log(resp);
//     console.log(err);
// })

// async function deleteCards() {
//     const cards = await db.getCorporateFwCards()
//     for (const card of cards) {
//         const resp = await terminateVirtualCard(card.vcn);
//         console.log(resp);
//     }
// }
// deleteCards();

