export enum StatusType {
    SUCCESS,
    REFUND,
    ERROR,
    INSUFFICIENT_FUNDS,
    BANK_DECLINE,
    BLOCKED,
    SYS_FAILURE,
    SERVICE_UNAVAILABLE,
    VALIDATION_ERROR,
    KYC,
    DUPLICATE,
        STOPPED

}
export enum MERCHANT_TRANSACTION_TYPE {
    REVERSAL,
    CREDIT,
    SETTLEMENT,
    CUSTOMER_REFUND,
    SYSTEM_REFUND

}
export enum SETTLEMENT_SCHEDULE{
    NONE,
    INSTANT_PAYOUT,
    DAILY,
    WEEKLY,
    MONTHLY,
    ABOVE_LIMIT,
    MANUAL,
}
export enum SCHEDULED_PAYMENT_FREQUENCY{
    NONE,
    MONTHLY,
    WEEKLY,
    DAILY,
}
export class IssuerCodes{
    static APPROVAL = "00";//success
    static CALL_BANK = "02";
    static TERMINAL_ID_ERROR = "03";
    static BANK_DECLINE = "05";//Do not honor
    static MERCHANT_NOT_FOUND = "06";
    static NO_ORIGINAL = "09";//Void doesn't reference an original transaction
    static AMOUNT_ERROR = "13";//Amount Error
    static CARDNO_ERROR = "14";//Issuer cannot find account number
    static NO_SUCH_ISSUER = "15";//No such issuer
    static INVALID_ICC = "25";//invalid icc data
    static LOST_CARD = "41";//lost card
    static STOLEN_CARD = "43";//stolen card
    static PICKUP_CARD = "44";//pickup card
    static INSUFFICIENT_FUNDS = "51";//no funds
    static EXPIRED_CARD = "54";//expired card
    static INVALID_CARD = "56";//invalid card
    static WITHDRAWAL_LIMIT = "61";//limit
    static DECLINE_FRAUD = "62";//potential fraud
    static SYSTEM_ERROR = "96";//limit
    static FRAUD_DETECTED = "FR";//limit
    static INCORRECT_CVV2 = "N7";//limit
}
export class BoyaIssuerCodesMessage{
    static APPROVAL = "SUCCESS";//success
    static CALL_BANK = "Contact your bank to approve transactions";
    static TERMINAL_ID_ERROR = "03";
    static BANK_DECLINE = "Your bank declined to process Nash Cash Transaction. Please contact the bank to authorise Boya Payments on your card.";//Do not honor
    static MERCHANT_NOT_FOUND = "WE are not able to complete this transaction at the moment";
    static NO_ORIGINAL = "Reversal failed";//Void doesn't reference an original transaction
    static AMOUNT_ERROR = "Sorry, your transaction failed to a bank issue with the amount";//Amount Error
    static CARDNO_ERROR = "Sorry, your bank declined this transaction due to invalid account number";//Issuer cannot find account number
    static NO_SUCH_ISSUER = "Sorry, we could not find a bank/issuer for your card";//No such issuer
    static INVALID_ICC = "Invalid ICC data";//invalid icc data
    static LOST_CARD = "Sorry, the card you are using is not allowed to transact by the issuing bank. Kindly contact your bank";//lost card
    static STOLEN_CARD = "Sorry, the card you are using is not allowed to transact by the issuing bank. Kindly contact your bank";//stolen card
    static PICKUP_CARD = "Your card is not ready to process transactions. Kindly contact your bank.";//pickup card
    static INSUFFICIENT_FUNDS = "Sorry, your card has insufficient funds to complete this transaction";//no funds
    static EXPIRED_CARD = "Sorry, the card you selected is expired";//expired card
    static INVALID_CARD = "Sorry, the card you selected is invalid";//invalid card
    static WITHDRAWAL_LIMIT = "Sorry, your card has reached maximum withdrawals";//limit
    static DECLINE_FRAUD = "Sorry, we cannot allow transactions from this card as informed by your bank";//potential fraud
    static SYSTEM_ERROR = "Sorry, your bank has system down-time at the moment. We are unable to complete the transaction";//system down
    static FRAUD_DETECTED = "Sorry, your card has high level of fraud risk detected. Please contact your bank";//limit
    static INCORRECT_CVV2 = "Sorry, the CVV/CVC you submitted for this card is incorrect.";//limit
}
