const express = require('express');
var request = require('request');
var mysql = require('./mysql');
var b2b = require('./b2b');
var dateFormat = require('dateformat');
// var smsSender = require('./sms_sender');
// const jenga = require('./../equity_jenga/b2b');
var payMpesaB2B = require('../bloc/workpy');
// var payBIllWithiPay = require('../ipay/billing');
var onTransXComplete = require('./ontransaction_complete');
// var carcCharge = require('./card_charge');
// var kaps = require('../kaps/parking');
const firestore = require('../firebase');




function timeoutHandler(req, res) {
    console.log('mpesa timeout callback');
    let resp = req.body.Result;
    let ConversationID = resp.ConversationID;
    let mpesa_resp = {
        provider_response: resp,
        status: 5,
        errorMessage: "error: Timeout waiting for response. Try again"
    };
    updateC2BWithCheckoutId(5, JSON.stringify(resp), ConversationID);
    getTransactionByCID(ConversationID, (rows) => {
        if (rows.length > 0) {
            var ref = rows[0].ref;
            firestore.collection('transactions').doc(ref).update(mpesa_resp).then((result) => {
                console.log('mpesa push callback result saved');
                res.status(200).end();
            }).catch((err) => {
                console.error(err);
                console.log('mpesa push callback result error', err);
            })
        }
    });
}
async function logMerchantSale(pObj, status, provider_resp = "", reason = "") {
    if (status == 3) {
        let update = {status: 3, provider_response: provider_resp};
        update = Object.assign(pObj, update);
        await firestore.collection('transactions')
            .doc(pObj.ref).update({status: 3, provider_response: provider_resp});
        onTransXComplete(update);
    } else {
        //in another place, refunds will be triggered if document status changes to 0 or -1 and a successful c2b exists.
        await firestore.collection('transactions')
            .doc(pObj.ref).update({status: status, provider_response: provider_resp, errorMessage: reason},);
    }

}

// function payKaps(paymentObj, callback) {
//     paymentObj.reference = `${new Date().getMilliseconds()}${Date.now()}`.substring(0, 12);
//     //check if merchant exists
//     b2b.merchantInfo("000001", (merchant) => {
//         if (merchant) {
//             paymentObj.merchant_id = merchant.merchant_id;
//             archiveB2B(paymentObj, (saved) => {
//                 if (!saved) {
//                     console.error('error creating b2b record in db');
//                     callback(4, {errorMessage: "Unable to initiate merchant transaction. payment failed"});
//                     return;
//                 }
//                 b2b.creditBoyaMerchantAcc(merchant.merchant_id, paymentObj.ref, paymentObj.amount, (state) => {
//                     if (state == 1) {
//                         console.log('credited merchant', merchant);
//                         if (merchant.auto_settle == 1) {
//                             //automatic settlement
//                             paymentObj.bank = {
//                                 BANKCODE:merchant.bankCode,
//                             };
//                             paymentObj.accno = merchant.accno;
//                             bankTransfer(paymentObj,(settlement_type,rs)=>{
//                                 tillPaymentCallback(settlement_type,merchant,paymentObj,rs,callback);
//                             });
//                         } else {
//                             //todo debit/settle later
//                             console.log('later settlement');
//                             tillPaymentCallback('NONE',merchant,paymentObj,{status:"SUCCESS"},callback);

//                         }
//                     } else {
//                         //unable to credit boya merchant account
//                         console.log('unable to credit boya merchant acc.');
//                         tillPaymentCallback('NONE',merchant,paymentObj,{status:"FAILED",
//                             errorMessage: "Unable to complete payment. please try later"},callback);
//                     }
//                 });


//             });
//         } else {
//             console.error("merchant does not exist");
//             //merchant does not exists.
//             //todo reverse
//             callback(4, {errorMessage: "Invalid merchant shortcode. payment failed"});
//         }


//     });

// }
function payAcquiredMerchantTill(paymentObj, callback) {
    paymentObj.reference = `${new Date().getMilliseconds()}${Date.now()}`.substring(0, 12);
    //check if merchant exists
    b2b.merchantInfo(paymentObj.receiver, (merchant) => {
        if (merchant) {
            paymentObj.merchant_id = merchant.id;
            archiveB2B(paymentObj, (saved) => {
                if (!saved) {
                    console.error('error creating b2b record in db');
                    callback(4, {errorMessage: "Unable to initiate merchant transaction. payment failed"});
                    return;
                }
                b2b.creditBoyaMerchantAcc(merchant.id, paymentObj.ref, paymentObj.amount, (state) => {
                    if (state == 1) {
                        console.log('credited merchant', merchant);
                        if (merchant.auto_settle == 1) {
                            //automatic settlement
                            paymentObj.bank = {
                                BANKCODE:merchant.bankCode,
                            };
                            paymentObj.accno = merchant.accno;
                            if(merchant.till_type == "MPESA"){
                                //transfer to bank ...until mpesa b2b is enabled
                                bankTransfer(paymentObj,(settlement_type,rs)=>{
                                    tillPaymentCallback(settlement_type,merchant,paymentObj,rs,callback);
                                });

                            }else{
                                //eazy till
                                payEazyTill(merchant.shortcode,paymentObj.amount,paymentObj.reference,(settlement_type,rs)=>{
                                    tillPaymentCallback(settlement_type,merchant,paymentObj,rs,callback);
                                });

                            }



                        } else {
                            //todo debit/settle later
                            console.log('later settlement')
                            tillPaymentCallback('NONE',merchant,paymentObj,{status:"SUCCESS"},callback);

                        }
                    } else {
                        //unable to credit boya merchant account
                        console.log('unable to credit boya merchant acc.');
                        tillPaymentCallback('NONE',merchant,paymentObj,{status:"FAILED",
                            errorMessage: "Unable to complete payment. please try later"},callback);
                    }
                });


            });
        } else {
            console.error("merchant does not exist");
            //merchant does not exists.
            //todo reverse
            callback(4, {errorMessage: "Invalid merchant shortcode. payment failed"});
        }


    });

}
function payMpesaAccount(paymentObj, callback) {
    paymentObj.reference = `${new Date().getMilliseconds()}${Date.now()}`.substring(0, 12);
            paymentObj.merchant_id = paymentObj.reference;
            archiveB2B(paymentObj, (saved) => {
                if (!saved) {
                    console.error('error creating b2b record in db');
                    callback(4, {errorMessage: "Unable to initiate merchant transaction. payment failed"});
                    return;
                }
                        mpesaBusinessTransfer(paymentObj,(rs)=>{
                            switch (rs.status) {
                                case 3:
                                    updateB2B(3, paymentObj.ref, JSON.stringify(rs.second_stage_response));
                                    callback(3, rs);
                                    break;
                                default:
                                    updateB2B(4, paymentObj.ref, JSON.stringify(rs.second_stage_response));
                                    callback(4, {errorMessage: "Unable to pay merchant. payment failed"});
                                    break;

                            }
                        });
            });
}

function payBankAcc(paymentObj, callback) {
    paymentObj.reference = `${new Date().getMilliseconds()}${Date.now()}`.substring(0, 12);
    //check if merchant exists

    paymentObj.merchant_id = paymentObj.accno;
    if (paymentObj.accno == null || paymentObj.accno == undefined) {
        paymentObj.merchant_id = paymentObj.receiver_data
    }
    archiveB2B(paymentObj, (saved) => {
        if (!saved) {
            console.error('error creating b2b record in db');
            callback(4, {errorMessage: "Unable to initiate merchant transaction. payment failed"});
            return;
        }
        bankTransfer(paymentObj,(settlement_type,rs)=>{
            switch (rs.status) {
                case 'SUCCESS':
                    updateB2B(3, paymentObj.ref, JSON.stringify(rs));
                    callback(1, rs);
                    break;
                default:
                    updateB2B(4, paymentObj.ref, JSON.stringify(rs));
                    callback(4, rs);
                    break;

            }
        });

    });
}
function bankTransfer(paymentObj, callback) {
        var bank = paymentObj.bank;
        var bankcode = `${bank.BANKCODE}`;
        if (bankcode === "68") {
            //do internal transfer;
            console.log('doing internal transfer', paymentObj.accno);
            // jenga.sendMoneyToEquityAccount(`${paymentObj.accno}`, "68",
            //     `${paymentObj.amount}`, paymentObj.reference, (resp) => {
            //         let result = resp;
            //         var rs = result;
            //         try {
            //             rs = JSON.parse(rs);
            //         } catch (e) {

            //         }
            //         callback("INTERNAL",rs);


            //     });
        } else {
            console.log('doing pesalink transfer');
            // jenga.sendMoneyToBankAccWithPesalink(
            //     paymentObj.receiver,
            //     `${paymentObj.accno}`,
            //     `${bankcode}`,
            //     `${paymentObj.amount}`, paymentObj.reference, paymentObj.phone, (resp) => {
            //         let result = resp;
            //         console.log('pesalink transfer result', result);
            //         var rs = result;
            //         try {
            //             rs = JSON.parse(rs);
            //         } catch (e) {

            //         }
            //         callback("PESALINK",rs);

            //     });
        }

}
function mpesaBusinessTransfer(paymentObj, callback) {
    switch (paymentObj.group) {
        case "bills":
            paymentObj.CommandID = "BusinessPayBill";
            break;
        case "tills":
            paymentObj.CommandID = "BusinessBuyGoods";
            break;
        default:
            paymentObj.CommandID = "MerchantToMerchantTransfer";
            break;
    }
    var pOBj = {
        "Initiator": "",
        "SecurityCredential": "",
        "CommandID": paymentObj.CommandID,
        "SenderIdentifierType": "4",
        "RecieverIdentifierType": "4",
        "Amount": paymentObj.amount,
        "PartyA": "",
        "PartyB": paymentObj.receiver,
        "AccountReference": paymentObj.receiver_data!=undefined && paymentObj.receiver_data!=null?paymentObj.receiver_data:paymentObj.accno==null?`${paymentObj.txRef}`:paymentObj.accno,
        "Remarks": `${paymentObj.txRef!=undefined?paymentObj.txRef:paymentObj.ref}`,
        "QueueTimeOutURL": '',
        "ResultURL": '',
        "status":1
    };
    payMpesaB2B(pOBj,(resp)=>{
        console.log('mpesa-payment callback',resp);
        callback(resp);
    });
}
function payEazyTill(eazytillno,amount,reference,callback){
    // jenga.fundMerchantTill(`${eazytillno}`,amount,reference,(resp)=>{
    //     let result = resp;
    //     try {
    //         result = JSON.parse(result);
    //     }catch (e) {

    //     }
    //     callback("EAZY-TILL",result);
    // });
}

async function createB2CPayment(b2cObj) {
    var b2c = {
        "customer_id": b2cObj.uid,
        "uid": b2cObj.uid,
        "amount": b2cObj.amount,
        "description": b2cObj.description,
        "phone": b2cObj.receiver.toString().trim(),
        "ref": b2cObj.ref,
        "currency": b2cObj.currency,
        "payment_type": b2cObj.payment_type,
        "status": 1
    };
    firestore.collection('b2c').doc(b2cObj.ref).create(b2c).catch((err)=>{
        console.error('error creating b2c record',err.toLocaleString());
    });

}

async function triggerRefund(payObj) {
    payObj.description = "reversal";
    payObj.status = 1;
    if (payObj.payment_type === "MPESA") {
        payObj.receiver = payObj.phone;
        payObj.status = 1;
    }
    await firestore.collection('reversals')
        .doc(payObj.ref).create(payObj);
    // else {
    //     //create card refund;
    //     await firestore.collection('users')
    //         .doc(payObj.uid).collection("refunds").add({
    //             chargeId:payObj.id,
    //             uid:payObj.uid,
    //             amount:payObj.amount,
    //         });
    // }
}

async function triggerPayment(ref) {
    await firestore.collection('transactionsmonitor').doc(ref).create({since:dateFormat(new Date(), "mmmm dS, yyyy")});
    await firestore.collection(`transactions`).doc(ref)
        .update({"xstage":2,dateTime: dateFormat(new Date(), "mmmm dS, yyyy")});
    var doc = await firestore.collection('transactions').doc(ref).get();
    var payObj = doc.data();
    // console.log(payObj);
    if (payObj == null || payObj == undefined) {
        //TODO gross misconduct. refund
        console.log('no data fetched');
        return;
    }
    switch (payObj.group) {
        case "parking":
            if(payObj.ticket!==undefined){
                kaps.payParking(payObj,(resp,err)=>{
                    if(!err){
                        logMerchantSale(payObj, 3, resp, "");
                        console.log('kaps parking payment response',resp);
                        payKaps(payObj,(ress,err)=>{
                            console.log('response',ress);
                            console.log('error',err);
                        });
                    }else{
                        logMerchantSale(payObj, 4, resp, "parking payment failed. reversal in progress");
                        payObj.description = "reversal";
                        triggerRefund(payObj);
                    }
                });
            }else {
                logMerchantSale(payObj, 4, resp, "parking payment failed. Not yet supported. reversal in progress");
                payObj.description = "reversal";
                triggerRefund(payObj);
            }
            break;
        case "bills":
            payObj.reference = `${new Date().getMilliseconds()}${Date.now()}`.substring(0, 12);
            if(payObj.receiver==="kplc_prepaid" ||
                payObj.receiver==="kplc_postpaid" ||
                payObj.receiver==="GoTV" ||
                payObj.receiver==="DSTV"){
                payObj.receivername = payObj.receiver.toString().split("_").join(" ").toUpperCase();
                //pay using iPay
                console.log('its a iPay transaction',payObj);
                // payBIllWithiPay.payBill(payObj.receiver.toLowerCase(),
                    // payObj.amount,
                    // payObj.accno,
                    // payObj.phone,(resp,err)=>{
                    //     console.log('ipay responded',resp);
                    //     if (!err) {
                    //         try {
                    //             if(resp.details!==undefined && resp.details.token!==undefined){
                    //                 payObj.token =  ` Units ${resp.details.units} , Token: ${resp.details.token}`
                    //             }
                    //         }catch (e) {
                    //             console.error(e);
                    //         }
                    //         logMerchantSale(payObj, 3, resp, "");
                    //     } else {
                    //         //reverse
                    //         console.log(`XXXXX payment to iPay biller failed`, resp);
                    //         logMerchantSale(payObj, 4, resp, "bill payment failed. reversal in progress");
                    //         payObj.description = "reversal";
                    //         triggerRefund(payObj);
                    //         //call refund function
                    //     }
                    // });
            }else{
                switch (payObj.receiver) {
                    case "ZUKU":
                        payObj.receiver = 320320;
                        payObj.receivername = "ZUKU";
                        break;
                    case "ZUKU SATELLITE":
                        payObj.receiver = 320323;
                        payObj.receivername = "ZUKU SATELLITE";
                        break;
                    case "ZUKU VOICE":
                        payObj.receiver = 320321;
                        payObj.receivername = "ZUKU VOICE";
                        break;
                    case "kplc_prepaid":
                        payObj.receiver = 888880;
                        payObj.receivername = "KPLC PREPAID";
                        break;
                    case "kplc_postpaid":
                        payObj.receiver = 888888;
                        payObj.receivername = "KPLC POSTPAID";
                        break;
                    case "DSTV":
                        payObj.receiver = 444900;
                        payObj.receivername = "DSTV";
                        break;
                    case "JTL":
                        payObj.receiver = 330330;
                        payObj.receivername = "JTL";
                        // payObj.billerCode = 776611;
                        break;
                    case "GoTV":
                        payObj.receiver = 423655;
                        // payObj.billerCode = 6800009;
                        payObj.receivername = "GoTV";
                        break;
                    case "ECITIZEN":
                        payObj.receiver = 206206;
                        payObj.receivername = "ECITIZEN";
                        break;
                    case "IPAY":
                        payObj.receiver = 510800;
                        payObj.receivername = "IPAY";
                        break;
                    case "KENYA AIRWAYS":
                        payObj.receiver = 777887;
                        break;
                    case "KENYATTA NATIONAL HOSPITAL":
                        payObj.receiver = 247257;
                        break;
                }
                // b2b.payBill(payObj, (res) => {
                //     console.log('bill payment reponse');
                //     if (res == 0) {
                //         //refund
                //         triggerRefund(payObj);
                //         logMerchantSale(payObj, 4, res)
                //     } else {
                //         //log successful bill payment
                //         console.log('sucessful bill payment');
                //         //update transaction record
                //         logMerchantSale(payObj, 3, res)
                //     }
                // });
                if( payObj.accno!==undefined){
                    payMpesaAccount(payObj,(state,response)=>{
                        if (state === 3) {
                            logMerchantSale(payObj, 3, response, "");
                        } else {
                            //reverse
                            console.log(`XXXXX payment to MPESA failed`, response);
                            logMerchantSale(payObj, 4, response, "payment failed. reversal in progress");
                            payObj.description = "reversal";
                            triggerRefund(payObj);
                            //call refund function
                        }
                    });
                }else {
                    logMerchantSale(payObj, 4, {errorMessage:"invalid accno"}, "payment failed, invalid accno. refunding...");
                    payObj.description = "reversal";
                    triggerRefund(payObj);
                }

            }

            break;
        case "airtime":
            //send airtime
            // sendAirtime(payObj);
            break;
        case "mpesa":
            //send money to customer or pay till
            //mobile number
            createB2CPayment(payObj);
            break;
        case "rent":
            payBankAcc(payObj, (state, response) => {
                console.log(`b2b response:`, response);
                if (state === 1) {
                    logMerchantSale(payObj, 3, response, "");
                } else {
                    //reverse
                    console.log(`XXXXX payment to bank failed`, response);
                    logMerchantSale(payObj, 4, response, "payment failed. reversal in progress");
                    payObj.description = "reversal";
                    triggerRefund(payObj);
                    //call refund function
                }
            });
            break;
        case "tills":
            // payAcquiredMerchantTill(payObj, (state, response) => {
            //     console.log(`till state: ${state}`);
            //     console.log(`till response:`, response);
            //     if (state !=3) {
            //         //reverse
            //         logMerchantSale(payObj, 4, response, "payment failed. reversal in progress");
            //         payObj.description = "reversal";
            //         triggerRefund(payObj);
            //         //call refund function
            //     } else {
            //         logMerchantSale(payObj, 3, response, "");
            //     }
            // });
            payMpesaAccount(payObj,(state,response)=>{
                if (state === 3) {
                    logMerchantSale(payObj, 3, response, "");
                } else {
                    //reverse
                    console.log(`XXXXX payment to MPESA failed`, response);
                    logMerchantSale(payObj, 4, response, "payment failed. reversal in progress");
                    payObj.description = "reversal";
                    triggerRefund(payObj);
                    //call refund function
                }
            });
            break;
    }
}
function runDebitTries(merchant,paymentObj,settlement_type,rs) {
    b2b.debitBoyaMerchantAcc(merchant, paymentObj.reference,settlement_type, rs,paymentObj.amount, (state) => {
        if (state == 1) {
            console.log(`settlement successful to ${paymentObj.accno}`);
        } else {
            //RED alert
            //todo try to debit amount from merchant account again
            //todo schedule a local debit
            console.error(`unable to debit amount from merchant acc:${merchant.id}`, paymentObj);
            // smsSender(`unable to debit merchant account ref: ${paymentObj.txRef} ${JSON.stringify(merchant)}`,"0718555832",(res)=>{

            // });
            setTimeout(()=>{
                //try after 2minutes
                runDebitTries(merchant,paymentObj,settlement_type,rs);
            },2*60*1000);
        }
    });
}
function tillPaymentCallback(settlement_type,merchant,paymentObj,rs,callback) {
    paymentObj.merchant = merchant;
    var status = rs.status!==undefined?rs.status:rs.response_status!==undefined?rs.response_status:rs.status;
    switch (status) {
        case 'SUCCESS':
            // updateB2B(3, paymentObj.ref, JSON.stringify(rs));
            if(settlement_type !== "NONE"){
                //settlement was done. deduct amount from merchant account
                b2b.debitBoyaMerchantAcc(merchant, paymentObj.reference+"-"+paymentObj.txRef,settlement_type, rs,paymentObj.amount, (state) => {
                    callback(3, rs);
                    if (state == 1) {
                        console.log(`settlement successful to ${paymentObj.accno}`);
                    } else {
                        //RED alert
                        runDebitTries(merchant,paymentObj,settlement_type,rs);
                        console.error(`unable to debit amount from merchant acc:${merchant.id}`, paymentObj);
                    }
                });
            }else{
                callback(3, rs);
            }
            break;
        default:
            //settlement failed.
            //todo: either send sms to merchant indicating money is received or reverse
            console.log(`unable to settle to merchant account`,rs);
            b2b.deleteCreditRecord(paymentObj.ref);
            // updateB2B(4, paymentObj.ref, JSON.stringify(rs));
            callback(4, {errorMessage: "Unable to pay merchant. payment failed"});
            break;

    }
}

function pushCallBackHandler(req, res) {
    console.log('mpesa push callback');
    try {
        var results = req.body;
        try {
            results = JSON.parse(results)
        } catch (e) {

        }
        let resp = results.Body.stkCallback;
        let mpesa_resp = {provider_response: resp};
        let ConversationID = resp.CheckoutRequestID;

        if (parseInt(resp.ResultCode) == 1032) {
            mpesa_resp.errorMessage = "Cancelled By user";
            updateC2BWithCheckoutId(0, JSON.stringify(resp), ConversationID);
            mpesa_resp.status = 0;
        }
        if (parseInt(resp.ResultCode) == 0) {
            mpesa_resp.status = 2;


            updateC2BWithCheckoutId(3, JSON.stringify(resp), ConversationID);
            resp.CallbackMetadata.Item.forEach((record) => {
                console.log(record);
                switch (record.Name) {
                    case "Amount":
                        mpesa_resp.amount = record.Value;
                        break;
                    case "MpesaReceiptNumber":
                        mpesa_resp.MpesaReceiptNumber = record.Value;
                        break;
                    case "TransactionDate":
                        mpesa_resp.date = record.Value;
                        break;
                    case "PhoneNumber":
                        mpesa_resp.phone_no = record.Value;
                        break;
                }
            });
            delete resp.CallbackMetadata;
        } else {
            mpesa_resp.status = 0;
            mpesa_resp.errorMessage = resp.ResultDesc;
            updateC2BWithCheckoutId(0, JSON.stringify(resp), ConversationID);
        }

        getTransactionByCID(ConversationID, (rows) => {
            if (rows.length > 0) {
                var ref = rows[0].ref;
                if(mpesa_resp.status ==2)
                triggerPayment(ref);
                firestore.collection('transactions').doc(ref).update(mpesa_resp).then((result) => {
                    console.log('mpesa push callback result saved');
                    res.status(200).end();
                }).catch((err) => {
                    console.error(err);
                    console.log('mpesa push callback result error', err);
                })
            }
        });


    } catch (e) {
        console.error(e);
    }
}

function ipnCallBackHandler(req, res) {
    console.log('ipn callback');
    var results = req.body;
    try {
        results = JSON.parse(results)
    } catch (e) {

    }
    var transaction = results;
    var fullname = transaction.FirstName + " " + transaction.MiddleName + " " + transaction.LastName;
    var i = '' + transaction.TransTime;

    var timestamp = i[0] + '' + i[1] + '' + i[2] + '' + i[3] + '-' + i[4] + i[5] + '-' + i[6] + i[7] + ' ' + i[8] + i[9] + ':' + i[10] + i[11]
    console.log({
        PhoneNumber: transaction.MSISDN,
        Paybill: transaction.BusinessShortCode,
        Amount: transaction.TransAmount,
        MpesaReceiptNumber: transaction.TransID,
        TransactionDate: timestamp,
        CustomerName: fullname,
        merchantID: transaction.BillRefNumber
    });
    res.status(200).end();


}

function timeOutRoute() {
    const router = express.Router();
    router.route('/')
        .all((req, res, next) => {
            res.setHeader("Access-Control-Allow-Origin", "*");
            // res.writeHead(200, {'Content-Type': 'application/json'});
            next();
        }).post(timeoutHandler);
    return router;

}

function pushCallBackRoute() {
    const router = express.Router();
    router.route('/')
        .all((req, res, next) => {
            res.setHeader("Access-Control-Allow-Origin", "*");
            // res.writeHead(200, {'Content-Type': 'application/json'});
            next();
        }).post(pushCallBackHandler);
    return router;

}
function sendSMsRouter() {
    const router = express.Router();
    router.route('/')
        .all((req, res, next) => {
            res.setHeader("Access-Control-Allow-Origin", "*");
            // res.writeHead(200, {'Content-Type': 'application/json'});
            next();
        }).post((req,res)=>{
            if(req.body.receiver!==undefined && req.body.message!==undefined){
                try {
                    // smsSender(req.body.message,req.body.receiver,(resp)=>{
                    //     res.end(JSON.stringify({response:resp}));
                    // });
                }catch (e) {
                    console.error(e);
                }


            }
    });
    return router;

}
function transferMoneyRouter() {
    const router = express.Router();
    router.route('/')
        .all((req, res, next) => {
            res.setHeader("Access-Control-Allow-Origin", "*");
            // res.writeHead(200, {'Content-Type': 'application/json'});
            next();
        }).post((req,res)=>{
        if(req.body.merchant_id!==undefined && req.body.amount!==undefined){
            b2b.merchantInfoById(req.body.merchant_id,(merchant)=>{
                if(merchant){
                    var transferAmount = parseFloat(req.body.amount);
                    if(parseFloat(merchant.balance)>transferAmount){
                        if(transferAmount>999999){
                            res.status(500).end(JSON.stringify({status:"failed",message:`Maximum transfer amount is KES999,999`}));
                        }else{

                            const reference = `${new Date().getMilliseconds()}${Date.now()}`.substring(0, 12);

                            var transferObj = {
                                accno: merchant.accno,
                                reference: reference,
                                merchant_id: req.body.merchant_id,
                                txRef: "TR-"+Date.now(),
                                amount: transferAmount,
                                group: "transfer",
                                receiver: merchant.accno,
                                receiver_type: 2,
                                ref: "TR-"+Date.now(),
                                phone: merchant.phone,
                                bank: {
                                    BANKCODE: merchant.bankCode,
                                    BANKNAME: merchant.bank_name
                                }
                            };
                            bankTransfer(transferObj,(settlement_type,rs)=>{
                                tillPaymentCallback(settlement_type,merchant,transferObj,rs,(state,resp)=>{
                                    switch (state) {
                                        //successful settlement
                                        case 3:
                                            res.end(JSON.stringify({
                                                status:"success",
                                                message:"transfer approved",
                                                response:rs
                                            }));
                                            break;
                                        default:
                                            res.status(403).end(JSON.stringify({
                                                status:"error",
                                                message:"",
                                                response:rs
                                            }));
                                            break;

                                    }
                                });
                            });
                        }
                    }else{
                        res.status(400).end(JSON.stringify({status:"failed",message:`Insufficient Balance`}));
                    }
                }else{
                    res.status(400).end(JSON.stringify({status:"failed",message:"Merchant does not exist"}));
                }
            })

        }
    });
    return router;

}

function IPNRoute() {
    const router = express.Router();
    router.route('/')
        .all((req, res, next) => {
            res.setHeader("Access-Control-Allow-Origin", "*");
            // res.writeHead(200, {'Content-Type': 'application/json'});
            next();
        }).post(ipnCallBackHandler);
    return router;

}

module.exports = {
    pushCallBack: pushCallBackRoute(),
    timeout: timeOutRoute(),
    ipn: IPNRoute(),
    sms: sendSMsRouter(),
    settle: transferMoneyRouter(),
};
function getTransactionByCID(checkoutID, callback) {
    mysql.runQuery("select * from boya.c2boya where checkoutId = '" + checkoutID + "'", (row) => {
        return {id: row.id, ref: row.txRef}
    }, (results, err) => {
        if (!err) {
            callback(results)
        } else {
            callback([]);//assume exists
        }
    })
}
function archiveB2B(row, callback) {
    mysql.insertRecord("boya", "boya2b",
        {
            code: row.ref,
            amount: row.amount,
            merchant_id: row.merchant_id,
            txRef: row.ref,
            receiver: row.receiver,
            status: 1,
            receiver_type: row.receiver_type,
            reference: row.reference,//rrn for banks
        }, (result, err) => {
            if (!err) {
                console.log('b2b request saved')
                callback(true);
            } else {
                console.log('b2b request error')
                callback(false);
            }

        });
}
function updateB2B(status, ref, response) {
    mysql.updateRecord("boya", "boya2b",
        [`${status}`, response, ref],
        ["status", "provider_response", "txRef"], (result, err) => {
            if (err) {
                console.error(err);
            } else {
                console.log('b2b record updated');
            }
        })
}

function updateC2BWithCheckoutId(status, response, checkoutId) {
    mysql.updateRecord("boya", "c2boya",
        [status, response, checkoutId],
        ["status", "provider_response", "checkoutId"], (result, err) => {
            if (err) {
                console.error(err);

            } else {
                console.log('b2c record updated');
            }
        })
}
function formatAirtimePhone(phone) {
    let countrycode = "+254";
    let phone2 = phone.split(' ').join('').match(/\d+/)[0];
    if (phone.toString().substring(0, 1) === "+") {
        phone2 = phone;
    } else if (phone.toString().substring(0, 2) == "00") {
        phone2 = countrycode + "" + phone.toString().substring(2)
    } else if (phone.toString().substring(0, 1) == "0") {
        phone2 = countrycode + "" + phone.toString().substring(1)
    } else if (phone.length == 12) {
        phone2 = "+" + phone;
    }
    return phone2;
}
