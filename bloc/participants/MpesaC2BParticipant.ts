import {TransactionParticipant} from "../interfaces/TransactionParticipant";
import {Transaction} from "../models/Transaction";
import {PrepResult, TransactionResult} from "../models/TransactionResult";
import {DataStore} from "../models/DataStore";
import {StatusType} from "../utils/StatusType";
import {formatMpesaPhone, validatePhone} from "../utils/commonfuncs";
import request = require('request');

export class MpesaC2BParticipant implements TransactionParticipant{
    TAG = "MpesaC2B";
    transaction: Transaction;
    database:DataStore;

    /**
     * @description constructor
     * @param transaction 
     * @param db 
     */
    constructor(transaction:Transaction,db:DataStore) {
        this.transaction = transaction;
        this.database =db;
    }
    /**
     * @description abort transaction
     */
    abort(): Promise<VoidFunction> {
        return undefined;
    }

    /**
     * @description complete transaction
     */

    complete(): Promise<TransactionResult> {
        return undefined;
    }

    /**
     * @description prepare transaction
     * @function checkConsumerPhone
     * 
     */

    prepare(): Promise<PrepResult> {
        return new Promise<PrepResult>(resolve => {
            if(this.transaction.payment_type !== "MPESA"){
                return resolve(new PrepResult(StatusType.ERROR,{"errorMessage":"Invalid method"}))
            }
            this.checkConsumerPhone().then(result=>{
                resolve(result);
            })

        });
    }

    /**
     * @description check cinsumer phone
     * @function validatePhone
     * @param phone
     */
    private checkConsumerPhone():Promise<PrepResult>{
        return new Promise<PrepResult>((resolve) => {
            if(this.transaction.phone === undefined){
                resolve(new PrepResult(StatusType.ERROR,{errorMessage:"Invalid phone number"}));
            }else if(validatePhone(this.transaction.phone)){
                resolve(new PrepResult(StatusType.SUCCESS,{}));
            }else{
                resolve(new PrepResult(StatusType.ERROR,{errorMessage:"Invalid phone number"}));
            }
        });
    }

    /**
     * @description upate transaction
     * @param result 
     * @function updateTransaction
     * @param checkoutId
     * @param result.provider_response
     * @param result.status
     */
    updateDbTransaction(result:TransactionResult){
        this.database.updateTransaction(this.transaction.ref,{
            checkoutId:this.transaction.checkoutId,
            provider1_resp:result.provider_response,
            status:result.status
        }).then((saved)=>{
            console.log("[C2BParticipant]: updated DB transaction");
        }).catch((failed)=>{
            console.error("[C2BParticipant]: failed to update transaction");
        });
    }

    /**
     * @description process payment
     * @function getCustomerBalance
     * @param customer_id
     * @function initiatePush
     * @function updateDbTransaction
     * @function fetchCallbackResponse
     * @function updateTransaction
     * 
     */


    process(): Promise<TransactionResult> {
        return new Promise<TransactionResult>(async (resolve) => {
            const customer_balance = await this.database.getCustomerBalance(this.transaction.customer_id);
            const walletObj = await this.database.getWalletByID(this.transaction.customer_id)

            if(walletObj.amount > (this.transaction.amount + this.transaction.fee)){
                this.transaction.stripeAmount = 0;
                this.transaction.wallet_charge = this.transaction.amount + this.transaction.fee;
            }

            if (this.transaction.stripeAmount === 0 && walletObj.amount > this.transaction.wallet_charge ) {
                this.transaction.card_response = {
                    "id": "none",
                    source: {card: {brand: "Credit", last4: `BoyaCash`}},
                    payment_method_details: {card: {brand: "Credit", last4: `BoyaCash`}},
                    "promoused": false,
                    "walletused": true,
                };
                console.log("wallet applies");
                return resolve(new TransactionResult(StatusType.SUCCESS, {"message": "wallet Applied"}))
            }

            if(customer_balance>0){
                //customer has enough balance to complete transaction
                if(customer_balance>this.transaction.amount){
                    this.transaction.stripeAmount = 0;
                    this.transaction.promo_amount = this.transaction.amount;
                }else{
                    if(this.transaction.amount- customer_balance <51){
                        //will not use customer account balance
                        this.transaction.promo_amount = 0
                        this.transaction.stripeAmount = this.transaction.amount;
                    }else {
                        //we charge less credit balance
                        this.transaction.stripeAmount = (this.transaction.amount-customer_balance);
                        this.transaction.promo_amount = customer_balance;
                    }
                }

            }
            if(this.transaction.stripeAmount<1 && this.transaction.promo_amount>0){
                this.transaction.card_response = {"id":"none",
                    source:{card:{brand:"Credit",last4:`BoyaCash`}},
                    payment_method_details:{card:{brand:"Credit",last4:`BoyaCash`}},
                    "promoused":true};
                return resolve(new TransactionResult(StatusType.SUCCESS,{"message":"Promo Applied"}))
            }
            this.initiatePush().then((result => {
                //set checkout Id
                if(result.status === StatusType.SUCCESS){
                    console.log(result.provider_response);
                    this.transaction.checkoutId = result.provider_response["CheckoutRequestID"];
                    //update transaction state
                    this.updateDbTransaction(result);
                    this.fetchCallbackResponse().then((result)=>{
                        resolve(result);
                    });
                }else{
                    this.database.updateTransaction(this.transaction.ref,{
                        status:StatusType.ERROR,
                        provider1_resp:result.provider_response,
                    }).then((saved)=>{

                    }).catch((failed)=>{
                        console.log("failed to update transaction");
                    });
                    resolve(result);
                }

            }))
        });
    }

    /**
     * @description initiate push
     * @param customer_id
     * @param amount
     * @param charge_id
     * @param description
     * @param email
     * @param phone
     * @param ref
     * @param callbackurl
     * @param currency
     */
    private initiatePush():Promise<TransactionResult>{
        return new Promise<TransactionResult>((resolve) => {
            request(
                {
                    method: 'POST',
                    url: `${process.env.HOST}/mpesa/push/charge`,
                    json: {
                        "customer_id": this.transaction.customer_id,
                        "charge_id": this.transaction.ref,
                        "amount": this.transaction.amount,
                        "description": this.transaction.description,
                        "email": this.transaction.email,
                        "phone": formatMpesaPhone(this.transaction.phone),
                        "ref": this.transaction.ref,
                        "callbackurl": `${process.env.HOST}/c2bpushcb`,
                        "currency": this.transaction.currency,
                    }
                },
                function (error, response, body) {
                    console.log('got status code ', response.statusCode);
                    if(response.statusCode === 200){
                        if (!error && body.error == undefined && body.errorMessage == undefined) {
                            const resp = body;
                            if (parseInt(resp.ResponseCode) != 0) {
                                //failed to fund
                                // initial.errorMessage = resp.ResponseDescription ? resp.ResponseDescription : resp.error;
                                console.log('push charge rejected');
                                resolve(new TransactionResult(StatusType.ERROR,resp));
                            } else {
                                //probable fund
                                //wait for results
                                console.log('push charge accepted');
                                resolve(new TransactionResult(StatusType.SUCCESS,resp));


                            }
                        } else {
                            console.log('error processing',body);
                            const errorMessage = body.errorMessage ? body.errorMessage : "error processing request";
                            resolve(new TransactionResult(StatusType.ERROR,{errorMessage:errorMessage}));
                        }
                    }else{
                        console.log(body);
                        const errorMessage = body.errorMessage ? body.errorMessage : "error processing request";
                        resolve(new TransactionResult(StatusType.ERROR,{errorMessage:errorMessage}));
                    }

                });
        });

    }

    /**
     * @description fetch callback response
     * @function getTransactionByRef
     * @param ref
     * @function verifyPushResponse
     * @param provider_response
     * @function updateTransaction
     * @param ref
     * @param status
     * @function clearInterval
     * 
     */
    private fetchCallbackResponse(): Promise<TransactionResult>{

        return new Promise<TransactionResult>((resolve) => {
            const intervalId = setInterval(()=>{
                this.database.getTransactionByRef(this.transaction.ref).then((value)=>{
                    if(value.status == StatusType.SUCCESS){
                        let provider_response = value.response["provider1_resp2"];

                        if(provider_response!==undefined && provider_response!==''){
                            try {
                                provider_response = JSON.parse(provider_response);
                            }catch (e) {

                            }
                            const result = this.verifyPushResponse(provider_response);
                            console.log('c2b callback decoded',result);
                            switch (result.status) {
                                case StatusType.ERROR:
                                    clearInterval(intervalId);
                                    //failed
                                    this.database.updateTransaction(
                                        this.transaction.ref,
                                        {status: StatusType.ERROR,provider1_resp2:provider_response})
                                        .then(r  =>console.log("transaction status updated"));
                                    resolve(new TransactionResult(StatusType.ERROR,result));
                                    break;
                                case StatusType.SUCCESS:
                                    clearInterval(intervalId);
                                    provider_response.receiver_data = result;
                                    provider_response.MpesaReceiptNumber = result.MpesaReceiptNumber;
                                    // this.database.updateTransaction(
                                    //     this.transaction.ref,
                                    //     {status: StatusType.SUCCESS,provider1_resp:provider_response})
                                    //     .then(r  =>console.log("transaction status updated"));
                                    resolve(new TransactionResult(StatusType.SUCCESS,provider_response));
                                    break;
                            }
                        }else{
                            this.transaction.tries +=1;
                            console.log('count',this.transaction.tries);
                            if(this.transaction.tries>25){
                                clearInterval(intervalId);
                                resolve(new TransactionResult(StatusType.SUCCESS, {errorMessage: "Failed to get response from MPESA"}));
                            }
                        }
                    }else {
                        clearInterval(intervalId);
                        console.error("Missing transaction record or error fetching record");
                        resolve(new TransactionResult(StatusType.ERROR,{errorMessage:"Failed to complete transaction"}));
                    }
                });
            },3000);
        });
    }

    /**
     * @description verify push response
     * @param results 
     * 
     */
    private verifyPushResponse(results):{status:number,sms:String,MpesaReceiptNumber:String,TransactionID:String}{
        const result = {status:StatusType.ERROR,sms:'',MpesaReceiptNumber:'',TransactionID:''};
        let res = results.Body;
        try{
            res = JSON.parse(res);
        }catch (e) {

        }
        let resp = res.stkCallback;
        try{
            resp = JSON.parse(resp);
        }catch (e) {

        }
        if (resp.ResultCode === 0 || resp.ResultCode === "0") {
            // let records = resp.ResultParameters !== undefined ? resp.ResultParameters.ResultParameter : undefined;
            // let message = "";
            // if (records != undefined)
                // records.forEach(function (record) {
                //     if (record["Key"] === "ReceiverPartyPublicName") {
                //         message = message + "Sent to " + record["Value"] + "\n";
                //     }
                //     if (record["Key"] === "TransactionAmount") {
                //         message = message + "Amount " + record.Value + "\n";
                //     }
                //     if (record["Key"] === "TransactionReceipt") {
                //         message = message + "MPESA-REF: " + record.Value + ".\n";
                //     }
                //     if (record["Key"] === "TransactionCompletedDateTime") {
                //         message = message + "Date: " + record.Value + ".\n";
                //     }
                // });
            // result.sms = message;
            // if (message !== "") {
                result.status =StatusType.SUCCESS;
                // result.sms = message;
                // result.MpesaReceiptNumber = resp.TransactionID;
                // result.TransactionID = resp.TransactionID;
            // }
            return result;
        }
        else {
            result["errorMessage"] = `Failed to process Mpesa, ${resp.ResultDesc}`;
            console.log('MPESA c2b stage 1 failed',resp);
            return result;

        }
    }
}
