import {DataStoreFactory, TransactionParticipant} from "../interfaces/TransactionParticipant";
import {DataStore} from "../models/DataStore";
import {Transaction} from "../models/Transaction";
import {MpesaB2BParticipant} from "./MpesaB2BParticipant";
import {MpesaB2CParticipant} from "./MpesaB2CParticipant";
import {MpesaC2BParticipant} from "./MpesaC2BParticipant";
import {FlutterwaveParticipant} from "./FlutterwaveParticipant";
import {NotificationParticipant} from "./NotificationParticipant";
import {RefundParticipant} from "./RefundParticipant";
import {MerchantPayParticipant} from "./MerchantPayParticipant";


/**
 * @description add participant
 */

export enum Particpant {
    MERCHANT_PAY,
    MPESAC2B,
    MPESAB2B,
    MPESAB2C,
    FLUTTERWAVE,
    NOTIF,
    REFUND,


}
export class ParticipantAdapter {
    /**
     * @description get participant
     * @param type
     * @param transaction
     */

    static getParticipant(type:Particpant,transaction:Transaction):TransactionParticipant{
        switch (type) {
            case Particpant.MERCHANT_PAY: return new MerchantPayParticipant(transaction,DataStoreFactory.getDatabase());
            case Particpant.MPESAB2B: return new MpesaB2BParticipant(transaction,DataStoreFactory.getDatabase());
            case Particpant.MPESAB2C: return new MpesaB2CParticipant(transaction,DataStoreFactory.getDatabase());
            case Particpant.MPESAC2B: return new MpesaC2BParticipant(transaction,DataStoreFactory.getDatabase());
            case Particpant.FLUTTERWAVE: return new FlutterwaveParticipant(transaction,DataStoreFactory.getDatabase());
            case Particpant.NOTIF: return new NotificationParticipant(transaction,DataStoreFactory.getDatabase());
            case Particpant.REFUND: return new RefundParticipant(transaction,DataStoreFactory.getDatabase());

        }
    }
}
