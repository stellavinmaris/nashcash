import {TransactionParticipant} from "../interfaces/TransactionParticipant";
import {DataStore} from "../models/DataStore";
import {Transaction} from "../models/Transaction";
import {PrepResult, Result, TransactionResult} from "../models/TransactionResult";
// const triggerCardRefund =  require("../refund_node");
import {StatusType} from "../utils/StatusType";
import {PSPs} from "./Groups";
import {refundChargeFW} from "../vgs/flutterwave";
import stripePoint = require("../card_charge");
// import {refundCharge} from "../vgs/stripe";
// import {refundConsumer} from "../utils/pendingRefunds";

export class RefundParticipant implements TransactionParticipant{
    TAG = "RefundParticipant";
    database:DataStore;
    transaction: Transaction;
    /**
     * @description construction
     * @param transaction 
     * @param db 
     */
    constructor(transaction:Transaction,db:DataStore) {
        this.transaction = transaction;
        this.database =db;
    }

    /**
     * @description abort transaction
     */
    abort(): Promise<VoidFunction> {
        return undefined;
    }

    /**
     * @description complete transaction
     */

    complete(): Promise<Result> {
        return undefined;
    }

    /**
     * @description prepare transaction
     */

    prepare(): Promise<Result> {
        return new Promise<Result>((resolve => {
                resolve(new PrepResult(StatusType.SUCCESS,{}))
            }
        ));
    }

    /**
     * @description process transaction
     * @function refund
     */

    process(): Promise<Result> {
        return this.refund();
    }

    /**
     * @description refund to mpesa
     * @function refundCustomer
     * @param customer_id
     * @param txRef
     * @param amount
     */
    refundToMpesa():Promise<TransactionResult>{
        //credit user account with credit worth M-Pesa amount.
        return new Promise<TransactionResult>(resolve => {
            this.database.refundCustomer(this.transaction.customer_id,this.transaction.txRef,this.transaction.amount,
                (state)=>{
                resolve(new TransactionResult(state?StatusType.SUCCESS:StatusType.ERROR,{"errorMessage":"failed to refund"}));
                });
        })
    }
    /**
     * @description refund transaction
     * @function refundToMpesa
     * @function updateDbTransaction
     * @function logRefund
     * 
     */
    refund():Promise<TransactionResult>{
        if (this.transaction.payment_type === "MPESA") {
            console.log('mpesa refund');
           return  this.refundToMpesa();
        }else{
            return new Promise<TransactionResult>(resolve => {
                // refundConsumer(this.transaction,(refunded)=>{
                //     if(refunded){
                //         this.updateDbTransaction(new TransactionResult(StatusType.REFUND,{}))
                //     }
                //     try{
                //         this.database.logRefund(this.transaction,this.transaction.provider_response["errorMessage"],(saved)=>{})
                //     }catch (e) {
                //         console.error(e)
                //     }
                //     resolve(new TransactionResult(refunded?StatusType.SUCCESS:StatusType.ERROR,{}));

                // })
            })
        }


    }

    /**
     * @description update transaction
     * @param result 
     * @function updateTransaction
     * @param ref
     * @param refund_reason
     * @param result.status
     */

    updateDbTransaction(result: TransactionResult) {
        this.database.updateTransaction(this.transaction.ref,{
            refund_reason:this.transaction.refund_reason,
            status:result.status
        }).then((saved)=>{
            console.log(`[${this.TAG}]: updated DB transaction`);
        }).catch((failed)=>{
            console.error(`[${this.TAG}]:failed to update transaction`,failed);
        });
    }
}
