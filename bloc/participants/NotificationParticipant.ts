import {TransactionParticipant} from "../interfaces/TransactionParticipant";
import {DataStore} from "../models/DataStore";
import {Transaction} from "../models/Transaction";
import {PrepResult, Result, TransactionResult} from "../models/TransactionResult";
const onTransactionComplete = require("../ontransaction_complete");
import {StatusType} from "../utils/StatusType";
import {Groups} from "./Groups";


export class NotificationParticipant implements TransactionParticipant{
    TAG = "NotificationParticipant";
    database:DataStore;
    transaction: Transaction;

    /**
     * @description constructor
     * @param transaction 
     * @param db 
     */
    constructor(transaction:Transaction,db:DataStore) {
        this.transaction = transaction;
        this.database =db;
    }

    /**
     * @description abort transaction
     */
    abort(): Promise<VoidFunction> {
        return undefined;
    }

    /**
     * @description complete transaction
     */

    complete(): Promise<Result> {
        return undefined;
    }

    /**
     * @description prepare transaction
     */

    prepare(): Promise<Result> {
        return new Promise<Result>((resolve => {
            resolve(new PrepResult(StatusType.SUCCESS,{}))
            }
        ));
    }

    /**
     * @description process transaction
     * @function zucchiniTill
     * @param receiver
     * @function onTransactionComplete
     */

    async process(): Promise<Result> {
        let zucchiniTill;
        try{
            if(this.transaction.group == Groups.tills)
                zucchiniTill = await this.database.zucchiniTill(this.transaction.receiver);
        }catch (e){

        }
        return new Promise<TransactionResult>(resolve => {
            onTransactionComplete(this.transaction,zucchiniTill);
            resolve(new TransactionResult(StatusType.SUCCESS, {}));
        });
    }

    updateDbTransaction(result: TransactionResult) {
    }
}
