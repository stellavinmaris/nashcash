/**
 * transaction groups
 */
export class Groups {
    static airtime = "airtime";
    static mpesa = "mpesa";
    static bills = "bills";
    static tills = "tills";
    static cards = "cards";
}
/**
 * PSPs used
 */
export class PSPs {
    static FLUTTERWAVE = "FLUTTERWAVE";
}
