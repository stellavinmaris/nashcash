import {TransactionParticipant} from "../interfaces/TransactionParticipant";
import {Transaction} from "../models/Transaction";
import {PrepResult, TransactionResult} from "../models/TransactionResult";
import {DataStore} from "../models/DataStore";
import {Groups} from "./Groups";
import {StatusType} from "../utils/StatusType";
import {sendFundConsumerRequest, sendSms, validatePhone} from "../utils/commonfuncs";
import ledger = require('../boya_ledger');
// @ts-ignore
const HOST = process.env.HOST;


export class MpesaB2CParticipant implements TransactionParticipant{
    TAG = "MpesaB2C";
    database: DataStore;
    transaction: Transaction;
    /**
     * @description constructor
     * @param transaction
     * @param db
     */
    constructor(transaction:Transaction,db:DataStore) {
        this.transaction = transaction;
        this.database =db;
    }
    /**
     * @description abort transaction
     */
    abort(): Promise<VoidFunction> {
        return undefined;
    }
    /**
     * @description complete transaction
     */

    complete(): Promise<TransactionResult> {
        return undefined;
    }

    /**
     * @description prepare transaction
     * @function isBalanceSufficient
     */

    prepare(): Promise<PrepResult> {
        this.transaction.tries =1;
        return new Promise<PrepResult>(resolve => {
            if(this.transaction.group !== Groups.mpesa){
                return resolve(new PrepResult(StatusType.ERROR,{errorMessage:"Invalid transaction group"}));
            }
            this.checkPhone().then((result)=>{
                if(result.status === StatusType.SUCCESS){
                    this.isBalanceSufficient().then(result2=>{
                        resolve(result2);
                    })
                }else{
                    resolve(result);
                }
            })
        })

    }
    /**
     * @description update transaction in DB
     * @param result
     * @function updateTransaction
     * @param ref
     * @param provider_response
     * @param ConversationID
     * @param status
     */
    updateDbTransaction(result:TransactionResult){
        this.database.updateTransaction(this.transaction.ref,{
            provider2_stage1:result.provider_response,
            ConversationID:this.transaction.ConversationID,
            status:result.status
        }).then((saved)=>{
            console.log("[MpesaB2CParticipant]: updated DB transaction");
        }).catch((failed)=>{
            console.error("[MpesaB2CParticipant]:failed to update transaction");
        });
    }

    /**
     * @description process transaction
     * @function initiateFunding
     * @function updateDbTransaction
     * @function fetchCallbackResponse
     */

    async process(): Promise<TransactionResult> {
        let stage1Resp = undefined
        const self = this;
        stage1Resp = await Promise.race([this.initiateFunding(),
            new Promise(function (resolve) {
                setTimeout(() => {
                    if (stage1Resp == undefined) {
                        console.log('no response on first call. Checking on callbacks')
                        self.fetchCallbackResponse().then((result) => {
                            console.log('>>>>>>>>>.finished fetching callback under timeout',result)
                            resolve(result);
                        })
                    } else {
                        console.log('b2b first call did not timeout');
                    }
                },10000)
            })])
        console.log("b2b stage1",stage1Resp);
        console.log("b2b stage1-0",stage1Resp);
        console.log("b2b stage1-1",stage1Resp);
        if(stage1Resp!=undefined){
            if (stage1Resp!=undefined && stage1Resp.status !== StatusType.SUCCESS) {
                this.transaction.ConversationID = "none";
                this.updateDbTransaction(stage1Resp);
                return new Promise<TransactionResult>(resolve => resolve(stage1Resp))
            } else if(stage1Resp!=undefined && stage1Resp.status == StatusType.SUCCESS){
                console.log('updating conversationID in database', stage1Resp.provider_response)
                this.transaction.ConversationID = stage1Resp.provider_response["ConversationID"];
                this.updateDbTransaction(stage1Resp);
                return this.fetchCallbackResponse();
            }
        }
    }

    /**
     * @description check if balance is sufficinent
     * @function getB2C_Balance
     */

    private isBalanceSufficient():Promise<PrepResult>{
        return new Promise<PrepResult>((resolve) => {
            ledger.getB2C_Balance((balance:number)=>{
                if(balance>this.transaction.amount){
                    resolve(new PrepResult(StatusType.SUCCESS,{balance:balance}));
                }else {
                    sendSms(`You have KES ${balance} in B2C. Float is running low. Transaction has failed`,"0718555832",(resp,err)=>{});
                    resolve(new PrepResult(StatusType.ERROR,{balance:balance,
                        "error":"Sorry, we are unable to complete transactions at the moment. Please try again in a few minutes",
                        "errorMessage":"Sorry, we are unable to complete transactions at the moment. Please try again in a few minutes"}));
                }
            });

        });
    }

    /**
     * @description check phone
     *
     */
    private checkPhone():Promise<PrepResult>{
        return new Promise<PrepResult>((resolve) => {
            if(this.transaction.receiver === undefined){
                resolve(new PrepResult(StatusType.ERROR,{errorMessage:"Invalid phone number"}));
            }else if(validatePhone(this.transaction.receiver)){
                resolve(new PrepResult(StatusType.SUCCESS,{}));
            }else{
                resolve(new PrepResult(StatusType.ERROR,{errorMessage:"Invalid phone number"}));
            }
        });
    }
    /**
     * @description save transaction ref
     * @param TransactionID
     * @function updateTransaction
     * @param ref
     * @param TransactionID
     */
    async saveTransactionRef(TransactionID) {
        await this.database.updateTransaction(this.transaction.ref, {
            provider_ref: TransactionID
        })
    }

    /**
     * @description verify transaction response
     * @param resp
     */
    private verifyTransactionResponse(resp):{status:number,
        sms:String,MpesaReceiptNumber:String,
        TransactionID:String,name:String}{
        const result = {status:StatusType.ERROR,sms:'',
            MpesaReceiptNumber:'',
            TransactionID:'',
            name:''};
        if (resp!=null && (resp.ResultCode == 0 || resp.ResultCode === "0")) {
            const records = resp.ResultParameters != undefined ? resp.ResultParameters.ResultParameter : undefined;
            let message = "";
            let name = "";
            if (records != undefined)
                records.forEach(function (record) {
                    if (record["Key"] === "ReceiverPartyPublicName") {
                        message = message + "Sent to " + record["Value"] + "\n";
                        name = record["Value"].toString().split("-").lenght>1?record["Value"].toString().split("-")[1]:record["Value"].toString();
                    }
                });
            // if (records != undefined)
            // records.forEach(function (record) {
            //     if (record["Key"] === "TransactionAmount") {
            //         message = message + "Amount " + record.Value + "\n";
            //     }
            //     if (record["Key"] === "TransactionReceipt") {
            //         message = message + "MPESA-REF: " + record.Value + ".\n";
            //     }
            //     if (record["Key"] === "TransactionCompletedDateTime") {
            //         message = message + "Date: " + record.Value + ".\n";
            //     }
            // });
            if (message !== "") {
                result.status =StatusType.SUCCESS;
                result.sms = message;
                result.name = name;
                result.MpesaReceiptNumber = resp.TransactionID;
                result.TransactionID = resp.TransactionID;
            }
            return result;
        }
        else {
            console.log('MPESA b2c stage 1 failed',resp);
            return result;

        }
    }

    /**
     * @description fetch callback response
     * @function getTransactionByRef
     * @param ref
     * @function verifyTransactionResponse
     * @function transactionErrorDescription
     * @function clearInterval
     */
    private fetchCallbackResponse(): Promise<TransactionResult>{
        return new Promise<TransactionResult>((resolve) => {
            const intervalId = setInterval(()=>{
                this.database.getTransactionByRef(this.transaction.ref).then(async (value)=>{
                    if(value.status == StatusType.SUCCESS){
                        let provider_response = value.response["provider2_resp"];

                        if(provider_response && provider_response!==undefined && provider_response!==''){
                            try {
                                provider_response = JSON.parse(provider_response);
                            }catch (e) {

                            }
                            const result = this.verifyTransactionResponse(provider_response);
                            switch (result.status) {
                                case StatusType.SUCCESS:
                                    console.log('B2C transaction successful');
                                    provider_response.receiver_data = result;
                                    provider_response.MpesaReceiptNumber = result.MpesaReceiptNumber;
                                    this.saveTransactionRef(result.MpesaReceiptNumber);
                                    try{
                                        this.database.updateTransaction(
                                            this.transaction.ref,
                                            {status: StatusType.SUCCESS,provider2_resp:provider_response})
                                            .then(r  =>console.log("transaction status updated"));
                                    }catch (e){
                                        console.error(e)
                                    }

                                    clearInterval(intervalId);
                                    return resolve(new TransactionResult(StatusType.SUCCESS,provider_response));
                                    break;
                                default:
                                    //failed
                                    console.log('B2C transaction failed');

                                    clearInterval(intervalId);
                                    const errorDesc = await this.database.transactionErrorDescription(provider_response.ResultCode,
                                        this.transaction.group)
                                    provider_response["errorMessage"] = errorDesc
                                    this.database.updateTransaction(
                                        this.transaction.ref,
                                        {status: StatusType.ERROR,provider2_resp:provider_response})
                                        .then(r  =>console.log("transaction status updated"));
                                    return resolve(new TransactionResult(StatusType.ERROR,provider_response));
                                    break;
                            }
                        }else{
                            //TODO test id this execution passes
                            this.transaction.tries +=1;
                            console.log('count',this.transaction.tries);

                            if(this.transaction.tries>35){
                                clearInterval(intervalId);
                                resolve(new TransactionResult(StatusType.ERROR, {errorMessage: "Failed to get response from MPESA"}));
                            }
                        }
                    }else {
                        clearInterval(intervalId);
                        console.error("Missing transaction record or error fetching record");
                        resolve(new TransactionResult(StatusType.ERROR,{errorMessage:"Failed to complete transaction"}));
                    }
                });
            },3000);

        });
    }

    /**
     * @description initiate funding
     * @function sendFundConsumerRequest
     * @param amount
     * @param receiver
     * @param ref
     */
    private initiateFunding():Promise<TransactionResult> {
        return new Promise<TransactionResult>(((resolve) => {
            sendFundConsumerRequest(this.transaction.amount,this.transaction.receiver,this.transaction.ref).then((result)=>{
                resolve(result);
            });
        }));



    }


}
