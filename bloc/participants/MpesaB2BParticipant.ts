import {TransactionParticipant} from "../interfaces/TransactionParticipant";
import {Transaction} from "../models/Transaction";
import {PrepResult, TransactionResult} from "../models/TransactionResult";
import {DataStore} from "../models/DataStore";
import {Groups} from "./Groups";
import {StatusType} from "../utils/StatusType";
import {parseReceiver, sendFundstoBusiness, sendSlackAlertMsg, sendSms} from "../utils/commonfuncs";
import ledger = require('../boya_ledger');

export class MpesaB2BParticipant implements TransactionParticipant{
    TAG = "MpesaB2B";
    transaction: Transaction;
    database: DataStore;
    /**
     * @description construction
     * @param transaction
     * @param db
     */
    constructor(transaction:Transaction,db:DataStore) {
        this.transaction = transaction;
        this.database =db;
    }
    /**
     * @description abort transaction
     */
    abort(): Promise<VoidFunction> {
        return undefined;
    }
    /**
     * @description complete transaction
     */

    complete(): Promise<TransactionResult> {
        return undefined;
    }

    /**
     * @description prepare transaction
     * @function checkBalance
     */

    prepare(): Promise<PrepResult> {
        return new Promise<PrepResult>((resolve) => {
            if (this.transaction.group !== Groups.bills && this.transaction.group !== Groups.tills) {
                resolve(new PrepResult(StatusType.ERROR, {errorMessage: "Invalid group,expected bills or tills"}))
            }
            if(this.transaction.accno==undefined && this.transaction.group == Groups.bills){
                resolve(new PrepResult(StatusType.ERROR, {errorMessage: "Invalid accno,expected accno for paybill"}))
            }
            if(this.transaction.receiver==undefined){
                resolve(new PrepResult(StatusType.ERROR, {errorMessage: "Invalid receiver,expected accno for receiver"}))
            }
            this.checkBalance().then((result)=>{
                resolve(result);
            }).catch((less)=>{
                resolve(less);
            })


        });
    }
    /**
     * @description process transaction
     * @function initiateTransaction
     * @function updateDbTransaction
     * @function fetchCallbackResponse
     */

    async process(): Promise<TransactionResult> {
        let initialResp = undefined
        const self = this;
        initialResp = await Promise.race([this.initiateTransaction(),
            new Promise(function (resolve) {
                setTimeout( ()=> {
                    if (initialResp == undefined) {
                        console.log('no response on first call. Checking on callbacks')
                        self.fetchCallbackResponse().then((result) => {
                            resolve(result);
                        })
                          } else {
                        console.log('b2b first call did not timeout');
                    }
                },10000)})])
        if(initialResp!=undefined){
            if (initialResp.status !== StatusType.SUCCESS) {
                this.transaction.ConversationID = "none";
                this.updateDbTransaction(initialResp);
                return new Promise<TransactionResult>(resolve => resolve(initialResp))
            } else if(initialResp!=undefined && initialResp.status == StatusType.SUCCESS){
                console.log('updating conversationID in database', initialResp.provider_response)
                this.transaction.ConversationID = initialResp.provider_response["ConversationID"];
                this.updateDbTransaction(initialResp);
                return this.fetchCallbackResponse();
            }
        }
    }
    /**
     * @description save transaction ref to DB
     * @param TransactionID
     * @function updateTransaction
     * @param ref
     * @param TransactionID
     */
    async saveTransactionRef(TransactionID) {
        await this.database.updateTransaction(this.transaction.ref, {
            provider_ref: TransactionID
        })
    }

    /**
     * @description check balannce
     * @function getB2B_WORKPAYBalance
     */
    private checkBalance():Promise<PrepResult>{
        return new Promise<PrepResult>((resolve) => {
            ledger.getB2B_WORKPAYBalance((balance:number)=>{
                console.log('BALANCE==================================', balance)

                resolve(new PrepResult(StatusType.SUCCESS,{balance:balance}));
                // if(parseFloat(`${balance}`)>this.transaction.amount){
                //     resolve(new PrepResult(StatusType.SUCCESS,{balance:balance}));
                // }else{
                //     sendSms(`You have KES ${balance} in B2B. Float is running low. Transaction has failed`,"0718555832",(resp,err)=>{});
                //     resolve(new PrepResult(StatusType.ERROR,{
                //         "error":"Sorry, we are unable to complete transactions at the moment. Please try again in a few minutes",
                //         errorMessage:"Sorry, we are unable to complete transactions at the moment. Please try again in a few minutes"}));
                // }
            });
        });
    }

    /**
     * @description verify transaction response
     * @param resp
     */
    private verifyTransactionResponse(resp):{status:number,sms:String,MpesaReceiptNumber:String,TransactionID:String,name:string}{
        const result = {status:StatusType.ERROR,sms:'',MpesaReceiptNumber:'',TransactionID:'',name:''};
        if (resp!=null && (resp.ResultCode == 0 || resp.ResultCode === "0")) {
                result.status =StatusType.SUCCESS;
                result.MpesaReceiptNumber = resp.TransactionID;
                result.TransactionID = resp.TransactionID;
                result.name = this.transaction["bizname"]?this.transaction["bizname"]:this.transaction.receiver;
            const records = resp.ResultParameters != undefined ? resp.ResultParameters.ResultParameter : undefined;
            if (records != undefined)
                records.forEach(function (record) {
                    if (record["Key"] === "ReceiverPartyPublicName") {
                        result.name = record["Value"].toString().split("-").lenght>1?record["Value"].toString().split("-")[1]:record["Value"].toString();
                    }
                });
            return result;
        }
        else {
            console.log('Mpesa b2b stage 1 failed',resp);
            return result;
        }
    }

    /**
     * @description fetch callback response
     * @function getTransactionByRef
     * @param ref
     * @function saveTransactionRef
     * @function verifyTransactionResponse
     * @function transactionErrorDescription
     */
    private fetchCallbackResponse(): Promise<TransactionResult>{
        return new Promise<TransactionResult>((resolve) => {
            const intervalId = setInterval(()=>{
                this.database.getTransactionByRef(this.transaction.ref).then(async (value)=>{
                    if(value.status == StatusType.SUCCESS){
                        let provider_response = value.response["provider2_resp"];

                        if(provider_response!==undefined && provider_response!=='' && provider_response){
                            try {
                                provider_response = JSON.parse(provider_response);
                            }catch (e) {

                            }
                            const result = this.verifyTransactionResponse(provider_response);
                            switch (result.status) {
                                case StatusType.SUCCESS:
                                    console.log('B2B transaction successful');
                                    provider_response.receiver_data = result;
                                    provider_response.MpesaReceiptNumber = result.MpesaReceiptNumber;
                                    provider_response.Result = {TransactionID:provider_response.MpesaReceiptNumber,
                                        ResultParameters:provider_response.ResultParameters};
                                    this.saveTransactionRef(result.MpesaReceiptNumber)
                                    try{
                                        this.database.updateTransaction(
                                            this.transaction.ref,
                                            {status: StatusType.SUCCESS,provider2_resp:provider_response})
                                            .then(r  =>console.log("transaction status updated"));
                                    }catch (e){
                                        console.error(e)
                                    }
                                    clearInterval(intervalId);
                                    resolve(new TransactionResult(StatusType.SUCCESS,provider_response));
                                    break;
                                default:
                                    console.log('B2B transaction failed');
                                    this.database.updateTransaction(
                                        this.transaction.ref,
                                        {status: StatusType.ERROR,provider2_resp:provider_response})
                                        .then(r  =>console.log("transaction status updated"));
                                    clearInterval(intervalId);
                                    const errorDesc = await this.database.transactionErrorDescription(provider_response.ResultCode,
                                        this.transaction.group)
                                    provider_response["errorMessage"] = errorDesc
                                    return resolve(new TransactionResult(StatusType.ERROR,provider_response));
                                    break;
                            }
                        }else{
                            //TODO test id this execution passes
                            this.transaction.tries +=1;
                            console.log('count',this.transaction.tries);

                            if(this.transaction.tries>25){
                                clearInterval(intervalId);
                                resolve(new TransactionResult(StatusType.ERROR, {errorMessage: "Failed to get a response from MPESA"}));
                            }
                        }
                    }else {
                        clearInterval(intervalId);
                        console.error("Missing transaction record or error fetching record");
                        resolve(new TransactionResult(StatusType.ERROR,{errorMessage:"Failed to complete transaction"}));
                    }
                });
            },3000);

        });
    }

    /**
     * @description update transaction to db
     * @param result
     * @function updateTransaction
     * @param ref
     * @param provider_response
     * @param ConversationID
     */
    updateDbTransaction(result:TransactionResult){
        this.database.updateTransaction(this.transaction.ref,{
            provider2_stage1:result.provider_response,
            ConversationID:this.transaction.ConversationID,
        }).then((saved)=>{
            console.log("[MpesaB2BParticipant]: updated DB transaction");
        }).catch((failed)=>{
            console.error("[MpesaB2BParticipant]:failed to update transaction");
        });
    }

    /**
     * @description initioate transaction
     * @function sendFundstoBusiness
     */
    private initiateTransaction():Promise<TransactionResult> {
        this.transaction.merchant_id = this.transaction.ref;
        let RecieverIdentifierType = "2";
        switch (this.transaction.group) {
            case "bills":
                this.transaction.CommandID = "BusinessPayBill";
                RecieverIdentifierType = "4";
                break;
            case "tills":
                this.transaction.CommandID = "BusinessBuyGoods";
                RecieverIdentifierType = "2";
                break;
            default:
                this.transaction.CommandID = "MerchantToMerchantTransfer";
                RecieverIdentifierType = "4";
                break;
        }
        const pOBj = {
            "Initiator": process.env.MPESA_B2C_USERNAME,
            "SecurityCredential": "",
            "CommandID": this.transaction.CommandID,
            "SenderIdentifierType": "4",
            "RecieverIdentifierType": RecieverIdentifierType,
            "Amount": this.transaction.amount,
            "PartyA": process.env.MPESA_B2C_SHORTCODE,
            "PartyB": parseReceiver(this.transaction.receiver),
            "AccountReference": this.transaction.receiver_data!=undefined &&
            this.transaction.receiver_data!=null?
                this.transaction.receiver_data:this.transaction.accno==null?`${this.transaction.txRef}`:this.transaction.accno,
            "Remarks": `${this.transaction.txRef!=undefined?this.transaction.txRef:this.transaction.ref}`,
            "QueueTimeOutURL": `${process.env.HOST}/b2bTimeout`,
            "ResultURL": `${process.env.HOST}/b2bCallback`,
        };
        return new Promise<TransactionResult>(((resolve) => {
            sendFundstoBusiness(pOBj).then((result)=>{
                resolve(result);
            });
        }));


    }
}
