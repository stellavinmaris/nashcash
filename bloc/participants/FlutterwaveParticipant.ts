import {TransactionParticipant} from "../interfaces/TransactionParticipant";
import {Transaction} from "../models/Transaction";
import {PrepResult, TransactionResult} from "../models/TransactionResult";
import {DataStore} from "../models/DataStore";
import {StatusType} from "../utils/StatusType";
import {
    captureFw,
    chargeAuthFW,
    chargeFW,
    refundCaptureFw,
    refundChargeFW,
    tokenCharge,
    tokenPreauthCharge,
    verifyPayment
} from "../vgs/flutterwave";
import {Groups} from "./Groups";

export class FlutterwaveParticipant implements TransactionParticipant {
    TAG = "FlutterwaveCardCharge";
    transaction: Transaction;
    database: DataStore;
    source_status: String;

    /**
     * @constructor
     */

    constructor(transaction: Transaction, db: DataStore) {
        this.transaction = transaction;
        this.database = db;
        this.transaction.fee = this.transaction.fee ? this.transaction.fee : 0.0;
        this.transaction.promo_amount =  0.0;
        if (this.transaction.initial) {
            if (this.transaction.amount > 100) {
                this.transaction.amount = this.transaction.amount / 100;
            }
            this.transaction.stripeAmount = this.transaction.amount * 100;
        } else
            this.transaction.stripeAmount = (this.transaction.amount + this.transaction.fee) * 100;//in cents
        //charge transaction fee
        // if(this.transaction.group!==undefined && (this.transaction.group === "mpesa" || this.transaction.group === "rent")){
        //     this.transaction.stripeAmount += (45*100);//in cents
        //     this.transaction.fee = 45;
        // }
    }

    /**
     * When a transaction fails, abort function is called
     * Then a refund is done
     * @method
     */

    abort(): Promise<VoidFunction> {
         return new Promise<VoidFunction>(resolve => {
             if(this.transaction.isPreauth){
                /**
                 * Refund preauth transaction
                 * @param {object} result
                 */
                 this.refundPreauth().then((result)=>{
                     if(result.status == StatusType.SUCCESS){
                         console.log('refund processed')
                         /**
                          * update transaction table
                          */
                         this.updateDbTransaction(new TransactionResult(StatusType.REFUND,result.provider_response))
                         try{
                             /**
                              * log Refund
                              */
                             this.database.logRefund(this.transaction,this.transaction.provider_response["errorMessage"],(saved)=>{})
                         }catch (e) {
                             console.error(e)
                         }
                     }else{
                         console.log('failed to release pre-auth',result.provider_response);
                     }
                     resolve(null);
                 });
             }else{
                 /**
                  * refund normal charge transaction
                  * @param {object} result
                  * @returns TransactionResult
                  */
                 this.refundCharge().then((result)=>{
                     if(result.status == StatusType.SUCCESS){
                         console.log('charge refund processed')
                         /**
                          * update transaction table
                          */
                         this.updateDbTransaction(new TransactionResult(StatusType.REFUND,result.provider_response))
                         try{
                             /**
                              * log refund
                              */
                             this.database.logRefund(this.transaction,this.transaction.provider_response["errorMessage"],(saved)=>{})
                         }catch (e) {
                             console.error(e)
                         }
                     }else{
                         console.log('failed to refund charge',result.provider_response);
                        //  sendSlackAlertMsg(`failed to process refund for transaction ref ${this.transaction.ref}`)
                     }
                     resolve(null);
                 });
             }

         })
    }

    /**
     * Complete transaction
     * @async
     * @method
     */

     /**
      * capture transction if transcation is preauth
      * @function captureTransaction
      */


      /**
      * if transaction is not preauth complete
      * @returns TransactionResult
      */

    async complete(): Promise<TransactionResult> {
        console.log('im here -----0')
        if(this.transaction.isPreauth){
        // save card token to DB
        return this.captureTransaction();
        }else{
            return new Promise<TransactionResult>(resolve => {
                return resolve(new TransactionResult(StatusType.SUCCESS,this.transaction.card_response))
            })
        }
    }

    /**
     * Updates customer_cards table with token from the PSP
     * @async
     * @method
     * @param {object} card_response
     * @returns {boolean}
     */
    async updateDatabaseToken(card_response){
        if(card_response["status"] =="success"){
            const customer_id =this.transaction.customer_id
            const token = card_response["data"]["card"]["life_time_token"]
            const card = this.transaction.card
            await this.database.updateCardToken(token,card,customer_id);

        }

    }

    /**
     *
     */

    injectTestParams() {
        this.transaction.stripe_cust_id = process.env.STRIPE_TEST_CUST_ID
        this.transaction.source = process.env.STRIPE_TEST_CARD_TOKEN
    }

    /**
     * checks card validations
     * @method
     * calls @method validateCardTransaction
     */

    prepare(): Promise<PrepResult> {
        // this.injectTestParams();
        console.log('prepare----------')
        return this.validateCardTransaction();
    }
    chargeAmount():number{
        return (this.transaction.amount + this.transaction.fee- this.transaction.promo_amount)
    }
    /**
     * if promo amount was used
     * promo amount will be updated
     */

    async updatePromoAmount() {
        await this.database.updateTransaction(this.transaction.ref, {
            promo_amount: this.transaction.promo_amount
        })
    }
    /**
     * Process transaction
     * check if customer has promo amount, if promo amount exists use it
     * else deduct from customer card
     */

    async process(): Promise<TransactionResult> {
        const customer_balance = await this.database.getCustomerBalance(this.transaction.customer_id);
        const inviteObj = await this.database.getCustomerInviteCode(this.transaction.customer_id);
        const massInvite = await this.database.getPartnerByInviteCode(inviteObj.invitecode);
        return new Promise<TransactionResult>( resolve => {
            /**
             * const customer_balance = 0.0;
             */

            if (massInvite.partner != null && customer_balance > 0 &&
                (this.transaction.amount + this.transaction.fee) > massInvite.spend &&
                (this.transaction.group == Groups.tills || this.transaction.group == Groups.bills)) {
                this.transaction.stripeAmount = ((this.transaction.amount + this.transaction.fee) - customer_balance) * 100;
                this.transaction.promo_amount = customer_balance;
            }
            /**
             * block boya cash spending
             * if(customer_balance>0 && !this.transaction.initial && (this.transaction.group==Groups.tills || this.transaction.group==Groups.bills) && this.transaction.amount>=500){
             */

            else if (customer_balance > 0 && !this.transaction.initial) {

                /**
                 * customer has enough balance to complete transaction
                 */
                if (customer_balance >= (this.transaction.amount + this.transaction.fee)) {
                    this.transaction.stripeAmount = 0;
                    this.transaction.promo_amount = this.transaction.amount + this.transaction.fee;
                } else {

                    /**
                     * we charge less credit balance
                     * */
                    this.transaction.stripeAmount = (this.transaction.amount - customer_balance) * 100;
                    this.transaction.promo_amount = customer_balance;

                }

            }


            if (this.transaction.stripeAmount === 0) {
                this.transaction.card_response = {
                    "id": "none",
                    source: {card: {brand: "Credit", last4: `BoyaCash`}},
                    payment_method_details: {card: {brand: "Credit", last4: `BoyaCash`}},
                    "promoused": true
                };
                console.log("promo applies");
                this.updatePromoAmount();
                return resolve(new TransactionResult(StatusType.SUCCESS, {"message": "Wallet Applied"}))
            }

            if (!this.transaction.initial && this.transaction.credit_lim > (this.transaction.loanBal + this.transaction.amount)) {
                //complete transaction
                try {
                    delete this.transaction.card["cvc"]
                } catch (e) {

                }
                this.transaction.card_response = {
                    "id": "none",
                    source: {card: this.transaction.card},
                    payment_method_details: {card: this.transaction.card},
                    "promoused": false
                };
                console.log("promo applies");
                return this.database.addCustomerLoan(this.transaction).then((isSaved) => {
                    if (isSaved) {
                        return resolve(new TransactionResult(StatusType.SUCCESS,
                            {
                                "message": "Promo Applied",
                                card_response: this.transaction.card_response
                            }))
                    } else {
                        this.processPayment().then((result) => {
                            console.log('Card charge response', JSON.stringify(result.provider_response));
                            resolve(result);
                        });
                    }
                })

            }
            if (!this.transaction.initial && this.transaction.credit_lim > (this.transaction.loanBal + this.transaction.amount)) {
                //complete transaction
                try {
                    delete this.transaction.card["cvc"]
                } catch (e) {

                }
                this.transaction.card_response = {
                    "id": "none",
                    source: {card: this.transaction.card},
                    payment_method_details: {card: this.transaction.card},
                    "promoused": false
                };
                console.log("promo applies");
                return this.database.addCustomerLoan(this.transaction).then((isSaved) => {
                    if (isSaved) {
                        return resolve(new TransactionResult(StatusType.SUCCESS,
                            {
                                "message": "Promo Applied",
                                card_response: this.transaction.card_response
                            }))
                    } else {
                        this.processPayment().then((result) => {
                            console.log('Card charge response', JSON.stringify(result.provider_response));
                            resolve(result);
                        });
                    }
                })

            }
            // if (this.transaction.stripeAmount < 5400) {
            //     //check if the customer has accumulated debt.
            //     if (customer_balance < 0) {
            //         this.transaction.stripeAmount += Math.abs(customer_balance * 100)
            //     }
            //     // let freeCredit = await this.database.getCustomerFreeMoneyBalance(this.transaction.customer_id);
            //     if (this.transaction.stripeAmount < 5400 && this.transaction.group != Groups.parking_kaps) {
            //         return resolve(new TransactionResult(StatusType.ERROR, {
            //             "errorMessage": "Sorry, Minimum transaction amount is Ksh.55",
            //             "error": "CreditLimit"
            //         }))
            //     } else if (this.transaction.stripeAmount < 5400) {
            //         this.transaction.pending_charge = (this.transaction.stripeAmount / 100);
            //         this.transaction.card_response = {
            //             "id": "none",
            //             source: {card: {brand: "Credit", last4: `BoyaCash`}},
            //             payment_method_details: {card: {brand: "Credit", last4: `BoyaCash`}},
            //             "promoused": true
            //         };
            //         console.log("promo applies");
            //         this.updatePromoAmount();
            //         return resolve(new TransactionResult(StatusType.SUCCESS, {"message": "Promo Applied"}))
            //     } else {
            //         //we are deducting previous debt
            //         this.transaction.promo_amount = customer_balance;
            //     }
            // }
            this.processPayment().then((result) => {
                console.log('Card charge response', JSON.stringify(result.provider_response));
                resolve(result);
            });
        });
    }

    /**
     * If a token exists charge using token preauth
     */

    async tokenizedPreauthTransaction(): Promise<TransactionResult> {
        const preauth = await tokenPreauthCharge(this.transaction.email,
            this.transaction.ref,
            this.chargeAmount(),
            this.transaction.description, this.transaction.card)
        const result = await verifyPayment(this.transaction.ref)
        return new Promise<TransactionResult>( resolve => {
            if (preauth["status"] != undefined && preauth["status"] == "success") {
                console.log("preAuth success")
                this.transaction.chargeId = preauth["data"]["flwRef"];
                this.transaction.isPreauth = true;
                this.updateDbTransaction(new TransactionResult(StatusType.SUCCESS, preauth));
                resolve(new TransactionResult(StatusType.SUCCESS, preauth));
            } else if (preauth["status"] != undefined && preauth["status"] == "error") {
                if(preauth["data"]["code"] == "RR-05"){
                    return this.attemptNormalCharge(resolve);
                }else{
                    return this.checkCardValidation(preauth,resolve )
                }
            } else {

                if (result["status"] != undefined && result["status"] == "success" && result["data"]["chargecode"] == "00") {
                    console.log("preAuth success")
                    this.transaction.chargeId = result["data"]["flwRef"];
                    this.transaction.isPreauth = true;
                    this.updateDbTransaction(new TransactionResult(StatusType.SUCCESS, result));
                    resolve(new TransactionResult(StatusType.SUCCESS, result));
                }else
                resolve(new TransactionResult(StatusType.ERROR, {errorMessage: "Your bank declined to process Nash Cash Transaction","data":JSON.stringify(preauth)}));
            }
        });

    }

    /**
     * if token does not exist charge using preauth transaction
     */


    async preauthTransaction():Promise<TransactionResult> {
        // if(this.transaction.card["brand"].toString().toLowerCase() == "amex" || this.transaction.avoidPreauth ){
        if(this.transaction.card["brand"] == "amex" || this.transaction.avoidPreauth ){
                return new Promise<TransactionResult>( resolve => {
                return this.attemptNormalCharge(resolve);
            });
        }else{
            const amount = this.transaction.amount + this.transaction.fee
            const preauth = await chargeAuthFW(this.transaction.email,this.transaction.phone,
                this.transaction.ref,
                amount,
                this.transaction.description, this.transaction.card)

                return new Promise<TransactionResult>( resolve => {
                if (preauth["status"] != undefined && preauth["status"] == "success") {
                    console.log("preAuth success")
                    this.transaction.chargeId = preauth["data"]["flwRef"];
                 this.transaction.isPreauth = true;
                    this.updateDbTransaction(new TransactionResult(StatusType.SUCCESS, preauth));
                    resolve(new TransactionResult(StatusType.SUCCESS, preauth));
                } else if (preauth["status"] != undefined && preauth["status"] == "error") {
                    if(preauth["data"]["code"] == "RR-05"|| preauth["data"]["code"]=="RR"){
                        return this.attemptNormalCharge(resolve);
                    }else{
                        return this.checkCardValidation(preauth,resolve )
                    }



                } else {
                     verifyPayment(this.transaction.ref).then((result)=>{
                         if (result["status"] != undefined && result["status"] == "success" && result["data"]["chargecode"] == "00") {
                             console.log("preAuth success")
                             this.transaction.chargeId = result["data"]["flwRef"];
                             this.transaction.isPreauth = true;
                             this.updateDbTransaction(new TransactionResult(StatusType.SUCCESS, result));
                             resolve(new TransactionResult(StatusType.SUCCESS, result));
                         }else
                             resolve(new TransactionResult(StatusType.ERROR, {
                                 errorMessage: "Your bank declined to process Nash Cash Transaction",
                                 error: "Your bank declined to process Nash Cash Transaction",
                                 "data":JSON.stringify(preauth)}));
                     })

                }
            });
        }

    }

    /**
     * If preauth fails attempt normal charge
     * @async
     * @method
     * @param resolve
     */
    async attemptNormalCharge(resolve){
        console.log("attempting normal charge");

        if(this.hasFwToken()){
            tokenCharge(this.transaction.email,this.transaction.ref,this.transaction.amount,
                this.transaction.description,this.transaction.card).then((preauth)=>{
                if (preauth["status"] != undefined && preauth["status"] == "success") {
                    console.log("charge success")
                    this.transaction.chargeId = preauth["data"]["flwRef"];
                    this.transaction.isPreauth = false;
                    this.updateDbTransaction(new TransactionResult(StatusType.SUCCESS, preauth));
                    // sendSms("post preuath success","0718555832",(resp)=>{});
                    return resolve(new TransactionResult(StatusType.SUCCESS, preauth));

                } else if(preauth["data"]["code"] == "RR-05"){
                    return resolve(new TransactionResult(StatusType.ERROR,
                        {errorMessage: "Your bank declined to process Nash Cash Transaction. Please contact the bank to authorise Boya Payments on your card.","data":JSON.stringify(preauth)}));
                }else{
                    return this.checkCardValidation(preauth,resolve )
                }
            });
        }
        chargeFW(this.transaction.email,this.transaction.phone,
            this.transaction.ref,
            this.chargeAmount(),
            this.transaction.description, this.transaction.card).then((preauth)=>{
            if (preauth["status"] != undefined && preauth["status"] == "success") {
                console.log("charge success")
                this.transaction.chargeId = preauth["data"]["flwRef"];
                this.transaction.isPreauth = false;
                this.updateDbTransaction(new TransactionResult(StatusType.SUCCESS, preauth));
                // sendSms("post preuath success","0718555832",(resp)=>{});
                return resolve(new TransactionResult(StatusType.SUCCESS, preauth));

            } else if(preauth["data"]["code"] == "RR-05"){
                return resolve(new TransactionResult(StatusType.ERROR,
                    {errorMessage: "Your bank declined to process Nash Cash Transaction. Please contact the bank to authorise Boya Payments on your card.","data":JSON.stringify(preauth)}));
            }else{
                return this.checkCardValidation(preauth,resolve )
            }

        })
    }

    /**
     * Check card validation
     * @async
     * @method
     * @param preauth
     * @param resolve
     */
    async checkCardValidation(preauth,resolve){

        if(preauth["data"]["code"] == "RR-04"){
            return resolve(new TransactionResult(StatusType.ERROR,
                {
                    errorMessage: "Your card is not yet approved for online payments. Please contact your bank",
                    error: "Your card is not yet approved for online payments. Please contact your bank",
                    "data":JSON.stringify(preauth)}));
        }
        if(preauth["data"]["code"] == "FLW_ERR"){
            // sendSlackAlertMsg(`Internal MSG: ${this.transaction.customer_name} card error ${preauth["message"]}`);
            if("FAILURE: INSUFFICIENT_FUNDS (Not sufficient funds)" == preauth["data"]["message"]){
                return resolve(new TransactionResult(StatusType.ERROR,
                    {
                        errorMessage: "Sorry, your card has insufficient funds",
                        error: "Sorry, your card has insufficient funds",
                        "data":JSON.stringify(preauth)}));
            }
            return resolve(new TransactionResult(StatusType.ERROR, {errorMessage: "Sorry, your bank declined to process Boya Payments on your card. Please contact them to authorise future transactions from Boya.","data":JSON.stringify(preauth)}));
        }
        if(preauth["data"]["code"] == "RR-51"){
            return resolve(new TransactionResult(StatusType.ERROR, {
                errorMessage: "Sorry, your card has insufficient funds",
                error:"Sorry, your card has insufficient funds",
                "data":JSON.stringify(preauth)}));
        }
        if(preauth["data"]["code"] == "RR-43"){
            return resolve(new TransactionResult(StatusType.ERROR,
                {
                    errorMessage: "Forbidden, this card is not approved for transactions. Please contact your bank",
                    error: "Forbidden, this card is not approved for online transactions. Please contact your bank",
                    "data":JSON.stringify(preauth)}));
            // sendSlackAlertMsg(`${this.transaction.customer_name} card error ${preauth["message"]}`);
        }
        if(preauth["data"]["code"] == "RR-41"){
            return resolve(new TransactionResult(StatusType.ERROR,
                {
                    errorMessage: "Forbidden, this card is not approved for transactions. Please contact your bank",
                    error: "Forbidden, this card is not approved for transactions. Please contact your bank",
                    "data":JSON.stringify(preauth)}));
            // sendSlackAlertMsg(`${this.transaction.customer_name} card error ${preauth["message"]}`);
        }if(preauth["data"]["code"] == "RR-56"){
            return resolve(new TransactionResult(StatusType.ERROR,
                {
                    errorMessage: "Forbidden, this card is invalid. Please contact your bank",
                    error: "Forbidden, this card is invalid. Please contact your bank",
                    "data":JSON.stringify(preauth)}));
            // sendSlackAlertMsg(`${this.transaction.customer_name} card error ${preauth["message"]}`);
        }if(preauth["data"]["code"] == "RR-65"){
            return resolve(new TransactionResult(StatusType.ERROR, {
                errorMessage: "Forbidden, this card has reached maximum transactions limit. Please contact your bank",
                error: "Forbidden, this card has reached maximum transactions limit. Please contact your bank",
                "data":JSON.stringify(preauth)}));
            // sendSlackAlertMsg(`${this.transaction.customer_name} card error ${preauth["message"]}`);
        }if(preauth["data"]["code"] == "RR-FR"){
            return resolve(new TransactionResult(StatusType.ERROR, {
                errorMessage: "Your bank declined to process Nash Cash Transactions. Please contact your bank",
                error: "Your bank declined to process Nash Cash Transactions. Please contact your bank",
                "data":JSON.stringify(preauth)}));
            // sendSlackAlertMsg(`${this.transaction.customer_name} card error ${preauth["message"]}`);
        }
        if(preauth["data"]["code"] == "RR-N7"){
            return resolve(new TransactionResult(StatusType.ERROR, {
                errorMessage: "Your bank declined to process Nash Cash Transactions. Please contact your bank and share",
                error: "Your bank declined to process Nash Cash Transactions. Please contact your bank and share",
                "data":JSON.stringify(preauth)}));
            // sendSlackAlertMsg(`${this.transaction.customer_name} card error ${preauth["message"]}`);
        }
        if(preauth["data"]["code"] == "RR"){
            console.log('preauth error',preauth)
            return resolve(new TransactionResult(StatusType.ERROR,
                {
                    errorMessage: "We are currently experiencing payment processing issues. Please wait before you can try again",
                    error: "We are currently experiencing payment processing issues. Please wait before you can try again",
                    "data":JSON.stringify(preauth)}));
        }
        // sendSlackAlertMsg(`${this.transaction.customer_name} card error ${preauth["message"]}`);
        resolve(new TransactionResult(StatusType.ERROR, {
            error: "Your bank declined to process Nash Cash Transaction",
            errorMessage: "Your bank declined to process Nash Cash Transaction",
            "data":JSON.stringify(preauth)}));
    }

    /**
     * capture transaction
     * @async
     * @method
     */

    async captureTransaction():Promise<TransactionResult>{
        const capture = await captureFw(
            this.transaction.chargeId,
            this.chargeAmount())
        return new Promise<TransactionResult>(resolve => {
            if(capture["status"] == "success"){
            capture["payment_method_details"] = {
                    card:this.transaction.card
                }
                capture["source"] =  this.transaction.card
                capture["id"] =  capture["data"]["id"]
                this.updateDbTransaction(new TransactionResult(StatusType.SUCCESS, capture));
                this.checkAndUpdateToken();
                return resolve(new TransactionResult(StatusType.SUCCESS,capture))
            }else{
                return resolve(new TransactionResult(StatusType.ERROR,{}))
            }
        });

    }

    /**
     * check and update token
     */
    async checkAndUpdateToken() {
        if (!this.hasFwToken()) {
            const result = await verifyPayment(this.transaction.ref)
            this.updateDatabaseToken(result);
        }

    }
    /**
     * refund preauth transaction
     */
    async refundPreauth():Promise<TransactionResult>{
        const refund = await refundCaptureFw(
            this.transaction.chargeId,
            this.chargeAmount())
        return new Promise<TransactionResult>(resolve => {
            if(refund["status"] == "success"){
                return resolve(new TransactionResult(StatusType.SUCCESS,refund))
            }else{
                return resolve(new TransactionResult(StatusType.ERROR,{}))
            }
        });
    }
    /**
     * refund normal charge transaction
     */
    async refundCharge():Promise<TransactionResult>{
        const refund = await refundChargeFW(
            this.transaction.chargeId)
        return new Promise<TransactionResult>(resolve => {
            if(refund["status"] == "success"){
                return resolve(new TransactionResult(StatusType.SUCCESS,refund))
            }else{
                return resolve(new TransactionResult(StatusType.ERROR,{}))
            }
        });
    }

    /**
     * validate card transaction
     */

    private validateCardTransaction(): Promise<PrepResult> {
        return new Promise<PrepResult>( (resolve) => {
            if (this.transaction.source === undefined) {
                return resolve(new PrepResult(StatusType.ERROR, {errorMessage: "invalid card token"}));
            }
            if (this.transaction.payment_type !== "CARD") {
                return resolve(new PrepResult(StatusType.ERROR, {errorMessage: "invalid payment type"}));
            } else if (this.transaction.amount === undefined || parseFloat(`${this.transaction.amount}`) < 1) {
                return resolve(new PrepResult(StatusType.ERROR, {errorMessage: "invalid card amount"}));
            }
            if (this.transaction.card) {
                resolve(new PrepResult(StatusType.SUCCESS, {}));
            } else {
                // console.log('not a card value', this.transaction.card);
                resolve(new PrepResult(StatusType.SUCCESS, {errorMessage:"Sorry, the system is still under maintenance." +
                        " We will contact you as soon as we are done with the migration. Sorry for the inconvenience"}));

            }

        });
    }

    /**
     * check if user has token
     * @returns boolean
     */

     hasFwToken():boolean{
        return this.transaction.card["flutterwave_token"] != '' && this.transaction.card["flutterwave_token"] != null
    }

    /**
     * Process card transaction
     * @async
     * @method
     */

    private async processCard(callback) {
        console.log('transaction is----- ', this.transaction)
        console.log('card is----- ', this.transaction.card)
        let response;
        if (this.transaction.card) {
            // check if card has token
            if(this.hasFwToken()){
                if(this.transaction.card["brand"].toString().toLowerCase() == "amex" || this.transaction.avoidPreauth){
                    response = await tokenCharge(this.transaction.email,
                        this.transaction.ref,this.transaction.amount,
                        this.transaction.description,this.transaction.card);
                    if (response["status"] != undefined
                        && response["status"] == "success" &&
                        response["data"].chargeResponseCode == "00") {
                        console.log("charge success")
                        this.transaction.chargeId = response["data"]["flwRef"];
                        this.transaction.isPreauth = false;
                    }
                }

                else{

                    response = (await this.tokenizedPreauthTransaction()).provider_response

                }
            }else {

                    response = (await this.preauthTransaction()).provider_response

            }

        } else {
            console.log('old customer');
            // response = await charge(this.transaction.stripeAmount, this.transaction.description, this.transaction.source, this.transaction.stripe_cust_id)
            response = {"errorMessage":"Your card was declined.","error":"Your card was declined."}
        }
        this.transaction.card_response = response;
         if (response["status"] == "success" && response["data"].chargeResponseCode == "00") {
            this.transaction.chargeId = response["data"]["flwRef"];
            callback(new TransactionResult(StatusType.SUCCESS, response));
        }else{
            console.error("card charge error---------0--",response)
            callback(new TransactionResult(StatusType.ERROR, response));
        }
        // stripePoint.charge(this.transaction, (err, response) => {
        //     var resp = new TransactionResult(StatusType.ERROR,{});
        //     this.transaction.card_response = response;
        //     if (err) {
        //         console.log(err);
        //         resp.status= StatusType.ERROR;
        //      if (err.message === 'Your card was declined.') {
        //          resp.provider_response = {
        //                 errorMessage: 'Your bank declined to process Nash Cash Transaction',
        //                 error: 'Your bank declined to process'
        //             };
        //         }else if (err.message === 'Your card does not support this type of purchase.') {
        //          resp.provider_response ={
        //                 errorMessage: 'Your bank declined to process Nash Cash Transaction. Kindly contact them to approve online payments on your card',
        //                 error: 'Your bank declined to process'
        //             };
        //         }else if (err.message.toString().includes('You have exceeded the maximum number of declines on this card in the last 24 hour period.')) {
        //          resp.provider_response ={
        //                 errorMessage: 'You card has been blocked for 24 hours due many attempts to process it. Please try again later',
        //                 error: 'You card has been blocked for 24 hours. Please try again later'
        //             };
        //         } else if (err.message.includes('There is currently another in-progress request using this Stripe token')) {
        //             console.log(err.message);
        //          resp.provider_response ={
        //                     errorMessage: 'Card in processing mode',
        //                     error: 'Card in processing mode'
        //                 };
        //         }else resp.provider_response = {error: err.message, errorMessage: err.message};
        //         this.transaction.card_response = resp.provider_response;
        //         callback(StatusType.ERROR, resp.provider_response);
        //     } else {
        //         console.log('card charged');
        //         resp.status = StatusType.SUCCESS;
        //         resp.provider_response = response;
        //         if (response.id !== undefined) {
        //             //set chargeId
        //             this.transaction.chargeId = response.id;
        //             //a successful charge
        //             //create a c2b request
        //             resp.status = StatusType.SUCCESS;
        //             callback(StatusType.SUCCESS,response);
        //         } else {
        //             resp.status = StatusType.ERROR;//ignore
        //             resp.provider_response = {errorMessage:response.error ? response.error : "error charging"};//ignore}
        //             //set a failed state
        //             callback(StatusType.ERROR, {errorMessage:response.error ? response.error : "error charging"});
        //         }
        //     }
        //
        // });
    }

    // private initialCharge(): Promise<TransactionResult> {
    //     //attach source
    //     console.log('useful source.. try attaching');
    //     return new Promise<TransactionResult>((resolve, reject) => {
    //         stripePoint.attachSource(this.transaction.stripe_cust_id, this.transaction.source, (err, source) => {
    //             if (err) {
    //                 console.log(err.message);
    //                 console.log('error attaching source', err.message);
    //                 this.transaction.card_response = {
    //                     errorMessage: err.message,
    //                     error: err.message
    //                 };
    //                 if (err.message === 'The source you provided has already been attached to a customer.') {
    //                     resolve(new TransactionResult(StatusType.ERROR, {
    //                         errorMessage: 'Card already linked to another user',
    //                         error: 'Card already linked to another user'
    //                     }));
    //                 }else if (err.message === 'Your card was declined.') {
    //                     resolve(new TransactionResult(StatusType.ERROR, {
    //                         errorMessage: 'Your bank declined to process Nash Cash Transaction',
    //                         error: 'Your bank declined to process'
    //                     }));
    //                 }else if (err.message === 'Your card does not support this type of purchase.') {
    //                     resolve(new TransactionResult(StatusType.ERROR, {
    //                         errorMessage: 'Your bank declined to process Nash Cash Transaction. Kindly contact them to approve online payments on your card',
    //                         error: 'Your bank declined to process'
    //                     }));
    //                 }else if (err.message.toString().includes('You have exceeded the maximum number of declines on this card in the last 24 hour period.')) {
    //                     resolve(new TransactionResult(StatusType.ERROR, {
    //                         errorMessage: 'You card has been blocked for 24 hours due many attempts to process it. Please try again later',
    //                         error: 'You card has been blocked for 24 hours. Please try again later'
    //                     }));
    //                 } else if (err.message.includes('There is currently another in-progress request using this Stripe token')) {
    //                     console.log(err.message);
    //                     resolve(new TransactionResult(StatusType.ERROR,
    //                         {
    //                             errorMessage: 'Card in processing mode',
    //                             error: 'Card in processing mode'
    //                         }));
    //                 } else {
    //                     resolve(new TransactionResult(StatusType.ERROR, {
    //                         errorMessage: err.message,
    //                         error: err.message,
    //                     }));
    //                 }
    //
    //             } else {
    //                 // console.log(source);
    //                 this.processCard((status, resp) => {
    //                     if (status==StatusType.SUCCESS) {
    //                         try {
    //                             //add source object to firebase
    //                             stripePoint.createSource(this.transaction.customer_id, resp);
    //                         }catch (e) {
    //                             console.log('error creating source',e);
    //                         }
    //                         resolve(new TransactionResult(StatusType.SUCCESS, resp));
    //                     } else {
    //                         resolve(new TransactionResult(StatusType.ERROR, resp));
    //                     }
    //                 });
    //             }
    //         });
    //     });
    //
    // }

    /**
     * save card charged Id to database
     * @async
     * @method
     */
    async saveChargeId() {
        if (this.transaction.chargeId) {
            await this.database.updateTransaction(this.transaction.ref, {
                chargeId: this.transaction.chargeId,
            })
        }
    }
    /**
     * update transaction in the database
     * @param result
     */

    updateDbTransaction(result: TransactionResult) {
        this.database.updateTransaction(this.transaction.ref, {
            provider1_resp: result.provider_response,
            status: result.status
        }).then((saved) => {
            console.log("[StripeParticipant]: updated DB transaction");
        }).catch((failed) => {
            console.error("[StripeParticipant]:failed to update transaction");
        });
    }

    /**
     * process payment
     */

    private processPayment(): Promise<TransactionResult> {
        return new Promise<TransactionResult>((resolve) => {
            try {
                // if (this.transaction.initial) {
                //     this.initialCharge().then((result) => {
                //         this.updateDbTransaction(result);
                //         resolve(result);
                //     }).catch((err) => {
                //         this.updateDbTransaction(err);
                //         resolve(err);
                //     })
                // } else {
                console.log('charge card');
                this.processCard((resp) => {
                    console.log(resp);
                    if (resp.status == StatusType.SUCCESS) {
                        this.saveChargeId();
                        this.updatePromoAmount();
                        this.updateDbTransaction(resp);
                        resp.provider_response.payment_method_details = {
                            card:this.transaction.card
                        }
                        resp.provider_response.source =  this.transaction.card
                        resp.provider_response.id =  resp.provider_response["data"]["id"]
                        resolve(resp);
                    } else {
                        const err = resp;
                        this.updateDbTransaction(err);
                        resolve(err);
                    }
                });
                // }

            } catch (e) {
                console.error(e);
            }

        });


    }
}
