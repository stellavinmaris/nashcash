import {TransactionParticipant} from "../interfaces/TransactionParticipant";
import {Transaction} from "../models/Transaction";
import {PrepResult, TransactionResult} from "../models/TransactionResult";
import {DataStore} from "../models/DataStore";
import {StatusType} from "../utils/StatusType";
import {sendFundstoBusiness} from "../utils/commonfuncs";

const SETTLEMT_INTERNAL = "INTERNAL"
const SETTLEMT_PESALINK = "PESALINK"
const SETTLEMT_RTGS = "RTGS"
const MPESA_DIRECT = "MPESA"
export class MerchantPayParticipant implements TransactionParticipant{
    TAG = "MerchantPayParticipant";
    transaction: Transaction;
    database: DataStore;
    /**
     * @description contructor
     * @param transaction 
     * @param db 
     */
    constructor(transaction:Transaction,db:DataStore) {
        this.transaction = transaction;
        this.database =db;
    }
    /**
     * @description abort transaction
     */
    abort(): Promise<VoidFunction> {
        return undefined;
    }
    /**
     * @description complete transaction
     */

    complete(): Promise<TransactionResult> {
        return undefined;
    }

    /**
     * @description prepare transaction
     * @function updateTransaction
     */

    prepare(): Promise<PrepResult> {
        return new Promise<PrepResult>((resolve) => {
            if(this.transaction.merchant_id === undefined){
                resolve(new PrepResult(StatusType.ERROR, {errorMessage: "Invalid merchant detail"}))
            }
            this.database.updateTransaction(this.transaction.ref,{
                merchant_id:this.transaction.merchant_id
            })
            resolve(new PrepResult(StatusType.SUCCESS,{}));
        });
    }
    /**
     * @description process transaction
     * @function processPayment
     */

    process(): Promise<TransactionResult> {
        return this.processPayment();
    }

    /**
     * @description process payment
     * @function creditMerchantAccount
     * @param merchant_id
     * @param txRef
     * @param amount
     * @function creditMerchantCustomerLoyalty
     * @function merchantHasBankAccount
     * @function sendMoneyToMpesa
     * @function updateDbTransaction
     */
    private processPayment():Promise<TransactionResult>{
        return new Promise<TransactionResult>((resolve) => {
            this.database.creditMerchantAccount(
                this.transaction.merchant_id,
                this.transaction.txRef,
                this.transaction.amount,
                (status)=>{
                    if(status === StatusType.SUCCESS){
                        const result = new TransactionResult(StatusType.SUCCESS,{status:"success"});
                        this.database.creditMerchantCustomerLoyalty(this.transaction);
                        this.database.merchantHasBankAccount(this.transaction.merchant_id).then((hasBank)=>{
                            if(!hasBank){
                                //send money to mpesa paybill or till
                                this.sendMoneyToMpesa().then((result2)=>{
                                    if(result2.status = StatusType.SUCCESS)
                                        resolve(result2);
                                    else resolve(result)


                                });
                            }else{
                                //check if it an instant settlement
                                this.updateDbTransaction(result);
                                resolve(result);
                            }
                        })

                    }else {
                        const err_result = new TransactionResult(
                            StatusType.ERROR,
                            {errorMessage:"Unable to credit merchant account"});
                        this.updateDbTransaction(err_result);
                        resolve(err_result);

                    }
                });
        });
    }

    /**
     * @description verify transaction response
     * @param resp 
     */
    private verifyTransactionResponse(resp):{status:number,sms:String,MpesaReceiptNumber:String,TransactionID:String,name:string}{
        let result = {status:StatusType.ERROR,sms:'',MpesaReceiptNumber:'',TransactionID:'',name:''};
        if (resp!=null && (resp.ResultCode == 0 || resp.ResultCode === "0")) {
            result.status =StatusType.SUCCESS;
            result.MpesaReceiptNumber = resp.TransactionID;
            result.TransactionID = resp.TransactionID;
            result.name = this.transaction["bizname"]?this.transaction["bizname"]:this.transaction.receiver;
            let records = resp.ResultParameters != undefined ? resp.ResultParameters.ResultParameter : undefined;
            if (records != undefined)
                records.forEach(function (record) {
                    if (record["Key"] === "ReceiverPartyPublicName") {
                        result.name = record["Value"].toString().split("-").lenght>1?record["Value"].toString().split("-")[1]:record["Value"].toString();
                    }
                });
            return result;
        }
        else {
            console.log('Mpesa b2b stage 1 failed',resp);
            return result;
        }
    }
    /**
     * @description fetch callback response
     * @function getTransactionByRef
     * @function verifyTransactionResponse
     * @function updateTransaction
     */
    private fetchCallbackResponse(): Promise<TransactionResult>{
        return new Promise<TransactionResult>((resolve) => {
            const intervalId = setInterval(()=>{
                this.database.getTransactionByRef(this.transaction.ref).then((value)=>{
                    if(value.status == StatusType.SUCCESS){
                        let provider_response = value.response["provider2_resp"];

                        if(provider_response!==undefined && provider_response!=='' && provider_response){
                            try {
                                provider_response = JSON.parse(provider_response);
                            }catch (e) {

                            }
                            let result = this.verifyTransactionResponse(provider_response);
                            switch (result.status) {
                                case StatusType.ERROR:
                                    //failed
                                    console.log('B2B transaction failed');
                                    this.database.updateTransaction(
                                        this.transaction.ref,
                                        {status: StatusType.ERROR,provider2_resp:provider_response})
                                        .then(r  =>console.log("transaction status updated"));
                                    clearInterval(intervalId);
                                    resolve(new TransactionResult(StatusType.ERROR,provider_response));
                                    break;
                                case StatusType.SUCCESS:
                                    console.log('B2B transaction successful');
                                    provider_response.receiver_data = result;
                                    provider_response.MpesaReceiptNumber = result.MpesaReceiptNumber;
                                    provider_response.Result = {TransactionID:provider_response.MpesaReceiptNumber,
                                        ResultParameters:provider_response.ResultParameters};
                                    this.database.updateTransaction(
                                        this.transaction.ref,
                                        {status: StatusType.SUCCESS,provider2_resp:provider_response})
                                        .then(r  =>console.log("transaction status updated"));
                                    clearInterval(intervalId);
                                    resolve(new TransactionResult(StatusType.SUCCESS,provider_response));
                                    break;
                            }
                        }else{
                            //TODO test id this execution passes
                            this.transaction.tries +=1;
                            console.log('count',this.transaction.tries);

                            if(this.transaction.tries>25){
                                clearInterval(intervalId);
                                resolve(new TransactionResult(StatusType.ERROR, {errorMessage: "Failed to get B2B response from MPESA"}));
                            }
                        }
                    }else {
                        clearInterval(intervalId);
                        console.error("Missing transaction record or error fetching record");
                        resolve(new TransactionResult(StatusType.ERROR,{errorMessage:"Failed to complete transaction"}));
                    }
                });
            },3000);

        });
    }

    /**
     * @description send money to mpesa
     * @function initiateTransaction
     * @function updateDbTransactionDates
     * @function fetchCallbackResponse
     * @function createSettlement
     */
    async sendMoneyToMpesa():Promise<TransactionResult> {

        const result = await this.initiateTransaction()
        return new Promise<TransactionResult>(resolve => {
            if(result.status !== StatusType.SUCCESS){
                this.transaction.ConversationID = "none";
                this.updateDbTransactionDates(result);
                resolve(result)
            }else{
                this.transaction.ConversationID = result.provider_response["ConversationID"];
                this.updateDbTransactionDates(result);
                this.fetchCallbackResponse().then((result)=>{
                    if(result.status == StatusType.SUCCESS){
                        console.log('Mpesa settlement complete')
                        this.database.createSettlement(this.transaction.merchant_id,
                            MPESA_DIRECT,this.transaction.amount,
                            `debit-${this.transaction.ref}`,
                            this.transaction.receiver,0,0,JSON.stringify(result.provider_response))
                            .then((settled)=>{
                                resolve(result)
                        });
                    }
                })
            }
        })

    }

    /**
     * @description initiate transaction
     * @function sendFundstoBusiness
     */

    private initiateTransaction():Promise<TransactionResult> {
        let RecieverIdentifierType = "2";
        switch (this.transaction.group) {
            case "bills":
                this.transaction.CommandID = "BusinessPayBill";
                RecieverIdentifierType = "4";
                break;
            case "tills":
                this.transaction.CommandID = "BusinessBuyGoods";
                RecieverIdentifierType = "2";
                break;
            default:
                this.transaction.CommandID = "MerchantToMerchantTransfer";
                RecieverIdentifierType = "4";
                break;
        }
        const pOBj = {
            "Initiator": process.env.MPESA_B2C_USERNAME,
            "SecurityCredential": "",
            "CommandID": this.transaction.CommandID,
            "SenderIdentifierType": "4",
            "RecieverIdentifierType": RecieverIdentifierType,
            "Amount": this.transaction.amount,
            "PartyA": process.env.MPESA_B2C_SHORTCODE,
            "PartyB": this.transaction.receiver,
            "AccountReference": this.transaction.receiver_data!=undefined &&
            this.transaction.receiver_data!=null?
                this.transaction.receiver_data:this.transaction.accno==null?`${this.transaction.txRef}`:this.transaction.accno,
            "Remarks": `${this.transaction.txRef!=undefined?this.transaction.txRef:this.transaction.ref}`,
            "QueueTimeOutURL": `${process.env.HOST}/b2bTimeout`,
            "ResultURL": `${process.env.HOST}/b2bCallback`,
        };
        return new Promise<TransactionResult>(((resolve) => {
            sendFundstoBusiness(pOBj).then((result)=>{
                resolve(result);
            });
        }));


    }

    /**
     * @description update transaction dates
     * @param result 
     * @function updateTransaction
     * @param ref
     * @param result.provider_response
     * @param ConversationID
     */

    updateDbTransactionDates(result:TransactionResult){
        this.database.updateTransaction(this.transaction.ref,{
            provider2_stage1:result.provider_response,
            ConversationID:this.transaction.ConversationID,
        }).then((saved)=>{
            console.log("[MpesaB2BParticipant]: updated DB transaction");
        }).catch((failed)=>{
            console.error("[MpesaB2BParticipant]:failed to update transaction");
        });
    }

    /**
     * @description update transaction
     * @param result 
     * @function updateTransaction
     * @param ref
     * @param result.provider_response
     * @param ConversationID
     */
    updateDbTransaction(result:TransactionResult){
        this.database.updateTransaction(this.transaction.ref,{
            provider2_resp:result.provider_response,
            status:result.status
        }).then((saved)=>{
            console.log("[MerchantParticipant]: updated DB transaction");
        }).catch((failed)=>{
            console.error("[MerchantParticipant]:failed to update transaction");
        });
    }
}
