import {Transaction} from "../models/Transaction";
import {Result, TransactionResult} from "../models/TransactionResult";
import {DataStore} from "../models/DataStore";

export interface TransactionParticipant {
    TAG: String;
    database: DataStore;
    transaction: Transaction;
    prepare(): Promise<Result>
    process(): Promise<Result>
    abort(): Promise<VoidFunction>
    complete(): Promise<Result>;
    updateDbTransaction(result:TransactionResult);
}
export class DataStoreFactory{
    private static database:DataStore;
    static getDatabase():DataStore{
        if(this.database!=null){
            return this.database;
        }else{
            this.database = new DataStore();
            return this.database;
        }
    }
}