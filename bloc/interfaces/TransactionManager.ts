import {Transaction} from "../models/Transaction";
import {DataStore} from "../models/DataStore";
import {Result, TransactionResult} from "../models/TransactionResult";

export interface TransactionManager {
    transaction:Transaction;
    transact():Promise<Result>;
    refund():Promise<Result>;
}