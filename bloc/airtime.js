const AfricasTalking = require('africastalking')({
    apiKey: process.env.AFRICAS_API_KEY,
    username: process.env.AFRICAS_USERNAME
});
module.exports= AfricasTalking;
