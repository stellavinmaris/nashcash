import {Transaction} from "./models/Transaction";
import {TransactionContext} from "./TransactionContext";
import {StatusType} from "./utils/StatusType";
import {Result, TransactionResult} from "./models/TransactionResult";
import {DataStore} from "./models/DataStore";
import {DataStoreFactory} from "./interfaces/TransactionParticipant";
import {Groups, PSPs} from "./participants/Groups";
import {
    addToAlgolia,
    createPendingAmountState,
    dumpTransactionResultToFirestore, flagAsFraud, forceUserKyc,
    formatMpesaPhone, removePendingPayment, sendSlackAlertMsg,
    sendSms
} from "./utils/commonfuncs";
import {LocalDate} from "js-joda";

export class App {
    transaction: Transaction;
    transactionContext: TransactionContext;
    database: DataStore;
    /**
     * @description constractor
     * @param txObj
     */
    constructor(txObj) {
        this.transaction = txObj as Transaction;
        this.transactionContext = new TransactionContext(this.transaction);
        this.database = DataStoreFactory.getDatabase()
    }

    /**
     * @description create refund
     */

    private async createRefund() {
            const result = await this.transactionContext.refund();
            if (result.status !== StatusType.SUCCESS) {
                console.log(`failed to create refund for ref ${this.transaction.txRef}`);
                //TODO fire an action to credit customer account with balance
                sendSms(`Failed to reverse initial transaction with ref ${this.transaction.txRef}`,"0718555832",(rep,err)=>{});
            }

    }

    /**
     * @description generate random charge amount
     */
    private randomChargeAmount(){
        return Math.floor(Math.random() * (99 - 54 + 1) + 54);
    }
    async checkPartnerActivations(){
        try {
            const inviteObj = await this.database.getCustomerInviteCode(this.transaction.customer_id);
            const isUsed = inviteObj.avalaible && await this.database.usedInviteCode(inviteObj.invitecode,this.transaction.customer_id);
            if(isUsed)
                return;
            console.log('customer has not received bonus');
            //check partner codes
            const massInvite = await this.database.getPartnerByInviteCode(inviteObj.invitecode);
            if(massInvite.partner!=null){
                const invitedByHasHitMax = await this.database.partnerPromoExhausted(massInvite.max,inviteObj.invitecode)
                if (!invitedByHasHitMax) {
                    await this.awardBonus(this.database,false,this.transaction.phone,this.transaction.customer_id,this.transaction.txRef,inviteObj.invitecode,massInvite.amount);
                } else {
                    console.log(`xxx partner code ${inviteObj.invitecode} has reached max invites`);
                }
            }else{
                console.log('xxx no partner for invite code');
            }
        }catch (e){
            console.error(e);
            sendSms("failed to award bonus","0718555832",(data,err)=>{

            });
        }


    }
    async awardBonus(db,owner,phone,customer_id,transactionRef,referral,bonusAmount) {


        if (bonusAmount > 0) {
            const status = await db.creditCustomerForReferral(
                customer_id,
                transactionRef,
                referral,
                bonusAmount
            );
            if (status == StatusType.SUCCESS) {
                let msg = `Congratulations! You have earned Kes ${bonusAmount} credit on Boya. The credit will be applied when your next payment to a lipa na Mpesa or Paybill is over Kes 1000.`;
                if(owner)
                    msg = `Congratulations! You have earned Kes ${bonusAmount} bonus to spend on Boya. Invite your friends to earn using your referral link`;
                sendSms(msg
                    ,
                    phone, (resp) => {
                    });
            }
        }

    }
    async awardBonusRef(db,owner,phone,customer_id,transactionRef,referral,bonusAmount) {


        if (bonusAmount > 0) {
            const status = await db.creditCustomerForReferral(
                customer_id,
                transactionRef,
                referral,
                bonusAmount
            );
            if (status == StatusType.SUCCESS) {
                let msg = `Congratulations! You have earned Kes ${bonusAmount} credit on Boya. The credit will be applied when your next payment.`;
                if(owner)
                    msg = `Congratulations! You have earned Kes ${bonusAmount} bonus to spend on Boya. Invite your friends to earn using your referral link`;
                sendSms(msg
                    ,
                    phone, (resp) => {
                    });
            }
        }

    }
    /**
     * @description check referrals
     */
    async checkReferrals() {
        if(this.transaction.initial){
            return await  this.checkPartnerActivations();
        }

        try {
            this.checkPartnerActivations()
            console.log('testing referral earns');
            const isOver3K = await this.database.hasSpentOver(this.transaction.customer_id,3000)
            if (isOver3K) {
                const inviteObj = await this.database.getCustomerInviteCode(this.transaction.customer_id);
                //check if the customer has already been rewarded
                const isUsed = inviteObj.avalaible && await this.database.usedInviteCode(inviteObj.invitecode,this.transaction.customer_id);
                console.log('invite code',inviteObj);
                if (!isUsed) {

                    //get customer who invited transacting customer
                    const custObj = await this.database.getCustomerByInviteCode(inviteObj.invitecode)

                    if (custObj.customer_id !== null) {
                        //check if the customer with this invite code has reached max invites
                        const invitedByHasHitMax = await this.database.sharedMax(inviteObj.invitecode,custObj.customer_id)
                        if (!invitedByHasHitMax) {
                            const earnAmount = await this.database.getReferralCompensationAmount();
                            await this.awardBonusRef(this.database,true,this.transaction.phone,custObj.customer_id,this.transaction.txRef,inviteObj.invitecode,earnAmount);
                            await this.awardBonusRef(this.database,false,this.transaction.phone,this.transaction.customer_id,this.transaction.txRef,inviteObj.invitecode,earnAmount);
                        } else {
                            console.log(`${inviteObj.invitecode} has reached max invites`);
                        }


                    }
                }
            }
        } catch (e) {
            console.error(e);
        }
    }
    /**
     * @description check flags
     * check clients that have been flagged
     */
    async checkFlags(){
        const isSecure = await this.database.hasClearanceKyc(this.transaction.customer_id);
        if(!isSecure && this.transaction.amount>=5000){
           await forceUserKyc(this.transaction.customer_id);
        }
    }
    /**
     * @description debit promos
     * if promo amount was used, debit promo amount
     */
    async debitPromos() {
        if (this.transaction.promo_amount) {
            console.log('promo amount',this.transaction.promo_amount);
            const shareCodeObj = await this.database.getCustomerShareCode(this.transaction.customer_id);
            const status = await this.database.debitCustomerPromoUse(this.transaction.customer_id, this.transaction.txRef, shareCodeObj.sharecode, this.transaction.promo_amount)
            if(status !== StatusType.SUCCESS){
                console.error('unable to debit promo amount')
                sendSms(`unable to debit promo amount transaction_ref ${this.transaction.txRef}`,"0718555832",(resp)=>{});
            }
        }

        if (this.transaction.pending_charge) {
            console.log('pending charge',this.transaction.pending_charge);
            const status = await this.database.addPendingCharge(this.transaction);
            if(status !== StatusType.SUCCESS){
                console.error('unable to add pending charge');
                sendSms(`unable to add pending charge ${this.transaction.pending_charge} transaction_ref ${this.transaction.txRef}`,
                    "0718555832",(resp)=>{});
            }
        }
    }
    _isInMBankCard(bank):boolean{
        return (bank.toString().toLowerCase() == 'i and m bank, ltd.'
            || bank.toString().toLowerCase() == 'investments and mortgages bank, ltd.'
            || bank.toString().toLowerCase() == 'enterprise national bank')
    }
    _hasEquityToken():boolean{
        return this.transaction.card != undefined && this.transaction.card["equity_token"] != undefined &&
            this.transaction.card["equity_token"]!='' &&
            this.transaction.card["equity_token"]!=null;
    }
    _hasFlutterwaveToken():boolean{
        return this.transaction.card["flutterwave_token"] != undefined &&
            this.transaction.card["flutterwave_token"]!='' &&
            this.transaction.card["flutterwave_token"]!=null;
    }

    _isBarclaysBankCard(bank):boolean{
        return (bank.toString().toLowerCase().includes('barclays'))
    }
    /**
    * PSP routing logic.
     * Can be extended based on other rules
    * */
    async resolveCardProcessor(){
        this.transaction.provider1 = PSPs.FLUTTERWAVE

        // const res = await this.database.getBankBin(this.transaction.card['bin'])
      
        // if(res!=null && res['bank']!= undefined && this._isBarclaysBankCard(res["bank"])){
        //     this.transaction.avoidPreauth = true
        // }
        // if(this.transaction.card != undefined && this.transaction.card["employee_id"]!=null){
        //     this.transaction.provider1 = PSPs.BOYA
        //     this.transaction.business_id = await this.database.businessID(this.transaction.card["employee_id"])
        //     this.transaction.is_business = true
        // }else if(res!=null && res['bank']!= undefined && this._isInMBankCard(res["bank"])){
        //     this.transaction.provider1 = PSPs.EQUITY
        // }else if(this._hasEquityToken()){
        //     this.transaction.provider1 = PSPs.EQUITY
        // }else if(this.transaction.card != undefined && this.transaction.card["brand"] && this.transaction.card["brand"].toString().toLowerCase()=="amex"){
    //         this.transaction.provider1 = PSPs.FLUTTERWAVE
    //     } else {
    //         switch(this.transaction.group){
    //             case Groups.airtime:
    //                 this.transaction.provider1 = PSPs.EQUITY
    //                 break;
    //             case Groups.bills:
    //                 this.transaction.provider1 = PSPs.EQUITY
    //                 break;
    //             case Groups.mpesa:
    //                 this.transaction.provider1 = PSPs.EQUITY
    //                 break;
    //             case undefined:
    //                 this.transaction.provider1 = PSPs.EQUITY
    //                 break;
    //             // case null:
    //             //     this.transaction.provider1 = PSPs.EQUITY
    //             //     break;
    //             default:
    //                 this.transaction.provider1 = PSPs.FLUTTERWAVE
    //                 break;
    //         }
    //   }
    }
    async isPartnerReferral():Promise<boolean>{
        return new Promise<boolean>(async resolve => {
            const inviteObj = await this.database.getCustomerInviteCode(this.transaction.customer_id);
            //check partner codes
            const massInvite = await this.database.getPartnerByInviteCode(inviteObj.invitecode);
            return resolve(massInvite.partner!=null)
        })

    }
    /**
     * @description process transaction
     *
     */
    processTransaction(): Promise<Result> {
        return new Promise(async resolve => {
            this.transaction.customer_name = await this.database.getTransactionCustomerName(this.transaction.customer_id);
            if(this.transaction.group == Groups.tills || Groups.bills){
                this.transaction.receivername = await this.database.getReceiverName(this.transaction.receiver);
            }
            // if(this.transaction.group!==undefined && this.transaction.payment_type==="MPESA"){
            //     return resolve(new TransactionResult(StatusType.ERROR,
            //         {"errorMessage":"Mpesa payment option not enabled at the moment",
            //             "error":"Mpesa not enabled at the moment","code":StatusType.SERVICE_UNAVAILABLE}));
            // }
            if((this.transaction.group==Groups.tills || this.transaction.group==Groups.bills || this.transaction.group== Groups.mpesa) && this.transaction.amount>100000){
                console.log("50K limit enforced");
                return resolve(new TransactionResult(StatusType.ERROR,
                    {"errorMessage":"Sorry, you may only transact upto 100,000 per transaction today. So sorry for the inconvenience",
                        "error":"Sorry, you may only transact upto 100,000 per transaction today. So sorry for the inconvenience","code":StatusType.INSUFFICIENT_FUNDS}));
            }
            if(this.transaction.group!==undefined && this.transaction.receiver==="JTL"){
                this.transaction.receiver = "776611";
            }
            // if(this.transaction.group!==undefined && this.transaction.receiver==="kplc_postpaid"){
            //     return resolve(new TransactionResult(StatusType.ERROR,
            //         {"errorMessage":"KPLC PostPaid payments not enabled at the moment",
            //             "error":"KPLC PostPaid not enabled at the moment","code":StatusType.SYS_FAILURE}));
            // }
            if(this.transaction.group!==undefined && this.transaction.receiver=="330330"){
                return resolve(new TransactionResult(StatusType.ERROR,
                    {"errorMessage":"paybill 330330 not supported at the moment",
                        "error":"paybill 330330 not enabled at the moment","code":StatusType.SYS_FAILURE}));
            }if(this.transaction.group!==undefined && this.transaction.receiver=="516600"){
                return resolve(new TransactionResult(StatusType.ERROR,
                    {"errorMessage":"Sorry, the paybill 516600 for Diamond Trust Bank (DTB) does not allow payments from Boya. Please try a different option. We will enable direct bank transfers soon",
                        "error":"paybill 516600 not enabled at the moment","code":StatusType.SYS_FAILURE}));
            }if(this.transaction.group!==undefined && this.transaction.receiver=="888888"){
                this.transaction.receiver = "kplc_postpaid";
                // return resolve(new TransactionResult(StatusType.ERROR,
                //     {"errorMessage":"KPLC Postpaid services are not available at the moment",
                //         "error":"KPLC Postpaid services are not available at the moment","code":StatusType.SYS_FAILURE}));
            }
            // if(this.transaction.group!==undefined && this.transaction.receiver=="kplc_postpaid"){
            //     // this.transaction.receiver = "888888";
            //     return resolve(new TransactionResult(StatusType.ERROR,
            //         {"errorMessage":"KPLC Postpay services are not available at the moment",
            //             "error":"KPLC Postpay services are not available at the moment","code":StatusType.SYS_FAILURE}))
            // }
            //check if kplc tokens paybill
            if(this.transaction.group!==undefined && this.transaction.receiver=="888880"){
                this.transaction.receiver = "kplc_prepaid";
            }
            //check limits
            try{
                if(this.transaction.group!==undefined && (this.transaction.group==Groups.mpesa || this.transaction.group==Groups.bills)){
                    const maxLimit = await this.database.customerMaxLimit(this.transaction.customer_id,this.transaction.group)
                    if(maxLimit>0){
                        const hasExceededLimit = await this.database.hasSpentOverInPeriod(this.transaction.customer_id,LocalDate.now().minusDays(7).toString(),maxLimit)
                        if(hasExceededLimit){
                            sendSlackAlertMsg(`Stopped ${this.transaction.customer_name}, trying to transact ${this.transaction.amount}`);
                            return resolve(new TransactionResult(StatusType.ERROR,
                                {"errorMessage":`Sorry, your account has reached the limit of Ksh ${maxLimit} for this week`,
                                    "error":`Sorry, your account has reached the limit of Ksh ${maxLimit} for this week`,"code":StatusType.STOPPED}));
                        }
                    }
                }
            }catch (e){
                console.error(e)
            }


            //check if blocked
            let isBlocked =  await  this.database.isBlockedCustomerId(this.transaction.customer_id);
            if(isBlocked){
                sendSlackAlertMsg(`Blocked ${this.transaction.customer_name}, trying to transact ${this.transaction.amount}`);
                console.log("blocked customer id ",this.transaction.customer_id);
                flagAsFraud(this.transaction.customer_id);
                return resolve(new TransactionResult(StatusType.ERROR,
                    {"errorMessage":"Your bank declined to process Nash Cash Transaction",
                        "error":"Cannot complete transactions at the moment","code":StatusType.BLOCKED}));
            }
            try{

            }catch (e) {

            }
             isBlocked =  !this.transaction.initial &&  await  this.database.isBlockedPhone(this.transaction.phone);
            if(isBlocked){
                sendSlackAlertMsg(`Blocked ${this.transaction.customer_name}, trying to transact ${this.transaction.amount}`);
                console.log("blocked customer phone ",this.transaction.phone);
                flagAsFraud(this.transaction.customer_id);
                return resolve(new TransactionResult(StatusType.ERROR,
                    {"errorMessage":"Your bank declined to process Nash Cash Transaction",
                        "error":"Cannot complete transactions at the moment","code":StatusType.BLOCKED}));
            }
            isBlocked =  !this.transaction.initial && await  this.database.isBlockedPhone((this.transaction.receiver));
            if(isBlocked){
                sendSlackAlertMsg(`Blocked ${this.transaction.customer_name}, trying to transact ${this.transaction.amount}.`);
                await this.database.blockUser(this.transaction,"phone is blocked")
                flagAsFraud(this.transaction.customer_id);
                console.log("blocked receiver phone ",this.transaction.receiver);
                return resolve(new TransactionResult(StatusType.ERROR,
                    {"errorMessage":"Your bank declined to process Nash Cash Transaction",
                        "error":"Cannot complete transactions at the moment","code":StatusType.BLOCKED}));
            }
            isBlocked =  !this.transaction.initial && await  this.database.isBlockedCard(this.transaction.source);
            if(isBlocked){
                sendSlackAlertMsg(`Blocked ${this.transaction.customer_name}, trying to transact ${this.transaction.amount}`);
                console.log("blocked card ",this.transaction.source);
                await this.database.blockUser(this.transaction,"card is blocked")
                flagAsFraud(this.transaction.customer_id);
                return resolve(new TransactionResult(StatusType.ERROR,
                    {"errorMessage":"Your bank declined to process Nash Cash Transaction",
                        "error":"Cannot complete transactions at the moment","code":StatusType.BLOCKED}));
            }
            isBlocked =   await  this.database.isBlockedIP(this.transaction.ip);
            if(isBlocked){
                sendSlackAlertMsg(`Blocked ${this.transaction.customer_name}, trying to transact ${this.transaction.amount} (IP)`);
                console.log("blocked ip ",this.transaction.ip);
                flagAsFraud(this.transaction.customer_id);
                return resolve(new TransactionResult(StatusType.ERROR,
                    {"errorMessage":"Your bank declined to process Nash Cash Transaction",
                        "error":"Cannot complete transactions at the moment","code":StatusType.BLOCKED}));
            }
            // const isSecure = await this.database.hasClearanceKyc(this.transaction.customer_id);
            // if(!isSecure && this.transaction.amount>20000){
            //     await forceUserKyc(this.transaction.customer_id);
            //     return resolve(new TransactionResult(StatusType.ERROR,
            //         {"errorMessage":"Your bank declined to process Nash Cash Transaction, error 403. Please update your app",
            //             "error":"Cannot complete transactions at the moment","code":StatusType.KYC}));
            // }
            // const hasSpentOver1K = await this.database.hasSpentOver(this.transaction.customer_id,1000)
            // if(!isSecure && hasSpentOver1K){
            //     await forceUserKyc(this.transaction.customer_id);
            //     return resolve(new TransactionResult(StatusType.ERROR,
            //         {"errorMessage":"Your bank declined to process Nash Cash Transaction, error 403. Please update your app",
            //             "error":"Cannot complete transactions at the moment","code":StatusType.KYC}));
            // }

            const hasSpentOver5K = await this.database.hasSpentOver(this.transaction.customer_id,5000)
            const hasSpentOver2K = await this.database.hasSpentOver(this.transaction.customer_id,2000)
            const isRecentSignup = await this.database.recentSignup(this.transaction.customer_id)
            const isVIP = await this.database.isClearedVIP(this.transaction.customer_id)
            const isactivation = await this.isPartnerReferral()
            const msg = `Hi ${this.transaction.customer_name.split(" ")[0]}, your security is our priority, kindly complete your profile in order to continue using Boya. If you have already done so, please wait for us to verify and contact you`

            if(!hasSpentOver2K && this.transaction.amount>=2000 && !isVIP && !isactivation){
                sendSlackAlertMsg(`Blocked ${this.transaction.customer_name}, trying to transact ${this.transaction.amount}`);
                await forceUserKyc(this.transaction.customer_id);
                await this.database.blockUser(this.transaction,"new signup,amount above 5k")
                return resolve(new TransactionResult(StatusType.ERROR,
                    {"errorMessage":msg,
                        "error":msg,"code":StatusType.KYC}));
            }
            if(isRecentSignup && hasSpentOver5K && !isVIP && !isactivation){
                sendSlackAlertMsg(`Blocked ${this.transaction.customer_name}, trying to transact ${this.transaction.amount}`);
                await forceUserKyc(this.transaction.customer_id);
                // await this.database.blockUser(this.transaction,"new signup,amount above 5k")
                return resolve(new TransactionResult(StatusType.ERROR,
                    {"errorMessage":msg,
                        "error":msg,"code":StatusType.KYC}));
            }
            const isSecure = await this.database.hasClearanceKyc(this.transaction.customer_id);
            if(isactivation && isRecentSignup && this.transaction.amount>20000 && !isVIP && !isSecure){
                await forceUserKyc(this.transaction.customer_id);
                return resolve(new TransactionResult(StatusType.ERROR,
                    {"errorMessage":msg,
                        "error":msg,"code":StatusType.KYC}));
            }
            if(!hasSpentOver2K && this.transaction.amount>10000 && !isVIP && !isSecure){
                await forceUserKyc(this.transaction.customer_id);
                return resolve(new TransactionResult(StatusType.ERROR,
                    {"errorMessage":msg,
                        "error":msg,"code":StatusType.KYC}));
            }

            const hasToomanyAttempts = await this.database.hasToomanyAttempts(this.transaction.customer_id)
            if(hasToomanyAttempts && isRecentSignup){
                sendSlackAlertMsg(`Blocked ${this.transaction.customer_name}, trying to transact ${this.transaction.amount} too many attempts`);
                await forceUserKyc(this.transaction.customer_id);
                await this.database.blockUser(this.transaction,"has too many card verification attempts")
                return resolve(new TransactionResult(StatusType.ERROR,
                    {"errorMessage":"Your bank declined to process Nash Cash Transaction",
                        "error":"Cannot complete transactions at the moment","code":StatusType.KYC}));
            }
            isBlocked =  !this.transaction.initial && this.transaction.group == Groups.tills && await this.database.isBlockedTill(this.transaction.receiver);
            if(isBlocked){
                sendSlackAlertMsg(`Blocked ${this.transaction.customer_name}, trying to transact ${this.transaction.amount} into blocked till`);
                console.log("blocked till ",this.transaction.receiver);
                flagAsFraud(this.transaction.customer_id);
                await this.database.blockUser(this.transaction,"paying into a till number that is blocked")
                return resolve(new TransactionResult(StatusType.ERROR,
                    {"errorMessage":"Your bank declined to process Nash Cash Transaction",
                        "error":"Cannot complete transactions at the moment","code":StatusType.BLOCKED}));
            }
            //set customer name
            console.log('source>>>----',this.transaction.source);
            if(this.transaction.payment_type == "CARD"){
                const card = await this.database.getCustomerCard(this.transaction.customer_id,this.transaction.source)
                console.log('card---------', card)
                if(card && card!=undefined){
                    console.log('card found');
                    if(this.transaction.initial || card["verified"]){
                        this.transaction.card = card;
                    }
                }else{
                    console.log('missed card>>>',card);
                    sendSlackAlertMsg(`Stopped ${this.transaction.customer_name}- ${this.transaction.phone}, ${this.transaction.amount}   transaction, Old version!!`);
                    return resolve(new TransactionResult(StatusType.ERROR,
                        {"errorMessage":"Sorry, we need a little help from you. To enable transactions, please update your app from Google play store or Appstore",
                            "error":"Invalid version","code":StatusType.SERVICE_UNAVAILABLE}));
                }
            }

            if(this.transaction.group!==undefined && this.transaction.payment_type!=="MPESA")
                    this.transaction.fee = await this.database.getTransactionFee(this.transaction.group,this.transaction.amount);
            else this.transaction.fee = 0.0;
            if(this.transaction.group!==undefined && this.transaction.group===Groups.tills)
                this.transaction["bizname"] = await this.database.getTransactionBizname(this.transaction.receiver);
            if(this.transaction.initial){
                console.log('setting random charge amount');
                this.transaction.amount = this.randomChargeAmount();
            }
            const startTime = Date.now();
            if(this.transaction.payment_type != "MPESA")
                    await this.resolveCardProcessor();
            console.log('resolved processor')
            if (this.transaction.group === Groups.tills || this.transaction.group === Groups.bills) {
                const merchantConfig = await this.database.getMerchantByShortcode(this.transaction.receiver);
                if (merchantConfig) {
                    this.transaction.merchant_id = merchantConfig["id"];
                    this.transaction.merchant_obj = merchantConfig;
                    this.transaction.receivername = merchantConfig["name"]
                }
                // this.transactionContext.configureMerchant(merchantConfig)
            }
            this.transactionContext.setGroup();
            console.log('resolved group')
            this.transaction.credit_lim = await this.database.customerCreditLim(this.transaction.customer_id);
            console.log('resolved credit_lim');
            this.transaction.loanBal = await this.database.customerLoan(this.transaction.customer_id);
            console.log('resolved loanBal');
            console.log('calling createTransaction');
            this.database.createTransaction(this.transaction).then(async created => {
                if (created) {
                    console.log('created Transaction in ledger');
                    this.transactionContext.init();
                    console.log('transactionContext.init complete')
                    const result = await this.transactionContext.transact();
                    console.log('transactionContext.transact complete')
                    console.log('transat---',result)
                    this.transaction.status = result.status;
                    let response = result.response;
                    if(result instanceof TransactionResult)
                        response = result.provider_response;
                    this.transaction.provider_response = response;
                    try {
                        const endTime = Date.now();
                        const timetaken = (endTime-startTime)/1000

                        console.log('time taken----000-----', timetaken)
                        await this.database.updateTransaction(this.transaction.ref,
                            {
                                status:this.transaction.status,
                                timetaken:timetaken,
                                provider2_resp:response,
                                tdump:this.transaction});
                        console.log('customer_id',this.transaction.customer_id);
                        const ress = {
                            ref:this.transaction.ref,
                            card_response:this.transaction.card_response,
                            cust_id:this.transaction.stripe_cust_id,
                            initial:this.transaction.initial?1:0,
                            customer_id:this.transaction.customer_id,
                            phone:this.transaction.phone,
                            amount:this.transaction.amount,
                            dateString:this.transaction.timestamp,
                            timestamp:this.transaction.timestamp,
                            // add wallet_accno
                            wallet_charge: this.transaction.wallet_charge,
                            // provider_response:response,
                            description:this.transaction.description,
                            group:this.transaction.group?this.transaction.group:'none',
                            status:this.transaction.status,
                            receiver:this.transaction.receiver?this.transaction.receiver:'none',
                            accno:this.transaction.accno?this.transaction.accno:'',
                            // provider2_resp:response,
                            errorMessage:response["errorMessage"]?response["errorMessage"]:'none',
                        };
                        console.log("provider resp",response);
                        if(response){
                            ress["provider_response"] = response;
                        }
                        ress.status = result.status;
                        const tr = (await this.database.getTransactionsOfRef(this.transaction.ref))[0];
                        await dumpTransactionResultToFirestore(this.transaction.customer_id,
                            this.transaction.ref,
                            ress).then((result)=>{
                            addToAlgolia(tr)
                        })



                    } catch (e) {
                        console.log(e);
                    }
                    switch (result.status) {
                        case StatusType.REFUND:
                            this.createRefund();
                            resolve(result);
                            break;
                        case StatusType.ERROR:
                            console.log("error processing transaction", result.response);
                            resolve(result);
                            break;
                        default:
                            this.checkReferrals();
                            this.checkFlags();
                            this.debitPromos();
                            removePendingPayment(this.transaction.amount,this.transaction.accno,this.transaction.customer_id);
                            const resp = {
                                dateString:this.transaction.timestamp,
                                ref:this.transaction.ref,amount:this.transaction.amount,
                                description:this.transaction.description,
                                group:this.transaction.group?this.transaction.group:'initial',
                                phone:this.transaction.phone,
                                payment_type:this.transaction.payment_type,
                                card_response:this.transaction.card_response,
                                status:this.transaction.status
                            };
                            let rr = response;
                            try{
                                rr= JSON.parse(rr.toString());
                            }catch(e){
                            console.error(e)
                            }
                            if(this.transaction["bizname"]!==undefined){
                                resp["bizname"] = this.transaction["bizname"];
                            }
                            if(!this.transaction.initial){
                                const v = Object.assign(rr,resp);
                                // console.log('result will carry',JSON.stringify(v));
                                resolve(new TransactionResult(result.status,v));
                            }else{
                                this.createPendingState();
                                resolve(new TransactionResult(result.status,resp));
                            }

                    }
                }
                return resolve(new TransactionResult(StatusType.ERROR, {errorMessage: "request rejected"}))
            }).catch(e=>{
                console.log('error createTransaction=',e);
                sendSlackAlertMsg("Transaction terminated due to poor connection")
                return resolve(new TransactionResult(StatusType.ERROR, {errorMessage: "Sorry, your connection is not stable. Please try again"}))
            });

        })


    }

    /**
     * @description create pending amount state
     * @param transaction
     * @param id
     */

    private async createPendingState() {
        await createPendingAmountState(this.transaction, this.transaction.card_response["id"]);
    }
}
