// //called when a transaction is complete
// const {getReferralDynamicLinkUrl}=  require("./utils/commonfuncs");

// const StatusType = require("./utils/StatusType").StatusType;
// const dateFormat = require('dateformat');
// // const smsSender = require('./sms_sender');
// // const mailer = require('./sendmail');
// const boyaledger = require('./boya_ledger');
// const jengaBal = require('../equity_jenga/b2b');
// const firestore = require('../firebase');
// const chatLink = "\nClick here for help https://chat.boya.co/mVFa"


// async function sendReceiptEmail(pObj) {
//     // //create a trigger to send transaction email
//     // await mailer.serveReceiptMail(pObj);
// }
// function formatMoney(number) {
//     try{
//         return parseFloat(`${number}`)
//             .toLocaleString('en-US', { style: 'currency', currency: 'KES' })
//             .replace(".00",'');
//     }catch (e) {
//         return `KES ${number}`;
//     }

// }
// function updateB2BLedger(paymentObj,mResult){
//     try {
//         mResult.ResultParameters.ResultParameter.forEach((item)=>{
//             if(item.Value.toString().includes('BasicAmount')){
//                 let balance = item.Value.toString().split('BasicAmount=')[1].toString().replace('}}','');
//                 try {
//                     balance = parseFloat(balance);
//                     try {
//                         boyaledger.updateAccountBalance(paymentObj.group,paymentObj.receiver,paymentObj.amount,balance,paymentObj.ref);
//                     }catch (e) {
//                         console.error(e);
//                     }
//                 }catch (e) {

//                 }
//             }
//         });
//     }catch (e) {

//     }

// }
// async function sendSmsToInitiator(paymentObj) {
//     let sms = "";
//     let phone = paymentObj.phone;
//     switch (paymentObj.group) {
//         case `airtime`:
//             console.log('will send airtime sms');
//             sms = `Airtime purchase successful. Your ${formatMoney(paymentObj.amount)} airtime credit will be sent to ${paymentObj.receiver} ${chatLink}`;
//             try {
//                 boyaledger.updateAccount(paymentObj.group,paymentObj.receiver,paymentObj.amount,paymentObj.ref);
//             }catch (e) {
//                 console.error(e);
//             }
//             break;
//         case "parking":
//             const option = `Please exist from parking as soon as possible`;
//             sms = `Parking payment successful. ${(paymentObj.provider_response.result!==undefined)?paymentObj.provider_response.result:option} ${chatLink}`;
//             break;
//         case "mpesa":
//             if (paymentObj.provider_response["ResultParameters"] !== undefined) {
//                 sms = `${paymentObj.provider_response.MpesaReceiptNumber} Confirmed. You have sent ${formatMoney(paymentObj.amount)} to`;
//                 paymentObj.provider_response["ResultParameters"]["ResultParameter"].forEach((record) => {
//                     if (record["Key"] === "ReceiverPartyPublicName") {
//                         sms += " " + record["Value"] + "\n";
//                     }
//                     if (record["Key"] === "B2CUtilityAccountAvailableFunds") {
//                         const balance = record["Value"];
//                         try {
//                             boyaledger.updateAccountBalance(paymentObj.group,paymentObj.receiver,paymentObj.amount,balance,paymentObj.ref);
//                         }catch (e) {
//                             console.error(e);
//                         }
//                     }

//                 });
//                 sms += ` ${chatLink}`;

//             } else {
//                 sms = `${paymentObj.ref} Confirmed. You have sent ${formatMoney(paymentObj.amount)} to ${paymentObj.receiver} on ${dateFormat(new Date(),
//                     "dd/mm/yy HH:MM")} ${chatLink}`;
//             }
//             //update ledger


//         async function sendReceiverSms() {
//             const doc2 = await firestore.collection('users')
//                 .doc(paymentObj.customer_id).get();
//             const user = doc2.data();
//             const name = user.name.toString().split(" ")[0]
//             const shortLink = await getReferralDynamicLinkUrl(user.shareCode);
//             const sms2 = `${name} just sent you ${formatMoney(paymentObj.amount)} from their Card! Boya lets you use your Card to pay Till numbers, Pay bills and Transfer money to Mpesa countrywide! Download, register, add your card, try it! ${shortLink}`;
//             // const sms2 = `${name} just sent you ${formatMoney(paymentObj.amount)} into your M-Pesa using Boya. Use Boya just like ${name}, download it here ${shortLink}`;
//             // const sms2 = `I just sent you money from my Card! Boya lets me use my Card to pay Till numbers, Pay bills and Transfer money to Mpesa countrywide! Download, register, add your card and try it! download it here ${shortLink}`;
//             // smsSender( sms2,paymentObj.receiver, (resp) => {
//             //     console.log(`receiver sms sent?`, resp);
//             //     try {
//             //         boyaledger.updateAccount("airtime",paymentObj.receiver,1,"sms");
//             //     }catch (e) {
//             //         console.error(e);
//             //     }
//             // });
//         }

//             try {
//                 sendReceiverSms().catch((e) => {
//                     console.error(e)
//                 })
//             } catch (e) {
//                 console.log(e);
//             }

//             break;
//         case "tills":
//             try {
//                 let resp = paymentObj.provider_response;
//                 try {
//                     resp = JSON.parse(paymentObj);
//                 }catch (e) {

//                 }
//                 // console.log('provider response',resp);
//                 sms = `Transaction confirmed ${resp.Result.TransactionID}. Your payment amount ${formatMoney(paymentObj.amount)} has been made to till ${paymentObj.receiver} on ${dateFormat(new Date(),
//                     "dd/mm/yy HH:MM")}, Mpesa Ref: ${resp.Result.TransactionID} ${chatLink}`;
//                 try {
//                     updateB2BLedger(paymentObj,resp.Result)
//                 }catch (e) {

//                 }
//             }catch (e) {
//                 sms = `Transaction confirmed ${paymentObj.ref}. Your payment amount ${formatMoney(paymentObj.amount)} has been made to till ${paymentObj.receiver} on ${dateFormat(new Date(),
//                     "dd/mm/yy HH:MM")} ${chatLink}`;
//             }
//             if (paymentObj.merchant_id!=undefined && paymentObj.merchant_id !=null){
//                 console.log('has boya merchant, send notification',paymentObj.merchant_obj)
//                 try{
//                     sendSmsToMerchant(paymentObj);
//                 }catch(e){
//                     console.log(e)
//                 }
//             }

//             try {
//                 boyaledger.updateAccount(paymentObj.group,paymentObj.receiver,paymentObj.amount,paymentObj.ref);
//             }catch (e) {
//                 console.error(e);
//             }
//             break;
//         case "bills":
//             if (paymentObj.merchant_id!=undefined)
//                    try {
//                        sendSmsToMerchant(paymentObj);
//                    }catch(e){
//                         console.log(e)
//                    }
//             try {
//                 const doc2 = await firestore.collection('users')
//                     .doc(paymentObj.customer_id).get();
//                 const user = doc2.data();
//                 phone = user.phone;
//             }catch (e) {
//                 console.error(e);
//             }
//             // if (paymentObj.token !== undefined && paymentObj.token!=null) {
//             //     const sms2 = `KPLC Prepaid Transaction ${paymentObj.txRef}. ${formatMoney(paymentObj.amount)} for ${paymentObj.token} for meter no. ${paymentObj.accno} on ${dateFormat(new Date(),
//             //         "dd/mm/yy HH:MM")} ${chatLink}`;
//             //     smsSender( sms2,paymentObj.phone, (resp) => {
//             //         console.log(`receiver sms sent?`, resp);
//             //         try {
//             //             boyaledger.updateAccount("airtime",paymentObj.receiver,1,"sms-tokens");
//             //         }catch (e) {

//             //         }
//             //     });
//             // }
//             try {
//                 boyaledger.updateAccount(paymentObj.group,paymentObj.receiver,paymentObj.amount,paymentObj.ref);
//             }catch (e) {
//                 console.error(e);
//             }
//             let ref = "";
//             let mpesaref = paymentObj.txRef;
//             try {
//                 let resp = paymentObj.provider_response;
//                 try {
//                     resp = JSON.parse(paymentObj);
//                 }catch (e) {

//                 }
//                 updateB2BLedger(paymentObj,resp.Result)
//                 if(resp.Result!=undefined && resp.Result.TransactionID){
//                     ref = `Mpesa Reference: ${resp.Result.TransactionID}`;
//                     mpesaref = resp.Result.TransactionID;
//                 }else{

//                 }
//             }catch (e) {

//             }
//             sms = `Transaction confirmed ${mpesaref}. Your payment amount ${formatMoney(paymentObj.amount)} has been made to ${paymentObj.receivername?paymentObj.receivername:paymentObj.receiver} for accno ${paymentObj.accno} on ${dateFormat(new Date(),
//                 "dd/mm/yy HH:MM")}. ${ref}  ${chatLink}`;
//             break;
//         case "rent":
//             //TODO replace MpesaReceiptNumber with Bank deposit Reference
//             //TODO check for bankname in paymentObj
//             sms = `Transaction confirmed ${paymentObj.ref}. Your payment amount ${formatMoney(paymentObj.amount)} has been made to ${paymentObj.bank.BANKNAME}, accno ${paymentObj.accno} on ${dateFormat(new Date(),
//                 "dd/mm/yy HH:MM")} ${chatLink}`;
//             break;
//         case "transfer":
//             sms = `Transaction completed ${paymentObj.ref}. Your transfer amount ${formatMoney(paymentObj.amount)} has been made to ${paymentObj.bank.BANKNAME}, accno ${paymentObj.accno} on ${dateFormat(new Date(),
//                 "dd/mm/yy HH:MM")} ${chatLink}`;
//             try {
//                 jengaBal.boyaBalance((resp)=>{
//                     if(resp.balances){
//                         boyaledger.updateAccountBalance(
//                             paymentObj.group,
//                             `${paymentObj.bank.BANKNAME}, accno ${paymentObj.accno}`,
//                             paymentObj.amount,
//                             resp.balances[0].amount,
//                             paymentObj.ref);
//                     }
//                 });

//             }catch (e) {
//                 console.error(e);
//             }
//             break;

//     }
//     // console.log(`sending sms`, sms);
//     if (sms === "") {
//         return;
//     }

//     // smsSender( sms,phone, (resp,err) => {
//     //     console.log(`sms sent?`, resp);
//     //     try {
//     //         boyaledger.updateAccount("airtime",phone,1,"sms");
//     //     }catch (e) {
//     //         console.error(e);
//     //     }
//     // });
//     try {
//         await sendReceiptEmail(paymentObj)
//     } catch (e) {
//         console.log('failed to send email');
//         console.error(e)
//     }

// }

// function sendSmsToMerchant(paymentObj) {
//     console.log('sending merchant sms');
//     if(paymentObj.merchant_obj == undefined)
//         return;
//     // smsSender(`Transaction ${paymentObj.ref} confirmed. You have received ${formatMoney(paymentObj.amount)} from client phone ending in ***${paymentObj.phone.substring(9)} on ${dateFormat(new Date(),
//     //     "dd/mm/yy HH:MM")} ${chatLink}`,paymentObj.merchant_obj.phone, (resp) => {
//     //     // console.log(`merchant sms sent?`, JSON.stringify(resp));
//     //     try {
//     //         boyaledger.updateAccount("airtime",paymentObj.merchant_obj.phone,1,"sms");
//     //     }catch (e) {
//     //         console.error(e);
//     //     }
//     // });

//     sendEmailToMerchant(paymentObj);
// }
// function sendSmsToZucchini(paymentObj,tillno) {
//     console.log('sending merchant sms');
//     // smsSender(`Transaction ${paymentObj.ref} confirmed. You have received ${formatMoney(paymentObj.amount)} from client phone ${paymentObj.phone} on ${dateFormat(new Date(),
//     //     "dd/mm/yy HH:MM")} ${chatLink}`,tillno.phone, (resp) => {
//     //     console.log(`zucchini sms sent?`, JSON.stringify(resp));
//     //     try {
//     //         boyaledger.updateAccount("airtime",paymentObj.merchant_obj.phone,1,"sms");
//     //     }catch (e) {
//     //         console.error(e);
//     //     }
//     // });

//     sendEmailToMerchant(paymentObj);
// }
// function sendEmailToMerchant(paymentObj) {
//     console.log('sending merchant email');
//     // mailer.sendEmailToMerchant(paymentObj, (resp) => {
//     //     // console.log("-------------------------",resp)
//     // });
// }


// function sendRefundSms(paymentObj) {
//     // smsSender( `We are unable to process your payment at this time. You will receive a refund ${formatMoney(paymentObj.amount)} immediately. Sorry for the inconvenience`,
//     //     paymentObj.phone,
//     //     (resp) => {
//     //     console.log(`sms sent?`, resp);
//     //     try {
//     //         boyaledger.updateAccount("airtime",paymentObj.merchant_obj.phone,1,"sms");
//     //     }catch (e) {
//     //         console.error(e);
//     //     }
//     // });
// }

// //when transaction status changes to finished. this is the flush flow
// function onTransactionComplete(paymentObj,zucchiniTill) {
//     console.log('calling onTransactionComplete');
//     switch (parseInt(paymentObj.status)) {
//         case StatusType.REFUND:
//             sendRefundSms(paymentObj);
//             // mailer.serveRefundMail(paymentObj);
//             //refund
//             break;
//         case StatusType.ERROR:
//             //failed.
//             break;
//         default:
//             console.log('sending sms...');
//             sendSmsToInitiator(paymentObj);
//             if(zucchiniTill!=null){
//                 sendSmsToZucchini(paymentObj,zucchiniTill)
//             }
//             break;


//     }

// }

// module.exports = onTransactionComplete;
