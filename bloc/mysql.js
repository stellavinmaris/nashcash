const mysql      = require('mysql');
const write_pool  = mysql.createPool({
    connectionLimit : 10,
    host     : process.env.MYSQL_HOST,
    user     : process.env.MYSQL_USER,
    password : process.env.MYSQL_PASSWORD,
    multipleStatements: true,
    acquireTimeout: Number.POSITIVE_INFINITY,
    waitForConnections:true
});

const utils = function () {
    return {
        close: ()=>{
            write_pool.end(function (err) {
                // all connections in the pool have ended
                console.log('closed mysql connections',err)
            });
        },
        runQuery:  function(sql, rowFormater, callback) {

            const results = [];
            write_pool.query(sql, function (err, results, fields) {
                if (err) {
                    console.log(err);
                    callback(results, err);

                }else{
                    const res= [];
                    results.forEach((row)=>{
                        res.push(rowFormater(row));
                    })
                    callback(res, false);
                }
            });
            // pool.getConnection(function (err, connection) {
            //     console.log(err);
            //     var query = connection.query(sql);
            //     query
            //         .on('error', function (err) {
            //             console.log(err);
            //             callback(results, err);
            //             // Handle error, an 'end' event will be emitted after this as well
            //         })
            //         .on('fields', function (fields) {
            //             // the field packets for the rows to follow
            //         })
            //         .on('result', function (row) {
            //             // Pausing the connnection is useful if your processing involves I/O
            //
            //         })
            //         .on('end', function () {
            //             callback(results, false);
            //             connection.release();
            //         });
            //
            // });
        },
        deleteRecord:  function(sql, refvalue, callback) {

            write_pool.query(sql,refvalue,(err,results,fields)=>{
                if(err){
                    console.error(err);
                }
                console.log('Deleted Row(s):', results.affectedRows);
                callback(err);
            });
        },
        insertRecord: function (storedb_name,tablename,record,callback) {
            write_pool.query("INSERT INTO "+storedb_name+"."+tablename+" SET ?", record,
                function (error, results, fields) {
                    if (error){
                        console.log(error);
                        callback(null,error);
                        return;
                    }
                    console.log('item added');
                    callback(results,false);
                });
        },
        updateRecord: function (storedb_name,tablename,updatevalues,updatenames,callback) {
            let sql = "UPDATE " + storedb_name + "."+tablename+" SET ";
            let count = 0;
            updatenames.forEach(function (name) {
                if(updatenames.length - count === 1){
                    sql += " where "+name+" = ?";
                }else{
                    if(count!==0)
                        sql+=", ";
                    sql += ""+name+" = ? "
                }
                count++;
            });
            write_pool.query(sql, updatevalues,
                function (error, result, rows, fields) {
                    if (error){
                        console.log(error);
                        callback(null,error);
                        return;
                    }
                    callback(result,false);
                });
        },
        updateRecordMultipleWhere: function (storedb_name,tablename,updatevalues,updatenames,whereCount,callback) {
            let sql = "UPDATE " + storedb_name + "."+tablename+" SET ";
            let count = 0;
            let wheres = 0;
            updatenames.forEach(function (name) {
                if((updatenames.length - count) <= whereCount){
                    wheres++;
                    if(wheres>1){
                        sql += " and "+name+" = ?";
                    }else{
                        sql += " where "+name+" = ?";
                    }

                }else{
                    if(count!==0)
                        sql+=", ";
                    sql += ""+name+" = ? ";
                }
                count++;
            });
            write_pool.query(sql, updatevalues,
                function (error, result, rows, fields) {
                    if (error){
                        console.log(error);
                        callback(null,error);
                        return;
                    }
                    callback(result,false);
                });
        },
    }
}();

module.exports = utils;
