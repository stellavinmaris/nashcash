export class Card {
    cvc:string;
    number:string;
    exp_month:string;
    exp_year:string;
    cardHolder:string;
}
