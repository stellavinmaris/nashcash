import {Card} from "./Card";
const forge = require("node-forge");

const request = require('request');
const SECRET_KEY = process.env.FW_SECKEY;
const {DataStoreFactory} = require("../interfaces/TransactionParticipant");
const db = DataStoreFactory.getDatabase();
const {StatusType,SETTLEMENT_SCHEDULE} = require("../utils/StatusType");


/**
 * @description charge flatterwave
 * @param email
 * @param phone
 * @param ref
 * @param amount
 * @param description
 * @param card
 * @returns {Array}
 */
export async function chargeFW(email,phone,ref, amount, description, card): Promise<object> {
    const encrypted = await encryptFW(email,phone,ref, amount, description, card, false)
    encrypted["PBFPubKey"] = process.env.FW_PUBKEY
    return new Promise<object>(resolve => {
        request({
            url: 'https://api.ravepay.co/flwv3-pug/getpaidx/api/charge',
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${process.env.FW_SECKEY}`
            },
            json: encrypted
        }, function (error, response, body) {
            if (error) {
                console.log('error', error);
                resolve({error: error})
            } else {

                    console.log('Status:', response.statusCode);
                    console.log(body);
                    resolve((body));
                }
            });
        });


}

/**
 * @description charge auth flatterwave
 * @param email
 * @param phone
 * @param ref
 * @param amount
 * @param description
 * @param card
 * @returns {Array}
 */
export async function chargeAuthFW(email,phone,ref, amount, description, card): Promise<object> {
    const encrypted = await encryptFW(email,phone,ref, amount, description, card,true)

    console.log('encrypted--------', encrypted)

    encrypted["PBFPubKey"] = process.env.FW_PUBKEY

    console.log('object--------',encrypted )

    const cipher = forge.cipher.createCipher(
     "3DES-ECB",
     forge.util.createBuffer(process.env.FW_SECKEY_TOKEN)
    );
    cipher.start({ iv: "" });
    cipher.update(forge.util.createBuffer(encrypted, "utf-8"));
    cipher.finish();
    const encrypted_ = cipher.output;
    const xx = forge.util.encode64(encrypted_.getBytes());

    return new Promise<object>(resolve => {
        request({
            url: 'https://api.flutterwave.com/v3/charges?type=card',
            // url:'https://api.ravepay.co/flwv3-pug/getpaidx/api/charge',
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${process.env.FW_SECKEY}`
            },
            json: xx
        }, function (error, response, body) {
            if (error) {
                console.log('error', error);
                resolve({error: error})
            } else {
                console.log('Status:', response.statusCode);
                console.log('------------0----------',body);
                resolve((body));
            }
        });
    });


}

/**
 * @description token preauth charge
 * @param email
 * @param ref
 * @param amount
 * @param description
 * @param card
 * @returns {Array}
 */

export async function tokenPreauthCharge(email,ref, amount, description, card): Promise<object> {

    const payload ={
        "SECKEY":process.env.FW_SECKEY,
        "token":card.flutterwave_token,
        "currency":"KES",
        "country":"KE",
        "amount":`${amount}`,
        "email":email,
        "firstname":"",
        "lastname":"",
        "IP":"",
        "narration":description,
        "txRef":ref,
        "meta":""
    }

    return new Promise<object>(resolve => {
        request({
            url: 'https://api.ravepay.co/flwv3-pug/getpaidx/api/tokenized/preauth_charge',
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            json: payload
        }, function (error, response, body) {
            if (error) {
                console.log('error', error);
                resolve({error: error})
            } else {
                console.log('Status:', response.statusCode);
                // console.log(body);
                resolve((body));
            }
        });
    });
}
/**
 * @description token charge
 * @param email
 * @param ref
 * @param amount
 * @param description
 * @param card
 */
export async function tokenCharge(email,ref, amount, description, card): Promise<object> {

    const payload ={
        "SECKEY":process.env.FW_SECKEY,
        "token":card.flutterwave_token,
        "currency":"KES",
        "country":"KE",
        "amount":`${amount}`,
        "email":email,
        "firstname":"",
        "lastname":"",
        "IP":"",
        "narration":description,
        "txRef":ref,
        "meta":""
    }

    return new Promise<object>(resolve => {
        request({
            url: 'https://api.ravepay.co/flwv3-pug/getpaidx/api/tokenized/charge',
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            json: payload
        }, function (error, response, body) {
            if (error) {
                console.log('error', error);
                resolve({error: error})
            } else {
                console.log('Status:', response.statusCode);
                // console.log(body);
                resolve((body));
            }
        });
    });
}

/**
 * @description verify payment
 * @param tref
 */

export async function verifyPayment(tref): Promise<object> {
    const payload =  {
        "txref": tref,
        "SECKEY": process.env.FW_SECKEY
    }
    return new Promise<object>(resolve => {
        request({
            url: 'https://api.ravepay.co/flwv3-pug/getpaidx/api/v2/verify',
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            json: payload
        }, function (error, response, body) {
            if (error) {
                console.log('error', error);
                resolve({error: error})
            } else {
                console.log('Status:', response.statusCode);
                // console.log(body);
                resolve((body));
            }
        });
    });


}

/**
 * @description capture flutterwave
 * @param flwRef
 * @param amount
 */


export async function captureFw(flwRef,amount){
    return new Promise<object>(resolve => {
        request({
            url: 'https://api.ravepay.co/flwv3-pug/getpaidx/api/capture',
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            json: {
                SECKEY: process.env.FW_SECKEY,
                flwRef: flwRef,
                amount: amount
            }
        }, function (error, response, body) {
            if (error) {
                console.log('error', error);
                resolve({error: error})
            } else {
                console.log('Status:', response.statusCode);
                // console.log(body);
                resolve((body));
            }
        });
    });
}

/**
 * @description fefund capture fw
 * @param flwRef
 * @param amount
 */
export async function refundCaptureFw(flwRef,amount){
    return new Promise<object>(resolve => {
        request({
            url: 'https://api.ravepay.co/flwv3-pug/getpaidx/api/refundorvoid',
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            json: {
                SECKEY: process.env.FW_SECKEY,
                ref: flwRef,
                action: "void"
            }
        }, function (error, response, body) {
            if (error) {
                console.log('error', error);
                resolve({error: error})
            } else {
                console.log('Status:', response.statusCode);
                // console.log(body);
                resolve((body));
            }
        });
    });
}

/**
 * @description encrypt fw
 * @param email
 * @param phone
 * @param ref
 * @param amount
 * @param description
 * @param card
 * @param preauth
 */
export async function encryptFW(email,phone,ref, amount, description, card,preauth): Promise<object> {
    return new Promise<object>(resolve => {
        const payload =  {
            "PBFPubKey": process.env.FW_PUBKEY,
            "cardno": card.number,
            "cvv": card.cvc,
            "expirymonth": card.exp_month,
            "expiryyear": card.exp_year,
            "currency": "KES",
            "country": "KE",
            "amount": `${amount}`,
            "email": email,
            "phonenumber": phone,
            "txRef": ref
        }
        if(preauth)
            payload["charge_type"] = "preauth";
        request({
            url: 'http://localhost:7270/fwencrypt',
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'X-Private-Key': `${process.env.FW_SECKEY_TOKEN}`
            },
            json: payload
        }, function (error, response, body) {
            if (error) {
                console.log('error', error);
                resolve({error: error})
            } else {
                console.log('Status:', response.statusCode);
                // console.log(body);
                resolve((body));
            }
        });
    });

}

/**
 * @description refund charge Fw
 * @param fwRef
 */

export function refundChargeFW(fwRef:string): Promise<object>{
    return new Promise<object>(resolve => {
        request({
            url: 'https://api.ravepay.co/gpx/merchant/transactions/refund',
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            json: {
                "ref":fwRef,
                "seckey":process.env.FW_SECKEY
            }
        }, function(error, response, body){
            if(error) {
                console.log('error',error);
                resolve({"error":error})
            } else {
                console.log('Status:', response.statusCode);
                // console.log(body);
                resolve((body))
            }
        });
    })

}

/**
 * @description get issuing country
 * @param bin
 */
export function getIssuingCountry(bin:string) {
    return new Promise<String>(resolve => {
        request({
            url: `https://api.flutterwave.com/v3/card-bins/${bin}`,
            method: 'GET',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Authorization': `Bearer ${SECRET_KEY}`
            }
        }, function(error, response, body){
            if(error) {
                console.log('error',error);
            } else {
                console.log('Status:', response.statusCode);
                console.log(body);
                const resp = JSON.parse(body);
                const issuing_country = resp.status == "success"? resp["data"]["issuing_country"]:undefined;
                resolve(issuing_country)
            }
        });
    })
}
export class FWVCardResp{
    status:number
    response

    constructor(statusCode,resp) {
        this.status=statusCode
        this.response = resp;
    }

}
export const STATUS_200 = 200
export function issueVirtualCard(nameOnCard,balance):Promise<FWVCardResp>{
    const payload = {
        "currency": "USD",
        "debit_currency":"KES",
        "amount": balance,
        "name": nameOnCard,
        "userID": nameOnCard,
        "brand":"Visa",
        "billing_name":nameOnCard
    }
    return new Promise<FWVCardResp>(resolve => {
        const options ={
            "url": `https://api.flutterwave.com/v3/virtual-cards`,
            "method": 'POST',
            "headers": {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${process.env.FW_SECKEY}`
            },
            json: payload
        };
        request(options, function(error, response, body){
            console.log('vgs new issued error',error);
            console.log('vgs new issued status',response.statusCode);
            console.log('vgs new issued card resp',body);
            resolve(new FWVCardResp(response.statusCode,(body)))

        });
    })


}
export function fetchVirtualCardDetails(vcn):Promise<FWVCardResp>{
    return new Promise<FWVCardResp>(resolve => {
        const options ={
            "url": `https://api.flutterwave.com/v3/virtual-cards/${vcn}`,
            "method": 'GET',
            "headers": {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${process.env.FW_SECKEY}`
            },
        };
        request(options, function(error, response, body){
            console.log('fw card error',error);
            console.log('fw card status',response.statusCode);
            console.log('fw card resp',body);
            resolve(new FWVCardResp(response.statusCode,JSON.parse(body)))

        });
    })


}
export function fundVirtualCard(vcn,amount):Promise<FWVCardResp>{
    const payload = {
        "amount": amount,
        "debit_currency":"KES"
    }
    return new Promise<FWVCardResp>(resolve => {
        const options ={
            "url": `https://api.flutterwave.com/v3/virtual-cards/${vcn}/fund`,
            "method": 'POST',
            "headers": {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${process.env.FW_SECKEY}`
            },
            json: payload
        };
        request(options, function(error, response, body){
            console.log('fw fund card error',error);
            console.log('fw fund card status',response.statusCode);
            console.log('fw fund card resp',body);
            resolve(new FWVCardResp(response.statusCode,JSON.parse(body)))

        });
    })


}
export function withdrawFunds(vcn,amount):Promise<FWVCardResp>{
    return new Promise<FWVCardResp>(resolve => {
        const options ={
            "url": `https://api.flutterwave.com/v3/virtual-cards/${vcn}/withdraw`,
            "method": 'POST',
            "headers": {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${process.env.FW_SECKEY}`
            },
            json: {
                "amount":amount
            }
        };
        request(options, function(error, response, body){
            console.log('fw withdraw error',error);
            console.log('fw withdraw status',response.statusCode);
            console.log('fw withdraw resp',body);
            resolve(new FWVCardResp(response.statusCode,JSON.parse(body)))

        });
    })


}
export function blockVirtualCard(vcn):Promise<FWVCardResp>{
    return new Promise<FWVCardResp>(resolve => {
        const options ={
            "url": `https://api.flutterwave.com/v3/virtual-cards/${vcn}/status/block`,
            "method": 'PUT',
            "headers": {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${process.env.FW_SECKEY}`
            }
        };
        request(options, function(error, response, body){
            console.log('fw block error',error);
            console.log('fw block status',response.statusCode);
            console.log('fw block resp',body);
            resolve(new FWVCardResp(response.statusCode,JSON.parse(body)))

        });
    })


}
export function unBlockVirtualCard(vcn):Promise<FWVCardResp>{
    return new Promise<FWVCardResp>(resolve => {
        const options ={
            "url": `https://api.flutterwave.com/v3/virtual-cards/${vcn}/status/unblock`,
            "method": 'PUT',
            "headers": {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${process.env.FW_SECKEY}`
            }
        };
        request(options, function(error, response, body){
            console.log('fw block error',error);
            console.log('fw block status',response.statusCode);
            console.log('fw block resp',body);
            resolve(new FWVCardResp(response.statusCode,JSON.parse(body)))

        });
    })


}
export function terminateVirtualCard(vcn):Promise<FWVCardResp>{
    return new Promise<FWVCardResp>(resolve => {
        const options ={
            "url": `https://api.flutterwave.com/v3/virtual-cards/${vcn}/terminate`,
            "method": 'PUT',
            "headers": {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${process.env.FW_SECKEY}`
            }
        };
        request(options, function(error, response, body){
            console.log('fw delete card error',error);
            console.log('fw delete card status',response.statusCode);
            console.log('fw delete card resp',body);
            resolve(new FWVCardResp(response.statusCode,JSON.parse(body)))

        });
    })


}

export function getVirtualCardTransactions(vcn,fromdate,todate,lastFetchIndex):Promise<FWVCardResp>{
    return new Promise<FWVCardResp>(resolve => {
        const filter = `from=${fromdate}&to=${todate}&index=${lastFetchIndex}&size=10`
        const options ={
            "url": `https://api.flutterwave.com/v3/virtual-cards/${vcn}/transactions?${filter}`,
            "method": 'GET',
            "headers": {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${process.env.FW_SECKEY}`
            }
        };
        request(options, function(error, response, body){
            console.log('fw v-transactions error',error);
            console.log('fw v-transactions status',response.statusCode);
            console.log('fw v-transactions resp',body);
            resolve(new FWVCardResp(response.statusCode,JSON.parse(body)))

        });
    })


}
