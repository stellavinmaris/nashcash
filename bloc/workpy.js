const admin = require('firebase-admin');
const fs = require('fs');
const Firestore = require('@google-cloud/firestore');
const workpay_config = {
    "type": "service_account",
    "project_id": process.env.WORKPAY_FIRESTORE_PROJECTID,
    "private_key_id": process.env.WORKPAY_FIRESTORE_PRIVKEY_ID,
    "private_key": process.env.WORKPAY_FIRESTORE_PRIVKEY.replace(/\\n/g, '\n'),
    "client_email": process.env.WORKPAY_FIRESTORE_CLIENT_EMAIL,
    "client_id": process.env.WORKPAY_FIRESTORE_CLIENT_ID,
    "auth_uri": "https://accounts.google.com/o/oauth2/auth",
    "token_uri": "https://accounts.google.com/o/oauth2/token",
    "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
    "client_x509_cert_url": process.env.WORKPAY_FIRESTOE_CLIENT_CERT_URL};
admin.initializeApp({
    credential: admin.credential.cert(workpay_config),
    databaseURL: process.env.WORKPAY_FIREBASE_DATABASE_URL
},"bpay");
const settings = {
    timestampsInSnapshots: true
};
var firestore;
fs.writeFile('boyapayments.json', JSON.stringify(workpay_config), (err) => {
    // throws an error, you could also catch it here
    if (err) {console.error(err)};

    // success case, the file was saved
    console.log('boyapayments file created!');
    firestore = new Firestore({
        projectId: process.env.WORKPAY_FIRESTORE_PROJECTID,
        keyFilename: "./boyapayments.json",
        timestampsInSnapshots: true
    });
    firestore.settings(settings);
});


async function setData(b2bPayload,callback) {
    b2bPayload.DocumentID = b2bPayload.Remarks;
    b2bPayload.CollectionName = "b2b";
    await firestore.collection('b2b').doc(b2bPayload.Remarks).create(b2bPayload);
    listenToPayments(b2bPayload.Remarks,callback);
}

async function listenToPayments(Remarks,callback) {

    const collection = firestore.collection('b2b')
        .where("second_stage_status", "==", "failed")
        .where("Remarks", "==",Remarks);
    var unsub2;
    var unsub;
     unsub = collection.onSnapshot(docsnap => {
        console.log('tick next');
        docsnap.docs.forEach(async (doc) => {
            var c2bObj = doc.data();
            try {
                c2bObj = JSON.parse(c2bObj)
            }catch (e) {

            }
            let resp = (c2bObj.second_stage_response);
            try {
                resp = JSON.parse(resp)
            }catch (e) {

            }
            if(resp["Result"].ResultCode === 0 || resp["Result"].ResultCode === "0"){
                c2bObj.status = 0;
            }else{
                console.log('b2b transaction failed',resp.Result.ResultDesc);
                c2bObj.errorMessage = resp.Result.ResultDesc;
                c2bObj.status = 1;
            }
            callback(c2bObj);
            unsub();
            unsub2();
            console.log("remove mpesa-b2b listner");
        });


    }, error => {
        console.log(error);
        console.log('error catching snap');
        unsub();
         unsub2();
        console.error("unable to get response",error);
        // callback(c2bObj);
    });

    const collection2 = firestore.collection('b2b')
        .where("second_stage_status", "==", "successful")
        .where("Remarks", "==",Remarks);
     unsub2 = collection2.onSnapshot(docsnap => {
        console.log('tick next');
        docsnap.docs.forEach(async (doc) => {
            var c2bObj = doc.data();
            try {
                c2bObj = JSON.parse(c2bObj)
            }catch (e) {

            }
           let resp = (c2bObj.second_stage_response);
            try {
                resp = JSON.parse(resp)
            }catch (e) {

            }
            if(resp["Result"].ResultCode === 0 || resp["Result"].ResultCode === "0"){
                c2bObj.status = 0;
            }else{
                console.log('b2b transaction failed',resp.Result.ResultDesc);
                c2bObj.errorMessage = resp.Result.ResultDesc;
                c2bObj.status = 1;
            }
            callback(c2bObj);
            unsub();
            unsub2();
            console.log("remove mpesa-b2b listner");
        });


    }, error => {
        console.log(error);
        console.log('error catching snap');
        unsub2();
         unsub();
        console.error("unable to get response",error);
        // callback(c2bObj);
    });


}

module.exports = setData;
