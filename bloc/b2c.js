const Firestore = require('@google-cloud/firestore');
const express = require('express');
var request = require('request');
const LOCALHOST = "http://172.31.36.36:7271";
const HOST = "https://test.boya.co:7270";
var mysql = require('./mysql');
// const jenga = require('./../equity_jenga/b2b');
const onTransactionComplete = require('./ontransaction_complete');
var dateformat = require('dateformat');
const settings = {
    timestampsInSnapshots: true
};
const firestore = new Firestore({
    projectId: 'boya-608ea',
    keyFilename: process.env.FIREBASE_CONFIG_FILE_PATH,
    timestampsInSnapshots: true
});
firestore.settings(settings);

async function listen() {
    const collection = firestore.collection('b2c').where("status", "==", 1);
    var unsub = collection.onSnapshot(docsnap => {
        console.log('b2c next tick');
        docsnap.docs.forEach(async (doc) => {
            var c2bObj = doc.data();
            if (c2bObj != undefined) {
                console.log('mpesa b2c');
                doesTransactionExist(c2bObj.ref, (exists) => {
                    if (!exists) {
                        archiveB2C(c2bObj, async (saved) => {
                            console.log('b2c saved? ', saved);
                            if (!saved) {
                                console.log('X failed to save c2b record');
                                await firestore.collection("transactions").doc(c2bObj.ref)
                                    .update({"status": 0, "errorMessage": "Failed to send money, error: 3020"});
                            } else {
                                doesChargeExist(c2bObj.ref, async (clientCharged) => {
                                    if (clientCharged) {
                                        initiateFunding(c2bObj);
                                        await firestore.collection('b2c').doc(doc.id).update({status: 2});
                                    }else{
                                        console.log('no c2b successful record');
                                    }
                                })
                            }
                        });
                    }else{
                        firestore.collection('b2c').doc(doc.id).delete();
                    }
                })


            }
            // if(c2bObj.payment_type == "MPESA"){

            // }else{
            //     console.log('card c2b');
            // }
        });
    });
    setTimeout(()=>{
        unsub();
        console.log("remove tick b2c");
        setTimeout(()=>{
            listen();
        },1000)
    },60000*30);
}

function timeoutHandler(req, res) {
    console.log('mpesa timeout callback');
    let resp = req.body.Result;
    let ConversationID = resp.ConversationID;
    updateB2CWithCID(5, JSON.stringify(resp), ConversationID);
    firestore.collection('transactions').doc(ConversationID).update({
        status:5,
        errorMessage:"Transaction timeout"
    }).then((result) => {
        console.log('timeout callback result saved');
        res.status(200).end();
        // return cors(req, res, () => {
        //     res.status(200).end();
        // });
    }).catch((err) => {
        console.log('timeout callback result error', err);
    });
}
function callBackResults(req, res) {
    try {
        let resp = req.body.Result;
        const ConversationID = resp.ConversationID;
        console.log(ConversationID);
        var doc_id;
        var mpesa_resp = {provider_response :resp};
        if (resp.ResultCode == 0 || resp.ResultCode == "0") {
            updateB2CWithCID(3, JSON.stringify(resp), ConversationID);
            mpesa_resp.status =3;
            let records = resp.ResultParameters != undefined ? resp.ResultParameters.ResultParameter : undefined;
            let message = "";
            if (records != undefined)
                records.forEach(function (record) {
                    if (record["Key"] === "ReceiverPartyPublicName") {
                        message = message + "Sent to " + record["Value"] + "\n";
                    }
                    if (record["Key"] === "TransactionAmount") {
                        message = message + "Amount " + record.Value + "\n";
                    }
                    if (record["Key"] === "TransactionReceipt") {
                        message = message + "MPESA-REF: " + record.Value + ".\n";
                    }
                    if (record["Key"] === "TransactionCompletedDateTime") {
                        message = message + "Date: " + record.Value + ".\n";
                        mpesa_resp.date = dateformat(new Date(),"yyyymmddHHMMss");
                    }
                });
            if (message !== "") {
                mpesa_resp.sms = message;
                mpesa_resp.MpesaReceiptNumber = resp.TransactionID;
                mpesa_resp.TransactionID = resp.TransactionID;
            }
            getTransactionByCID(ConversationID, async (rows) => {
                if (rows.length > 0) {
                    var ref = rows[0].ref;
                    console.log(mpesa_resp);
                    await firestore.collection('transactions').doc(ref).update(mpesa_resp);
                    //get transaction obj
                    const doc = await firestore.collection('transactions')
                        .doc(ref).get();
                    const pObj = doc.data();


                    //send an SMS for successful transaction
                    onTransactionComplete(pObj);
                    res.status(200).end();
                } else {
                    console.log(`callback for unknown conversation id: ${ConversationID}`)
                }
            });
        }
        else {
            console.log(resp);
            updateB2CWithCID(0, JSON.stringify(resp), ConversationID);
            mpesa_resp.status =0;
            mpesa_resp.errorMessage = "Sorry, We could not complete transaction, a refund is in progress";
            res.status(200).end();
            getTransactionByCID(ConversationID,async (transactions)=>{
                var transaction = transactions[0];
                console.log(`mpesa_resp`,mpesa_resp);
                firestore.collection('transactions').doc(transaction.ref).update(mpesa_resp);

                getC2BTransactionByRef(transaction.ref,async (c2brecords)=>{
                    var c2brecord =  c2brecords[0];
                    var snapshot = await firestore.collection('transactions').doc(transaction.ref).get();
                    var paymentObj = snapshot.data();
                    var updateObj = {
                        status:1,
                        txRef:transaction.ref,
                        phone:paymentObj.phone,
                        ref:transaction.ref,
                        reason:mpesa_resp.errorMessage,
                        timeNote:Date.now(),
                        amount:transaction.amount,
                        "customer_id":c2brecord.customer_id,
                        "uid":c2brecord.customer_id,
                        "payment_type":c2brecord.payment_type
                    };
                    console.log(`update Obj`,updateObj);
                    firestore.collection('reversals').doc(transaction.ref)
                        .create(updateObj).then((done)=>{
                            console.log('refund raised');

                        //
                    }).catch((err)=>{
                        console.err('unable to do refund. raising alert',err);
                        //TODO escalate unable to raise
                    });
                });

            });

        }


    } catch (e) {
        console.error(e);
    }
}

function timeOutRoute() {
    const router = express.Router();
    router.route('/')
        .all((req, res, next) => {
            res.setHeader("Access-Control-Allow-Origin", "*");
            // res.writeHead(200, {'Content-Type': 'application/json'});
            next();
        }).post(timeoutHandler);
    return router;

}

function resultCallbackRoute() {
    const router = express.Router();
    router.route('/')
        .all((req, res, next) => {
            res.setHeader("Access-Control-Allow-Origin", "*");
            // res.writeHead(200, {'Content-Type': 'application/json'});
            next();
        }).post(callBackResults);
    return router;

}

module.exports = {
    resultCallBack: resultCallbackRoute(),
    timeout: timeOutRoute(),
};

function doesChargeExist(ref, callback) {
    mysql.runQuery("select * from boya.c2boya where txRef = '" + ref + "' and status = 3", (row) => {
        return {id: row.id}
    }, (results, err) => {
        if (!err) {
            callback(results.length > 0)
        } else {
            callback(true);//assume exists
        }
    })
}

function doesTransactionExist(ref, callback) {
    mysql.runQuery("select * from boya.boya2customer where txRef = '" + ref + "'", (row) => {
        return {id: row.id}
    }, (results, err) => {
        if (!err) {
            callback(results.length > 0)
        } else {
            callback(true);//assume exists
        }
    })
}

function getTransactionByCID(ConversationId, callback) {
    mysql.runQuery("select * from boya.boya2customer where ConversationID = '" + ConversationId + "'", (row) => {
        return {id: row.id,ref:row.txRef,amount:row.amount}
    }, (results, err) => {
        if (!err) {
            callback(results)
        } else {
            callback([]);//assume exists
        }
    })
}
function getC2BTransactionByRef(ref, callback) {
    mysql.runQuery("select * from boya.c2boya where txRef = '" + ref + "'", (row) => {
        return {id: row.id,ref:row.txRef,amount:row.amount,
            payment_type:row.payment_type,
            customer_id: row.customer_id}
    }, (results, err) => {
        if (!err) {
            callback(results)
        } else {
            callback([]);//assume exists
        }
    })
}

function archiveB2C(row, callback) {
    mysql.insertRecord("boya", "boya2customer",
        {
            id: row.id,
            code: row.ref,
            amount: row.amount,
            phone: row.phone,
            txRef: row.ref,
        }, (result, err) => {
            if (!err) {
                console.log('c2b request saved')
                callback(true);
            } else {
                console.log('c2b request error')
                callback(false);
            }

        });
}

function updateB2C(status, ref, response, conversationID = "") {
    mysql.updateRecord("boya", "boya2customer",
        [status, response, conversationID, ref],
        ["status","provider_response", "ConversationID", "txRef"], (result, err) => {
            if (err) {
                console.error(err);

            } else {
                console.log('b2c record updated');
            }
        })
}

function updateB2CWithCID(status, response, conversationID) {
    mysql.updateRecord("boya", "boya2customer",
        [status, response, conversationID],
        ["status","provider_response", "ConversationID"], (result, err) => {
            if (err) {
                console.error(err);

            } else {
                console.log('b2c record updated');
            }
        })
}
function formatPhone(inphone) {
    const phone = incphone.trim();
    let countrycode = "254";
    let phone2 = phone.match(/\d+/)[0];;
    if (phone.toString().trim().substring(0, 1) === "+") {
        phone2 = phone.toString().substring(1);
    } else if (phone.toString().substring(0, 2) == "00") {
        phone2 = countrycode + "" + phone.toString().substring(2)
    } else if (phone.toString().substring(0, 1) == "0") {
        phone2 = countrycode + "" + phone.toString().substring(1)
    } else if (phone.length == 12) {
    }
    return phone2.trim();
}
function formatPhoneJenga(inphone) {
    const phone = inphone.match(/\d+/)[0];;
    let countrycode = "254";
    let phone2 = phone.trim();
    if (phone.toString().trim().substring(0, 1) === "+") {
        phone2 = "0"+phone.toString().substring(4);
    }if (phone.toString().trim().substring(0, 3) === "254") {
        phone2 = "0"+phone.toString().substring(3);
    } else if (phone.toString().substring(0, 2) == "00") {
        phone2 = countrycode + "" + phone.toString().substring(1)
    } else if (phone.toString().substring(0, 1) == "0") {
        // phone2 = countrycode + "" + phone.toString().substring(1)
    } else if (phone.length == 12) {
    }
    return phone2;
}
function initiateJengaFunding(b2cObj) {
    mobileWalletTransfer(b2cObj, async (state, response) => {
        if (parseInt(state) != 3) {
            updateB2C(0, b2cObj.ref, JSON.stringify(response), b2cObj.ref);
            //failed to fund
            firestore.collection(`transactions`).doc(b2cObj.ref)
                .update({status: 0, provider_response: JSON.stringify(response), errorMessage: response.response_msg})
                .then((result) => {
                }).catch((err) => {
                console.error(err);
            });
            getC2BTransactionByRef(b2cObj.ref,(c2brecord)=>{
                firestore.collection('reversals').doc(b2cObj.ref)
                    .create({
                        status:1,
                        txRef:b2cObj.ref,
                        reason:""+response.response_msg,
                        timeNote:Date.now(),
                        amount:b2cObj.amount,
                        "customer_id":b2cObj.customer_id,
                        "uid":b2cObj.customer_id,
                        "payment_type":c2brecord.payment_type
                    }).then((done)=>{
                    console.log('refund raised');

                    //
                }).catch((err)=>{
                    console.err('unable to do refund. raising alert',err);
                    //TODO escalate unable to raise
                });
            });
        } else {
            updateB2C(3, b2cObj.ref, JSON.stringify(response), b2cObj.ref);
            //probable fund
            //wait for results
            console.log('jenga send money request accepted');
            var initial = {};
            initial.status = 3;
            initial.ConversationID = b2cObj.ref;
            initial.provider_response = response;

            firestore.collection(`transactions`).doc(b2cObj.ref)
                .update(initial).then(async (result) => {
                console.log('b2c resp saved');
                const doc = await firestore.collection('transactions')
                    .doc(b2cObj.ref).get();
                const pObj = doc.data();
                onTransactionComplete(pObj);

            }).catch((err) => {
                console.error('error saving b2c resp', err);
            });


        }
    });
}
function mobileWalletTransfer(paymentObj, callback) {
    paymentObj.reference = `${new Date().getMilliseconds()}${Date.now()}`.substring(0, 12);
    // jenga.sendMoneyToMobileWallet(
    //     paymentObj.ref!=undefined?paymentObj.ref:paymentObj.txRef,
    //     paymentObj.description,
    //     "Mpesa",formatPhoneJenga(paymentObj.phone),paymentObj.amount,paymentObj.reference,
    //     (resp)=>{
    //         let result = resp;
    //         try {
    //             result = JSON.parse(result);
    //         }catch (e) {

    //         }
    //         console.log('jenga b2c response',result);
    //         switch (result.status) {
    //             case "SUCCESS":
    //                 callback(3,result);
    //                 break;
    //             default:
    //                 callback(0,result);
    //                 break;
    //         }
    //     });
}
async function reverse(b2cObj) {
    b2cObj.description = "reversal";
    b2cObj.status = 1;
    if (b2cObj.payment_type === "MPESA") {
        b2cObj.receiver = b2cObj.phone;
        b2cObj.status = 1;
    }
    await firestore.collection('reversals')
        .doc(b2cObj.ref).create(b2cObj);
}
function initiateFunding(b2cObj) {
    // initiateJengaFunding(b2cObj);
    // return;
    request(
        {
            method: 'POST',
            url: `${LOCALHOST}/mpesa/fundConsumer`,
            // headers : {
            //     "Authorization" : authToken
            // },
            json: {
                "customer_id": b2cObj.uid,
                "amount": b2cObj.amount,
                "description": b2cObj.description,
                "email": b2cObj.email,
                "phone": formatPhone(b2cObj.phone),
                "ref": b2cObj.ref,
                "resultsCallback": `${HOST}/b2cCallback`,
                "timeoutCallback": `${HOST}/b2cTimeout`,
                "currency": b2cObj.currency,
            }
        },
        function (error, response, body) {
            if(!error && body.error==undefined && body.errorMessage==undefined){
                console.log(body);
                const resp = body
                if (parseInt(resp.ResponseCode) != 0) {
                    updateB2C(0, b2cObj.ref, JSON.stringify(response), resp["ConversationID"]);
                    //failed to fund
                    firestore.collection(`transactions`).doc(b2cObj.ref)
                        .update({status:0,provider_response:resp,errorMessage:resp.ResponseDescription})
                        .then((result) => {
                            console.log("c2c updated", resp["ConversationID"])
                        }).catch((err) => {
                        console.error(err);
                    });
                    //reverse:
                    reverse(b2cObj);
                } else {
                    updateB2C(2, b2cObj.ref, JSON.stringify(response), resp["ConversationID"]);
                    //probable fund
                    //wait for results
                    console.log('mpesa send money request accepted');
                    resp.ConversationID = resp.ConversationID;
                    var initial = {};
                    initial.status = 2;
                    initial.ConversationID = resp.ConversationID;
                    initial.provider_response = resp;

                    firestore.collection(`transactions`).doc(b2cObj.ref)
                        .update(initial).then((result)=>{
                        console.log('b2c resp saved');
                    }).catch((err)=>{
                        console.error('error saving b2c resp',err);
                    });

                }
            } else {
                console.error(error);
                var initial = {};
                initial.status = 0;
                initial.errorMessage = body.errorMessage!=undefined?`${body.errorMessage}. Amount will be refunded!`:"error processing request";
                initial.provider_response = "none";
                console.log('error processing b2c transaction:',b2cObj)
                updateB2C('0',b2cObj.ref,JSON.stringify({}),"none");
                firestore.collection(`transactions`).doc(b2cObj.ref).update(initial);
                //reverse:
                reverse(b2cObj);
            }
        });


}


// listen();
