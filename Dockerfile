FROM 319495235940.dkr.ecr.eu-west-2.amazonaws.com/node:15.7.0-alpine3.10
# create app dir
WORKDIR /usr/src/app
RUN apk add --no-cache git
RUN apk update && apk add python g++ make && rm -rf /var/cache/apk/*
# install dependencies
ENV TZ=Africa/Nairobi
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
EXPOSE 7270 80
CMD ["npm", "start"]
COPY package*.json ./
RUN npm install

# Bundle app source
COPY . .

