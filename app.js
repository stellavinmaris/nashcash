try {
    require('events').EventEmitter.defaultMaxListeners = 0

} catch (e) {
    console.error(e);
}
const express = require('express');
const path = require('path');
const fs = require('fs');
// const tutuka = require('./tutuka/tutuka_outbound')
// const tutuka = require('./bloc/tutuka/tutuka_inbound')
const cookieParser = require('cookie-parser');
const logger = require('morgan');
//init firebase
const admin = require('firebase-admin');
const firebase_config = require('./firebase_config');
const fireApp = admin.initializeApp({
    credential: admin.credential.cert(firebase_config),
    databaseURL: process.env.FIREBASE_DATABASE_URL
}, "boyan");
fs.writeFile('boya-fire.json', JSON.stringify(firebase_config), (err) => {
    // throws an error, you could also catch it here
    if (err) {
        console.error(err)
    }

    // success case, the file was saved
    console.log('file created!');
});
const app = express();
var cors = require('cors');
var helmet = require('helmet')
app.use(helmet())
var device = require('express-device');
app.use(device.capture({parseUserAgent: true}));
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
// app.use(logger('dev'));
var timeout = require('connect-timeout'); //express v4
app.use(timeout('300s'));
app.use(haltOnTimedout);
function haltOnTimedout(req, res, next) {
    if (!req.timedout) next();
    else {
        try {
            res.send(408);
        } catch (e) {

        }
        console.error(`timeout received`)
        // process.exit(0);
    }
}

var common = require('./bloc/utils/common');
var bloc_c2b = require('./bloc/c2b');
// //init routes
const indexRouter = require('./routes/index');
app.use('/', indexRouter);

// app.use('/newSMS',bloc_c2b.sms);


process.on('uncaughtException', (e) => {
    console.log('uncaught : ', e);
});
process.setMaxListeners(0);
module.exports = app;
