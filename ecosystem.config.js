module.exports = {
  apps : [{
    name: 'api',
    script: 'bin/www',

    // Options reference: https://pm2.io/doc/en/runtime/reference/ecosystem-file/
    args: '',
    instances: 1,
    autorestart: true,
    watch: false,
    max_memory_restart: '1G',
    env: {
      NODE_ENV: 'development'
    },
    env_production: {
      NODE_ENV: 'production'
    }
  }],

  deploy : {
    production : {
      key : '~/.ssh/boyan.pem',
      user : 'ubuntu',
      host : '3.16.122.230',
      ref  : 'origin/master',
      repo : 'git@bitbucket.org:boyagroup/boya-server.git',
      path : '/home/ubuntu/boya-server',
      'post-deploy' : 'npm install && pm2 reload ecosystem.config.js --env production'
    }
  }
};
