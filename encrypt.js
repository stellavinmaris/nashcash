const crypto = require('crypto');
const CryptoJS = require('crypto-js');

function getKey() {
    const md5 = require('md5');
    const keymd5 = md5(process.env.SECRET_KEY);
    const keymd5last6 = keymd5.substr(-8);

    const seckeyadjusted = crypto.randomBytes(256).toString('hex');
    const seckeyadjustedfirst12 = seckeyadjusted.substr(0, 8);

    return seckeyadjustedfirst12 + keymd5last6;
}

function encrypt(message) {

    const key = process.env.BOYA_ENC_KEY;
    const textWordArr = CryptoJS.enc.Utf8.parse(message);
    const keyHex = CryptoJS.enc.Hex.parse(key);
    const options = {
        mode: CryptoJS.mode.ECB,
        padding: CryptoJS.pad.Pkcs7
    };
    const encrypted = (CryptoJS.DES.encrypt(textWordArr, keyHex, options));
    return Buffer.from(encrypted.toString(), 'base64').toString('hex');
}

function decrypt(message) {

    const key = process.env.BOYA_ENC_KEY;
    const keyHex = CryptoJS.enc.Hex.parse(key);
    const options = {
        mode: CryptoJS.mode.ECB,
        padding: CryptoJS.pad.Pkcs7
    };
    const decrypted = CryptoJS.TripleDES.decrypt({
        ciphertext: CryptoJS.enc.Hex.parse(message)
    }, keyHex, options);
    return decrypted.toString(CryptoJS.enc.Utf8);
}
module .exports ={
    encrypt,
    decrypt
};
