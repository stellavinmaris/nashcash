const mpesa = require('./env');
const request = require('request');
var crypto = require("crypto");
var constants = require("constants");
var fs = require('fs');
const NodeCache = require( "node-cache" );
const myCache = new NodeCache();
const privatekey = fs.readFileSync('./mpesa/pub.cer');
function requestAuthToken(b2b, callback,count=1) {
    try{
        if(b2b){
            const token = myCache.get( "b2btoken" );
            if (token == null) {
                console.log("no token in cache")
            } else {
                console.log("using b2b token in cache")
                return callback({error: false, token: token});
            }
        }else{
            const token = myCache.get( "b2ctoken" );
            if (token == null) {
                console.log("no b2c token in cache")
            } else {
                console.log("using b2c token in cache")
                return callback({error: false, token: token});
            }
        }
    }catch (e){
        console.error(e)
    }

    const options = b2b ? {
        url: mpesa.OAUTH_URL,
        headers: {
            'Authorization': 'Basic ' +  Buffer.from(process.env.B2B_CONSUMER_KEY + ':' + process.env.B2B_CONSUMER_SECRET).toString('base64')
        }
    } : {
        url: mpesa.OAUTH_URL,
        headers: {
            'Authorization': 'Basic ' +  Buffer.from(process.env.C2B_CONSUMER_KEY + ':' + process.env.C2B_CONSUMER_SECRET).toString('base64')
        }
    };
    request(options,
        function (err, httpResponse, body) {
            if (err) {
                console.log(err);
                callback({error: err, token: false});
                return;
            }
            try{
                if(httpResponse.statusCode != 200){
                    console.log('[requestAuthToken] response code is ',httpResponse.statusCode)
                    if(count<4)
                        return requestAuthToken(b2b,callback,count+1)
                    else return callback({error: err, token: false});
                }
            }catch (e){
                console.error(e);
            }
            console.log((body));
            try {
                myCache.set(  b2b ?"b2btoken":"b2ctoken", JSON.parse(body).access_token, (parseInt(JSON.parse(body).expires_in)-60) );
            }catch (e) {
                console.error(e)
            }

            callback({error: false, token: JSON.parse(body).access_token});

        });
}
module.exports = {
    getPassword: (timestamp) => {
        const bufferToEncrypt =  Buffer.from(process.env.MPESA_C2B_SHORTCODE + process.env.BOYA_PASSKEY + timestamp);
        return (bufferToEncrypt.toString("base64"));
    },
    getSecurityCredential: () => {

        var bufferToEncrypt =  Buffer.from(process.env.MPESA_B2C_PASSWORD);
        var encrypted = crypto.publicEncrypt(
            {"key": privatekey, padding: constants.RSA_PKCS1_PADDING},
            bufferToEncrypt);

        return (encrypted.toString("base64"));
    },
    getBizSecurityCredential: () => {

        var bufferToEncrypt =  Buffer.from(process.env.MPESA_B2C_PASSWORD);
        var encrypted = crypto.publicEncrypt(
            {"key": privatekey, padding: constants.RSA_PKCS1_PADDING},
            bufferToEncrypt);

        return (encrypted.toString("base64"));
    },
    getToken: (b2b = false, callback) => {
        requestAuthToken(b2b, (obj) => {
            if (obj.token) {
                oauth_token = obj.token;
                const auth = "Bearer " + oauth_token;
                callback(auth);
            } else {
                console.error(object)
                callback(null);
            }

        });
    }
};
