var request = require('request');
var sec = require('./security');
var mpesa = require('./env');
var express = require('express');
var payB2B = require('../bloc/workpy');
const MPESA_OUTAGE = "Sorry, Mpesa service is no responding at the moment. Please try again later. This transaction has been reversed"
function sendMoneyToTill(paymentObj,callback,retries=0) {
    paymentObj.SecurityCredential = sec.getBizSecurityCredential();
    sec.getToken(true, (authToken) => {
        console.log('got b2b token');
        if(authToken==null){
            callback(MPESA_OUTAGE,{});
            return;
        }
        const payload =  {
            method: 'POST',
            url: mpesa.B2BURL,
            headers: {
                "Authorization": authToken
            },
            json: paymentObj
        };
        console.log('payload is ',(payload));
        request(
            payload,
            function (error, response, body) {
                // if(error && error.code === 'ETIMEDOUT'){
                //     if(retries<=3){
                //     return sendMoneyToTill(paymentObj,callback,retries+1)
                //     }else{
                //        return  callback({"errorMessage":MPESA_OUTAGE,"error":MPESA_OUTAGE},
                //             {"errorMessage":MPESA_OUTAGE,"error":MPESA_OUTAGE});
                //     }
                // }
                if(response && response.statusCode == 404){
                    //retry
                    if(retries<=3){
                        sendMoneyToTill(paymentObj,callback,retries+1)
                    } else
                    {
                        console.log("Error>>>> unable to reach B2B services, error 404");
                        callback({"errorMessage":MPESA_OUTAGE,"error":MPESA_OUTAGE},
                            {"errorMessage":MPESA_OUTAGE,"error":MPESA_OUTAGE});
                    }

                }else{
                    console.log(body);
                    callback(error,body);
                }


            }
        );
    });

}
function formatPhone(phone) {
    const countrycode = "254";
    let phone2 = phone.split(' ').join('').match(/\d+/)[0];
    if (phone.toString().substring(0, 1) === "+") {
        phone2 = phone.toString().substring(1);
    } else if (phone.toString().substring(0, 2) === "00") {
        phone2 = countrycode + "" + phone.toString().substring(2)
    } else if (phone.toString().substring(0, 1) === "0") {
        phone2 = countrycode + "" + phone.toString().substring(1)
    } else if (phone.length === 9) {
        phone2 = countrycode + "" + phone.toString();
    }
    return phone2;
}
function checkStatus(callback){
    const payload = {
        "Initiator": process.env.MPESA_B2C_USERNAME,
        "SecurityCredential":  sec.getSecurityCredential(),
        "CommandID": "TransactionStatusQuery",
        "TransactionID": "AG_20201020_0000621992683962d38c",
        "ShortCode": "553555",
        "PartyA": formatPhone("254718555832"),
        "IdentifierType": "1",
        "ResultURL": process.env.HOST,
        "QueueTimeOutURL": process.env.HOST,
        "Remarks": formatPhone("254718555832"),
        "MSISDN": formatPhone("254718555832"),
        "Occasion": "254718555832"
    }
    console.log('b2c payload',`${JSON.stringify(payload)}`);
    sec.getToken(true, (authToken) => {
        console.log('token: '+authToken);
        if(authToken==null){
            callback(MPESA_OUTAGE,{});
            return;
        }
    request(
        {
            method: 'POST',
            url: mpesa.STATUS_QUERY_URL,
            headers: {
                "Authorization": authToken
            },
            json: payload
        },
        function (error, response, body) {
            console.log(body);
            callback(error,body);
        }
    )})
}
function sendMoneyToPhone(phone, amount, ref,resultUrl=mpesa.B2B_RESULT_URL,timeoutUrl=mpesa.B2B_TIMEOUT_URL, callback) {
    sec.getToken(true, (authToken) => {
        console.log('token: '+authToken);
        if(authToken==null){
            callback(MPESA_OUTAGE,{});
            return;
        }
        const payload = {
            "InitiatorName": process.env.MPESA_B2C_USERNAME,
            "Initiator": process.env.MPESA_B2C_USERNAME,
            "SecurityCredential": sec.getBizSecurityCredential(),
            "CommandID": "BusinessPayment",
            "Amount": amount,
            "PartyA": process.env.MPESA_B2C_SHORTCODE,
            "PartyB": formatPhone(phone).trim(),
            "Remarks": ref,
            "RecieverIdentifierType": "1",
            "SenderIdentifierType": "4",
            "QueueTimeOutURL": timeoutUrl,
            "ResultURL": resultUrl
        };
        console.log('b2c payload',`${JSON.stringify(payload)}`);
        request(
            {
                method: 'POST',
                url: mpesa.B2CURL,
                headers: {
                    "Authorization": authToken
                },
                json: payload
            },
            function (error, response, body) {
                // if(error && error.code === 'ETIMEDOUT'){
                //     return callback({"error":"ETIMEDOUT","errorMessage":"ETIMEDOUT"})
                // }
                console.log(body);
                callback(error,body);
            }
        );
    });

}

function reverseTransaction(Ref, TransactionID, Amount, callback,resultcallbackUrl,timeOutUrl ) {
    sec.getToken(true, (authToken) => {
        if(authToken==null){
            callback("Mpesa service not available",undefined);
            return;
        }
        request(
            {
                method: 'POST',
                url: mpesa.REVERSAL_URL,
                headers: {
                    "Authorization": authToken
                },
                json: {
                    "Initiator": process.env.MPESA_B2C_USERNAME,
                    "SecurityCredential": sec.getSecurityCredential(),
                    "CommandID": "TransactionReversal",
                    "TransactionID": TransactionID,
                    "Amount": Amount,
                    "ReceiverParty": process.env.MPESA_B2C_SHORTCODE,
                    "RecieverIdentifierType": "4",
                    "ResultURL": resultcallbackUrl?resultcallbackUrl:mpesa.TRANSACTION_RESULT_URL + "/" + TransactionID,
                    "QueueTimeOutURL": timeOutUrl?timeOutUrl:mpesa.C2B_TIMEOUT_URL + "/" + TransactionID,
                    "Remarks": Ref,
                    "Occasion": " "
                }
            },
            function (error, response, body) {
                console.log(body);
                var res = {status: error ? 0 : 1};
                if(body.errorMessage !=undefined)
                    res.status =0;
                Object.assign(res, body);
                callback(res);
            }
        );
    });
}
module.exports = {
    payMerchantBillRoute: () => {
        var router = express.Router();
        router.route('/')
            .all((req, res, next) => {
                res.setHeader("Access-Control-Allow-Origin", "*");
                res.writeHead(200, {'Content-Type': 'application/json'});
                next();
            }).post(function (req, res) {
            //check for tillno, amount,ref
            if (!req.body.tillno) {
                res.status(403)
                    .end(JSON.stringify({'error': 'missing till no'}));
                return
            }
            if (!req.body.amount || isNaN(req.body.amount)) {
                res.status(403)
                    .end(JSON.stringify({'error': 'missing amount'}));
                return
            }
            // if (!req.body.merchantID || isNaN(req.body.merchantID)) {
            //     res.status(403)
            //         .end(JSON.stringify({'error': 'missing merchantID'}));
            //     return
            // }
            if (!req.body.ref) {
                res.status(403)
                    .end(JSON.stringify({'error': 'missing ref'}));
                return
            }
            // if (!req.body.charge_id) {
            //     res.status(403)
            //         .end(JSON.stringify({'error': 'missing charge_id'}));
            //     return
            // }
            //TODO verify merchantID to make sure this is our merchant
            //TODO verify charge_id and make sure consumer is already charged and we have receipts
            //TODO check for duplicates using ref-amount-till to avoid doubles
            sendMoneyToTill(req.body.tillno, req.body.amount, req.body.ref, (resp) => {
                console.log(resp);
                if (resp.status == 0) {
                    res.status(500)
                        .end(JSON.stringify(resp));
                    return
                }
                res.status(200)
                    .end(JSON.stringify(resp));
                return
            },req.body.timeoutUrl,req.body.callbackUrl);
        });
        return router;

    },
    paymentResultRouter: ()=>{
        var router = express.Router();
        router.route('/')
            .all((req, res, next) => {
                res.setHeader("Access-Control-Allow-Origin", "*");
                res.writeHead(200, {'Content-Type': 'application/json'});
                next();
            }).post(function (req,res) {
            console.log(req.body);
            res.end();
            //TODO b2b or b2c payment response

        });
        return router;

    },
    timeoutRouter: ()=>{
        var router = express.Router();
        router.route('/')
            .all((req, res, next) => {
                res.setHeader("Access-Control-Allow-Origin", "*");
                res.writeHead(200, {'Content-Type': 'application/json'});
                next();
            }).post(function (req,res) {
            console.log(req.body);
            //TODO capture timeout
            res.end();
        });
        return router;

    },
    payConsumerRoute: () => {
        var router = express.Router();
        router.route('/')
            .all((req, res, next) => {
                res.setHeader("Access-Control-Allow-Origin", "*");
                res.writeHead(200, {'Content-Type': 'application/json'});
                next();
            }).post(function (req, res) {
            //check for tillno, amount,ref
            if (!req.body.phone) {
                res.status(403)
                    .end(JSON.stringify({'error': 'missing phone no'}));
                return
            }
            if (!req.body.amount || isNaN(req.body.amount)) {
                res.status(403)
                    .end(JSON.stringify({'error': 'missing amount'}));
                return
            }
            if (!req.body.customer_id) {
                res.status(403)
                    .end(JSON.stringify({'error': 'missing customer_id'}));
                return
            }
            if (!req.body.ref) {
                res.status(403)
                    .end(JSON.stringify({'error': 'missing ref'}));
                return
            }

            //TODO verify customer_id
            //TODO check for duplicates using ref-phone
            sendMoneyToPhone(req.body.phone, req.body.amount, req.body.ref,
                req.body.resultsCallback,req.body.timeoutCallback, (resp) => {
                    if (resp.status == 0) {
                        res.status(500)
                            .end(JSON.stringify(resp));
                        return
                    }
                    res.status(200)
                        .end(JSON.stringify(resp));
                    return
                });
        });
        return router;

    },
    sendMoneyToPhone,
    sendMoneyToTill,
    checkStatus,
    reverseTransaction: () => {
        var router = express.Router();
        router.route('/')
            .all((req, res, next) => {
                res.setHeader("Access-Control-Allow-Origin", "*");
                res.writeHead(200, {'Content-Type': 'application/json'});
                next();
            }).post(function (req, res) {
            if (!req.body.customer_id) {
                res.status(403)
                    .end(JSON.stringify({'error': 'missing customer_id'}));
                return
            }
            if (!req.body.amount || isNaN(req.body.amount)) {
                res.status(403)
                    .end(JSON.stringify({'error': 'missing amount'}));
                return
            }
            //TODO verify customer_id
            if (!req.body.ref) {
                res.status(403)
                    .end(JSON.stringify({'error': 'missing ref'}));
                return
            }
            //TODO verify charge_id
            if (!req.body.charge_id) {
                res.status(403)
                    .end(JSON.stringify({'error': 'missing charge_id'}));
                return
            }
            var TransactionID = req.body.ref;
            var charge_id = req.body.charge_id;
            var amount = req.body.amount;
            reverseTransaction(charge_id, TransactionID, amount,req.body.callbackUrl,req.body.timeoutCallback, (resp) => {
                if (parseInt(resp.status) == 0) {
                    res.status(500)
                        .end(JSON.stringify(resp));
                    return
                }
                res.status(200)
                    .end(JSON.stringify(resp));
                return
            });

        });
        return router;
    },

};
