var request = require('request');
const mpesa = require('./env');
const sec = require('./security');
const dateFormat = require('dateformat');
const db = require('../database/realtimedb');
var express = require('express');


function processIPN(req,res){
    var results = req.body;
    try {
        results = JSON.parse(results)
    } catch (e) {

    }
    console.log(`received IPN`,results);
    var transaction = results;
    var fullname = transaction.FirstName + " " + transaction.MiddleName + " " + transaction.LastName;
    var i = '' + transaction.TransTime;

    var timestamp = i[0] + '' + i[1] + '' + i[2] + '' + i[3] + '-' + i[4] + i[5] + '-' + i[6] + i[7] + ' ' + i[8] + i[9] + ':' + i[10] + i[11]
    db.storeMpesaTransactionIPN( {
        PhoneNumber: transaction.MSISDN,
        Paybill: transaction.BusinessShortCode,
        Amount: transaction.TransAmount,
        MpesaReceiptNumber: transaction.TransID,
        TransactionDate: timestamp,
        CustomerName: fullname,
        merchantID: transaction.BillRefNumber
    });




}
function processCallBack(req){
    var results = req.body;
    try {
        results = JSON.parse(results)
    } catch (e) {

    }
    console.log(results);

    let mpesacall = results.Body.stkCallback;
    console.log(mpesacall);
    db.updateMPESACheckout(mpesacall);
    if(parseInt(mpesacall.ResultCode) == 0){
        //the transaction was successful.
        //log it.
        db.getMpesaCheckoutTransaction(mpesacall.CheckoutRequestID,(result)=>{
            if(result!=null){
                var amount = 0;
                var ref = "";
                var date = "";
                var phone_no = "";
                console.log(mpesacall.CallbackMetadata.Item);
                try {
                    mpesacall.CallbackMetadata.Item.forEach((record)=>{
                        console.log(record);
                        switch (record.Name) {
                            case "Amount":
                                amount = record.Value; break;
                            case "MpesaReceiptNumber":
                                ref = record.Value; break;
                            case "TransactionDate":
                                date = record.Value; break;
                            case "PhoneNumber":
                                phone_no = record.Value; break;
                        }
                    });
                }catch (e) {
                    console.log(e);
                }

                //TODO implement commission charge in object
                //TODO update mpesa charge on customer ledger sheet

                db.storeMpesaTransaction({
                    "MpesaReceiptNumber": ref,
                    "Amount": amount,
                    "TransactionDate": date,
                    "PhoneNumber": ""+phone_no,
                    "MerchantRequestID": ""+result.MerchantRequestID,
                    "CheckoutRequestID": ""+result.CheckoutRequestID,
                    "MerchantID":result.merchantID,
                });
            }
        });
    }else{
        console.log(mpesacall);
    }
}
function initiateMPESAPush(merchantID,amount,phone,ref,callback,CallBackURL=mpesa.PUSH_CALLBACK_URL){
    sec.getToken(false,(authToken)=>{
        const timestamp = dateFormat(new Date(),"yyyymmddHHMMss");
        request(
            {
                method: 'POST',
                url : mpesa.PUSH_URL,
                headers : {
                    "Authorization" : authToken
                },
                json : {
                    "BusinessShortCode": process.env.MPESA_C2B_SHORTCODE,
                    "Password": sec.getPassword(timestamp),
                    "Timestamp": timestamp,
                    "TransactionType": "CustomerPayBillOnline",
                    "Amount": amount,
                    "PartyA": phone,
                    "PartyB": process.env.MPESA_C2B_SHORTCODE,
                    "PhoneNumber": phone,
                    "CallBackURL": CallBackURL,
                    "AccountReference": ref,
                    "TransactionDesc": "payment"
                }
            },
            function (error, response, body) {

                if (error) {
                    console.log('error procesing push request');
                    console.log(error);
                    callback({status:0,error:error,message:'error procesing push request'})
                    return;
                }
                console.log('push response √√');

                var results = body;
                try {
                    results = JSON.parse(results)
                } catch (e) {

                }
                console.log(`response status ${response.status}`);
                console.log(results);
                if (CallBackURL !== undefined) {
                    results.CallBackURL = CallBackURL
                }
                if(results.ResponseCode != undefined && parseInt(results.ResponseCode) == 0){
                    db.storeMpesaCheckout({
                        merchantID:merchantID,
                        checkoutref:ref,
                        CheckoutRequestID:results.CheckoutRequestID,
                        MerchantRequestID:results.MerchantRequestID,
                        ResponseCode:results.ResponseCode,
                        ResponseDescription:results.ResponseDescription,
                    });
                    var response = {status:1};
                    Object.assign(results,response);
                    callback(results);
                }else{
                    var response = {status:0,errorMessage:results.errorMessage};
                    callback(response);
                }



            }
        );
    });

}
function verifyStatus(transactionID,charge_id){
    sec.getToken(false,(authToken)=>{
        request(
            {
                method: 'POST',
                url : mpesa.STATUS_QUERY_URL,
                headers : {
                    "Authorization" : authToken
                },
                json : {
                    "Initiator":process.env.MPESA_B2C_USERNAME,
                    "SecurityCredential":sec.getBizSecurityCredential(),
                    "CommandID":"TransactionStatusQuery",
                    "TransactionID":transactionID,
                    "PartyA":process.env.MPESA_B2C_SHORTCODE,
                    "IdentifierType":"4",
                    "ResultURL":mpesa.TRANSACTION_RESULT_URL + "/" + transactionID,
                    "QueueTimeOutURL":mpesa.C2B_TIMEOUT_URL + "/" + transactionID,
                    "Remarks":charge_id,
                    "Occasion":transactionID
                }
            },
            function (error, response, body) {

                if (error) {
                    console.log('error procesing push request');
                    console.log(error);
                    //TODO update charge status error
                    return;
                }

                var results = body;
                try {
                    results = JSON.parse(results)
                } catch (e) {

                }
                console.log(results);
                var response = {status:1};
                Object.assign(results,response);
                //he sender should be listening on result and timeout URL
            }
        );
    });

}

function reverseTransaction(Ref, TransactionID, Amount, callback,resultcallbackUrl,timeOutUrl ) {
    sec.getToken(false, (authToken) => {
        console.log(`auth token `,authToken);
        request(
            {
                method: 'POST',
                url: mpesa.REVERSAL_URL,
                headers: {
                    "Authorization": authToken
                },
                json: {
                    "Initiator": process.env.MPESA_B2C_USERNAME,
                    "SecurityCredential": sec.getSecurityCredential(),
                    "CommandID": "TransactionReversal",
                    "TransactionID": TransactionID,
                    "Amount": Amount,
                    "ReceiverParty": process.env.MPESA_C2B_SHORTCODE,
                    "RecieverIdentifierType": "11",
                    "ResultURL": resultcallbackUrl?resultcallbackUrl:mpesa.TRANSACTION_RESULT_URL + "/" + TransactionID,
                    "QueueTimeOutURL": timeOutUrl?timeOutUrl:mpesa.C2B_TIMEOUT_URL + "/" + TransactionID,
                    "Remarks": Ref,
                    "Occasion": " "
                }
            },
            function (error, response, body) {
                console.log(body);
                var res = body;
                callback(res);
            }
        );
    });
}
function registerIPNUrls(req,res) {
    sec.getToken(false,(authToken)=>{
        if (authToken) {
            var options = {
                url: mpesa.REGISTER_URL,
                method: 'POST',
                contentType: 'application/json',
                headers: {
                    'Authorization': authToken

                },
                json: {
                    ShortCode: process.env.MPESA_C2B_SHORTCODE,
                    ResponseType: 'Completed',
                    ConfirmationURL: mpesa.CONFIRM_URL,
                    ValidationURL: mpesa.VALIDATION_URL,
                    CommandID: 'RegisterURL'
                },
                timeout: 90000

            };
            request(options,
                function (err, httpResponse, body) {
                    if (err) {
                        console.log('error procesing request');
                        console.log(err);
                        res.end(err);
                        return;
                    }
                    console.log(body);
                    res.end('none');

                });
        } else {
            console.log('Missing Token');
            console.log(response.error);
        }

    });
}
function logTimeout(transactionId){
    console.log(`transaction: ${transactionId} has timed out`);
    //TODO update listeners
}

module.exports = {

    registerUrls: ()=>{
        var router = express.Router();
        router.route('/')
            .all((req, res, next) => {
                res.setHeader("Access-Control-Allow-Origin", "*");
                res.writeHead(200, {'Content-Type': 'application/json'});
                next();
            }).post(function (req,res) {
                registerIPNUrls(req,res);
        });
        return router;

    },
    pushCallbackRoute: ()=>{
        var router = express.Router();
        router.route('/')
            .all((req, res, next) => {
                res.setHeader("Access-Control-Allow-Origin", "*");
                res.writeHead(200, {'Content-Type': 'application/json'});
                next();
            }).post(function (req, res) {
                processCallBack(req);
                res.status(200).end()
            });
        return router;

    },
    pushChargeRoute: ()=>{
        var router = express.Router();
        router.route('/')
            .all((req, res, next) => {
                res.setHeader("Access-Control-Allow-Origin", "*");
                res.writeHead(200, {'Content-Type': 'application/json'});
                next();
            }).post(function (req, res) {
                //to look for merchantID,amount,phone,ref,callback
            if(!req.body.customer_id){
                res.status(403)
                    .end(JSON.stringify({'error':'missing customer id'}));
                return
            }
            if(!req.body.amount || isNaN(req.body.amount)){
                res.status(403)
                    .end(JSON.stringify({'error':'missing amount'}));
                return
            }
            if(!req.body.phone){
                res.status(403)
                    .end(JSON.stringify({'error':'missing phone'}));
                return
            }
            if(!req.body.ref){
                res.status(403)
                    .end(JSON.stringify({'error':'missing ref'}));
                return
            }
            initiateMPESAPush(req.body.customer_id,req.body.amount,req.body.phone,req.body.ref,(response)=>{
                        console.log(response);
                        if(parseInt(response.status) == 0){
                            //failed
                            res.status(500)
                                .end(JSON.stringify(response));
                            return
                        }
                res.status(200)
                    .end(JSON.stringify(response));
            },req.body.callbackurl);

        });
        return router;

    },
    IPNcallBackRoute: ()=>{
        var router = express.Router();
        router.route('/')
            .all((req, res, next) => {
                res.setHeader("Access-Control-Allow-Origin", "*");
                res.writeHead(200, {'Content-Type': 'application/json'});
                next();
            }).post(function (req, res) {
            processIPN(req,res);
        });
        return router;

    },
    validateCallbackUrl: ()=>{
        var router = express.Router();
        router.route('/')
            .all((req, res, next) => {
                res.setHeader("Access-Control-Allow-Origin", "*");
                res.writeHead(200, {'Content-Type': 'application/json'});
                next();
            }).post(function (req, res) {
            // processIPN(req);
        });
        return router;

    },
    //https://boya.co:7270/mpesa/transaction/{transactionID}
    transactionResultRoute: () => {
        var router = express.Router();
        router.route('/:TransactionID')
            .all((req, res, next) => {
                res.setHeader("Access-Control-Allow-Origin", "*");
                res.writeHead(200, {'Content-Type': 'application/json'});
                next();
            }).get(function (req, res) {
            console.log('reversal result');
            console.log(req.query);
            var TransactionID = req.params.TransactionID;
            //TODO query transaction record and respond with obj
            res.end(JSON.stringify({}))
        }).post(function (req, res) {
            console.log('reversal result');
            console.log(req.body);
            var TransactionID = req.params.TransactionID;
            console.log('create a comparison event to see difference');
            res.end(JSON.stringify({}))
        })
        return router;

    },
    reverseTransaction: () => {
        var router = express.Router();
        router.route('/')
            .all((req, res, next) => {
                res.setHeader("Access-Control-Allow-Origin", "*");
                res.writeHead(200, {'Content-Type': 'application/json'});
                next();
            }).post(function (req, res) {
            if (!req.body.customer_id) {
                console.log('missing customer_id');
                res.status(403)
                    .end(JSON.stringify({'error': 'missing customer_id'}));
                return
            }
            if (!req.body.amount || isNaN(req.body.amount)) {
                console.log('missing amount');
                res.status(403)
                    .end(JSON.stringify({'error': 'missing amount'}));
                return
            }
            //TODO verify customer_id
            if (!req.body.ref) {
                console.log('missing ref');
                res.status(403)
                    .end(JSON.stringify({'error': 'missing ref'}));
                return
            }
            // //TODO verify charge_id
            // if (!req.body.charge_id) {
            //     res.status(403)
            //         .end(JSON.stringify({'error': 'missing charge_id'}));
            //     return
            // }
            var TransactionID = req.body.ref;
            var charge_id = req.body.ref;
            var amount = req.body.amount;
            reverseTransaction(charge_id, TransactionID, amount,(resp) => {
                res.end(JSON.stringify(resp));
                return
            },req.body.callbackUrl,req.body.timeoutCallback);

        });
        return router;
    },
    timeOutRoute: ()=>{
        var router = express.Router();
        router.route('/:transactionId')
            .all((req, res, next) => {
                res.setHeader("Access-Control-Allow-Origin", "*");
                res.writeHead(200, {'Content-Type': 'application/json'});
                next();
            }).get(function (req, res) {
            var transactionId = req.params.transactionId;
            logTimeout(transactionId);
        })
            .post(function (req, res) {
            var transactionId = req.params.transactionId;
            logTimeout(transactionId);
        });
        return router;

    },
    paymentStatusRoute: ()=>{
        var router = express.Router();
        router.route('/:transactionId')
            .all((req, res, next) => {
                res.setHeader("Access-Control-Allow-Origin", "*");
                res.writeHead(200, {'Content-Type': 'application/json'});
                next();
            }).get(function (req, res) {
            var transactionId = req.params.transactionId;
            if (!req.query.charge_id) {
                res.status(403)
                    .end(JSON.stringify({'error': 'missing charge_id'}));
                return
            }
            if (transactionId==undefined) {
                res.status(403)
                    .end(JSON.stringify({'error': 'missing transactionId'}));
                return
            }
            var charge_id = req.query.charge_id;
            verifyStatus(transactionId,charge_id);
        })
            .post(function (req, res) {
                var transactionId = req.params.transactionId;
                if (!req.body.charge_id) {
                    res.status(403)
                        .end(JSON.stringify({'error': 'missing charge_id'}));
                    return
                }
                if (transactionId==undefined) {
                    res.status(403)
                        .end(JSON.stringify({'error': 'missing transactionId'}));
                    return
                }
                var charge_id = req.body.charge_id;
                verifyStatus(transactionId,charge_id);
            });
        return router;

    }

};

