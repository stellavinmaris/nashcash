var {DataStoreFactory} = require("./bloc/interfaces/TransactionParticipant");

const Firestore = require('@google-cloud/firestore');
const firestore = new Firestore({
    projectId: process.env.FIRESTORE_PROJECTID,
    keyFilename:"./boya-fire.json",
    "private_key_id": process.env.FIRESTORE_PRIVKEY_ID,
    "private_key": process.env.FIRESTORE_PRIVKEY.replace(/\\n/g, '\n'),
    "client_email": process.env.FIRESTORE_CLIENT_EMAIL,
    "client_id": process.env.FIRESTORE_CLIENT_ID,
    "auth_uri": "https://accounts.google.com/o/oauth2/auth",
    "token_uri": "https://accounts.google.com/o/oauth2/token",
    "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
    "client_x509_cert_url": process.env.FIRESTOE_CLIENT_CERT_URL,
    timestampsInSnapshots: true
});
const settings = {
    timestampsInSnapshots: true
};
firestore.settings(settings);
module.exports = firestore;
