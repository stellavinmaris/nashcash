const admin = require('firebase-admin');
const bdb = admin.initializeApp({
    credential: admin.credential.cert({
        "type": "service_account",
        "project_id": process.env.FIRESTORE_PROJECTID,
        "private_key_id": process.env.FIRESTORE_PRIVKEY_ID,
        "private_key": process.env.FIRESTORE_PRIVKEY.replace(/\\n/g, '\n'),
        "client_email": process.env.FIRESTORE_CLIENT_EMAIL,
        "client_id": process.env.FIRESTORE_CLIENT_ID,
        "auth_uri": "https://accounts.google.com/o/oauth2/auth",
        "token_uri": "https://accounts.google.com/o/oauth2/token",
        "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
        "client_x509_cert_url": process.env.FIRESTOE_CLIENT_CERT_URL}),
    databaseURL: process.env.FIREBASE_DATABASE_URL
},"boyadb");
let firebasedb = bdb.database();

module.exports = {
    getMerchants: (callback)=>{
        let merchants = [];
        firebasedb.ref("merchants").once("value", function (snapshot) {
            if (snapshot.val() === null) {
                callback([]);
            } else {
                snapshot.forEach(function (snapshop) {
                    let cc = (snapshop.val());
                    delete cc.password;
                    merchants.push(cc);
                });
                callback(merchants);
            }
        });
    },
    addMerchant: (merchant)=>{
        firebasedb.ref("merchants").child(merchant.accno).set(merchant);
        return true;
    },
    ifMerchantExists: (email,phone,till,callback)=>{
        firebasedb.ref("merchants").once('value',(snapshot)=>{
           var exists = false;
           var existingname = "";
            snapshot.forEach(function (snapshop) {
               var merchant = snapshop.val();
               if(merchant.till == till || merchant.email == email || phone == merchant.phone){
                   existingname = merchant.businessname;
                   exists = true;
               }
            });
            callback(exists,existingname);
        });
    },
    getMerchantbyProperties: (prop,value,prop2,value2, callback)=> {
        let merchants = [];
        firebasedb.ref("merchants").once("value", function (snapshot) {
            if (snapshot.val() === null) {
                callback([]);
            } else {
                snapshot.forEach(function (snapshop) {
                    let cc = (snapshop.val());
                    if (cc[prop] === value && cc[prop2] === value2)
                        merchants.push(cc);

                });
                callback(merchants);
            }
        });
    },
    storeMpesaTransaction: (mpesaTransaction)=>{
        firebasedb.ref("mpesa").child("transactions")
            .child(mpesaTransaction.MpesaReceiptNumber).set(mpesaTransaction);
    },
    storeMpesaTransactionIPN: (mpesaTransaction)=>{
        firebasedb.ref("mpesa").child("ipn_transactions")
            .child(mpesaTransaction.ConfirmationCode).set(mpesaTransaction);
    },
    storeMpesaCheckout: (mpesaPushResponse)=>{
        firebasedb.ref("mpesa").child("pushrequest")
            .child(mpesaPushResponse.CheckoutRequestID).set(mpesaPushResponse);
    },
    updateMPESACheckout: (paymentResponse)=>{
        firebasedb.ref("mpesa").child("pushrequest")
            .child(paymentResponse.CheckoutRequestID).update(paymentResponse);
    },
    getMpesaCheckoutTransaction: (checkoutId,callback)=>{
        firebasedb.ref("mpesa").child("pushrequest")
            .child(checkoutId).once('value', function (snap) {
            callback(snap.val());

        });
    }

};
