var {PrepResult} =  require("../bloc/models/TransactionResult").PrepResult;
var {StatusType} =  require("../bloc/utils/StatusType").StatusType;

var express = require('express');
var router = express.Router();
const common = require('../bloc/utils/common');
/* GET home page. */
router.get('/', function(req, res, next) {
    const ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    console.log(`Index test request from ${req.device.type.toUpperCase()} IP ${ip}`,req.device.name);
  res.render('index', { title: 'Keep Walking!' });
});
router.post('/', function(req, res, next) {
    const ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    console.log('root POST request',JSON.stringify(req.body));
    res.end();
});


router.route('/b2cCallback')
    .all((req, res, next) => {
      res.setHeader("Access-Control-Allow-Origin", "*");
      next();
    }).post(function (req, res) {
        common.b2cCallbackResults(req,res);
});
router.route('/b2bCallback')
    .all((req, res, next) => {
      res.setHeader("Access-Control-Allow-Origin", "*");
      next();
    }).post(function (req, res) {
        common.b2bCallbackResults(req,res);
});

router.route('/processTransaction')
    .all((req, res, next) => {
      res.setHeader("Access-Control-Allow-Origin", "*");
      next();
    }).post(function (req, res) {
  //to look for merchantID,amount,phone,ref,callback
    // const ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    // try{
    //     console.log(`new transaction request from ${req.device.type.toUpperCase()} IP ${ip}`,req.device.type);
    //     console.log(`device name `,req.device.name);

    // }catch(e){

    // }
  common.processTransaction(req,res);

});

router.route('/c2bpushcb')
    .all((req, res, next) => {
      res.setHeader("Access-Control-Allow-Origin", "*");
      next();
    }).post(function (req, res) {
        console.log('c2b callback');
        common.mpesaC2BResultCallbackRoute(req,res);
});


router.route('/mpesa/push/charge')
    .all((req, res, next) => {
      res.setHeader("Access-Control-Allow-Origin", "*");
      next();
    }).post(function (req, res) {
    const ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    console.log(`mpesa stk-push request from ${req.device.type.toUpperCase()} IP ${ip}`,req.device.name);
  //to look for merchantID,amount,phone,ref,callback
  if(!req.body.customer_id){
    res.status(403)
        .end(JSON.stringify({'error':'missing customer id'}));
    return
  }
  if(!req.body.amount || isNaN(req.body.amount)){
    res.status(403)
        .end(JSON.stringify({'error':'missing amount'}));
    return
  }
  if(!req.body.phone){
    res.status(403)
        .end(JSON.stringify({'error':'missing phone'}));
    return
  }
  if(!req.body.ref){
    res.status(403)
        .end(JSON.stringify({'error':'missing ref'}));
    return
  }
  common.initiateMPESAPush(req.body.amount,req.body.phone,req.body.ref,(response)=>{
    console.log(response);
    if(parseInt(response.status) === 0){
      //failed
      res.status(500)
          .end(JSON.stringify(response));
      return
    }
    res.status(200)
        .end(JSON.stringify(response));
  },req.body.callbackurl);

});


router.route('/fwencrypt')
    .all((req,res,next)=>{
        res.setHeader("Access-Control-Allow-Origin", "*");
        res.setHeader("Content-Type", "application/json");
        next();
    }).post(async (req,res)=>{
    let userObj = req.body;
    try{
        userObj = JSON.parse(userObj)
    }catch (e) {

    }
    res.end(JSON.stringify(userObj));
});

module.exports = router;
